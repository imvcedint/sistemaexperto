%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[twocolumn]{article}

\usepackage{graphicx}  % for includegraphics
\DeclareGraphicsExtensions{.jpg,.pdf,.png,.gif}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%             Printing Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setlength{\textwidth}{19.2cm}
\setlength{\textheight}{27.0cm}
\hoffset -1.7cm
\voffset -3.5cm

\parindent=0pt
%\parskip = 4pt

\mathsurround 1pt
\hfuzz=3pt

\usepackage{amsfonts}
\def\QQ{{\mathbb Q}}
\def\ZZ{{\mathbb Z}}
\def\cocoa
 {\mbox{\rm C\kern-.13em o\kern-.07em C\kern-.13em o\kern-.15em A}}
\def\cocoal
 {\mbox{\rm C\kern-.13em o\kern-.07em C\kern-.13em o\kern-.15em
A\kern-.1em L}}


\date{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\title{{\cocoa~4} Reference Card \vskip -1cm}

\maketitle
\label{firstpage}
\vskip -1cm
All command names and keywords begin with a capital letter.
Every command {\bf must end with a semicolon} (\verb|;|)
and may extend over several lines.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{Quitting \cocoa}

\begin{itemize}
\item \verb|Quit;| or \verb|Ciao;| will quit the {\cocoa} session
\end{itemize}
See also the section {\bf Help! {\cocoa} is not responding}.
%If these commands do not work see {\bf Help {\cocoa} is not responding}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{Online help}

{\cocoa} has on-line help for all commands and functions.

\begin{itemize}
\item \verb|?|{\it key-phrase} prints the manual entry associated with
{\it key-phrase}, if several entries are relevant their titles are printed.

\item \verb|??|{\it key-phrase} prints out the titles of all manual entries
associated with {\it key-phrase}.

\item \verb|?tutorial| will start the online {\cocoa} tutorial.
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{Ring Environments}

Before computing with polynomials you must specify the ring
to use via the command \verb|Use| or \verb|Using|.  The special
operator \verb|::=| stores a ring in a variable.
\begin{itemize}
%\item {\it CoeffRing\/}\verb|[|{\it indets\/}\verb|],|{\it ordering}
\item \verb|QQ[x,y,z];|~---~coeffs in $\QQ$, indets \verb|x,y,z|
\item \verb|ZZ/(23)[x,y,z];|~---~coeffs in $\ZZ/23\ZZ$
\item \verb|QQ[x,y,z],Lex;|~---~lexicographic term ordering
\item \verb|QQ[x[1..7],y[0..3]];|~---~subscripted indets
\item \verb|S ::= QQ[x[1..8]];|~---~store ring in variable \verb|S|

\item \verb|Use R;|~---~use ring stored in \verb|R|
\item \verb|Use R ::= QQ[x,y];|~---~store ring in \verb|R| and use it
\item \verb|Use QQ[x,y,z];|~---~use a newly created ring

\item \verb|Using R Do| {\it commands\/} \verb|EndUsing;|\\
---~use ring stored in \verb|R| for {\it commands}

\item \verb|CurrentRing()| returns the ring currently in use
\end{itemize}

\subsection*{Ring Indeterminates}

The name of an indeterminate is one lowercase letter possibly
indexed by integers: some examples are \verb|x|,
\verb|t[1]|, \verb|m[2,3]| and \verb|m[I,J+1]|.  An index may be any
expression having an integer value; the range of valid indices is
given when the ring is created, {\it e.g.} \verb|QQ[x[1..6,1..6]]|.

Juxtaposition of indeterminates represents product: \verb|xy| is
simply shorthand for \verb|x*y|, and \verb|xyzzy| is short for
\verb|x*y^2*z^2|.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{Storage Variables and arithmetic}

Variables are used for storing results which will be needed again
later.  The name of a variable must start with a capital letter, and
may be followed by further letters and/or digits: {\it e.g.}~\verb|X|,
\verb|X2|, \verb|Gamma|, and \verb|TotalDistance|.  To store a value
in a variable use the assignment operator \verb|:=|\\
\verb|NumPoints:=12;|\\
\verb|MinimalPoly:=x^6+2*x^5+3*x^4+4*x^3+5*x^2+6*x+7;|

\smallskip
The arithmetic operators are: \verb|+ - * / ^| being respectively sum,
difference, product, quotient, and power.

\medskip
{\bf WARNING} \verb|1/2*x| is the same as \verb|(1/2)*x|, to put
\verb|x| in the denominator you must write \verb|1/(2*x)| or
\verb|1/2/x|.  Also a negative exponent must be in parentheses
\verb|10^(-2)| otherwise you get a \verb|parse error|.

% Apart from numbers and polynomials, {\cocoa} can compute with lists and
% matrices.  See the two following sections.

\section*{Lists}

A list is created by writing several values between square brackets.
Use indexing to get at the entries in a list.\\
\smallskip
\verb|Empty:=[];|~---~store the empty list\\
\smallskip
\verb|Primes:=[2,3,5,7,11];|~---~store a list of five numbers\\
\smallskip
\verb|Primes[3];|~---~returns $3$rd element of the list \verb|Primes|\\
\smallskip
\verb|1..10|~---~returns the list of integers from 1 to 10\\
\smallskip
\verb|Concat([1,2],[3,4]);|~---~returns the concatenation\\
\smallskip
\verb|Append(Primes, 13);|~---~changes value of \verb|Primes|\\
\smallskip
\verb|Len(Primes);|~---~returns the number of elements in the list

\medskip
You can apply a function to some or all the elements of a list:\\
\verb![X^2 | X In [1,2,3]]!~---~produces \verb|[1,4,9]|\\
\verb![X^2 | X In [1,2,3] And X>2]!~---~produces \verb|[9]|

\medskip
\verb|[| {\it expr} \verb!|! {\it var\/} \verb|In| {\it list} \verb|]|
creates a list of values of {\it expr\/} as {\it var\/} runs through {\it list}.

\smallskip
\verb|[| {\it expr} \verb!|! {\it var\/} \verb|In| {\it list} \verb|And| {\it cond\/} \verb|]|
creates a list of values of {\it expr\/} whenever {\it cond\/} is true as {\it var\/} runs through {\it list}.

\medskip
See also the programming construct \verb|Foreach|.  Try typing \verb|??list|
in a {\cocoa} session for more information.

\section*{Matrices}

A list of lists can be converted into a matrix using \verb|Mat|:
\smallskip
\verb|Mat([[1,2],[0,1]])|~---~make matrix from list of rows\\
\smallskip
\verb|Det(M)|~---~returns determinant of the matrix \verb|M|\\
\smallskip
\verb|Transposed(M)|~---~have a guess what this does!\\
\smallskip
\verb|M^(-1)|~---~returns inverse of the matrix \verb|M|\\
\smallskip
\verb|M[I,J]|~---~returns entry (\verb|I|,\verb|J|) of matrix \verb|M|

\medskip
To solve a linear system see the online manual for \verb|LinSol| and
\verb|LinKer|.  Try typing \verb|??mat| in a {\cocoa} session for more
information about operations on matrices.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{Ideals, Modules, and Gr\"obner Bases}

To compute a G-basis you must first create an ideal/module:
\smallskip
\verb|Ideal(x,y,z^2)|~---~ideal generated by $x, y, z^2$\\
\smallskip
\verb|Ideal(L)|~---~ideal generated by elements of list \verb|L|\\
\smallskip
\verb|Module([x,y],[z,t])|~---~module gen'd by given vectors\\
\smallskip
\verb|Module(M)|~---~module gen'd by the rows of matrix \verb|M|\\
\smallskip
\verb|GBasis(A)|~---~G-basis of ideal/module \verb|A|\\
\smallskip
\verb|ReducedGBasis(A)|~---~reduced G-basis of ideal/module \verb|A|

The ordering used for the Gr\"obner basis is that associated with the
ring containing the generators~---~it was specified when the ring was created.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{Moving values between rings}

\begin{itemize}
\item \verb|BringIn|
\item \verb|Image|
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{Printing}

{\cocoa} automatically prints out the result of an arithmetic
expression whose value is not assigned to a variable.  Finer control
of printing may be achieved using the commands \verb|PrintLn| and
\verb|Print|~---~for details consult the online manual via
\verb|?print|

To output results to a file, there is a variant of the printing commands:
consult the online manual with \verb|?print ON|

The command \verb|Set Indentation;| tells {\cocoa} to print lists out
``vertically'' rather than ``horizontally''.

% Users want to know about StarPrint (StarPrintFold), and Latex


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Programming Constructs}

Here are the syntaxes of the more common constructs: keywords are in
\verb|typewriter|, {\it italics\/} describe the class of expression
to appear in that position.
\begin{itemize}
\item \verb|If| {\it cond\/} \verb|Then| {\it commands\/} \verb|EndIf;|

\item \verb|If| {\it cond\/} \verb|Then| {\it commands\/} \verb|Else| {\it commands\/} \verb|EndIf;|

\item \verb|While| {\it cond\/} \verb|Do| {\it commands\/} \verb|EndWhile;|

\item \verb|For| {\it var\/} \verb|:=| {\it lower\/} \verb|To| {\it upper\/} \verb|Do| {\it commands\/} \verb|EndFor;|\\
{\it lower\/} and {\it upper\/} must evaluate to integers, and if {\it lower\/}$>${\it upper\/} then {\it commands\/} are not executed

\item \verb|Foreach| {\it var\/} \verb|In| {\it list\/} \verb|Do| {\it commands\/} \verb|EndForeach;|

\item \verb|Define| {\it fn-name\/} \verb|(| {\it args\/} \verb|)| {\it commands\/} \verb|EndDefine;|\\
Give a definition of a new function/procedure.
{\it fn-name\/} comprises letters and digits, and must begin with a capital

\item \verb|Return;|~---~return from a procedure

\item \verb|Return| {\it expr\/}\verb|;|~---~return a value from a function
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Understanding errors}

If you get lots of error messages in succession, concentrate just on the
first one or two, and ignore the rest.  The most common mistakes are
omitting the semicolon at the end of a command, or not making the first
letter of a keyword of variable capital.  Sometimes the real error is
a line or two before where {\cocoa} said it had trouble.

An error message saying \verb|Undefined indeterminate| probably
means you forgot a capital letter at the start of a keyword,
variable or function name.

An error message saying \verb|Undefined variable|~{\it XYZ,}
where {\it XYZ\/} is a function probably means you put a
space between {\it XYZ\/} and the following bracket.  There
should be no space.  If you do leave a space ({\it e.g.}
\verb|F (N-1)|) then the expression is interpreted as the product
of the variable \verb|F| by the value \verb|(N-1)|.  If you meant
the product, write explicitly \verb|F*(N-1)|.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Help! {\cocoa} is not responding}

There are several reasons why {\cocoa} might seem to ignore commands.
If this happens, try to evaluate a simple command ({\it e.g.} \verb|1+1;|),
to see if you get an answer.  If not, read on.

\begin{itemize}
\item You may have omitted to type a semicolon (\verb|;|) at the end
of the previous command.  {\cocoa} will obey the command only when it
sees the semicolon.  So just enter a semicolon as the next line.

\item {\cocoa} might still be working on some previous lengthy
computation.  New commands will be handled only when the previous
computations have been completed.  To interrupt ({\it i.e.} stop and
discard) a lengthy computation: on Unix/Linux/MacOS~X type
\verb|control-C|, on Microsoft try clicking randomly on icons.

\item You might have forgotten an \verb|EndIf;| or other \verb|End|-word,
so try typing a hash (\verb|#|).  This will probably produce
a \verb|parse error| which you should ignore, and will cancel the last
command.  If you do not get a \verb|parse error|, try the following
suggestion.

\item You might have forgotten to end a string.  A string in {\cocoa}
may contain ``newline'' characters, so if you forget to close it then
subsequent lines will be swallowed up into the string giving the
impression that {\cocoa} is no longer responding.  To get out of this,
terminate the string by typing a single or double quote (depending on
which you used to start it) and then type a hash (\verb|#|).
\end{itemize}



% \section*{Surprises}

% While indeterminates may be multiplied by juxtapositon ({\it i.e.\/}
% \verb|xy| represents the product of \verb|x| by \verb|y|) the same
% is not true for variables: \verb|XY| refers to a variable whose name
% is two letters long, and is {\bf never} interpreted as the product of
% variables \verb|X| and \verb|Y|.  

% When calling a function it is important to place the open bracket
% immediately after the name of the function (thus: \verb|F(N-1)|)
% and not to leave any space.  If you do leave a space (like this:
% \verb|F (N-1)|) then the expression is interpreted as the product
% of a variable \verb|F| by the value \verb|(N-1)|.  If you really do
% want the product in such a case then we strongly urge you to write
% explicitly \verb|F*(N-1)| to avoid any possible misinterpretation.

% Strings in CoCoA may contain ``newline'' characters: this is achieved
% simply by typing enter/return/newline at the right place.  It does also
% mean that if you should forget to close a string then subsequent lines
% of command will be swallowed up into the string giving that impression
% that {\cocoa} is no longer responding.  In particular, typing
% \verb|Quit;| will not cause {\cocoa} to quit.  If you think you are
% in this situation then you should terminate the string (using either
% a single quote or a double quote depending on which you used to start
% the string) and then type a semicolon (\verb|;|).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\includegraphics{cocoa-diagram}


\end{document}
