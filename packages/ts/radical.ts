
-- $Id: radical.ts,v 1.9 2009/05/22 15:08:20 bigatti Exp $
-- Test suite for radical.pkg

Alias TSL := $ts/ts_lib;
TSL.Initialize();

-------------------------------
-- TEST 01 :

Test := Record[Id := "radical_01", Descr := "Oaku"];

Test.Input :="
MEMORY.RADICAL_PKG_OUT:=True;
Y := NewId();
Var(Y) ::= QQ[x[1..4]];
Use Var(Y);
I:=Ideal(
12*x[3]*x[4]*x[1]*x[2] - 15*x[4]^2*x[2]^2 + x[3]^2*x[1] + 8*x[3]*x[4]*x[1] - 2*x[3]*x[4]*x[2]
 - 4*x[4]^2*x[2],
3*x[3]^2*x[1]^2 - 3*x[4]^2*x[2]^2 + x[3]^2*x[1] + 2*x[3]*x[4]*x[1] - 2*x[3]*x[4]*x[2]
 - x[4]^2*x[2],
108*x[4]^3*x[2]^3 - 9*x[3]*x[4]^2*x[2]^2 - 72*x[4]^3*x[2]^2 - x[3]^3*x[1]
 + 64*x[3]*x[4]^2*x[1] + 2*x[3]^2*x[4]*x[2] - 60*x[3]*x[4]^2*x[2] -32*x[4]^3*x[2]);
E1:=EquiIsoDec(I);
E2:=Radical(I);
MEMORY.RADICAL_PKG_OUT:=False;

E1=[Ideal(x[1]x[2]^2x[4] - 2/9x[1]x[3] - 5/9x[2]x[4] - 2/27x[3] - 4/27x[4],
x[2]^2x[4]^2 + 1/9x[1]x[3]^2 - 8/9x[1]x[3]x[4] - 2/9x[2]x[3]x[4] + 4/9x[2]x[4]^2
- 4/9x[3]x[4], x[1]^2x[3] - 1/2x[1]x[2]x[4] + 1/3x[1]x[3] - 2/3x[2]x[4] -
1/6x[4], x[1]x[2]x[3] - 2x[2]^2x[4] + 4/3x[1]x[3] - 2/3x[2]x[4] + 1/3x[3],
x[1]^2x[2]^2 - 2/3x[1]x[2] - 4/27x[1] - 4/27x[2] - 1/27), Ideal(x[2], x[1]),
Ideal(x[3], x[2], x[1]), Ideal(1), Ideal(x[2]x[4] + 1/12x[3] + 2/3x[4], x[2]x[3]
+ 2/3x[3] + 16/3x[4], x[2]^2 + 4/3x[2], x[1]x[2] + 4/3x[1] + 1/4x[2] + 1/3,
x[3]^2 - 64x[4]^2, x[1]x[3] - 8x[1]x[4] + 1/4x[3] - 2x[4], x[1]^2 + 5/12x[1] -
7/192x[2] + 1/24), Ideal(x[4], x[1]^2x[2]^2 - 2/3x[1]x[2] - 4/27x[1] - 4/27x[2]
- 1/27, 7/2304x[3]), Ideal(x[2], x[1], x[3]^2 + 195/8x[3]x[4] - 259x[4]^2),
Ideal(1), Ideal(27x[1]^2x[2]^2 - 18x[1]x[2] - 4x[1] - 4x[2] - 1, 8x[3], x[4]),
Ideal(1), Ideal(x[3], x[2]), Ideal(4x[1]^2 + x[1], 2x[3]^2 + 16x[3]x[4], x[2]),
Ideal(x[1]x[3]x[4] - 8x[1]x[4]^2 - 5x[2]x[4]^2 - 1/6x[3]^2 - 1/6x[3]x[4] +
16/3x[4]^2, x[1]^2 + 1/32x[1]x[2] + 43/256x[2]^2 + 11/24x[1] + 25/128x[2] +
5/96, x[2]^2x[4] - 1/3x[1]x[3] + 8/3x[1]x[4] + 1/3x[2]x[4] - 1/6x[3], x[1]x[3]^2
- 64x[1]x[4]^2 - 24x[2]x[4]^2 - 5/6x[3]^2 - 2x[3]x[4] + 112/3x[4]^2, x[2]x[4]^3
+ 1/96x[3]^3 + 1/24x[3]^2x[4] - 7/12x[3]x[4]^2 - 2x[4]^3, x[3]^4 + 4x[3]^3x[4] -
56x[3]^2x[4]^2 - 256x[3]x[4]^3 - 512x[4]^4, x[2]x[3] + 2/3x[3] + 16/3x[4],
x[1]x[2]^2 + 1/3x[1]x[2] - 11/24x[2]^2 - 4/3x[1] - 31/36x[2] - 1/3, x[2]^3 +
8/3x[1]x[2] + x[2]^2 + 32/9x[1] + 2/9x[2] + 8/9, x[1]x[2]x[4] + 5/24x[1]x[3] -
1/3x[1]x[4] - 11/24x[2]x[4] - 1/144x[3] - 5/9x[4]), Ideal(x[3], x[2], x[1] +
1/4), Ideal(27x[1]^2x[2]^2 - 18x[1]x[2] - 4x[1] - 4x[2] - 1, x[3], x[4]),
Ideal(x[4], x[1]x[3]), Ideal(x[4], x[3]), Ideal(x[1]x[4] + 16/3x[3] + 2/3x[4],
x[1]x[3] + 2/3x[3] + 1/12x[4], x[1]^2 + 4/3x[1], x[3]^2 - 1/64x[4]^2, x[1]x[2] -
8x[2]^2 + 13/24x[1] - 2x[2], x[2]^2x[4] + 2/3x[2]x[3] + 1/3x[2]x[4] + 13/36x[3]
+ 13/288x[4], x[2]^2x[3] + 1/3x[2]x[3] + 1/96x[2]x[4] + 13/288x[3] +
13/2304x[4], x[2]^3 + 1/8x[2]^2 + 91/4608x[1] - 1/32x[2]), Ideal(4/5x[3], x[4],
24x[1]x[2] - 192x[2]^2 + 13x[1] - 48x[2])];
E2=Ideal(x[1]x[2]^2x[4] - 8/9x[1]^2x[3] + 2/9x[1]x[2]x[3] + 4/9x[1]x[2]x[4] -
4/9x[2]^2x[4] - 2/9x[1]x[3] - 1/9x[2]x[4], x[1]^2x[2]x[3] - 4/9x[1]^2x[3] +
4/9x[1]x[2]x[3] + 2/9x[1]x[2]x[4] - 8/9x[2]^2x[4] - 1/9x[1]x[3] - 2/9x[2]x[4],
x[1]x[2]x[3]x[4] - 5/4x[2]^2x[4]^2 + 1/12x[1]x[3]^2 + 2/3x[1]x[3]x[4] -
1/6x[2]x[3]x[4] - 1/3x[2]x[4]^2, x[1]^2x[3]^2 - x[2]^2x[4]^2 + 1/3x[1]x[3]^2 +
2/3x[1]x[3]x[4] - 2/3x[2]x[3]x[4] - 1/3x[2]x[4]^2, x[2]^3x[4]^2 +
1/9x[1]x[2]x[3]^2 - 2/9x[2]^2x[3]x[4] - 2/3x[2]^2x[4]^2 + 2/27x[1]x[3]^2 +
16/27x[1]x[3]x[4] - 16/27x[2]x[3]x[4] - 8/27x[2]x[4]^2);
";
Test.ExpectedOutput :=
"SQFR Found
SQFR Found
I2 Splitting (2)
I2 Splitting (3)
Gi Splitting (4)
-- Finale - Dim Pura 2 (3)
GCD splitting  Factoring (4)
I2 Splitting (5)
-- Mon-Dim Pura = 2 (4)
I2 Splitting (5)
-- Mon-Dim Pura = 1 (4)
-- CDim>Ht (3)
GCD splitting  Factoring (4)
Gi Splitting (5)
Gi Splitting (6)
-- Finale - Dim Pura 1 (5)
-- Finale - Dim Pura 1 (4)
I2 Splitting (5)
-- Finale - Dim Pura 1 (4)
-- CDim>Ht (3)
GCD splitting  Factoring (4)
-- Finale - Dim Pura 1 (3)
-- CDim>Ht (2)
GCD splitting  Factoring (3)
I2 Splitting (4)
-- Mon-Dim Pura = 2 (3)
-- Finale - Dim Pura 1 (2)
Gi Splitting (3)
-- Finale - Dim Pura 1 (2)
I2 Splitting (3)
-- Lt(I) Radicale (2)
-- Finale - Dim Pura 1 (1)
GCD splitting  Factoring (2)
-- Mon-Dim Pura = 2 (1)
GCD splitting  Factoring (2)
-- Mon-Dim Pura = 2 (1)
I2 Splitting (2)
-- Finale - Dim Pura 1 (1)
-- Lt(I) Radicale (0)

-------------------------------
SQFR Found
SQFR Found
I2 Splitting (2)
I2 Splitting (3)
Gi Splitting (4)
-- Finale - Dim Pura 2 (3)
GCD splitting  Factoring (4)
I2 Splitting (5)
-- Mon-Dim Pura = 2 (4)
I2 Splitting (5)
-- Mon-Dim Pura = 1 (4)
-- CDim>Ht (3)
GCD splitting  Factoring (4)
Gi Splitting (5)
Gi Splitting (6)
-- Finale - Dim Pura 1 (5)
-- Finale - Dim Pura 1 (4)
I2 Splitting (5)
-- Finale - Dim Pura 1 (4)
-- CDim>Ht (3)
GCD splitting  Factoring (4)
-- Finale - Dim Pura 1 (3)
-- CDim>Ht (2)
GCD splitting  Factoring (3)
I2 Splitting (4)
-- Mon-Dim Pura = 2 (3)
-- Finale - Dim Pura 1 (2)
Gi Splitting (3)
-- Finale - Dim Pura 1 (2)
I2 Splitting (3)
-- Lt(I) Radicale (2)
-- Finale - Dim Pura 1 (1)
GCD splitting  Factoring (2)
-- Mon-Dim Pura = 2 (1)
GCD splitting  Factoring (2)
-- Mon-Dim Pura = 2 (1)
I2 Splitting (2)
-- Finale - Dim Pura 1 (1)
-- Lt(I) Radicale (0)
++++ 6  Components
Main Cycle (2)
Main Cycle (1)
-- Radical by exhaustion
-- Monomial ideal
-- Monomial ideal
Main Cycle (3)
Main Cycle (2)
Main Cycle (1)
-- Radical by exhaustion
-- Monomial ideal
Main Cycle (3)
Main Cycle (2)
Main Cycle (1)
-- Radical by exhaustion
+++ Intersecting

-------------------------------
True
-------------------------------
True
-------------------------------
";

TSL.RegisterTest(Test);


-- TEST 02 :

Test := Record[Id := "radical_02", Descr := "T2Test"];

Test.Input :="
Use ZZ/(32003)[x[1..4]];
I:=Ideal(
x[2]^4 + x[1]^3*x[3] - 2*x[1]*x[3]^3 + x[1]^2 + x[2]*x[3],
- x[1]^2*x[2]^2 - x[2]^3*x[3] - 3*x[2]*x[3]^3 - x[3]^3,
x[2]^4 - 2*x[1]*x[2]*x[3]^2 - x[1]^2*x[3] + 2*x[2]^2*x[3]);
MEMORY.RADICAL_PKG_OUT:=True;
E:=RadicalOfUnmixed(I);
MEMORY.RADICAL_PKG_OUT:=False;
E=Ideal(x[2]^4 + x[1]^3*x[3] - 2*x[1]*x[3]^3 + x[1]^2 + x[2]*x[3], -x[1]^2*x[2]^2 -
x[2]^3*x[3] - 3*x[2]*x[3]^3 - x[3]^3, x[2]^4 - 2*x[1]*x[2]*x[3]^2 - x[1]^2*x[3] +
2*x[2]^2*x[3], x[2]^30 - 993*x[2]^29 + 7940*x[2]^28 + 12659*x[2]^27 + 8774*x[2]^26 -
500*x[2]^25 + 1266*x[2]^24 - 3594*x[2]^23 - 6212*x[2]^22 - 8032*x[2]^21 + 737*x[2]^20
+ 12167*x[2]^19 + 14825*x[2]^18 + 4633*x[2]^17 - 13782*x[2]^16 + 3147*x[2]^15 +
10094*x[2]^14 + 14552*x[2]^13 - 12563*x[2]^12 - 1781*x[2]^11 - 12780*x[2]^10 +
2139*x[2]^9 - 2766*x[2]^8 - 13986*x[2]^7 - 487*x[2]^6 - 5195*x[2]^5 + 394*x[2]^4 +
2096*x[2]^3 - 842*x[2]^2 - 6203*x[2], x[1]^30 - 6031*x[1]^29 - 9808*x[1]^28 +
15432*x[1]^27 + 8298*x[1]^26 + 10183*x[1]^25 - 12754*x[1]^24 + 9516*x[1]^23 -
1465*x[1]^22 - 2153*x[1]^21 + 2078*x[1]^20 - 91*x[1]^19 + 1238*x[1]^18 + 382*x[1]^17
+ 5152*x[1]^16 - 4819*x[1]^15 - 10180*x[1]^14 + 15246*x[1]^13 + 12691*x[1]^12 -
1049*x[1]^11 - 8004*x[1]^10 + 14757*x[1]^9 + 1966*x[1]^8 + 8621*x[1]^7 - 8666*x[1]^6
- 14313*x[1]^5 - 11811*x[1]^4 + 10360*x[1]^3 - 14808*x[1]^2 + 6253*x[1], x[3]^30 +
2716*x[3]^29 - 13509*x[3]^28 + 12004*x[3]^27 - 5997*x[3]^26 - 14078*x[3]^25 +
12648*x[3]^24 - 3187*x[3]^23 + 3848*x[3]^22 - 6570*x[3]^21 + 11902*x[3]^20 +
9205*x[3]^19 + 4796*x[3]^18 - 9673*x[3]^17 + 12422*x[3]^16 + 14867*x[3]^15 +
654*x[3]^14 + 15820*x[3]^13 + 8496*x[3]^12 + 12980*x[3]^11 + 5476*x[3]^10 +
4205*x[3]^9 - 15956*x[3]^8 - 10724*x[3]^7 - 2919*x[3]^6 - 4935*x[3]^5 + 9924*x[3]^4 -
14554*x[3]^3 + 12653*x[3]^2 - 10420*x[3]);
";

Test.ExpectedOutput := "Main Cycle (3)
Found Useful SQFR
--0-Dim (local) (0)
++++ 1  Components
Main Cycle (3)
Found Useful SQFR
--0-Dim (local) (0)
++++ 1  Components
Main Cycle (3)
Main Cycle (2)
Main Cycle (1)
Found Useful SQFR
--0-Dim (local) (0)
++++ 1  Components
Main Cycle (3)
Main Cycle (2)
Main Cycle (1)
-- Radical by exhaustion
+++ Intersecting
+++ Intersecting
+++ Intersecting

-------------------------------
True
-------------------------------
";

TSL.RegisterTest(Test);
