-- $Id: ts_lib.cpkg,v 1.10 2009/06/04 15:55:23 bigatti Exp $
-- ts_lib.pkg : Tests Sets common library

Package $ts/ts_lib

Alias TSL := $ts/ts_lib;

Define Initialize()
  MEMORY.PKG.TSL := Record[]; -- TODO: REMOVE
  MEMORY.PKG.TSL.TEST := Record[];
  TSL.SetTSID("NO_TSID");
  TSL.SetTESTID("NO_TESTID");
  TSL.SetRes(OpenOString("Res"));
EndDefine;

Define SetTSID(I) MEMORY.PKG.TSL.TSID := I EndDefine;
Define SetTESTID(I) MEMORY.PKG.TSL.TESTID := I EndDefine;
Define SetRes(R) MEMORY.PKG.TSL.Res := R EndDefine;

Define Res() Return MEMORY.PKG.TSL.Res; EndDefine;
Define TSID() Return MEMORY.PKG.TSL.TSID; EndDefine;
Define TESTID() Return MEMORY.PKG.TSL.TESTID; EndDefine;
Define FullID() Return TSL.TSID() + "-" + Sprint(TSL.TESTID()); EndDefine;

Define SaveResults()
  Print Cast(TSL.Res(),STRING);
EndDefine;

Define StoreComment(C)
  PrintLn "DB.Update(DB,\"", TSL.FullID(), "\",\"Comment\",\"",  C,  "\");" On TSL.Res();
EndDefine;

Define StoreNumIndets(N)
  PrintLn "DB.Update(DB,\"",  TSL.FullID(),  "\",\"NumIndets\",",  N,  ");" On TSL.Res();
EndDefine;

Define StoreSizeAndTime(L,Sz,T)
  ID := TSL.FullID();
  If TSL.InsideTripoli() Then
    PV :=  "EEE"+Sprint(L);
  Else
    PV := "MONOPOLI";
  EndIf;
  PrintLn "DB.Update(DB,\"",   ID,   "\",\"Size_",   PV,   "\",",    Sz,   ");" On TSL.Res();
  PrintLn "DB.Update(DB,\"",   ID,   "\",\"Timings_",   PV,    "\",",    T,   ");" On TSL.Res();
EndDefine;

Define ElimMat(X,Y)
  MEMORY.X := IndetIndex(X);
  MEMORY.Y := IndetIndex(Y);
  ElimMatAuxRing ::= ZZ/(3)[x[1..NumIndets()]],Elim(x[MEMORY.X]..x[MEMORY.Y]);
  Return Ord(ElimMatAuxRing);
EndDefine;

Define InsideTripoli()
  I := CocoaInfos();
  Return I.PolyVersion <> "[MONOPOLI/MONOPOLI]"
EndDefine;

Define SetLevels(L, Default)
  If Not TSL.InsideTripoli() Then Return [0]
  Elif Len(L) = 0 Then Return Default
  Elif Type(L[1]) = LIST Then
    If Len(L[1]) = 0 Then Return Default Else Return L[1] EndIf
  Else L
  EndIf;
EndDefine; -- SetLevels

Define PrintTime(T)
  PrintLn " --> ",T
EndDefine;

Define PrintTestInfo(Comment, Levels)
  PrintLn Dashes();
  I := CocoaInfos();
  PrintLn "-- Date   : ",Date();
  PrintLn "-- Version: ",I.Version," ", I.PolyVersion;
  PrintLn "-- Test-Id: ",TSL.FullID();
  PrintLn "-- Comment: ",Comment;
  If TSL.InsideTripoli() And Levels <> [] Then PrintLn "-- Levels : ",Levels EndIf;
  TSL.StoreComment(Comment);
EndDefine;

Define DoTests(L)
  Foreach T In L Do
    Call(Function("T"+Sprint(T)));
    PrintLn;
  EndForeach;
EndDefine;

Define Sprint_CpuTime(T)
  X := Div(Mod(T,1000),10);
  If X < 10 Then Dot := ".0" Else Dot := "." EndIf;
  Return Sprint(Div(T,1000))+Dot+Sprint(X);
EndDefine;

Define RevLexMat(N) Return Mat(Reversed(-Identity(N))); EndDefine;

Define TuplesAux(L,M)
  If M = 0 Then Return L
  Elif M = 1 Then Return [[X] | X In L]
  Elif L = [] Then Return []
  Else
    Return Concat([Concat([Head(L)],X)|X In TSL.TuplesAux(Tail(L),M-1)],
		  TSL.TuplesAux(Tail(L),M))
  EndIf;
EndDefine; -- TuplesAux


Define Tuples(L,M)
  If M < Len(L)/2 Then Return TSL.TuplesAux(L,M)
  Else Return [Diff(L,X)| X In Reversed(TSL.TuplesAux(L,Len(L)-M))]
  EndIf;
EndDefine; -- Tuples


Define SelElements(L,These) Return [L[X] | X In These]; EndDefine;


Define FirstInvertibleSubMat(M) -- FirstInvertibleCompletionMat() is ...
  -- ... more efficient
  Ts := TSL.Tuples(1..Len(M),NumIndets());
  For I := 1 To Len(Ts) Do
    MI := Mat(TSL.SelElements(M,Ts[I]));
    If Det(MI) <> 0 Then
      Return Ts[I]
    EndIf
  EndFor;
  Error("No invertible square submatrices");
EndDefine;

Define FirstInvertibleCompletionMat(M,C)
  -- M = First Rows of matrix
  -- C = Completion Candidates
  NR := Len(M);
  NC := Len(M[1]);
  If Len(C) < NC-NR Then Error("Not enough completion candidates") EndIf;
  Ts := TSL.Tuples(1..Len(C),NC-NR);
  For I := 1 To Len(Ts) Do
    ICM := Mat(Concat(M,TSL.SelElements(C,Ts[I])));
    If Det(ICM) <> 0 Then
      Return ICM
    EndIf
  EndFor;
  Error("No invertible square submatrices");
EndDefine;

------------------------------------
-- Execute and verify Tests from a database

Define Test(Id)
  If Type(Id) = INT And Id < 10 Then Id := "0"+Sprint(Id) EndIf;
  If Not IsDefined(MEMORY.PKG.TSL.TEST.Var(Sprint(Id))) Then
    Error("Test "+Sprint(Id)+" is undefined");
  Else
    Return MEMORY.PKG.TSL.TEST.Var(Sprint(Id))
  EndIf
EndDefine;

Define SetOutDev(D) MEMORY.PKG.TSL.OutDev := D EndDefine;
Define OutDev() Return MEMORY.PKG.TSL.OutDev; EndDefine;

Define SetTest(Id,T)
  If Type(Id) = INT And Id < 10 Then Id := "0"+Sprint(Id) EndIf;
  MEMORY.PKG.TSL.TEST.Var(Sprint(Id)) := T
EndDefine;

Define Output(Id) Return Comp(TSL.Test(Id),"Output"); EndDefine;
Define ExpectedOutput(Id) Return Comp(TSL.Test(Id),"ExpectedOutput");EndDefine;
Define Input(Id) Return Comp(TSL.Test(Id),"Input");EndDefine;
Define ResultFn(Id) Return Comp(TSL.Test(Id),"Result");EndDefine;

Define IsSelected(Id)
  T := TSL.Test(Id);
  Return T.Selected;
EndDefine;

Define Select(...)
  Foreach Id In ARGV Do
    T := TSL.Test(Id);
    T.Selected := True;
    TSL.SetTest(Id,T);
  EndForeach;
EndDefine;

Define Deselect(...)
  Foreach Id In ARGV Do
    T := TSL.Test(Id);
    T.Selected := False;
    TSL.SetTest(Id,T);
  EndForeach;
EndDefine;

Define List(...)
  L := Flatten(ARGV);
  If Len(L) = 0 Then L := TSL.All() EndIf;
  Foreach Id In L Do
    Print Format(Id,5);
    If TSL.IsSelected(Id) Then Print " * " Else Print "   " EndIf;
    PrintLn TSL.Descr(Id);
  EndForeach;
EndDefine;

Define StartTest()
  -- Print "-- Result : ";
  Reset();
  TSL.SetOutDev(OpenOString(""));
  CloseLog(DEV.STDOUT);
  OpenLog(TSL.OutDev());
EndDefine;

Define Run(T)
  If Type(T) <> RECORD Then Test := TSL.Test(T) Else Test := T EndIf;
  Source(OpenIString("",Test.Input));
EndDefine;

Define RunTests(...)  // was Do(...);
  TSL.DoAux(1,ARGV,Record[])
EndDefine;

Define DoRev(...)
  TSL.DoAux(1,Reversed(Flatten(ARGV)),Record[Reversed:=True])
EndDefine;

Define Do2(...)
  TSL.DoAux(2,ARGV,Record[])
EndDefine;

Define DoN(...)
  TSL.DoAux(Head(ARGV),Tail(ARGV),Record[])
EndDefine;

Define DoAux(N,L,F)
  L := Flatten(L);
  If L = [] Then L := TSL.All() EndIf;
  If IsDefined(F.Reversed) And F.Reversed Then L := Reversed(L) EndIf;
  Foreach Id In L Do
    If TSL.IsSelected(Id) Then
      For I := 1 To N Do
        Print "-- Test ",Format(Id,3)," : ";
        TSL.DoTest(Id);
	PrintLn TSL.ResultFn(Id);
      EndFor
    Else
      PrintLn "-- Test ",Format(Id,3)," : (not selected)"
    EndIf;
  EndForeach;
EndDefine;

Define All() Return Fields(MEMORY.PKG.TSL.TEST); EndDefine;
Define Selected() Return [Id In TSL.All() | TSL.IsSelected(Id)]; EndDefine;


Define DoTest(T)
  If Type(T) <> RECORD Then Test := TSL.Test(T) Else Test := T EndIf;
  X := DebugVersion();
  TSL.StartTest();
  Test.TimeElapsed := Time TSL.Run(Test);
  TSL.EndTest(Test);
  TSL.CheckTest(Test);
  If Type(T) <> RECORD Then TSL.SetTest(T,Test) EndIf;
EndDefine;

Define Descr(T)
  If Type(T) <> RECORD Then Test := TSL.Test(T) Else Test := T EndIf;
  If IsDefined(Test.Descr) Then
    Return Test.Descr;
  Else
    Return NULL;
  EndIf;
EndDefine;

Define EndTest(Var Test)
  CloseLog(TSL.OutDev());
  OpenLog(DEV.STDOUT);
  Test.Output := Cast(TSL.OutDev(),STRING);
  TSL.SetOutDev(NULL);
EndDefine;

Define CheckTest(Var Test)
  Sys := Comp(CocoaInfos(),"System");
  If Not IsDefined(Test.ExpectedOutput) Then
    Test.Result := "Finished (cannot check result)"
  Elif Test.Output = Test.ExpectedOutput
  Then Test.Result := "Succeeded"
  Else
    If Sys = "MAC" Then
      Test.Result := "FAILED! (output is different from expected)"
    Else
      F_Expected:=Sprint(Test.Id)+".expected";
      F_Found:=Sprint(Test.Id)+".found";
      Test.Result := Test.Descr + " FAILED! (see files "+F_Expected+" and "+F_Found+")";
      D := OpenOFile(F_Expected, "w");
      Print Test.ExpectedOutput On D;
      Close(D);
      D := OpenOFile(F_Found, "w");
      Print Test.Output On D;
      Close(D);
    EndIf;
  EndIf;
--   If Sys <> "MAC" Then
--     F_Result:=Sprint(Test.Id)+".result";
--     D := OpenOFile(F_Result, "w");
--     PrintLn Test.Result On D;
--     Close(D);
--   EndIf;
EndDefine;

Define RegisterTest(Test)
  Test.Selected := True;
  TSL.SetTest(Test.Id,Test);
EndDefine;

EndPackage; -- Package $ts/ts_lib

$ts/ts_lib.Initialize(); -- TODO: REMOVE

/*
--------------------------------------------------------------

Source "ts_lib.pkg";
Alias TSL := $ts/ts_lib;

Test := Record[Id := "A_Test"];
Test.Input := "1+1;";
Test.ExpectedOutput :=
"2
-------------------------------
";
TSL.RegisterTest(Test);
TSL.DoTest(Test);
TSL.DoTest("A_Test");
*/