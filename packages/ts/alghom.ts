-- Example/Test suite For algebra homomorphisms

Alias TSL := $ts/ts_lib;
TSL.Initialize();

Define RegisterTrue(Var T, N)
  Str := "True" + NewLine() + "-------------------------------" + NewLine();
  T.ExpectedOutput := Str;
  For I := 2 To N Do T.ExpectedOutput := T.ExpectedOutput + Str; EndFor;
  $ts/ts_lib.RegisterTest(T);
EndDefine; -- RegisterTrue
  
-------------------------------
-- TEST
Test := Record[Id := "alghom-01", Descr := "QQ[x,y,z] --> QQ[a,b]"];

Test.Input :="
    Rxyz ::= QQ[x,y,z];
    Rab  ::= QQ[a,b];

    Phi := $alghom.Map(\"Rxyz\", \"Rab\", Rab::[a+1, ab+3, b^2]);

    Ker(Phi) = Rxyz::Ideal(x^2z - y^2 - 2xz + 6y + z - 9);
    Not $alghom.IsInjective(Phi);
    Not $alghom.IsSurjective(Phi);

    $alghom.IsInImage(Rab::a^2, Phi);
    $alghom.PreImage(Rab::a^2, Phi) = Rxyz::(x^2 - 2x + 1);
    $alghom.Image(Rxyz::(x^2 - 2x + 1), Phi) = Rab::a^2;
";    RegisterTrue(Test, 6 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "alghom-02", Descr := "linear maps"];

Test.Input :="
    M := Mat([[1,2,3],[2,3,4],[1,0,1]]); -- invertible

    Use AMR ::= QQ[x[1..3]];
    L := [ Sum([ Row[I]x[I] | I In 1..Len(Row)]) | Row In M ];
    Psi := $alghom.Map(\"AMR\", \"AMR\", L);  -- linear map defined by M
    $alghom.IsInjective(Psi);
    $alghom.IsSurjective(Psi);

    Inv := [$alghom.PreImage(x[I], Psi) | I In 1..3];
    MM := Mat([ [ CoeffOfTerm(x[I], Inv[J]) | I In 1..3 ]
	      | J In 1..3]);
    M * MM = Identity(3);
";    RegisterTrue(Test, 3 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "alghom-03", Descr := "symmetric polynomials"];

Test.Input :="
    Use QQ[x,y,z];
    -- elementary symmetric polynomials
    S0 := 1;  S1 := x+y+z;  S2 := xy + xz + yz;  S3 := xyz;
    SA := SubalgebraMap([S0, S1, S2, S3]);
    -- x^4 + y^4 + z^4 is a symmetric polynomial
    IsInSubalgebra(x^4 + y^4 + z^4, SA);
    Repr := SubalgebraRepr(x^4 + y^4 + z^4, SA);
    Repr = SubalgebraRing::(x[2]^4 - 4x[2]^2x[3] + 2x[3]^2 + 4x[2]x[4]);
    Image(Repr, RMap([S0, S1, S2, S3])) = x^4 + y^4 + z^4;
    -- equivalent to
    $alghom.Image(Repr, SA) = x^4 + y^4 + z^4;
";    RegisterTrue(Test, 4 /* times True */);
