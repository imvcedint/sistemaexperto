-- Example/Test suite for CoCoAServer computations

Alias TSL := $ts/ts_lib;
TSL.Initialize();

Define CoCoAServerRegister(Var T, N)
  Str := "True
-------------------------------
";
  T.ExpectedOutput := Str;
  For I := 2 To N Do T.ExpectedOutput := T.ExpectedOutput + Str; EndFor;
  $ts/ts_lib.RegisterTest(T);
EndDefine; -- CoCoAServerRegister
  
-------------------------------
-- TEST
Test := Record[Id := "cocoa5-01", Descr := "ReducedGBasis5x Module posto"];

Test.Input :="
Use QQ[x,y,z],PosTo;
M := Module([x,z], [z,y]);
RGB := ReducedGBasis(M);
X := ReducedGBasis5x(M, Record[IsPosTo:=True]);// PosTo is default
X=       [Vector(z, y),       Vector(x, z),       Vector(0, x*y - z^2)];
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-02", Descr := "ReducedGBasis5x Module ToPos"];

Test.Input :="
Use QQ[x,y,z],PosTo;
M := Module([x,z], [z,y]);
X := ReducedGBasis5x(M, Record[IsPosTo:=False]);
X=       [       Vector(z, y),       Vector(x, z)];
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-03", Descr := "GBasis5x param"];

Test.Input :="
M := CoCoA5.AddParamOrdMat(DegRevLexMat(2), 1);
Use ZZ/(32003)[a, x,y], Ord(M);

I := Ideal((a-1)*x+(a^2+a)*y, (a+1)*x + y);
X := GBasis5x(I, Record[NumParams:=1]);
X=[x, y];
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-04", Descr := "GBasis5x Module shifts"];

Test.Input :="
Use QQ[x,y,z], Weights([1,2,4]);
M := Module([x^2-y,1],[x^4-z,y^2]);
Info5 := Record[];
Info5.ModuleShifts := Mat([[0,2]]);
X := GBasis5x(M, Info5);
X = [ Vector(x^2 - y, 1), Vector(y^2 - z, y^2 - x^2 - y)];
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-05", Descr := "ReducedGBasis5 Module OrdMat"];

Test.Input :="
OrdMat := Mat([[1,1,1],[2,1,1],[1,1,0]]); // Ring Grading (first 2 Rows) 
                                          // Plus order (last row)
Use ZZ/(101)[x,y,z], Ord(OrdMat), ToPos;
M := Module([y-x,0,0], [x,0,z], [0,y^2-z^2,0]);
X := ReducedGBasis5(Module(Gens(M)));
X= [Vector(x,0,z),Vector(y,0,z),Vector(0, 0, x*z-y*z),Vector(0, y^2-z^2, 0)];
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-06", Descr := "ReducedGBasis5x Module shifts gradingdim2"];

Test.Input :="
OrdMat := Mat([[1,1,1],[2,1,1],[1,1,0]]); // Ring Grading (first 2 Rows) 
                                          // Plus order (last row)
Use ZZ/(101)[x,y,z], Ord(OrdMat), ToPos;
M := Module([y-x,0,0], [x,0,z], [0,y^2-z^2,0]);
Info5 := Record[];
Info5.OrdMat := OrdMat;
Info5.GradingDim := 2;
Info5.ModuleShifts := Mat([[3,1,2],[2,2,5]]); // GrDim rows!!
X := ReducedGBasis5x(M, Info5);
X=[Vector(0, y^2-z^2, 0),Vector(x, 0, z),Vector(y, 0, z),Vector(0, 0, x*z - y*z)];
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-07", Descr := "Elim5"];

Test.Input :="
Use QQ[x,y,z,w[3..5]], Weights([7, 4, 3, 1, 1, 1]);
// Use ZZ/(7)[x,y,z,w[3..5]], Weights([7, 4, 3, 1, 1, 1]); // exbug

I := Ideal(
	   x - 7413431*w[4]^7 - 9162341*w[3]*w[4]*w[5]^5,
	   y - 6521443*w[4]^4 - 2312257*w[3]^2*w[4]*w[5],
	   z - 5329421*w[4]^3 - 2122414*w[3]*w[5]^2
	   );

E  := Elim([w[3],w[4]], I);
E5 := Elim5([w[3],w[4]], I);
E=E5;
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-08", Descr := "Intersection5"];

Test.Input :="
Use QQ[x,y,z], Weights(1,2,1);
I := Ideal(x*y, z^2);
J := Ideal(y*z, x-z);
I5 := Intersection5(I, J);
I4 := Intersection(I, J);
I4 = I5;
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-09", Descr := "Intersection5 ModuleShifts"];

Test.Input :="
Use QQ[x,y,z], Weights(1,2,1);
M := Module([x^2*y, 0], [x*z^2, 0], [-x*z + x^2, -z + x]);
N := Module([x*z^2, 0], [x*y*z, y*z], [-x*z + x^2, -z + x]);
I5 := Intersection5x(M, N, Record[ModuleShifts := [[1,2]]]);
I4 := Intersection(M, N);
I4 = I5;
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-10", Descr := "Intersection5 Module"];

Test.Input :="
Use ZZ/(101)[x,y,z,t],PosTo;
M:=Module([(x+y+t)^3,y^3],[(x-y-z)^3,z^3],[x^2-y^2,x^2-z^2],[x*y*z+y*z*t+z*t*x,x^3]);
N:=Module([x^2+x*y,y^2]);

I4 := Intersection(M,N);// Long
I5 := Intersection5(M,N);// Long translation
I4 = I5;
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-11", Descr := "Intersection5x 2 param"];

Test.Input :="
M := CoCoA5.AddParamOrdMat(DegRevLexMat(3), 2); -- 2 parameters
Use ZZ/(32003)[a,b, x,y,z], Ord(M);
II := Ideal(x-y);
I := (a-b+1) * x * II;
J := (a+1) * y * II;
X := Intersection5x(I,J, Record[NumParams:=2]); -- 2 parameters
X=x*y*II;
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-12", Descr := "Colon5x Module/Module 1"];

Test.Input :="
Use ZZ/(101)[x,y,z];
MZ := Module(Vector(x,y));
M := Module([x,y], [x,z]);
Info5 := Record[];
Info5.OrdMat := Mat([[1,2,2], [1,0,0], [0,0,-1]]);
Info5.GradingDim := 2;
Info5.ModuleShifts := Mat([[1,0], [0,1]]);
T := Colon5x(M, MZ, Info5);
T = Ideal(1);
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-13", Descr := "Colon5x Module/Module 2"];

Test.Input :="
Use ZZ/(101)[x,y,z];
MZ := Module(Vector(x,y));
M := Module([x^2,y^2], [x^2,z^2]);
T := Colon5(M, MZ);
T = Ideal(x*y^2 - x*z^2);
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-14", Descr := "Saturation5x"];

Test.Input :="
Use QQ[x,y,z,t];
M := Ideal(x^5, x^2*y^2);
Z := x^5;
T4 := Saturation(M, Ideal(Z));
T5 := Saturation5x(M, Ideal(Z), Record[]);
-- Ideal(x^2*y^2, x^5, 1)
T4 = T5;
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-15", Descr := "Saturation5x Module/Ideal"];

Test.Input :="
Use ZZ/(101)[x,y,z];
M := Module([x,y],[x,z]);
Z := x;

Info5 := Record[];
Info5.OrdMat := Mat([[1,2,2], [1,0,0], [0,0,-1]]);
Info5.GradingDim := 2;
Info5.ModuleShifts := Mat([[1,0], [0,1]]);
T5 := Saturation5x(M, Ideal(Z), Info5);
--    Module([x, z], [0, y - z], [y - z, 0])
T4 := Saturation(M, Ideal(Z));
T4 = T5;
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-16", Descr := "Saturation5 Module/Ideal"];

Test.Input :="
M:=Module([x^2,y^2],[x^2,z^2]);
T5 := Saturation5(M, Ideal(Z));
--    Module([x^2, z^2], [0, y^2 - z^2], [x*y^2 - x*z^2, 0], [y^2- z^2, 0])
T4 := Saturation(M, Ideal(Z));
T4 = T5;
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-17", Descr := "Saturation5 Module/Ideal"];

Test.Input :="
Use ZZ/(101)[x,y,z,t,u,v];
M:=Module([x*y*z+y*z*t+z*t*u,t*u*v+u*v*x+v*x*y],[x*y*z*t+y*z*t*u+z*t*u*x,z^4],[t*u*v+y*z*t+u*v*x,t^2*z]);
Z:=y^2;
X := Saturation5(M, Ideal(Z));
T4 := Saturation(M, Ideal(Z));
X = T4;
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-18", Descr := "SyzOfGens5x Module"];

Test.Input :="
Use ZZ/(101)[x,y,z], Weights(1,2,4);
L:=[Vector(x^2,x^2), Vector(x*y,x*y)];
S5 := SyzOfGens5x(Module(L), Record[]);
--    Module([y, -x])
S4 := SyzOfGens(Module(L));
S4 = S5;
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-19", Descr := "Syz5"];

Test.Input :="
Use ZZ/(101)[x,y,z,t,h];
L := [x+y+z+t,x*y+y*z+z*t+t*x,x*y*z+y*z*t+z*t*x+t*x*y,x*y*z*t-h^4];
S5 := SyzOfGens5x(Ideal(L), Record[]);
S4 := Syz(L);
S4 = S5;
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-20", Descr := "GBasis5x float"];

Test.Input :="
Use QQ[x[0..3]];
L := [Randomized(DensePoly(4)) | I In 1..2];
--GBasis5x(Ideal(L), Record[FloatPrecision:=128]);
X := LT5x(Ideal(L), Record[FloatPrecision:=128]);
X=       Ideal(  x[0]^4,  x[0]^3*x[1],  x[0]^2*x[1]^3,  x[0]*x[1]^5,  x[1]^7);

G0 := GBasis5(Ideal(L));
G128 := GBasis5x(Ideal(L), Record[FloatPrecision:=128]);
Len(G0) = Len(G128);

LT0 := LT5(Ideal(L));
LT128 := LT5x(Ideal(L), Record[FloatPrecision:=128]);
LT0 = LT128;
";    CoCoAServerRegister(Test, 3 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-22", Descr := "ReducedGBasis5(I)"];

Test.Input :="
Use QQ[t,x,y,z];
I := Ideal(t^3-x^3, t^5-y^5, t^7-z^7);
X := ReducedGBasis5(I);

X = [t^3 - x^3, t^2*x^3 - y^5, x^6 - t*y^5, t^2*y^5 - z^7, x^3*y^5 - t*z^7, y^10 - x^3*z^7];
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-23", Descr := "Elim5 Module"];

Test.Input :="
Use QQ[t,x,y,z];
I := Ideal(t^3-x^3, t^5-y^5, t^7-z^7);
X := Elim5([t],I);
X = Ideal(-y^10 + x^3*z^7, -x^12 + y^5*z^7, x^9*y^5 - z^14);

M := Module([0,x+y],[x,y+z]);
Info5:=Record[];
X := Elim5([z],M);
X = Module([0, x + y], [x^2 + x*y, 0]);
";    CoCoAServerRegister(Test, 2 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-24", Descr := "Colon5"];

Test.Input :="
Use QQ[t,x,y,z];
I:=Ideal(x,y);
J:=Ideal(y,z);
K:=Intersection5(I,J);
X := Colon5(Ideal(x^5*y,x*y^2,z^2),Ideal(x,y,z));
X = Ideal(z^2, x*y^2, x^4*y*z, x^5*y);
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-25", Descr := "Saturation5"];

Test.Input :="
Use QQ[t,x,y,z];
X := Saturation5(Ideal(x^5*y,x*y^2,z^2),Ideal(x,y,z));
X = Ideal(z^2, x*y);
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-26", Descr := "Homogenized5x"];

Test.Input :="
M1:=Mat([[2, 1, 1,0],
         [1, 2, 0,1],
         [0, 1, 0,1],
         [1,0,0,0]]);
Use QQ[x,y,a,b],Ord(M1),PosTo;
Info5 := Record[];
Info5.GradingDim := 2;
Info5.OrdMat :=M1;
I := Ideal(x+1,y^2+x);
X := Homogenized5x([a,b],I,Info5);
X = Ideal(a^2*b + x, x*b^3 + y^2, x^2*b^2 - y^2*a^2, y^2*a^4 + x^3*b);
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-27", Descr := "Saturation5"];

Test.Input :="
M1:=Mat([[2, 1, 1,0],
         [1, 2, 0,1],
         [0, 1, 0,1],
         [1,0,0,0]]);
Use QQ[x,y,a,b],Ord(M1),PosTo;
J:=Ideal(x+a^2*b,x*b^3+y^2);
X := Saturation5(J,Ideal(a*b));
X = Ideal(a^2*b + x, x*b^3 + y^2, x^2*b^2 - y^2*a^2, y^2*a^4 + x^3*b);
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-28", Descr := "Homogenized5"];

Test.Input :="
Use QQ[x,y,z,h];
I := Ideal(x^2*y+x*y+1,x*y^2+y^2+1);
X := Homogenized5([h],I);
X = Ideal(x - y, y^3 + y^2*h + h^3);
";    CoCoAServerRegister(Test, 1);


-------------------------------
-- TEST
Test := Record[Id := "cocoa5-29", Descr := "GBasis5x multi-param"];

Test.Input :="
M := CoCoA5.AddParamOrdMat(DegRevLexMat(2), 3);
Use ZZ/(32003)[a,b,c, x,y], Ord(M);

I := Ideal((a-1+b^5)*x+(a^2+c)*y, (a+b+c+1)*x + y);
X := GBasis5x(I, Record[NumParams:=3]);
X = [x, y];
";    CoCoAServerRegister(Test, 1);


-------------------------------
-- TEST
Test := Record[Id := "cocoa5-30", Descr := "Intersection5x multi-param"];

Test.Input :="
M := CoCoA5.AddParamOrdMat(DegRevLexMat(2), 2);
Use ZZ/(32003)[a,b, x,y], Ord(M);

II := Ideal(x-y);
I := (a+b-1) * x * II;
J := (a-b^2+1) * y * II;
X := Intersection5x(I,J, Record[NumParams:=2]);
X=x*y*II;
";    CoCoAServerRegister(Test, 1);


-------------------------------
-- TEST
Test := Record[Id := "cocoa5-31", Descr := "BBasis5 test 1"];

Test.Input :="
Use QQ[x, y], DegLex;
I := Ideal([x^2, x*y + y^2]);
BBasis := BBasis5(I);
-- Computed O_\sigma{Ideal(gens))}-border basis, sigma = DegLex
BBasis = [x*y + y^2, x^2, y^3, x*y^2];
";    CoCoAServerRegister(Test, 1);


-------------------------------
-- TEST
Test := Record[Id := "cocoa5-32", Descr := "BBasis5 test 2"];

Test.Input :="
Use QQ[x, y], DegLex;
I := Ideal([x^3 - x, y^3 - y, x^2*y - 1/2*y - 1/2*y^2, x*y - x - 1/2*y + x^2 - 1/2*y^2, x*y^2 - x - 1/2*y + x^2 - 1/2*y^2]);
BBasis := BBasis5(I);
-- Computed O_\sigma{Ideal(gens))}-border basis, sigma = DegLex
BBasis = [x^2 + x*y - 1/2*y^2 - x - 1/2*y, y^3 - y, x*y^2 - x*y, x^2*y - 1/2*y^2 - 1/2*y];
";    CoCoAServerRegister(Test, 1);


-------------------------------
-- TEST
Test := Record[Id := "cocoa5-33", Descr := "BBasis5 test 3"];

Test.Input :="
Use QQ[x, y], DegLex;
I := Ideal([x^3, x*y^2 + y^3]);
BBasis := BBasis5(I);
-- Computed O_\sigma{Ideal(gens))}-border basis, sigma = DegLex
BBasis = [x*y^2 + y^3, x^3, x*y^3 + y^4, x^2*y^2 - y^4, x^3*y, y^5, x*y^4];
";    CoCoAServerRegister(Test, 1);


-------------------------------
-- TEST
Test := Record[Id := "cocoa5-34", Descr := "BBasis5 test 4"];

Test.Input :="
Use QQ[x, y, z], DegRevLex;
I := Ideal([z^2 + 3*y - 7*z, y*z - 4*y, x*z - 4*y, y^2 - 4*y, x*y - 4*y, x^5 - 8*x^4 + 14*x^3 + 8*x^2 -15*x + 15*y]);
BBasis := BBasis5(I);
-- Computed O_\sigma{Ideal(gens))}-border basis, sigma = DegRevLex
BBasis = [z^2 + 3*y - 7*z, y*z - 4*y, x*z - 4*y, y^2 - 4*y, x*y - 4*y, x^2*z - 16*y, x^2y - 16*y, x^3*z - 64*y, x^3*y - 64*y, x^4*z - 256*y, x^4*y - 256*y, x^5 - 8*x^4 + 14*x^3 + 8*x^2 - 15*x + 15*y];
";    CoCoAServerRegister(Test, 1);


-------------------------------
-- TEST
Test := Record[Id := "cocoa5-35", Descr := "FGLM5 test 1"];

Test.Input :="
Use QQ[x, y, z], DegRevLex;
GBOld := List([z^4 -3*z^3 - 4*y*z + 2*z^2 - y + 2*z - 2, y*z^2 + 2*y*z - 2*z^2 + 1, y^2 - 2*y*z + z^2 - z, x + y - z]);
M := Mat([
  [1, 0, 0],
  [0, 1, 0],
  [0, 0, 1]
]);
GBNew := FGLM5(GBOld, M);
Use QQ[x, y, z], Ord(M);
-- New basis (Lex)
BringIn(GBNew) = [z^6 - z^5 - 4*z^4 - 2*z^3 + 1, y - 4/7*z^5 + 5/7*z^4 + 13/7*z^3 + 10/7*z^2 - 6/7*z - 2/7, x + 4/7*z^5 - 5/7*z^4 - 13/7*z^3 - 10/7*z^2 - 1/7*z + 2/7];
";    CoCoAServerRegister(Test, 1);


-------------------------------
-- TEST
Test := Record[Id := "cocoa5-36", Descr := "FGLM5 test 2"];

Test.Input :="
Use QQ[a, b, c, d];
I := Ideal([a^3 + 3*a*b^6 + 4, a*b*c, 21*a^2 + b^3, 36*a^2*d^3 + 39*b^2 + 23*c^6, a^3*b*c]);
GBOld := GBasis(I);
M := Mat([
  [0, 1, 0, 0],
  [0, 0, 1, 0],
  [1, 0, 0, 0],
  [0, 0, 0, 1]
]);
GBNew := FGLM5(GBOld, M);
Use QQ[a, b, c, d], Ord(M);
-- New basis (custom Lex: b > c > a > d)
BringIn(GBNew) = [d^45 + 1247608746734077/113246208*d^18 - 6581132396196015972769/402653184*d^9 + 104146360939610387004925670079/17179869184, a + 67108864/3839715628633792147200263667*d^36 + 1048576/80878732224530009679*d^27 + 16384/1703607756123*d^18 + 16/83096822016117*d^9 - 1/7001316, c, b + 4294967296/1144465504830883373680501869*d^42 + 1024/24767835839019*d^15 - 32/521703*d^6];
";    CoCoAServerRegister(Test, 1);


-------------------------------
-- TEST
Test := Record[Id := "cocoa5-37", Descr := "Gin5"];

Test.Input :="
Use QQ[x,y,z,w];
I := Ideal(x^3-y^3, y^2-z*w);
Gin5(I) = Ideal(x^2, x*y^2, y^4);
";    CoCoAServerRegister(Test, 1 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-38", Descr := "PreprocessPts5 - basic"];

Test.Input :="
Use QQ[x[1..4]];
PP := PreprocessPts5(GenericPoints(6), [0.1,0.1,0.1,0.1]);
Len(Comp(PP,\"Points\")) = 6;
Len(Comp(PP,\"Weights\")) = 6;
Sum(Comp(PP,\"Weights\")) = 6;
";    CoCoAServerRegister(Test, 3 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-39", Descr := "PreprocessPts5 - basic2"];

Test.Input :="
Use QQ[x[1..4]];
PP := PreprocessPts5(GenericPoints(6), [1,1,1,1]);
Len(Comp(PP,\"Points\")) = 2;
Len(Comp(PP,\"Weights\")) = 2;
Sum(Comp(PP,\"Weights\")) = 6;
";    CoCoAServerRegister(Test, 3 /* times True */);

-------------------------------
-- TEST
Test := Record[Id := "cocoa5-40", Descr := "Frobby"];

Test.Input :="
I := Ideal(x^2,x*y,y^2,z^2);
AlexanderDual_Frobby5(I, x^2y^2z^2) = Ideal(x^2yz, xy^2z);
MaximalStandardMonomials_Frobby5(I) = Ideal(yz, xz);
IrreducibleDecom_Frobby5(I) = [Ideal(x, y^2, z^2), Ideal(x^2, y, z^2)];
";    CoCoAServerRegister(Test, 3 /* times True */);
