-- $Id: random.ts,v 1.4 2009/04/28 16:51:51 bigatti Exp $
-- Test Suite: demo of some functions

Alias TSL := $ts/ts_lib;
TSL.Initialize();

-------------------------------
-- RANDOM 01 : Function Randomized

Test := Record[Id := "random_01",Descr := "Function Randomized"];

Test.Input :="
Use S ::= ZZ/(101)[xyzw];
Randomized(DensePoly(2));
";
TSL.RegisterTest(Test);
