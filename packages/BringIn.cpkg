Package $BringIn

Alias Self := $BringIn;

/*------------------------------------------------------------------*/
Define About()
  PrintLn  "    Version  : CoCoA 4.1";
  PrintLn  "    Date     : 11 March 2007";
  PrintLn  "    Author   : Anna Bigatti";
EndDefine;
/*------------------------------------------------------------------*/

Define BringIn(F)
  If Type(F) < POLY Then Return LC(Poly(F)); EndIf;
  If Type(F) = LIST Then Return [Self.BringIn(X) | X In F]; EndIf;
  If Type(F) = MAT Then Return Mat(Self.BringIn(Cast(F,LIST))); EndIf;
  If Type(F) = VECTOR Then Return Vector(Self.BringIn(Cast(F,LIST))); EndIf;
  If Type(F) = RATFUN Then Return Self.BringIn(Num(F))/Self.BringIn(Den(F)); EndIf;
  If Type(F) = IDEAL Then
    Error("To map an ideal type: Ideal(BringIn(Gens(I)))");
  EndIf;
  If Type(F) <> POLY Then Error("BringIn: cannot apply to "+Sprint(Type(F))); EndIf;
  ML := Self.MapList(F);
  Ch_F := Characteristic(Var(RingEnv(F)));
  Ch_Current := Characteristic();
  If Ch_F = Ch_Current Then
    Return $misc.Map(F, RingEnv(F), RingEnv(), ML);
  EndIf;
  If Ch_F = 0 Then
    AuxBringInRing ::= QQ[x[1..NumIndets()]];
    Using AuxBringInRing Do
      F := $misc.Map(F, RingEnv(F), RingEnv(), ML);
    EndUsing;
    Return QZP(F);
  EndIf;
  If Ch_Current = 0 Then
    AuxBringInRing ::= ZZ/(Ch_F)[x[1..NumIndets()]];
    Using AuxBringInRing Do
      F := $misc.Map(F, RingEnv(F), RingEnv(), ML);
    EndUsing;
    Return ZPQ(F);
  EndIf;
  Error("BringIn: cannot convert into different finite characteristic");
EndDefine;

Define MapList(F)
  IndetNames := [ IndetName(X)+Sprint(IndetInd(X)) | X In Indets() ];
  ML := NewList(NumIndets(), 0);
  Using Var(RingEnv(F)) Do
    T := Product([ LPP(X) | X In Support(F)]);
    IndetsInF := [ X In Indets() | Deg(T, X) <> 0 ];
    Foreach X In IndetsInF Do
      I := Self.FirstIndex(IndetName(X)+Sprint(IndetInd(X)), IndetNames);
      If I = 0 Then
	Str := IndetName(X);
	If IndetInd(X)<>[] Then Str := Str + Sprint(IndetInd(X)); EndIf;
	Error("BringIn: \""+Str+"\" is not in current ring");
      Else
	ML[I] := IndetIndex(X);
      EndIf;
    EndForeach;
  EndUsing;
  Return ML;
EndDefine; -- MapList


-- Returns the first index I such that L[I]=X, and 0 if X is not In L
Define FirstIndex(X, L)
  For I := 1 To Len(L) Do
    If L[I]=X Then Return I EndIf;
  EndFor;
  Return 0;
EndDefine; -- FirstIndex


Define Man()
  PrintLn " A package exporting the function BringIn which intelligently";
  PrintLn "maps a polynomial into the current ring preserving variable names.";
  PrintLn "";
  PrintLn "-- >EXAMPLES< --";
  PrintLn "";
  PrintLn "RR ::= QQ[x[1..4],z,y];";
  PrintLn "SS ::= ZZ/(101)[z,y,x[1..2]];";
  PrintLn "Use RR;";
  PrintLn "F := (x[1]-y-z)^5;";
  PrintLn "Len(F);";
  PrintLn "F;";
  PrintLn "Use SS;";
  PrintLn "Time B := BringIn(F);";
  PrintLn "B;";
  PrintLn "";
  PrintLn "-- >EXAMPLES< --";
  PrintLn "";
  PrintLn "Use R ::= QQ[x,y,z];";
  PrintLn "F := 1/2*x^3+34/567*x*y*z-890;   -- a poly with rational coefficients";
  PrintLn "Use S ::= ZZ/(101)[x,y,z];";
  PrintLn "QZP(F);";
  PrintLn "BringIn(F);";
EndDefine;

EndPackage;
