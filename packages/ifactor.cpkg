Package $ifactor  -- partial factorization of integers

Define About()
  PrintLn "    Author: J Abbott";
  PrintLn "    Date: 14 November 2005";
  PrintLn "    Comment: now requires second arg to be positive.";
EndDefine; -- About

-----------------------------------------------------------------------------
-- Finds small prime factors in the integer N.
-- Result is [P, N1] where P is a list of small primes (possibly
-- containing repeats) and N1 has only prime factors greater than MaxP.

Define SmoothFactor(N, MaxP)
  If Type(N) <> INT Or Type(MaxP) <> INT Then Error("SmoothFactor: both args must be integer"); EndIf;
  If MaxP <= 0 Then Error("SmoothFactor: second arg (prime limit) must be positive"); EndIf;
  If N IsIn [-1,0,1] Or MaxP < 2 Then Return [[],N]; EndIf;
  If N < 0 Then N := -N; EndIf;
  Facs := [];
  P := 2;
  While P <= MaxP And P^2 <= N Do
    While Mod(N, P) = 0 Do
      N := Div(N, P);
      Append(Facs, P);
    EndWhile;
    P := NextPPrime(P);
  EndWhile;
  If N > 1 And P^2 > N Then Append(Facs, N); N := 1; EndIf;
  Return [Facs, N]
EndDefine; -- SmoothFactor


EndPackage; -- ifactor
