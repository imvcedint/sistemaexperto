Package $relnotes

Define RelNotes()
  PrintLn "=======================================================================";
  PrintLn "==     Here is an excerpt from the release notes of version 4.7.5    ==";
  PrintLn "=======================================================================";
  PrintLn;
  PrintLn "==  A few new functions have been added  ==";
  PrintLn;
  PrintLn "--  MayerVietorisTreeN1 for monomial ideals (via CoCoAServer)";
  PrintLn "From the paper:  Anna M. Bigatti, Eduardo Saenz-de-Cabezon";
  PrintLn " *(n-1)-st Koszul homology and the structure of monomial ideals*";
  PrintLn;
  PrintLn "--  link to Frobby, a library For computations with monomial ideals";
  PrintLn "Type";
  PrintLn "  ? frobby";
  PrintLn "for more details";
  PrintLn;
  PrintLn "--  IsInRadical and MinDegInIdeal: functions for radical membership";
  PrintLn "--  Reg: the Castelnuovo-Mumford regularity";
  PrintLn "--  Ker: kernel of an algebra homomorphism";
  PrintLn "--  InverseSystem: the inverse system of an ideal of derivation";
  PrintLn;
  PrintLn "==  Changes to existing functions ==";
  PrintLn;
  PrintLn "Latex/LaTeX modified so that it returns a string.  This way the output";
  PrintLn "    can be printed on a file (see the manual for latex)";
  PrintLn;
  PrintLn "The  Source  command now issues an 'obsolescent' warning if used with";
  PrintLn "a package id; just specify the full path of the package as a string.";
  PrintLn;
  PrintLn "The function  $gb.Step(M)  is now obsolescent; replace it by the";
  PrintLn "equivalent  $gb.Steps(M,1)  to avoid the warning.";
  PrintLn;
  PrintLn "Printing of resolutions is now like this (we believe it's more readable)";
  PrintLn "  0 --> S(-4) --> S(-2) + S(-3)^2 --> S(-1)^2 + S(-2) --> S";
  PrintLn;
  PrintLn "==  Changes to the syntax  ==";
  PrintLn;
  PrintLn "The construct  Repeat...EndRepeat;  is now obsolescent.";
  PrintLn "It may easily be replaced by  While True Do ... EndWhile;";
  PrintLn;
  PrintLn "The construct  Cond...EndCond;  is now obsolescent.";
  PrintLn "You should use an  If...Then  construct instead.";
  PrintLn;
  PrintLn "The construct  Alias...In...EndAlias;  is now obsolescent.";
  PrintLn "You should use a global alias instead (or write out the package name";
  PrintLn "directly).";
  PrintLn;
  PrintLn "The trailing If construct is now obsolescent; just replace it by a";
  PrintLn "proper If...Then construct.";
  PrintLn;
  PrintLn "The keyword  ElsIf  is now obsolescent.  Just replace it by the";
  PrintLn "keyword  Elif.";
  PrintLn;
  PrintLn "The keyword  EoF  is now obsolescent; just comment out the rest of";
  PrintLn "the file.";
  PrintLn;
  PrintLn "The keyword  NewLine  is now obsolescent; just use several separate";
  PrintLn "PrintLn commands instead or the function NewLine()";
  PrintLn;
  PrintLn "The commands  Delete & Clear  are now obsolescent.";
  PrintLn "You can get a similar effect by assigning zero to the variables to be cleared.";
  PrintLn;
  PrintLn "The command  Destroy  is morally obsolescent as it will not be needed in CoCoA 5.";
  PrintLn;
  PrintLn "==  Several minor bugs have been fixed in the code and in the manual.  ==";
  PrintLn ""
EndDefine; -- RelNotes

EndPackage; -- $relnotes
