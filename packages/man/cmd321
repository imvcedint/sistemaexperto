
-- Automatically generated from the file CoCoAHelp.xml
-- RealRoots --

MEMORY.Doc.Syntax := ""  +NewLine() +
"RealRoots(F:POLY):LIST"  +NewLine() +
"RealRoots(F:POLY, Precision:RAT):LIST"  +NewLine() +
"RealRoots(F:POLY, Precision:RAT, Interval:[RAT, RAT]):LIST"  +NewLine() +
"";
MEMORY.Doc.Descr := "
This function computes isolating intervals for the real roots of a
univariate polyomial over QQ.  It returns the list of the real roots,
where a root is represented as a record containing either the exact root
(if the fields Inf and Sup are equal), or an open interval (Inf, Sup)
containing the root.  A third field (called CoeffList) has an obscure
meaning.

An optional second argument specifies the maximum width an isolating
interval may have.  An optional third argument specifies a closed
interval in which to search for roots.

The interval represented by a root record may be refined by using the
function \"RealRootRefine\".  The
function \"RealRootsApprox\" may be more useful to you: it
produces rational approximations to the real roots (but these cannot
later be refined).

//==========================  EXAMPLE  ==========================\\\\
  RealRoots(x^2-2);
[Record[CoeffList := [8, -16, 7], Inf := -4, Sup := 0],
Record[CoeffList := [8, 0, -1], Inf := 0, Sup := 4]]
-------------------------------
  RR := RealRoots((x^2-2)*(x-1), 10^(-5));
  FloatStr(RR[1].Inf);  -- left end of interval
-1.414213657*10^0
-------------------------------
  FloatStr(RR[1].Sup);  -- right end of interval
-1.414213419*10^0
-------------------------------
  RR := RealRoots(x^2-2, 10^(-20), [0, 2]);
  RR[1].Inf;                               -- incomprehensible
3339217363285192246361/2361183241434822606848
-------------------------------
  FloatStr(RR[1].Inf, 20);                 -- comprehensible
1.4142135623730950488*10^0
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    