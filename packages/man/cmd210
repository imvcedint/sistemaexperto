
-- Automatically generated from the file CoCoAHelp.xml
-- IsTree5 --

MEMORY.Doc.Syntax := ""  +NewLine() +
"IsTree5(L: LIST): [BOOL, LIST ]"  +NewLine() +
"IsTree5(L: LIST, NOOPT): [BOOL, LIST]"  +NewLine() +
"IsTree5(L: LIST, OPT): [BOOL, LIST]"  +NewLine() +
"IsTree5(L: LIST, CS_NOOPT): [BOOL, LIST]"  +NewLine() +
"IsTree5(L: LIST, CS_OPT): [BOOL, LIST]"  +NewLine() +
"";
MEMORY.Doc.Descr := "
This function is implemented in CoCoALib (requires an active \"CoCoAServer\").

This function tests whether the facet complex described by the
list L of square free power products is a tree, plus a list which:

- is empty if L is a tree

- contains three elements of a cycle of L if L is not a tree.

Four options \"NOOPT\",\"OPT\",\"CS_NOOPT\",\"CS_OPT\" are available as second
argument, specifying different algorithms; the default is \"CS_OPT\".

For a full description of the algorithms we refer to the paper by
M. Caboara, S. Faridi, and P. Selinger, \"Simplicial cycles and
the computation of simplicial trees\", Journal of Symbolic Computation,
vol.42/1-2, pp.77-88 (2006).

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[x,y,z,t];
  D := [xy, yz, zt, tx];
  IsTree5(D);
[False, [xy, xt, yt]]
-------------------------------
  IsTree5([xy, yz, zt]);
[True, [ ]]
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    