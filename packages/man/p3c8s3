
-- Automatically generated from the file CoCoAHelp.xml
-- Global Memory --
MEMORY.Doc.Descr := "
Starting with CoCoA 3.5, a *global* variable is one that is
accessible from within a user-defined function.  A global variable is
formed by using the prefix \"MEMORY\".  The special prefixes \"DEV\",
\"ENV\", \"ERR\", and \"PKG\" are shorthand for \"MEMORY.DEV\", \"MEMORY.ENV\",
etc.  Any global variable prefixed by \"MEMORY.ENV.R\" where \"R\" is the
identifier of a ring, becomes part of the ring-bound memory discussed
in the next section.  A list of the global variables which are not
ring-bound is provided by the function \"GlobalMemory\".

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[x,y,z];
  X := 5;  -- a variable called \"X\" in the working memory
  MEMORY.X := 7; -- a global variable
  X;
5
-------------------------------
  MEMORY.X;
7
-------------------------------
  Memory();  -- the working memory
[\"It\", \"X\"]
-------------------------------
  GlobalMemory(); -- the global memory
[\"DEV\", \"ENV\", \"ERR\", \"PKG\", \"X\"]
-------------------------------
  Define Test()
    PrintLn MEMORY.X;
    MEMORY.X := \"a new value\";
    PrintLn X;
  EndDefine;
  -- MEMORY.X is accessible from within a function
  -- X is not accessible within a function (thus we get an error)
  Test();
7

-------------------------------
ERROR: Undefined variable X
CONTEXT: PrintLn(X)
-------------------------------
  MEMORY.X;  -- the contents of the global memory can be changed from
             -- within a function
a new value
-------------------------------
  Fields(MEMORY.ENV);  -- a list of all defined rings
[\"QQ\", \"QQt\", \"R\", \"ZZ\"]
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";



    