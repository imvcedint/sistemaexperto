
-- Automatically generated from the file CoCoAHelp.xml
-- LinSol --

MEMORY.Doc.Syntax := ""  +NewLine() +
"LinSol(M:MAT, L:LIST):LIST"  +NewLine() +
""  +NewLine() +
"where M is an m x n matrix over ZZ or QQ, and L is a list of length m"  +NewLine() +
"with entries in ZZ or QQ (or a list of such lists)."  +NewLine() +
"";
MEMORY.Doc.Descr := "
This function finds a solution to the inhomogeneous system of
equations represented by M and L.
Specifically, it returns a list, X, of length n with entries in ZZ or QQ, 
such that \"M*Transposed(Mat(X)) = Transposed(Mat([L]))\",
if such X exists; otherwise, it returns the empty list.  Once a
solution is found, all solutions may be found using the function \"LinKer\".

NOTE: \"LinSol\" can solve several inhomogeneous systems at once.  If L
has the form [L_1,...,L_k] where each L_i is a list of length m with
entries in ZZ or QQ, then LinSol(M, L) returns a list [X_1,...,X_k] where
X_i is a solution to the system of linear equations represented by M
and L_i.

//==========================  EXAMPLE  ==========================\\\\
  M := Mat([[3,1,4],[1,5,9],[2,6,5]]);
  L := [123,456,789];
  LinSol(M, L);
[199/5, 742/5, -181/5]
-------------------------------
  M*Transposed(Mat([It]));
Mat([
  [123],
  [456],
  [789]
])
-------------------------------
  LinSol(M,[L,[1,2,3]]);
[[199/5, 742/5, -181/5], [4/15, 7/15, -1/15]]
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    