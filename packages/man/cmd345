
-- Automatically generated from the file CoCoAHelp.xml
-- Saturation --

MEMORY.Doc.Syntax := ""  +NewLine() +
"Saturation(I:IDEAL, J:IDEAL):IDEAL"  +NewLine() +
"";
MEMORY.Doc.Descr := "
This function returns the saturation of I with respect to J: the
ideal of polynomials F such that F*G is in I for all G in J^d
for some positive integer d.

The coefficient ring must be a field.

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[x,y,z];
  I := Ideal(x-z, y-2z);
  J := Ideal(x-2z, y-z);
  K := Intersection(I, J); -- ideal of two points in the
                           -- projective plane
  L := Intersection(K, Ideal(x,y,z)^3); -- add an irrelevant component
  Hilbert(R/L);
H(0) = 1
H(1) = 3
H(2) = 6
H(t) = 2   for t >= 3
-------------------------------
  Saturation(L, Ideal(x,y,z)) = K; -- saturating gets rid of the
                                   -- irrelevant component
True
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    