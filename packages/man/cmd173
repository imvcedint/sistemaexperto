
-- Automatically generated from the file CoCoAHelp.xml
-- In --

MEMORY.Doc.Syntax := ""  +NewLine() +
"[E:OBJECT | X In L:LIST And B:BOOL]:LIST"  +NewLine() +
"[X In L:LIST | B:BOOL]:LIST"  +NewLine() +
"[E:OBJECT | X In L]"  +NewLine() +
""  +NewLine() +
"where X is a variable identifier which may occur in B or E."  +NewLine() +
"";
MEMORY.Doc.Descr := "
In the first form, E is an arbitrary CoCoA expression and B is a
boolean expression, both of which are functions of the variable
X. Write E(X) for E and B(X) for B.  The first listed command then
returns the list of all E(X) such that X is in the list L and B(X)
evaluates to True.

//==========================  EXAMPLE  ==========================\\\\
  [[X^2, X^3] | X In [-2,-1,0,1,2] And X <> 0];
[[4, -8], [1, -1], [1, 1], [4, 8]]
-------------------------------
  [X In [1,2] >< [2,3,4] | X[1]+X[2]=4];
[[1, 3], [2, 2]]
-------------------------------
\\\\==========================  o=o=o=o  ===========================//

(Note: the >< operator is used to form Cartesian products; it is not
the same as the *not equal* operator, <>.)

The second form of the command is the same as the first with E = X.

//==========================  EXAMPLE  ==========================\\\\
  [X In [1,2,3] | X > 1];
[2, 3]
-------------------------------
\\\\==========================  o=o=o=o  ===========================//

The third form is the same as the first with B = True.

//==========================  EXAMPLE  ==========================\\\\
  [X^2 | X In [1,2,3]];
[1, 4, 9]
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    