
-- Automatically generated from the file CoCoAHelp.xml
-- HSaturation --

MEMORY.Doc.Syntax := ""  +NewLine() +
"HSaturation(I:IDEAL, J:IDEAL):IDEAL"  +NewLine() +
"";
MEMORY.Doc.Descr := "
This functions returns the saturation of I with respect to J: the
ideal of polynomials F such that F*G is in I for all G in J^d
for some positive integer d.

It calculates the saturation using a Hilbert-driven algorithm.
It differs from \"Saturation\" only when the
input is inhomogeneous, in which case, \"HSaturation\" may be faster.

The coefficient ring must be a field.

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[x,y];
  I := Ideal(x^4-x, yx-2x);
  Saturation(I, Ideal(x));
  HSaturation(I, Ideal(x));
\\\\==========================  o=o=o=o  ===========================//";




    