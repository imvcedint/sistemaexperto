
-- Automatically generated from the file CoCoAHelp.xml
-- PreprocessPts5 --

MEMORY.Doc.Syntax := ""  +NewLine() +
"PreprocessPts5(Pts: LIST, Toler: LIST): RECORD[Points: LIST, Weights: LIST]"  +NewLine() +
"PreprocessPts5(Pts: LIST, Toler: LIST, Grid): RECORD[Points: LIST, Weights: LIST]"  +NewLine() +
"PreprocessPts5(Pts: LIST, Toler: LIST, Subdiv): RECORD[Points: LIST, Weights: LIST]"  +NewLine() +
"PreprocessPts5(Pts: LIST, Toler: LIST, Aggr): RECORD[Points: LIST, Weights: LIST]"  +NewLine() +
"";
MEMORY.Doc.Descr := "
This function is implemented in CoCoALib (requires an active \"CoCoAServer\").

This function returns a record containing two fields \"Points\" which is a
list of *well-separated* points, and \"Weights\" which contains the
number of input points associated to each output point.
The first argument is a list of points in k-dimensional space, and the
second argument is list of k tolerances (one for each dimension).
The function detects groupings of close points, and chooses a single
representative for them (which lies within the given tolerance of each
original point); the result is the list of these representatives, and the
number of original points associated to each representative.

There is a third, optional argument: it should be one of the strings
\"Grid\", \"Subdiv\", \"Aggr\" and specifies which specific algorithm to
use.  If there are just two arguments, an automatic choice is made between
\"Subdiv\" and \"Aggr\"; the \"Subdiv\" method works well when the original
points are densely packed (so the result will be a small list), while the
\"Aggr\" method is better suited to situations where the original points are
less densely packed.  The \"Aggr\" and \"Subdiv\" methods regard the
tolerances as being slightly flexible.

For a full description of the algorithms we refer to the paper
J.Abbott, C.Fassino, L.Torrente
*Thinning Out Redundant Empirical Data* (arXiv:0702327).
//==========================  EXAMPLE  ==========================\\\\
  Pts := [[-1,0],[0,0],[1,0],[99,1],[99,0],[99,-1]];
  Toler := [3,3];
  PreprocessPts5(Pts, Toler);
Record[Points := [[99, 0], [0, 0]], Weights := [3, 3]]
-------------------------------
  PreprocessPts5(Pts, [0.8,0.8]);
Record[Points := [[-1/2, 0], [1, 0], [99, 1/2], [99, -1]], Weights := [2, 1, 2, 1]]
-------------------------------
  PreprocessPts5(Pts, [0.9,0.9], \"Aggr\"); -- exhibits tolerance flex
Record[Points := [[0, 0], [99, 0]], Weights := [3, 3]]
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    