
-- Automatically generated from the file CoCoAHelp.xml
-- BinExp --

MEMORY.Doc.Syntax := ""  +NewLine() +
"BinExp(N:INT, K:INT):TAGGED($binrepr.BinExp)"  +NewLine() +
"BinExp(N:INT, K:INT, Up:INT, Down:INT):INT"  +NewLine() +
""  +NewLine() +
"where N and K are positive integers, and Up and Down are integers."  +NewLine() +
"";
MEMORY.Doc.Descr := "
This function computes the K-binomial expansion of N, i.e., the
unique expression

  N = Bin(N(K),K) + Bin(N(K-1),K-1) + ... + Bin(N(I),I)

where N(K) > ... > N(I) >= 1, for some I.  The value returned is
tagged for pretty printing.

It can also compute the sum of the binomial coefficients
appearing in the K-binomial expansion of N after replacing each
summand Bin(N(J), J) by Bin(N(J)+Up, J+Down).  It is useful in
generalizations of Macaulay's theorem characterizing Hilbert
functions.

//==========================  EXAMPLE  ==========================\\\\
  BE := BinExp(13,4);
  BE;
Bin(5,4) + Bin(4,3) + Bin(3,2) + Bin(1,1)
-------------------------------
  BinExp(13,4,1,1);
16
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    