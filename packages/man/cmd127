
-- Automatically generated from the file CoCoAHelp.xml
-- GenericPoints --

MEMORY.Doc.Syntax := ""  +NewLine() +
"GenericPoints(NumPoints:INT):LIST"  +NewLine() +
"GenericPoints(NumPoints:INT, RandomRange:INT):LIST"  +NewLine() +
"";
MEMORY.Doc.Descr := "\"GenericPoints\" returns a list of NumPoints generic projective points
with integer coordinates; it is not guaranteed that these points are
distinct.  RandomRange specifies the largest value any coordinate may
take.  If the second argument is omitted, the largest value possible
is 100 (or P-1 where P is the characteristic of the coefficient ring).

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[x,y];  GenericPoints(7);
[[1, 0], [0, 1], [1, 1], [12, 59], [6, 63], [12, 80], [17, 63]]
-------------------------------
  GenericPoints(7,500);
[[1, 0], [0, 1], [1, 1], [220, 162], [206, 452], [98, 106], [403, 449]]
-------------------------------
  Use R ::= ZZ/(5)[x,y,z];
  GenericPoints(7);
[[1, 0, 0], [0, 1, 0], [0, 0, 1], [1, 1, 1], [2, 1, 1], [2, 2, 4], [3, 1, 3]]
-------------------------------
  GenericPoints(7,500);
[[1, 0, 0], [0, 1, 0], [0, 0, 1], [1, 1, 1], [1, 4, 2], [1, 3, 2], [2, 3, 3]]
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    