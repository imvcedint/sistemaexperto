
-- Automatically generated from the file CoCoAHelp.xml
-- Eliminating Variables --
MEMORY.Doc.Descr := "
The Cartesian equations of the space curve parametrized by
  t --> 
(t^31+t^6, t^8, t^10)
can be found by eliminating the indeterminate t in the ideal

(t^31+t^6-x, t^8-y, t^10-z)
//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[t,x,y,z];
  Set Indentation;
  Elim(t, Ideal(t^31+t^6-x, t^8-y, t^10-z));
Ideal(
  y^5 - z^4,
  -y^4z^5 + y^4 - 2xy^2z + x^2z^2,
  -z^8 - 2xy^3 + x^2yz + z^3,
  2xy^4z^4 + yz^7 + 3x^2y^2 - 2x^3z - yz^2,
  -y^2z^6 - 1/2xz^7 + 1/2x^3y + y^2z - 3/2xz^2,
  -1/3x^2y^4z^3 - y^3z^5 - 2/3xyz^6 + 1/3x^4 + y^3 - 4/3xyz)
-------------------------------
\\\\==========================  o=o=o=o  ===========================//

With the command \"Elim\", CoCoA automatically switches to a
term-ordering suitable for eliminating the variable t, then changes
back to the declared term-ordering (in this case the default
term-ordering, DegRevLex).

One may see the entire Groebner basis for our ideal with respect to
the elimination term-ordering for t as follows:

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[t,x,y,z], Elim(t);
  Set Indentation;
  GBasis(Ideal(t^31+t^6-x, t^8-y, t^10-z));
[
  -t^2y + z,
  y^5 - z^4,
  -t^6 - tz^3 + x,
  -tz^4 - y^2 + xz,
  -ty^2 + txz - y^4z,
  -y^4z^5 + y^4 - 2xy^2z + x^2z^2,
  -z^8 - 2xy^3 + x^2yz + z^3,
  2xy^4z^4 + yz^7 + 3x^2y^2 - 2x^3z - yz^2,
  tx^2y - tz^2 - y^2z^3 - xz^4,
  2txyz^3 - z^7 - x^2y + z^2,
  -y^2z^6 - 1/2xz^7 + 1/2x^3y + y^2z - 3/2xz^2,
  t^2x - tx^2z^2 + xy^4z^2 + yz^5 - y,
  t^2z + 2txz^3 - y^4z^3 - x^2,
  -3tx^2z^3 + 2xy^4z^3 + yz^6 + x^3 - yz,
  -1/3x^2y^4z^3 - y^3z^5 - 2/3xyz^6 + 1/3x^4 + y^3 - 4/3xyz,
  1/3tx^3 - 1/3tyz - 1/3x^2y^4 - 1/3y^3z^2 - 1/3xyz^3]
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";



    