-- $Id: stat.ts,v 1.9 2009/06/04 15:55:23 bigatti Exp $
-- Test suite for stat.pkg

Alias TSL := $ts/ts_lib;
TSL.Initialize();

/*
Source CocoaPackagePath() + "/contrib/stat.ts";
TSL.RunTests();  // was  TSL.Do();
*/

-------------------------------
-- TEST 01 :

Test := Record[Id := "stat_01", Descr := "StatEx_1"];

Test.Input :="
Max_Stat_R1 ::= QQ[x,y,z,t], DegRevLex;
Using Max_Stat_R1 Do
  D:=[1,1,1];
  Starts:=[0,0,0];
  Design:=Stat.FD([x,y,z],D);-- Making the terms associated to the design.
  Cut:=[xy,xz,yz];
  O:=Stat.FFD(Design,Cut);
  ParNo:=Stat.MakeParNo(Cut,O);-- computing the number of necessary parameters
  Equations:=Stat.FDEQ([x,y,z],D,Starts);-- Generates the equations of I(Design)
  Stat.BuildParamRing(\"Max_Stat_R2\",Indets(),\"a\",ParNo);
  Using Max_Stat_R2 Do
    Map_R_To_W:=RMap(First(Indets(),NumIndets(Max_Stat_R1)));
    Design:=Image(Design,Map_R_To_W);
    Cut:=Image(Cut,Map_R_To_W);
    Equations:=Image(Equations,Map_R_To_W);
    O:=Image(O,Map_R_To_W);
    GenericEQ:=Stat.Fill(Cut,O);
    Rel:=Stat.NewBuch(Equations,GenericEQ);
    ParameterSol:=Stat.Solve(Rel);
    Pts:=Stat.Points(Concat(Equations,GenericEQ),ParameterSol);
    Pt:=Stat.Point(Concat(Equations,GenericEQ),ParameterSol[1]);
    [[0, 0, 0], [0, 1, 0], [1, 1, 0], [1, 1, 1]] IsIn Pts;
    a[10]a[11] + a[3]a[12] - a[9] IsIn Rel;
--    Pts[34]=[[0, 0, 0], [0, 1, 0], [1, 1, 0], [1, 1, 1]];
--    Rel[22]=a[10]a[11] + a[3]a[12] - a[9];
  EndUsing;
EndUsing;
";
Test.ExpectedOutput :="Ring Max_Stat_R2 built........
Buch done, computing relations
Relations computed
..........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
TrueTrue
-------------------------------
";

TSL.RegisterTest(Test);

