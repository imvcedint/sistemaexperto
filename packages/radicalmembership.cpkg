Package $radicalmembership -- Radical memebrship functions

Alias HP := $hp,
      GB := $gb,
      SP := $sppoly;

Define About()
  PrintLn "    Author:  A.M.Bigatti";
  PrintLn "    Date:    9 Oct 2009";
EndDefine; -- About

--------------------------------------------------------------
Define IsInRadical(X, I); EndDefine; -- X IDEAL or POLY
Define MinPowerInIdeal(F, Var I); EndDefine; -- F^P In I, only For homog
--------------------------------------------------------------

Define IsInRadical(X, I)
  If Not Type(I) = IDEAL Then
    Error("IsInRadical: second arg must be IDEAL");
  EndIf;
  If Not Type(X) IsIn [POLY, IDEAL] Then
    Error("IsInRadical: first arg must be POLY or IDEAL");
  EndIf;
  If IsHomog(X) And IsHomog(Gens(I)) Then
    If Type(X) = POLY  Then Return $.IsInRadicalPolyH(X,I);  EndIf;
    If Type(X) = IDEAL Then Return $.IsInRadicalIdealH(X,I); EndIf;
  Else
    If Type(X) = POLY  Then Return $.IsInRadicalPoly(X,I);  EndIf;
    If Type(X) = IDEAL Then Return $.IsInRadicalIdeal(X,I); EndIf;
  EndIf;
EndDefine; -- IsInRadical

--------------------------------------------------------------

Define IsInRadicalIdeal(J, I)
  Foreach F In Gens(J) Do
    If Not $.IsInRadicalPoly(F, I) Then Return False EndIf;
  EndForeach;
  Return True;
EndDefine; -- IsInRadicalIdeal

Define IsInRadicalPoly(F, I)
  Using Var(RingEnv(I)) Do
    IsInRadicalPolyRing ::= CoeffRing[x[1..NumIndets()],t];
  EndUsing;
  Using IsInRadicalPolyRing Do
    Phi := RMap(AllIndetsCalled("x"));
    J := Ideal([Image(G,Phi) | G In Gens(I)]) + Ideal(Image(F,Phi)*t-1);
    Return 1 IsIn Elim([t], J);
  EndUsing;
EndDefine; -- IsInRadicalPoly


Define IsInRadicalIdealH(J, I)
  Foreach F In Gens(J) Do
    If Not $.IsInRadicalPolyH(F, I) Then Return False EndIf;
  EndForeach;
  Return True;
EndDefine; -- IsInRadicalIdeal


Define IsInRadicalPolyH(F, I)
  Return $.MinPowerInIdeal(F, I) <> -1;
EndDefine; -- IsInRadicalPolyH


Define MinPowerInIdeal(F, Var I)
  If IsHomog(F) And IsHomog(Gens(I)) Then
    Using Var(RingEnv(I)) Do
      IsInRadicalPolyHRing ::= CoeffRing[x[1..NumIndets()],t];
    EndUsing;
    Using IsInRadicalPolyHRing Do
      Phi := RMap(AllIndetsCalled("x"));
      D := Deg(F);
      J := Ideal([Image(G,Phi) | G In Gens(I)])
      + Ideal(Image(F,Phi)-t^D);
      L := [ F In ReducedGBasis(J) | Len(F)=1 And Deg(F)=Deg(F,t)];
      If L = [] Then Return -1; EndIf;
      F := L[1]; -- at most one element in L
      Return Deg(F)/D;
    EndUsing;
  Else
    If Not $.IsInRadicalPoly(F, I)
      Then Return -1;
    Else      
      GB := GBasis(I);
      D := 1;
      FD := F;
      While Not FD IsIn I Do
	FD := FD*F;
	D := D+1;
      EndWhile;
      Return D;
    EndIf;
  EndIf;
EndDefine; -- IsInRadicalPoly
--------------------------------------------------------------


EndPackage; -- Package $radicalmembership
