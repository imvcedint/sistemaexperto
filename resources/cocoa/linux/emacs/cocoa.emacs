;;==============================================================>
;;    CoCoA-Emacs CUSTOMIZATION: 
;;==============================================================>
;; probably you do not need to change any of these:

;; PATH of the CoCoA directory:
(setq cocoa-install-dir "/usr/local/cocoa-4.7/")     ;Unix/Linux
;(setq cocoa-install-dir "/Applications/CoCoA-4.7/")  ;Mac
;(setq cocoa-install-dir "C:\\cocoa-4.7\\")           ;Win
;(setq cocoa-install-dir "~/4.7/UI/")                 ;wip

;; PATH for the "cocoa.el" file
(setq load-path (cons (concat cocoa-install-dir "emacs/") load-path))
;(setq load-path (cons (concat cocoa-install-dir "emacs\\") load-path));Win

;; PATH for the "cocoa" start-up script file
;; this usually works:
(setq cocoa-executable (concat cocoa-install-dir "cocoa"))
(setq cocoaserver-executable (concat cocoa-install-dir "CoCoAServer"))
;(setq cocoa-executable (concat cocoa-install-dir "cocoa.bat"));Win
;(setq cocoaserver-executable (concat cocoa-install-dir "CoCoAServer.exe"));Win
;; if you moved the binaries into another directory, set the complete path:
;(setq cocoa-executable "/usr/local/bin/cocoa")
;(setq cocoaserver-executable "/Users/bigatti/CoCoALib-0.92/src/server/CoCoAServer")

;; PATH for the file containing the function names (to use dabbrev)
(setq cocoa-wordlist-default-file (concat cocoa-install-dir "wordlist.txt"))

;; PATH of the directory where you usually save your CoCoA files:
(setq cocoa-dir "~/CoCoA/")

;;==============================================================>

;; ------------------------------------------------------------
;; AUTO-LOAD cocoa.el for running CoCoA and cocoa-mode for files
;; with CoCoA extensions
(autoload 'cocoa       "cocoa.el" "CoCoA running mode" 't)
(autoload 'cocoaserver "cocoa.el" "CoCoA running mode" 't)
(autoload 'cocoa-mode  "cocoa.el" "CoCoA editing mode" 't)

(setq auto-mode-alist  (append auto-mode-alist '(
  ("\\.\\(coc\\|cocoa\\|cocoarc\\|pkg\\|cpkg\\|ts\\)\\'" . cocoa-mode) )))

;; ------------------------------------------------------------
;; AUTO-LOAD wordlist when running CoCoA or using cocoa-mode
(setq cocoa-auto-load-wordlist 't)

;; ------------------------------------------------------------
;; AUTOMATIC CAPITALIZATION of CoCoA keywords:
(add-hook 'cocoa-mode-hook '(lambda () (abbrev-mode 't)))

;; ------------------------------------------------------------
;; ------ OTHER USEFUL SETTINGS ------
;; ------------------------------------------------------------

;; ------------------------------------------------------------
;; AUTO-LOAD CUA-mode (COPY & PASTE with C-c C-x C-v C-z keys)
;; This is inactive by default! 
;; To activate it uncomment these two lines (remove ";") 
;(require 'cua)
;(CUA-mode 't)

;; ------------------------------------------------------------
;; TAB FOR COMPLETION
;; *customize* with menu "CoCoA --> Preferences"
;;     [ same as "<ESC>-x  customize-group <RET> cocoa"
;;       sets (defvar cocoa-tab-is-completion nil)  ]
;; *or* uncomment the following line
;(defvar cocoa-tab-is-completion 't)

;; ------------------------------------------------------------
;; OPEN RECENT: 
(require 'recentf)
(recentf-mode 1)

;; ------------------------------------------------------------
;; HIGHLIGHT REGION BETWEEN PARENTHESIS
(require 'paren)
(show-paren-mode 't)
(setq show-paren-style 'expression)

;; ------------------------------------------------------------
;; WORD COMPLETION: 
;; dynamic abbrev expansion (M-/) replace also CASE PATTERN 
(setq dabbrev-case-replace nil)

;; ------------------------------------------------------------
;; COLOURED SYNTAX (font-lock-mode)
(require 'font-lock) 

(if window-system 
    (if (string-match "XEmacs" (emacs-version))
	(font-lock-mode 't)
      (global-font-lock-mode 't)      )
  nil)

;; but if you want font-lock-mode ONLY in cocoa-mode 
;; or if you use XEmacs, you might have to choose these instead:
;(if window-system (font-lock-mode 't) nil)
;(add-hook 'cocoa-mode-hook '(lambda () (font-lock-mode 't)))

;; ------------------------------------------------------------
;; ENABLE MOUSE WHEEL
(setq mouse-wheel-mode 't)

;; ------------------------------------------------------------
;; PREVENT EXTRANEOUS TABS
(setq indent-tabs-mode 0)

;; ------------------------------------------------------------
;; DISCARD ALL ctrl-m CHARACTERS FROM SHELL OUTPUT
(add-hook 'comint-output-filter-functions 'comint-strip-ctrl-m)

;; ------------------------------------------------------------
;; MY PREFERRED SET OF COLOURS ;-)
(if window-system 
    (progn
      (set-face-foreground font-lock-comment-face       "red3" )
      (set-face-foreground font-lock-keyword-face       "SteelBlue4" )
      (set-face-foreground font-lock-string-face        "green4" )
      (set-face-foreground font-lock-constant-face      "Orange3" )
      (set-face-foreground font-lock-function-name-face "Blue" )
      (set-face-foreground font-lock-reference-face     "Sienna" )
      (set-face-foreground font-lock-type-face          "Orchid3" )
      )
  nil
  )

;; ------------------------------------------------------------
