Package $float  -- convert rationals to floating-point decimal strings

-----------------------------------------------------------------------------
--  Function to convert a rational to a decimal string of the form
--  -d.ddddd*10^-ddd; the optional second argument specifies how
-- many digits to print in the mantissa (min 2 digits).  There is no limit
-- on the exponent, but the code gets slow for very large or very
-- small inputs.

Define FloatStr(...)
  ErrMesg := "FloatStr: arg must be rational, with optional second integer arg";
  BadArgs := FALSE;
  If Len(ARGV) > 2 Or Len(ARGV) < 1 Then Error(ErrMesg); EndIf;
  Catch X := Cast(ARGV[1], RAT) In Err EndCatch;
  If Type(Err) = ERROR Or
     (Len(ARGV) = 2 And Type(ARGV[2]) <> INT) Then
    Error(ErrMesg);
  EndIf;
  If Len(ARGV) = 1 Then SigFig := 10;
  Else SigFig := ARGV[2]; If SigFig < 2 Then SigFig := 2; EndIf;
  EndIf;
  Sign := "";
  If X < 0 Then Sign := "-"; X := -X; EndIf;
  Rec := $float.MantissaAndExponent(X, SigFig);
  If Rec.Mantissa <> 0 Then Mant := Sprint(Rec.Mantissa);
  Else Mant := Sum(NewList(SigFig, "0")); EndIf;
  If Rec.Exponent < 0 Then Expo := "*10^("+Sprint(Rec.Exponent)+")";
  Else Expo := "*10^"+Sprint(Rec.Exponent); EndIf;
  Return Sign+Mant[1]+"."+Sum([Mant[I] | I In 2..Len(Mant)])+Expo;
EndDefine;


-- This function returns a Record[Mantissa, Exponent].
-- If X = 0 then both components in the record are set to 0.
-- Otherwise Mantissa is an integer with exactly SigFig decimal digits,
-- and Exponent is that integer such that 1 <= Abs(X)*10^Exponent < 10

Define MantissaAndExponent(X, SigFig)
  Catch X := Cast(X, RAT) In Err EndCatch;
  If Type(Err) = ERROR Or Type(SigFig) <> INT Then
    Error("MantissaAndExponent: first arg must be a rational, and second an integer");
  EndIf;
  If SigFig < 1 Then SigFig := 1; EndIf;
  If X = 0 Then Return Record[Mantissa:=0, Exponent:=0]; EndIf;
  Sign := 1;
  If X < 0 Then Sign := -1; X := -X; EndIf;
  N := Num(X);
  D := Den(X); -- we are guaranteed that D and N are coprime
  If N >= D Then
    Exponent := $float.ILogBase(Div(N, D), 10);
    D := D*10^Exponent;
  Else
    Exponent := - $float.ILogBase(Div(D, N), 10);
    Power10 := 10^(-Exponent);
    If N = 1 And D = Power10 Then -- X = 1/10^n is a special case
      D := 1;
    Else
      Exponent := Exponent - 1;
      N := 10*N*Power10;
    EndIf;
  EndIf;
  Scale := 10^(SigFig-1);
  -- the next line uses wastefully high precision in most cases...
  Mantissa := Div(Scale*N+Div(D, 2), D);
  If Mantissa >= 10*Scale Then -- Mantissa was rounded up to next "decade"
    Exponent := Exponent+1;
    Mantissa := Div(Mantissa, 10);
  EndIf;
  Mantissa := Sign*Mantissa;
  Return Record[Mantissa:=Mantissa, Exponent:=Exponent];
EndDefine;


Define ILogBase(N, Base)
  If Not(Type(N) IsIn [INT, RAT]) Or Type(Base) <> INT Then
    Error("ILogBase: first arg must be integer or rational, second arg must be integer");
  EndIf;
  If N = 0 Then Error("ILogBase: cannot compute logarithm of 0"); EndIf;
  If Base < 0 Then Base := -Base; EndIf;
  If Base < 2 Then Error("ILogBase: base must be at least 2"); EndIf;
  If N < 0 Then N := -N; EndIf;
  If Type(N) = RAT Then
    If Num(N) >= Den(N) Then
      N := Div(Num(N), Den(N));
    Else
      Tmp := ILogBase(1/N, Base);
      If Num(N) = 1 And Den(N) <> Base^Tmp Then Tmp := Tmp+1; EndIf;
      Return -Tmp;
    EndIf;
  EndIf;
  CurrPower := Base;
  Powers := [];
  Exponent := 1;
  While N >= CurrPower Do
    Append(Powers, CurrPower);
    CurrPower := CurrPower^2;
    Exponent := 2*Exponent;
  EndWhile;
  Reverse(Powers);
  LogN := 0;
  Foreach Power In Powers Do
    Exponent := Exponent/2;
    If N >= Power Then
      N := Div(N, Power);
      LogN := LogN + Exponent;
    EndIf;
  EndForeach;
  Return LogN;
EndDefine; -- ILogBase


-----------------------------------------------------------------------------

--  Function to convert a rational to a decimal string of the form
--  ddddd.ddd; the optional second argument specifies how many decimal
--  digits to print (default 3 digits).  There is no limit on the
--  precision of the integer part.

Define DecimalStr(...)
  ErrMesg := "DecimalStr: arg must be rational, with optional second integer arg";
  BadArgs := FALSE;
  If Len(ARGV) > 2 Or Len(ARGV) < 1 Then Error(ErrMesg); EndIf;
  Catch X := Cast(ARGV[1], RAT) In Err EndCatch;
  If Type(Err) = ERROR Or
     (Len(ARGV) = 2 And Type(ARGV[2]) <> INT) Then
    Error(ErrMesg);
  EndIf;
  If Len(ARGV) = 1 Then SigFig := 3;  Else SigFig := ARGV[2];   EndIf;
  Rec := $float.IntegerAndDecimal(X, SigFig);
  If Rec.Sign = -1 Then
    Return "-" + Sprint(Rec.Integer) + Rec.Decimal;
  EndIf;
  Return Sprint(Rec.Integer) + Rec.Decimal;
EndDefine; -- DecimalStr


-- This function returns a Record[Integer, Decimal].
-- If X = 0 then both components in the record are set to 0.
-- Otherwise Integer is the integer part given as an integer,
-- and Decimal is the decimal part given as a string which
--  . If X = Integer : is the empty string
--  . if X = Integer+Decimal : contains the dot and <=SigFig digits
--  . contains the dot and =SigFig digits otherwise

Define IntegerAndDecimal(X, SigFig)
  Catch X := Cast(X, RAT) In Err EndCatch;
  If Type(Err) = ERROR Or Type(SigFig) <> INT Then
    Error("IntegerAndDecimal: first arg must be a rational, and second an integer");
  EndIf;
  Rec := Record[Sign:=0, Integer:=0, Decimal:=""];
  If X = 0 Then Return Rec; EndIf;
  If X > 0 Then Rec.Sign := 1; Else Rec.Sign := -1; X := -X; EndIf;
  N := Num(X);
  D := Den(X);
  Rec.Integer := Div(N,D);
  N := Mod(N, D);
  If N = 0 Or SigFig <0 Then Return Rec; EndIf;
  Rec.Decimal := ".";
  For Digits := 1 To SigFig Do
    N := N*10;
    Rec.Decimal := Rec.Decimal + Sprint(Div(N, D));
    N := Mod(N, D);
    If N = 0 Then Return Rec; EndIf;
  EndFor;
  Return Rec;
EndDefine; -- IntegerAndDecimal

-----------------------------------------------------------------------------

-- This is a very simplistic implementation!
Define Atan(N, Digits) -- computes 10^Digits * atan(1/N)
  Ans := 0;
  Summand := Div(10^Digits, N);
  I := 1;
  Sign := 1;
  While Summand > 0 Do
    Ans := Ans + Div(Summand, I)*Sign;
    Sign := -Sign;
    I := I+2;
    Summand := Div(Summand, N*N);
  EndWhile;
  Return Ans;
EndDefine; -- Atan

Define Pi(N)
  -- Better to use 4*(2*Atan(3/79) + 3*Atan(1/7))
  -- Or even 4*(5*atan(3/79) + 7*atan(29/278))
  Numer := 4*(4*Atan(5, N+5) - Atan(239, N+5));
  Return Numer/10^(N+5);
EndDefine; -- Pi


EndPackage; -- end of package float.
