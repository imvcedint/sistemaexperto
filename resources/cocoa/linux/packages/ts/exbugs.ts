-- $Id: exbugs.ts,v 1.19 2009/09/25 09:20:29 bigatti Exp $
-- Test Suite: ex bugs (potentially reentrant)

Alias TSL := $ts/ts_lib;
TSL.Initialize();

-------------------------------
-- EXBUG 01 : GBasis(Ideal(x,0))
Test := Record[Id := "exbugs_01", Descr := "GBasis(Ideal(x,0))"];

Test.Input := "
Use QQ[x];
GBasis(Ideal(x,0));
";
Test.ExpectedOutput :=
"[x]
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 02 : MAT^(-1)

Test := Record[Id := "exbugs_02", Descr := "MAT^(-1)"];

Test.Input := "
Use QQ[x];
N := Mat([[1,2],[3,4]]);
N^(-1);
";
Test.ExpectedOutput :=
"Mat([
  [-2, 1],
  [3/2, -1/2]
])
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 03 : L := 1..5; Vector(x,y)/L[1];

Test := Record[Id := "exbugs_03", Descr := "L := 1..5; Vector(x,y)/L[1];"];

Test.Input := "
Use QQ[x,y];
L := 1..5;
Vector(x,y)/L[1];
";
Test.ExpectedOutput :=
"Vector(x, y)
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 04 : NF(x,[x])

Test := Record[Id := "exbugs_04", Descr := "NF(x,[x])"];

Test.Input := "
Use QQ[x];
NR(x,[x]);
";

Test.ExpectedOutput :=
"0
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 05 : Return

Test := Record[Id := "exbugs_05", Descr := "Return"];

Test.Input := "
Define Larger(A,B)
  If A > B Then Return A; EndIf;
  Return B;
EndDefine;
Larger(1,2);
Larger(2,1);

Define Larger2(A,B)
  If A > B Then Return; EndIf;
  PrintLn \"Second is larger\";
EndDefine;
Larger2(1,2);
Larger2(2,1);
";
Test.ExpectedOutput :=
"2
-------------------------------
2
-------------------------------
Second is larger

-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 06 : Image of constant Poly

Test := Record[Id := "exbugs_06", Descr := "Image of constant Poly"];

Test.Input := "
Use T ::= QQ[t,x,y,z];
Subst(x-x+1,x,1);
";
Test.ExpectedOutput :=
"1
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 07 : HDriven does not work

Test := Record[Id := "exbugs_07", Descr := "HDriven does not work"];

Test.Input := "
Use S ::= ZZ/(32003)[g,a,b,c,d],Lex;
I := Ideal(a + b + c + d, a*b+b*c+c*d+d*a,a*b*c+b*c*d+c*d*a+d*a*b,a*b*c*d-g^4);
PS := Poincare(S/I);
J := Ideal(a + b + c + d, a*b+b*c+c*d+d*a,a*b*c+b*c*d+c*d*a+d*a*b,a*b*c*d-g^4);
J.PSeries := PS;
E := GBasis(J);
If MEMORY.PKG.GB.LastTotSteps<> 7 Then
  Print \"HDRIVEN ERROR\"
Else
 Print \"HDRIVEN OK\"
EndIf;
";

Test.ExpectedOutput :=
"HDRIVEN OK
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 08 : HDriven Elim does not work

Test := Record[Id := "exbugs_08", Descr := "HDriven Elim does not work"];

Test.Input := "
Use S ::= ZZ/(2)[g,a,b,c,d,e,f],Lex;
I := Ideal(a + b,a*b + b*c,a*b*c + b*c*d,a*b*c*d + b*c*d*e,
a*b*c*d*e + b*c*d*e*f + c*d*e*f*a + d*e*f*a*b + e*f*a*b*c + f*a*b*c*d ,
g^6 + a*b*c*d*e*f);
P := Poincare(S/I);
I.PSeries := P;
E := Elim([a,b,c],I);
If MEMORY.PKG.GB.LastTotSteps<> 21 Then
  Print \"Elim HDRIVEN ERROR\"
Else
 Print \"Elim HDRIVEN OK\"
EndIf;
";

Test.ExpectedOutput :=
"Elim HDRIVEN OK
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 09 : Resolution after Equality Test

Test := Record[Id := "exbugs_09", Descr := "Resolution after Equality Test"];

Test.Input := "
Use S ::= QQ[x];
I := Ideal(x);
J := Ideal(x);
I=J;
Res(I);
";

Test.ExpectedOutput :=
"True
-------------------------------
0 --> S(-1)
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 10 : GMP 2.0.2 bug on add and sub
--
-- if it does not work then try to apply John's patch:
-- copy files patch_add.c to mpq/add.c and patch_sub.c to mpq/sub.c

Test := Record[Id := "exbugs_10", Descr := "GMP 2.0.2 bug on add and sub"];

Test.Input := "
Use QQ[t,x,y,z];
F := Product(Indets());
G := (F^10-1)/(F-1);
Subst(G,[[x,1/2],[y,1/3],[z,1/5],[t,1/7]]);
";

Test.ExpectedOutput :=
"798080429578995215311/794280046581000000000
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 11 : LC(1)

Test := Record[Id := "exbugs_11", Descr := "LC must return a coeff of the current ring"];

Test.Input := "
S1 ::= QQ[x];
S2 ::= ZZ/(5)[x];
S1 :: Type(LC(1));
S2 :: Type(LC(1));
";

Test.ExpectedOutput :=
"RAT
-------------------------------
ZMOD
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 12 : Res after Syz of Ideal

Test := Record[Id := "exbugs_12", Descr := "Res after Syz of Ideal"];

Test.Input := "
Use S ::= QQ[x,y,z];
I := Ideal(x^2,y^2,x*y+y*z);
X := SyzOfGens(I);
Res(I);
";

Test.ExpectedOutput :=
"0 --> S(-5) --> S(-3) + S(-4)^2 --> S(-2)^3
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 13 : Typo in the definition of a ring

Test := Record[Id := "exbugs_13", Descr := "Typo in the definition of a ring"];

Test.Input := "
Use S ::= QQ[a,b,c];
S :: QQ[a,b,c];
Use R;
//obsolescent Destroy S;
S :: QQ[n,b,c];
";

Test.ExpectedOutput :=
"ERROR: parse error in line 3 of device 
-------------------------------
[a, b, c]
-------------------------------
ERROR: parse error in line 6 of device 
-------------------------------
ERROR: Undefined indeterminate n
CONTEXT: [n, b, c]
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 14 : SyzMinGens after Syz and viceversa

Test := Record[Id := "exbugs_14", Descr := "SyzMinGens after Syz and viceversa"];

Test.Input := "
Use S ::= QQ[x,y];
I := Ideal(x,y,x+y);
SyzOfGens(I);
SyzMinGens(I);

Use S ::= QQ[x,y];
I := Ideal(x,y,x+y);
SyzMinGens(I);
SyzOfGens(I);
";

Test.ExpectedOutput :=
"Module([[1, 1, -1], [y, -x, 0]])
-------------------------------
ERROR: Unknown operator SyzMinGens
CONTEXT: SyzMinGens(I)
-------------------------------
ERROR: Unknown operator SyzMinGens
CONTEXT: SyzMinGens(I)
-------------------------------
Module([[1, 1, -1], [y, -x, 0]])
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 15 : CAST from POLY or INT to RATFUN

Test := Record[Id := "exbugs_15", Descr := "CAST from POLY or INT to RATFUN"];

Test.Input := "
Use S ::= QQ[x];
Cast(3,RATFUN);
Cast(x,RATFUN);
";

Test.ExpectedOutput :=
"3
-------------------------------
x
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 16 : Indeterminate doubly defined

Test := Record[Id := "exbugs_16", Descr := "Indeterminate doubly defined"];

Test.Input := "
Use S ::= QQ[x,y,x,z];
Use S ::= QQ[x[1..5],x[2..8]];
";

Test.ExpectedOutput :=
"ERROR: Indeterminate already defined x
CONTEXT: Use S ::= QQ[x,y,x,z]
-------------------------------
ERROR: Indeterminate already defined x
CONTEXT: Use S ::= QQ[x[1..5],x[2..8]]
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 17 : Bad ring definitions

Test := Record[Id := "exbugs_17", Descr := "Bad ring definitions"];

Test.Input := "
Use S ::= QQ[x[3..1]];
Use S ::= ZZ/(100003)[x];
";

Test.ExpectedOutput :=
"ERROR: Bad range 3..1
CONTEXT: Use S ::= QQ[x[3..1]]
-------------------------------
ERROR: Bad characteristic 100003
CONTEXT: Use S ::= ZZ/(100003)[x]
-------------------------------
";
TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 18 : Miscellanea

Test := Record[Id := "exbugs_18", Descr := "Miscellanea"];

Test.Input := "
Cond(III=JJJ,1,2);
Use S ::= QQ[x];
x>0;
Abs(x);
Bin(2,x);
IsEven(x);
IsEven(3/5);
IsOdd(x);
M := Mat([[1]]);
Minors(2,M);
Use S ::= ZZ/(101)[x];
I := Ideal(x);
HP.PSeries(1,0);
Use ZZ/(2^31-1);
Use S ::= QQ[x,y];
Set FullRed;
NF(x+y,Ideal(y));
Unset FullRed;
NF(x+y,Ideal(y));

Use QQ[a,b,c,d,e,f];
B := 230498234098234098234098234098234;
M := Mat([
   [ 0, a/b, b*B, c ],
   [ -a/b, 0, d, e],
   [ -b*B, -d, 0, 1/2],
   [ -c, -e, -1/2, 0]
]);
Pfaffian(M);
NewMat(2);

Use S ::= QQ[x,y,z];
I := Ideal(x^2-y, x^3-z);
LT(I);
I.GBasis;
";

Test.ExpectedOutput :=
"ERROR: Undefined variable III
CONTEXT: Cond(III = JJJ,1,2)
-------------------------------
ERROR: Cannot compare (expected terms)
CONTEXT: x > 0
-------------------------------
ERROR: Cannot cast to INT (non constant poly)
CONTEXT: Y := Cast(X,RAT)
-------------------------------
ERROR: Bin: Second argument must be an integer
CONTEXT: Error(\"Bin: Second argument must be an integer\")
-------------------------------
ERROR: Bad parameters x
CONTEXT: Error(ERR.BAD_PARAMS,X)
-------------------------------
ERROR: Bad parameters 3/5
CONTEXT: Error(ERR.BAD_PARAMS, X)
-------------------------------
ERROR: Bad parameters x
CONTEXT: Error(ERR.BAD_PARAMS, X)
-------------------------------
ERROR: Bad parameters
CONTEXT: Minors(2,M)
-------------------------------
(1)
-------------------------------
ERROR: Bad characteristic
CONTEXT: Use CurrentRingEnv ::= ZZ/(2^31 - 1)
-------------------------------
x
-------------------------------
x + y
-------------------------------
(bcd - 230498234098234098234098234098234b^2e + 1/2a)/b
-------------------------------
ERROR: Bad number of parameters
CONTEXT: NewMat(2)
-------------------------------
Ideal(x^2, xy, y^2)
-------------------------------
[x^2 - y, -xy + z, -y^2 + xz]
-------------------------------
";
TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 19 : DensePoly and Weights

Test := Record[Id := "exbugs_19", Descr := "DensePoly and Weights"];

Test.Input := "
Use A ::= QQ[x], Weights(0);
Use S ::= QQ[a,b,c], Weights(1,2,3);
DensePoly(1);
DensePoly(2);
DensePoly(3);
";

Test.ExpectedOutput :=
"ERROR: Principal weights must all be strictly positive
CONTEXT: Use A ::= QQ[x],Weights(0)
-------------------------------
a
-------------------------------
a^2 + b
-------------------------------
a^3 + ab + c
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 20 : Parameter must be an indeterminate

Test := Record[Id := "exbugs_20", Descr := "Parameter must be an indeterminate"];

Test.Input := "
Use S ::= QQ[x,y];
IndetName(x*x);
IndetIndex(x*x);
x^2..y;
Der(x^5,x^2);
Der(x^5*y^5,x*y);
";

Test.ExpectedOutput :=
"ERROR: Expected indeterminate
CONTEXT: IndetName(x * x)
-------------------------------
ERROR: Expected indeterminate
CONTEXT: IndetIndex(x * x)
-------------------------------
ERROR: Expected indeterminate
CONTEXT: x^2..y
-------------------------------
ERROR: Expected indeterminate
CONTEXT: Der(x^5,x^2)
-------------------------------
ERROR: Expected indeterminate
CONTEXT: Der(x^5 * y^5,x * y)
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 21 : Presentation of partial resolution

Test := Record[Id := "exbugs_21", Descr := "Presentation of partial resolution"];

Test.Input := "
Use S ::= QQ[x,y,z];
I := Ideal(x^2-z*y,x*y-z^2,x*y);
GB.GetRes(I);
X := Res(I);
GB.GetRes(I);
";

Test.ExpectedOutput :=
"0 --> 
-------------------------------
0 --> S(-5)^2 --> S(-4)^4 --> S(-2)^3
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 22 : Compliments of John and Dave

Test := Record[Id := "exbugs_22", Descr := "Compliments of John and Dave"];

Test.Input := "
S ::= QQ[a,b];
Ring(S);
S ::= QQ[a,b];
Ring(S);
Ring(S);
";

Test.ExpectedOutput :=
"QQ[a,b]
-------------------------------
QQ[a,b]
-------------------------------
QQ[a,b]
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 23 : Image (Map of Tagged)
Test := Record[Id := "exbugs_23", Descr := "Image (Map of Tagged)"];

Test.Input := "
Use T ::= QQ[t,x,y,z];
A := z;

Use S ::= QQ[x,y,z], Weights([1,1,2]);
Image(A, RMap(0,x,y,z));
";
Test.ExpectedOutput :=
"z
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 24 : I := Ideal(x); I.GBasis := [x]; I=I;

Test := Record[Id := "exbugs_24", Descr := "I := Ideal(x); I.GBasis := [x]; I=I;"];

Test.Input := "
Use S ::= QQ[t,x,y,z];
I := Ideal(x);
I.GBasis := [x];
I=I;
";
Test.ExpectedOutput :=
"True
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 25 : Hilbert di artiniani

Test := Record[Id := "exbugs_25", Descr := "Hilbert(R/I, 2); -- caso artiniano"];

Test.Input := "
Use QQ[x,y,z,t];
I := Ideal(t^2,x^3,y^4, z^5);
Hilbert(CurrentRing()/I, 2);
";
Test.ExpectedOutput :=
"9
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 26 : Sundry details

Test := Record[Id := "exbugs_26", Descr := "Sundries"];

Test.Input := "
Use QQ[x,y];
Mat([ [1,2,3], [1,2] ]);
M := Mat([[1,Poly(0)],[0,1]]);
LinKer(M);  --> SEGV, fixed this_arpoly2arbrat (polys.c)

Use QQ[x[0..99999]];                --> SEGV, fixed indet_info_add (indets.c)
Use QQ[x[1..15,1..15,1..15,1..15]]; --> SEGV, almost same bug as above
Use QQ[x[0..10,0..10,0..10,0..10,0..10]]; --> gave no error, but should have
-- x[9,9,9,9,9]^5;                    --> x[9,9,10,2,1]^5

If Rand(2,1) > 2 Then PrintLn \"Random bug\"; EndIf;  --> gave division by 0 error, fixed misc.pkg
Rand(1/2,3/2);  --> now gives error, previously did not  (misc.pkg)
Rand(x,y);      --> now gives error, previously did not  (misc.pkg)

Use ZZ/(25)[x];
Factor(x);      --> now gives error, previously SEGVed  (jintf.c & jintf.h)
Use QQ[x];
Factor(Poly(0));--> now gives error, previously did not  (coclib.pkg)

-- This used to claim that A.X#1 did not exist.
-- (fixed cterm_subst_vars, engine.c)
A := Record[X:=999];
For X := 1 To 2 Do PrintLn A.X; EndFor;

Vector(0,0) = Vector(0,0,0);  --> now gives False, previously gave True
                              -- (fixed pv_eq in pvector.c)
Vector(0,0,0) = Vector(0,0);  --> now gives False, previously gave True
";
Test.ExpectedOutput :=
"ERROR: Matrix constructor requires (rectangular) list of lists
CONTEXT: Mat([[1, 2, 3], [1, 2]])
-------------------------------
[ ]
-------------------------------
ERROR: Too many indeterminates
CONTEXT: Use CurrentRingEnv ::= QQ[x[0..99999]]
-------------------------------
ERROR: Too many indeterminates
CONTEXT: Use CurrentRingEnv ::= QQ[x[1..15,1..15,1..15,1..15]]
-------------------------------
ERROR: Too many indeterminates
CONTEXT: Use CurrentRingEnv ::= QQ[x[0..10,0..10,0..10,0..10,0..10]]
-------------------------------
ERROR: Bad parameters
CONTEXT: Return(ERR.BAD_PARAMS)
-------------------------------
ERROR: Bad parameters
CONTEXT: Return(ERR.BAD_PARAMS)
-------------------------------
-- WARNING: Coeffs are not in a field
-- GBasis-related computations could fail to terminate or be wrong

-------------------------------
ERROR: Factor: factorization over ZZ/(n) not supported for n non-prime
CONTEXT: Error(\"Factor: factorization over ZZ/(n) not supported for n non-prime\")
-------------------------------
ERROR: Cannot factorize 0
CONTEXT: Error(\"Cannot factorize 0\")
-------------------------------
999
999

-------------------------------
False
-------------------------------
False
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 27 : Deg, MDeg

Test := Record[Id := "exbugs_27", Descr := "Deg of Zero"];

Test.Input := "
Use QQ[x,y];
Deg(0);
Deg(Poly(0));
V := Vector([0,0]);
Deg(V);
MDeg(0);
MDeg(2);
MDeg(\"Ciao;\");
";
Test.ExpectedOutput :=
"ERROR: Deg of zero is not defined
CONTEXT: Error(\"Deg of zero is not defined\")
-------------------------------
ERROR: Deg of zero is not defined
CONTEXT: Error(\"Deg of zero is not defined\")
-------------------------------
ERROR: Deg of zero is not defined
CONTEXT: Error(\"Deg of zero is not defined\")
-------------------------------
ERROR: MDeg of zero is not defined
CONTEXT: Error(\"MDeg of zero is not defined\")
-------------------------------
0
-------------------------------
ERROR: MDeg: argument must be POLY or VECTOR
CONTEXT: Error(\"MDeg: argument must be POLY or VECTOR\")
-------------------------------
";

TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 28 : Comp

Test := Record[Id := "exbugs_28", Descr := "Comp"];

Test.Input := "
Comp(\"ciao;\", 1);
Comp([1,2,3], 1);
Comp([[1,100],2,3], 1,2);
Comp([Record(A=100),2,3], 1, \"A\");
Comp(\"ciao\", \"x\");
Use QQ[x,y];
Comp(Vector(x,y), 1);
Comp([0, Record(B=Vector(x,y))], 2, \"B\", 1);
";
Test.ExpectedOutput :=
"c
-------------------------------
1
-------------------------------
100
-------------------------------
100
-------------------------------
ERROR: Comp: argument must be LIST/VECTOR/STRING-INT or RECORD-STRING
CONTEXT: Error(\"Comp: argument must be LIST/VECTOR/STRING-INT or RECORD-STRING\")
-------------------------------
x
-------------------------------
x
-------------------------------
";

TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 29 : Factor

Test := Record[Id := "exbugs_29", Descr := "Factor"];

Test.Input := "
Use S29 ::= ZZ/(3)[a];
F1 := a-1;
F2 := 3*a;
F3 := a/a;

Use QQ[x];
Factor(F1);
Factor(F2);
Factor(F3);
";
Test.ExpectedOutput :=
"[[S29 :: a - 1, 1]]
-------------------------------
ERROR: Cannot factorize 0
CONTEXT: Error(\"Cannot factorize 0\")
-------------------------------
[ ]
-------------------------------
";

TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 30 : Diff

Test := Record[Id := "exbugs_30", Descr := "Diff"];

Test.Input := "
Use QQ[x];
Diff([x], Ideal(x));
";
Test.ExpectedOutput :=
"ERROR: Diff: arguments must be lists
CONTEXT: Error(\"Diff: arguments must be lists\")
-------------------------------
";

TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 31 : Colon, Mingens

Test := Record[Id := "exbugs_31", Descr := "Ambiguity Ideal : Product; MinGens"];

Test.Input := "
Use QQ[x,y,z];
Print(\"-- Ambiguity Ideal : Product\");
Ideal(x^2):y*z;

Print(\"-- MinGens - Computation in Progress\");
I := Ideal([y^2 - x^2 - x*y , x^2 - y*z - z^2]);
MinGens(I);
GBasis(I);
";
Test.ExpectedOutput :=
"-- Ambiguity Ideal : Product
-------------------------------
ERROR: Colon: arguments must be [MODULE,MODULE], [MODULE,IDEAL], or [IDEAL,IDEAL]
CONTEXT: Error(\"Colon: arguments must be [MODULE,MODULE], [MODULE,IDEAL], or [IDEAL,IDEAL]\")
-------------------------------
-- MinGens - Computation in Progress
-------------------------------
[x^2 - yz - z^2, -xy + y^2 - yz - z^2]
-------------------------------
[x^2 - yz - z^2, -xy + y^2 - yz - z^2, y^3 - 3y^2z - xz^2 - yz^2 + z^3]
-------------------------------
";

TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 32 : Det

Test := Record[Id := "exbugs_32", Descr := "Det bugs due to this_arpoly2arbint"];

Test.Input := "
Use QQ[x];
M1 := Mat([[Poly(1),Poly(0)],[Poly(0),Poly(1/7)]]);
Det(M1);
M2 := Mat([[Poly(1/2)]]);
Det(M2);
";
Test.ExpectedOutput :=
"1/7
-------------------------------
1/2
-------------------------------
";

TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 33 : Factor

Test := Record[Id := "exbugs_33", Descr := "Factor bugs (actually degrevlex coded wrongly) -- used to hang"];

Test.Input := "
-- With a bad choice of random number Factor(x^2-y) used to hang.
-- Bug was actually because DMPZorder_deglex was coded wrongly.
Use QQ[x,y];For I := 1 To 100 Do NoPrint := Factor(x^2-y); EndFor;
-- Used to get the sign wrong in some factorizations.
FacPows := Factor(-x*y);
Product([A[1]^A[2] | A In FacPows]) = -x*y;
NoodleRing ::= QQ[a,b,c,d,e,f];
Using NoodleRing Do
  F := (-a^2*d^2 + a^2*d*e + a*d^2*e - a*d*e^2 + a^2*d*f + a*d^2*f - a^2*e*f - 2*a*d*e*f -
  d^2*e*f + a*e^2*f + d*e^2*f - a*d*f^2 + a*e*f^2 + d*e*f^2 - e^2*f^2);
  FacPows := Factor(F);
  Product([A[1]^A[2] | A In FacPows]) = F;
EndUsing;
";
Test.ExpectedOutput :=
"True
-------------------------------
True
-------------------------------
";

TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 34 : IdealOfProjectivePoints

Test := Record[Id := "exbugs_34", Descr := "Problem working over a finite field, due to an incorrect Cast"];

Test.Input := "
DoodleRing ::= ZZ/(32003)[x,y,z,t,u];
Using DoodleRing Do
  X := [[ Randomized(Poly(1)) | A In 1..4] | B In 1..9];
  For A := 1 To 9 Do
    X[A] := Concat(X[A], [Poly(X[A][4]^2/X[A][3])]);
  EndFor;
  NoPrint := IdealOfProjectivePoints(X);
EndUsing;
";
Test.ExpectedOutput :=
"";

TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 35 : Syz input buglet

Test := Record[Id := "exbugs_35", Descr := "Syz did not allow a list of vectors"];

Test.Input := "
Use QQ[x,y];
I := Ideal(x);
Minimalized(Syz(Gens(I)));
I := Module([[x,y]]);
Minimalized(Syz(Gens(I)));
";
Test.ExpectedOutput :=
"Module([[0]])
-------------------------------
Module([[0]])
-------------------------------
";

TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 36 : Yet more factorizer bugs...

Test := Record[Id := "exbugs_36", Descr := "Factor used to hang sometimes mod p"];

Test.Input := "
BoodleRing ::= ZZ/(32003)[x];
Using BoodleRing Do
  F := (2*x+3)^45-1;
  NoPrint := Factor(F);
EndUsing;
";
Test.ExpectedOutput :=
"";

TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 37 : Silly GenRepr bug

Test := Record[Id := "exbugs_37", Descr := "GenRepr used to segv on this"];

Test.Input := "
YodelRing ::= QQ[x,y,z];
Using YodelRing Do
  M := Module([[1,1],[x^2,0],[x*y,0],[0,y],[0,z]]);
  S := SyzOfGens(M);
  NoPrint := ReducedGBasis(S);  -- works OK without this line
  V := Vector(0,y,-x,0,0);
  GenRepr(V,S);
EndUsing;
";
Test.ExpectedOutput :=
"[0, x, 0, 1]
-------------------------------
";

TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 38 : Print NewLine

Test := Record[Id := "exbugs_38", Descr := "Print NewLine used to SEGV"];

Test.Input := "
Print NewLine;
";
Test.ExpectedOutput :=
"

-------------------------------
";

// TSL.RegisterTest(Test); // obsolescent


-------------------------------
-- EXBUG 39 : Log(1) used to crash, and Log(0) didn't give error.

Test := Record[Id := "exbugs_39", Descr := "Log(1) used to SEGV; Log(0) was not an error"];

Test.Input := "
Log(0);
Use LogRing1 ::= QQ[x,y,z];
F := x^3*y*z;
Log(F);
Log(1);
Use LogRing2 ::= QQ[x[1..9]];
Log(F);
G := Product(Indets())^7;
Log(1);
Log(G);
Use LogRing1;
Log(G);
";
Test.ExpectedOutput :=
"ERROR: Log(0) is undefined
CONTEXT: Log(0)
-------------------------------
[3, 1, 1]
-------------------------------
[0, 0, 0]
-------------------------------
[3, 1, 1]
-------------------------------
[0, 0, 0, 0, 0, 0, 0, 0, 0]
-------------------------------
[7, 7, 7, 7, 7, 7, 7, 7, 7]
-------------------------------
[7, 7, 7, 7, 7, 7, 7, 7, 7]
-------------------------------
";

TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 40 : GMP 3.0.1 bug, provoked infinite loop

Test := Record[Id := "exbugs_40", Descr := "GMP 3.0.1 BUG in mpz_gcd, used to be infinite loop"];

Test.Input := "
1/40051618821241492889002174242929929255240418389805157062255602795630693435668945312500000000000000000000000000000000+
1/8010323764248298577800434848585985851048083677961031412451120559126138687133789062500000000000000000000000000000000;
";
Test.ExpectedOutput :=
"3/20025809410620746444501087121464964627620209194902578531127801397815346717834472656250000000000000000000000000000000
-------------------------------
";

TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 41 : silly off-by-1 errors in QuickSort

Test := Record[Id := "exbugs_41", Descr := "QuickSort soak test"];

Test.Input := "
For I := 1 To 20 Do
  L := [Rand(-99,99) | I In 1..99];
  Sort(L);
  If [I In 1..(Len(L)-1) | L[I] > L[I+1]] <> [] Then PrintLn \"Sort bug\"; EndIf;
EndFor;";
Test.ExpectedOutput :=
"";

TSL.RegisterTest(Test);





-------------------------------
-- EXBUG 42 : Aldo's bug, a missing zero component in a resolution

Test := Record[Id := "exbugs_42", Descr := "missing zero component in a resolution"];

Test.Input := "
AldoResBugRing ::= ZZ/(2)[x[1..6]];
Using AldoResBugRing Do
  I := Ideal(x[1]*x[2]*x[3], x[1]*x[2]*x[4], x[1]*x[3]*x[5], x[2]*x[4]*x[5],
  x[3]*x[4]*x[5], x[2]*x[3]*x[6], x[1]*x[4]*x[6], x[3]*x[4]*x[6], x[1]*x[5]*x[6],
  x[2]*x[5]*x[6]);
  M := CurrentRing()/I;
  PrintLn Res(M);
  Describe Res(M); PrintLn;
  PrintLn GB.GetNthSyz(M,4);
  PrintLn GB.GetNthSyzShifts(M,4);
EndUsing;
";
Test.ExpectedOutput :=
"0 --> AldoResBugRing(-6) --> AldoResBugRing(-5)^6 + AldoResBugRing(-6) --> AldoResBugRing(-4)^15 --> AldoResBugRing(-3)^10 --> AldoResBugRing
Mat([
  [x[2]x[5]x[6], x[1]x[5]x[6], x[3]x[4]x[6], x[1]x[4]x[6], x[2]x[3]x[6], x[3]x[4]x[5], x[2]x[4]x[5], x[1]x[3]x[5], x[1]x[2]x[4], x[1]x[2]x[3]]
])
Mat([
  [0, x[4], 0, x[3], 0, x[1], 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, x[4], 0, x[3], x[2], 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [x[5], 0, 0, 0, 0, 0, x[2], x[1], 0, 0, 0, 0, 0, 0, 0],
  [0, 0, x[5], 0, 0, 0, 0, x[3], x[2], 0, 0, 0, 0, 0, 0],
  [0, 0, 0, x[5], 0, 0, x[4], 0, 0, x[1], 0, 0, 0, 0, 0],
  [x[6], 0, 0, 0, 0, 0, 0, 0, 0, 0, x[2], x[1], 0, 0, 0],
  [0, x[6], 0, 0, 0, 0, 0, 0, 0, 0, x[3], 0, x[1], 0, 0],
  [0, 0, 0, 0, x[6], 0, 0, 0, 0, 0, 0, x[4], 0, x[2], 0],
  [0, 0, 0, 0, 0, 0, 0, 0, x[6], 0, 0, 0, x[5], 0, x[3]],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, x[6], 0, 0, 0, x[5], x[4]]
])
Mat([
  [x[2], x[1], 0, 0, 0, 0, 0],
  [x[3], 0, x[1], 0, 0, 0, 0],
  [0, x[3], x[2], 0, 0, 0, x[2]x[3]],
  [x[4], 0, 0, x[1], 0, 0, 0],
  [0, x[4], 0, x[2], 0, 0, x[2]x[4]],
  [0, 0, x[4], x[3], 0, 0, 0],
  [x[5], 0, 0, 0, x[1], 0, 0],
  [0, x[5], 0, 0, x[2], 0, 0],
  [0, 0, x[5], 0, x[3], 0, x[3]x[5]],
  [0, 0, 0, x[5], x[4], 0, 0],
  [x[6], 0, 0, 0, 0, x[1], 0],
  [0, x[6], 0, 0, 0, x[2], 0],
  [0, 0, x[6], 0, 0, x[3], 0],
  [0, 0, 0, x[6], 0, x[4], x[4]x[6]],
  [0, 0, 0, 0, x[6], x[5], x[5]x[6]]
])
Mat([
  [x[1]],
  [x[2]],
  [x[3]],
  [x[4]],
  [x[5]],
  [x[6]],
  [0]
])

Module([[x[1], x[2], x[3], x[4], x[5], x[6], 0]])
Shifts([x[2]x[3]x[4]x[5]x[6], x[1]x[3]x[4]x[5]x[6], x[1]x[2]x[4]x[5]x[6], x[1]x[2]x[3]x[5]x[6], x[1]x[2]x[3]x[4]x[6], x[1]x[2]x[3]x[4]x[5], x[1]x[2]x[3]x[4]x[5]x[6]])

-------------------------------
";

TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 43 : various limit cases for the For construct

Test := Record[Id := "exbugs_43", Descr := "limit cases for For"];

Test.Input := "
For I :=    2^31-1 To 2^31-1           Do PrintLn I; EndFor;
For I :=         1 To     10 Step 0    Do PrintLn I; EndFor;
For I :=         1 To    -10 Step 0    Do PrintLn I; EndFor;
For I :=     -2^31 To 2^31-1 Step 2^30 Do PrintLn I; EndFor;
For I := -2^31+100 To  -2^31 Step -100 Do PrintLn I; EndFor;
";
Test.ExpectedOutput :=
"2147483647

-------------------------------
ERROR: Zero step size in For loop
CONTEXT: For I := 1 To 10 Step 0 Do PrintLn(I);  EndFor
-------------------------------
ERROR: Zero step size in For loop
CONTEXT: For I := 1 To -10 Step 0 Do PrintLn(I);  EndFor
-------------------------------
-2147483648
-1073741824
0
1073741824

-------------------------------
-2147483548
-2147483648

-------------------------------
";

TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 44 : Range operator on indets from other rings

Test := Record[Id := "exbugs_44", Descr := "range operator in other rings"];

Test.Input := "
Use Ring9 ::= QQ[x[1..9]];
X1 := x[1];
X9 := x[9];
Use Ring3 ::= QQ[x,y,z];
X1..X9;
x..(y+1);
x..X1;
";
Test.ExpectedOutput :=
"[Ring9 :: x[1], Ring9 :: x[2], Ring9 :: x[3], Ring9 :: x[4], Ring9 :: x[5], Ring9 :: x[6], Ring9 :: x[7], Ring9 :: x[8], Ring9 :: x[9]]
-------------------------------
ERROR: Expected indeterminate
CONTEXT: x..(y + 1)
-------------------------------
ERROR: Parameters have different base rings
CONTEXT: x..X1
-------------------------------
";

TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 45 : Det of matrix with ZMOD entries
Test := Record[Id := "exbugs_45", Descr := "Det(Mat(ZMOD))"];

Test.Input := "
M := Mat([[1,2],[3,4%5]]);
Det(M);
M := Mat([[1,2],[3,4%6]]);
Det(M);
M := Mat([[1,2],[3%5,4%6]]);
Det(M);
";
Test.ExpectedOutput :=
"-2 % 5
-------------------------------
-2 % 6
-------------------------------
ERROR: Bad parameters
CONTEXT: Return(Det(M))
-------------------------------
";
TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 46 : GBM bugs
Test := Record[Id := "exbugs_46", Descr := "GBM bugs"];

Test.Input := "
Use QQ[x,y,z];
GBM([Ideal(x,y,z),Ideal(x+1,y+1,z+1)^2]);
";
Test.ExpectedOutput :=
"Ideal(yz, xz, y^2, xy, x^2, z^3)
-------------------------------
";
TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 47 : matrix inversion bug (actually omat_adjoint was wrong)
Test := Record[Id := "exbugs_47", Descr := "matrix inversion bug"];

Test.Input := "
Use QQ[x,y,z];
M := Mat([[Poly(2)]]);
M^(-1);
";
Test.ExpectedOutput :=
"Mat([
  [1/2]
])
-------------------------------
";
TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 48 : Poly(0) not mapped correctly to (machine) integer
Test := Record[Id := "exbugs_48", Descr := "Poly(0) --> int  bug"];

Test.Input := "
Use QQ[x,y,z];
2^Poly(0);
";
Test.ExpectedOutput :=
"1
-------------------------------
";
TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 49 : More factorizer bugs, sometimes sign was wrong for multivariates
Test := Record[Id := "exbugs_49", Descr := "multivariate factorization sign bug"];

Test.Input := "Use QQ[x,y];
F := 2*x*y^3 - 3*x^2*y - 2*x*y^2 + 3*x*y - 2*y^2 + 3*x + 2*y - 3;
For I := 1 To 10 Do
  NoPrint := Factor(F);
  If Product([FacPow[1]^FacPow[2] | FacPow In NoPrint]) <> F Then
    PrintLn \"Bungled factorization\";
  EndIf;
EndFor;

/* Below is the old test which was really a bit slow; the simpler  */
/* code above exhibited the same bug much faster.                  */
/*
Use QQ[b[1..10]c[1..10]];

F := -b[5]b[8]c[1]^2c[4]c[8] + 2/3b[5]^2c[1]c[4]c[5]c[8]
 + b[1]b[8]c[1]c[4]c[5]c[8] - 2/3b[4]b[5]c[1]c[5]^2c[8]
 - 2/3b[1]b[5]c[4]c[5]^2c[8] + 2/3b[1]b[4]c[5]^3c[8]
 + b[1]b[5]c[1]c[4]c[8]^2 - b[1]^2c[4]c[5]c[8]^2
 + 1/3b[5]b[8]c[1]^2c[3]c[9] - 2/9b[5]^2c[1]c[3]c[5]c[9]
 - 1/3b[1]b[8]c[1]c[3]c[5]c[9] + 2/9b[3]b[5]c[1]c[5]^2c[9]
 + 2/9b[1]b[5]c[3]c[5]^2c[9] - 2/9b[1]b[3]c[5]^3c[9]
 - 1/3b[1]b[5]c[1]c[3]c[8]c[9] + 1/3b[1]^2c[3]c[5]c[8]c[9];

For I := 1 To 100 Do
  NoPrint := Factor(F);
  If Product([FacPow[1]^FacPow[2] | FacPow In NoPrint]) <> F Then
    PrintLn \"Bungled factorization\";
  EndIf;
EndFor;
*/
";
Test.ExpectedOutput :=
"";
TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 50 : Call does not work for builtin functions, now gives error
Test := Record[Id := "exbugs_50", Descr := "Call applied to built-in function"];

Test.Input := "Call(Function(\"$builtin.Size\"), 3);
";
Test.ExpectedOutput :=
"ERROR: Call may not be used to apply builtin functions
CONTEXT: Call(Function(\"$builtin.Size\"),3)
-------------------------------
";
TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 51 : While did not handle errors in the condition correctly
Test := Record[Id := "exbugs_51", Descr := "While: error in condition"];

Test.Input := "While X>1 Do Print \"Ciao;\" EndWhile;
BBBB;1;1;
";
Test.ExpectedOutput :=
"ERROR: Undefined variable X
CONTEXT: While X > 1  Do Print(\"Ciao;\") EndWhile
-------------------------------
ERROR: Undefined variable BBBB
CONTEXT: BBBB
-------------------------------
1
-------------------------------
1
-------------------------------
";
TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 52 : IntersectionList worked properly only for DegRevLex
Test := Record[Id := "exbugs_52", Descr := "IntersectionList: trouble with Lex"];

Test.Input := "RingExbug52 ::= QQ[x,y],Lex;
Using RingExbug52 Do
I := IntersectionList([Ideal(x-1,y),Ideal(x+1,y+1),Ideal(x,y-2)]);
NF(x^2,I);
EndUsing;";
Test.ExpectedOutput :=
"-1/6y^2 - 1/6y + 1
-------------------------------
";
TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 53 : indet range bug (Antonio's ghost strikes again)
Test := Record[Id := "exbugs_53", Descr := "Indet range bug (used to crash on Alpha)"];

Test.Input := "For I := 2 To 32 Do
  RR ::= QQ[x[1..I]];
  Using RR Do
    NoPrint := x[1]..x[2];
  EndUsing;
EndFor;
";
Test.ExpectedOutput :=
"";
TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 54 : sundry little bugs surfacing just prior to 4.1 release
Test := Record[Id := "exbugs_54", Descr := "Sundries at 4.1-beta"];

Test.Input := "Use ZZ/(11)[x,y,z];
M := Module([[x],[x+1]]);
I := Ideal(M); -- used to give error
Sz := Syz([x,y,0]); -- failed to recognize 0 as a poly
M := Module([[x,y,z], [y,z,x]]);
K := Module([[x,y,z], [y,z,x], [z,z,z]]);
Rs := Res(K/M);  -- previously did not give error, but gave a misleading answer
Rs := Res(CurrentRing()^2/M); -- similar problem to above
Rs := Res(CurrentRing()^3/M); -- this is the correct one
Define Func(Var X)
  PrintLn \"Normal\";
EndDefine;
Func(1/0);   --- the error used not to be reported
Use R; -- revert to a sane ring
";
Test.ExpectedOutput :=
"ERROR: Module quotient must be between ambient freemodule and a submodule
CONTEXT: Rs := Res(K / M)
-------------------------------
ERROR: Module quotient must be between ambient freemodule and a submodule
CONTEXT: Rs := Res(CurrentRing()^2 / M)
-------------------------------
ERROR: Division by zero
CONTEXT: 1 / 0
-------------------------------
";
TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 55 : more bugs surfacing just prior to 4.1 release
Test := Record[Id := "exbugs_55", Descr := "More sundries at 4.1-beta"];

Test.Input := "Untagged(Tagged([1],\"tag\")) = [1];
Tagged([1],\"\");
Tagged([1]);
Inverse(2%4);
Untagged(Type(Tagged([1],\"abc\")));
Tagged(NULL, \"abc\");
Tagged([], \"abc\");
Type(It);
Type(It);

Use RingExbug55 ::= QQ[x,y,z];
S ::= ZZ/(5)[t];
Q := S::(t^4+2);
P := S::(t^2-2*t-2);
NR(P,[Q]);
";
Test.ExpectedOutput :=
"True
-------------------------------
ERROR: Tag must be a non-empty string
CONTEXT: Tagged([1],\"\")
-------------------------------
ERROR: Bad number of parameters
CONTEXT: Tagged([1])
-------------------------------
ERROR: Cannot invert
CONTEXT: Return(X^(-1))
-------------------------------
TAGGED(\"abc\")
-------------------------------
ERROR: Type names may not be tagged
CONTEXT: Tagged(NULL,\"abc\")
-------------------------------
Tagged([ ], \"abc\")
-------------------------------
TAGGED(\"abc\")
-------------------------------
TYPE
-------------------------------
S :: t^2 - 2t - 2
-------------------------------
";
TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 56 : PosTo bug for vector orderings
Test := Record[Id := "exbugs_56", Descr := "PosTo vector ordering bug"];

Test.Input := "Exbug56ring ::= QQ[x,y],PosTo;
Using Exbug56ring Do
  Vector(y, 0) > Vector(0, x);
  Vector(y, 0) < Vector(0, x);
EndUsing;
";
Test.ExpectedOutput :=
"TrueFalse
-------------------------------
";
TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 57 : difficulties with the zero module
Test := Record[Id := "exbugs_57", Descr := "zero module quirks"];

Test.Input := "Exbug57ring ::= QQ[x,y];
Using Exbug57ring Do
  M := Module([Vector([0,0,0])]);
  Minimalize(M);
  M;
EndUsing;
";
Test.ExpectedOutput :=
"Module([[0, 0, 0]])
-------------------------------
";
TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 58 : more thorough checks for zero
Test := Record[Id := "exbugs_58", Descr := "zero checks"];

Test.Input := "Use QQ[x,y,z];
LPos(Poly(0));
LPos(Vector([0,0]));
LPP(Poly(0));
LPP(Vector([0,0]));
FirstNonZero(Vector([0]));
FirstNonZeroPos(Vector([0]));
IsTerm(Poly(0));
IsTerm(Vector([0,0]));
";
Test.ExpectedOutput :=
"ERROR: LPos: Vector expected
CONTEXT: LPos(Poly(0))
-------------------------------
0
-------------------------------
ERROR: LPP: zero poly
CONTEXT: LPP(Poly(0))
-------------------------------
ERROR: LPP: zero vector
CONTEXT: LPP(Vector([0, 0]))
-------------------------------
ERROR: FirstNonZero: zero vector
CONTEXT: FirstNonZero(Vector([0]))
-------------------------------
ERROR: FirstNonZeroPos: zero vector
CONTEXT: FirstNonZeroPos(Vector([0]))
-------------------------------
False
-------------------------------
False
-------------------------------
";
TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 59 : more thorough checks for zero
Test := Record[Id := "exbugs_59", Descr := "Factorizer, Mod2Rat bug"];

Test.Input := "Use QQ[x,y,z];
Bug59Deg := 6;
For I := 1 To 200 Do
  F := Product([Sum([x^K*Rand(1,9) | K In 0..Bug59Deg]) | J In 1..3]);
  If Len([FacPow In Factor(F) | Deg(FacPow[1]) > Bug59Deg]) > 0 Then
    PrintLn(\"Bad factorization\");
  EndIf;
EndFor;
Mod2Rat(5231818081,17^8,16);
Mod2Rat(-5231818081,17^8,16);
Mod2Rat(5231818081,-17^8,16);
Mod2Rat(-5231818081,-17^8,16);
Mod2Rat(5231818081,17^8,-16);
";
Test.ExpectedOutput :=
"1/4
-------------------------------
-1/4
-------------------------------
1/4
-------------------------------
-1/4
-------------------------------
0
-------------------------------
";
TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 60 : silly bug in GBM
Test := Record[Id := "exbugs_60", Descr := "GBM bug"];

Test.Input := "Use Exbug60Ring ::= QQ[x[1..4]];
L3 := [Ideal(x[4]^2-x[4]+1, x[3]^2-x[3]+1, x[2]^2-x[2]+1, x[1]^2-x[1]+1),
      Ideal(x[4]^2-x[4]+1, x[3]^2-x[3]+1, x[2]^2-x[2]+2, x[1]^2-2*x[1]+1),
      Ideal(x[4]^2-x[4]+1, x[3]^2-x[3]+1, x[2]^2-2*x[2]+1, x[1]^2-x[1]+1)];
NoPrint := GBM(L3); -- used to give a SEGV (in v4.2)
";
Test.ExpectedOutput := "";
TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 61 : bug in Cast
Test := Record[Id := "exbugs_61", Descr := "Cast bug"];

Test.Input := "Use Exbug61Ring ::= QQ[x[1..4]];
Cast(Poly(1/2),INT);
";
Test.ExpectedOutput := "ERROR: Cannot cast POLY to INT
CONTEXT: Cast(Poly(1 / 2),INT)
-------------------------------
";
TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 62 : bug in Factor for non-standard degrees
Test := Record[Id := "exbugs_62", Descr := "Factor non-standard degree bug"];

Test.Input := "Use Exbug62Ring ::= QQ[x,y,z],Weights([2,2,3]);
Factor(x^3);
";
Test.ExpectedOutput := "[[x, 3]]
-------------------------------
";
TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 63 : division by a non-obvious zero
Test := Record[Id := "exbugs_63", Descr := "division by a non-obvious zero"];

Test.Input := "Use Exbug63Ring ::= ZZ/(2)[x];
x/2; -- used To SEGV
";
Test.ExpectedOutput := "ERROR: Division by zero
CONTEXT: x / 2
-------------------------------
";
TSL.RegisterTest(Test);


-------------------------------
-- EXBUG 64 : SortedBy with a non-strict order
Test := Record[Id := "exbugs_64", Descr := "SortedBy with a non-strict order"];

Test.Input := "L := [5,4,4,3,3,3,2,2,1];
Define Bug64LessThan(X,Y)
  Return X <= Y;
EndDefine; -- Bug64LessThan
SortedBy(L,Function(\"Bug64LessThan\"));
";
Test.ExpectedOutput := "[1, 2, 2, 3, 3, 3, 4, 4, 5]
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 65 : Wrong answer from Radical
Test := Record[Id := "exbugs_65", Descr := "Wrong answer from Radical"];

Test.Input := "Use RingExbug65 ::= QQ[x[1..4]];
J := Ideal(-x[1]*x[3] + x[2]*x[4], -x[3]*x[4] + x[4]^2);
I := Radical(J);
I = Ideal(x[1]*x[3] - x[2]*x[4], x[1]*x[4] - x[2]*x[4], x[3]*x[4] - x[4]^2);
";
Test.ExpectedOutput := "True
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 66 : DecimalStr(-0.3)=0.3 found by Matthias Machnik
Test := Record[Id := "exbugs_66", Descr := "DecimalStr(-0.3);"];

Test.Input := "DecimalStr(-0.3);
";
Test.ExpectedOutput := "-0.3
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 67 : Remove() for nested lists fixed by Karsten Hiddemann
Test := Record[Id := "exbugs_67", Descr := "Remove() for nested lists"];

Test.Input := "Unset Indentation;
L := [[1,2,3,4]];
T := L;
Remove(T[1],4);
Print \"L=\", L;
Print \"T=\", T;
";
Test.ExpectedOutput := "L=[[1, 2, 3, 4]]
-------------------------------
T=[[1, 2, 3]]
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 68 : Cartesian product of size zero
Test := Record[Id := "exbugs_68", Descr := "Cartesian product of size 0"];

Test.Input := "Unset Indentation;
NoPrint := []><[1];
NoPrint := [1]><[]><[2];
";
Test.ExpectedOutput := "";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 69 :   J:I > J;  gave False with Lex (Conca 4.0)
Test := Record[Id := "exbugs_69", Descr := "BUG-46 gave False with Lex (Conca 4.0)"];

Test.Input := "
Use QQ[a[1..10]], Lex;
W :=
Mat([
   [a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10]],
   [24*a[1]*a[4]*a[6] - 6*a[1]*a[5]^2 - 8*a[2]^2*a[6] + 8*a[2]*a[3]*a[5] -
8*a[3]^2*a[4], 24*a[1]*a[4]*a[9] - 24*a[1]*a[5]*a[8] + 72*a[1]*a[6]*a[7] -
8*a[2]^2*a[9] + 16*a[2]*a[3]*a[8] - 8*a[2]*a[4]*a[6] + 2*a[2]*a[5]^2 -
24*a[3]^2*a[7], 72*a[1]*a[4]*a[10] - 24*a[1]*a[5]*a[9] + 24*a[1]*a[6]*a[8] -
24*a[2]^2*a[10] + 16*a[2]*a[3]*a[9] - 8*a[3]^2*a[8] - 8*a[3]*a[4]*a[6] +
2*a[3]*a[5]^2, 72*a[1]*a[7]*a[9] - 24*a[1]*a[8]^2 - 8*a[2]*a[4]*a[9] +
24*a[2]*a[6]*a[7] + 16*a[3]*a[4]*a[8] - 24*a[3]*a[5]*a[7] - 8*a[4]^2*a[6] +
2*a[4]*a[5]^2, 216*a[1]*a[7]*a[10] - 24*a[1]*a[8]*a[9] - 24*a[2]*a[4]*a[10] -
8*a[2]*a[5]*a[9] + 24*a[2]*a[6]*a[8] + 24*a[3]*a[4]*a[9] - 8*a[3]*a[5]*a[8] -
24*a[3]*a[6]*a[7] - 8*a[4]*a[5]*a[6] + 2*a[5]^3, 72*a[1]*a[8]*a[10] -
24*a[1]*a[9]^2 - 24*a[2]*a[5]*a[10] + 16*a[2]*a[6]*a[9] + 24*a[3]*a[4]*a[10] -
8*a[3]*a[6]*a[8] - 8*a[4]*a[6]^2 + 2*a[5]^2*a[6], 24*a[2]*a[7]*a[9] -
8*a[2]*a[8]^2 - 8*a[4]^2*a[9] + 8*a[4]*a[5]*a[8] - 6*a[5]^2*a[7],
72*a[2]*a[7]*a[10] - 8*a[2]*a[8]*a[9] + 24*a[3]*a[7]*a[9] - 8*a[3]*a[8]^2 -
24*a[4]^2*a[10] + 16*a[4]*a[6]*a[8] + 2*a[5]^2*a[8] - 24*a[5]*a[6]*a[7],
24*a[2]*a[8]*a[10] - 8*a[2]*a[9]^2 + 72*a[3]*a[7]*a[10] - 8*a[3]*a[8]*a[9] -
24*a[4]*a[5]*a[10] + 16*a[4]*a[6]*a[9] + 2*a[5]^2*a[9] - 24*a[6]^2*a[7],
24*a[3]*a[8]*a[10] - 8*a[3]*a[9]^2 - 6*a[5]^2*a[10] + 8*a[5]*a[6]*a[9] -
8*a[6]^2*a[8]]
]);
J := Ideal(Minors(2,W));
J:Ideal(a[1])>J;";
Test.ExpectedOutput := "True
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- EXBUG 70 : Multivariate factorizer/gcd bugs
Test := Record[Id := "exbugs_70", Descr := "Multivariate factorizer/gcd bugs"];

Test.Input := "Use QQ[b,c,t];
J := (c-t)*(- b*c*t + b*t^2 + 2*c^2 + 2*c*t);
NoPrint := Factor(J); // used to trigger a factorizer error message
For I := 1 To 100 Do NoPrint := Factor(J); EndFor;

Use QQ[x,y];
F := Product([y-I | I In 0..98]);
G := x*F+1;
NoPrint := GCD(G,G); // used to cause an infinte loop

//---------------------------------
Use QQ[a[1..3], b[1..3]];

E := [Sum(a[1]..a[3]) * (a[I]+b[I]) / Sum(a[1]..b[3]) | I In 1..3];
F := [(a[I] - E[I])^2/E[I] | I In 1..3];
F := Subst(F,a[1],0);

// Used To disappear into an infinite loop after 14 iters.
For I := 1 To 100 Do
  Print I;
  A := Sum(F);
EndFor;
";
Test.ExpectedOutput := "123456789101112131415161718192021222324252627282930313233343536373839404142434445464748495051525354555657585960616263646566676869707172737475767778798081828384858687888990919293949596979899100
-------------------------------
";
TSL.RegisterTest(Test);

----------------------------------------
-- EXBUG 71 : Non-deterministic determinant
Test := Record[Id := "exbugs_71", Descr := "Non-deterministic determinant"];

Test.Input := "
Bug71Mat := Mat([[1, 11, -19, -209],
                 [1, 147, -43, -6321],
                 [1, -34, 97, -3298],
                 [1, 85, 98, 8330]
                ]);

If Len(MakeSet([Det(Bug71Mat) | I In 1..100])) > 1 Then PrintLn \"BUGGY DET\"; EndIf;
";
Test.ExpectedOutput := "";
TSL.RegisterTest(Test);

----------------------------------------
-- EXBUG 72 : fix on dirty "Z/" workaround
Test := Record[Id := "exbugs_72", Descr := "dirty \"Z/\" workaround"];

Test.Input := "
Z := 4;  Z/2;  ZZ := 14;  ZZ/2;
";
Test.ExpectedOutput := "2
-------------------------------
7
-------------------------------
";
TSL.RegisterTest(Test);
