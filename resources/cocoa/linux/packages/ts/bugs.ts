-- $Id: bugs.ts,v 1.5 2009/05/22 15:08:20 bigatti Exp $
-- Test Suite: bugs

Alias TSL := $ts/ts_lib;
TSL.Initialize();

-------------------------------
-- BUG-28 Intersection but not colon (Caboara)

Test := Record[Id := 28,
  Descr := "Intersection but not colon (Caboara)"];

Test.Input :="
Use R::=QQ[habcde],PosTo;

C4:=Ideal(a+b+c+d,ab+bc+cd+da,abc+bcd+cda+dab,abcd-h^4);
C5:=Ideal(a+b+c+d+e,ab+bc+cd+de+ea,abc+bcd+cde+dea+eab,abcd+bcde+cdea+deab+eabc,
          abcde-h^5);
Time C:=Intersection(C4,C5);
Time T:=C5:C4;
";
-- Test.ExpectedOutput := "";
TSL.RegisterTest(Test);

-------------------------------
-- BUG-45 Ideal(x) <= Ideal(x,y) --33d14-> segfault

Test := Record[Id := 45,
  Descr := "BUG-45 Ideal(x) <= Ideal(x,y) --33d14-> segfault"];

Test.Input :="
Ideal(x) <= Ideal(x,y);
";
Test.ExpectedOutput := "True
-------------------------------
";
TSL.RegisterTest(Test);
