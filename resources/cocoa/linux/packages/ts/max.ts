-- $Id: max.ts  /*MAX*/
-- Test suite for the new functions of 3.6.2


Alias TSL := $ts/ts_lib;
TSL.Initialize();

/*

Source CocoaPackagePath() + "/lib/cocoa/ts/max.ts";
TSL.RunTests();  // was  TSL.Do();

*/


-------------------------------
-- TEST 01 :
Test := Record[Id := "max_01", Descr := "CoeffOfTerm correct"];

Test.Input :="A := NewId();
Use Var(A) ::= QQ[x,y], ToPos;
[CoeffOfTerm(Poly(1),x+4),CoeffOfTerm(x,2*x),CoeffOfTerm(x,5*x+y)];
[CoeffOfTerm(Vector(0,0,x*y,0),Vector(x,x,x^2+5*x*y+y,y))];
Use MXX ::= QQ[t];
//obsolescent Destroy Var(A);
";
Test.ExpectedOutput := "[4, 2, 5]
-------------------------------
[5]
-------------------------------
";
TSL.RegisterTest(Test);



-------------------------------
-- TEST 02 :
Test := Record[Id := "max_02", Descr := "CoeffOfTerm wrong"];
Test.Input :="Use Var(NewId()) ::= QQ[x,y], ToPos;
CoeffOfTerm(x+y,x);
CoeffOfTerm(1,x+y);
CoeffOfTerm(x,1);
";

Test.ExpectedOutput := "ERROR: CoeffOfTerm: first argument must be a term
CONTEXT: Error(\"CoeffOfTerm: first argument must be a term\")
-------------------------------
0
-------------------------------
0
-------------------------------
";
TSL.RegisterTest(Test);


----------------------------------------------------


-------------------------------
-- TEST 03 :
Test := Record[Id := "max_03", Descr := "IsTerm"];
Test.Input :="Use Var(NewId()) ::= QQ[x,y,z];
[IsTerm(x),IsTerm(Poly(1)),IsTerm(x+y),IsTerm(3*x),IsTerm(Poly(0))];
[IsTerm(Vector(x)),IsTerm(Vector(x,0)),IsTerm(Vector(1)),IsTerm(Vector(3*x)),
IsTerm(Vector(0)),IsTerm(Vector(x,y)),IsTerm(Vector(0,1,x+y))];
IsTerm(0);
";

Test.ExpectedOutput := "[True, True, False, False, False]
-------------------------------
[True, True, True, False, False, False, False]
-------------------------------
ERROR: IsTerm: Poly/Vector expected
CONTEXT: IsTerm(0)
-------------------------------
";
TSL.RegisterTest(Test);


-------------------------------
-- TEST 04 :
Test := Record[Id := "max_04", Descr := "Monomials/Support/Coeffs"];
Test.Input :="Use Var(NewId()) ::= QQ[t,x,y,z];
V:=Vector(0,x,2*y,3*z+4*t,1,0);
Support(V);
Monomials(V);
Coefficients(V);
[Support(Poly(1)),Monomials(Poly(1)),Coefficients(Poly(1))];
[Support(Poly(0)),Monomials(Poly(0)),Coefficients(Poly(0))];
[Support(Vector(0)),Monomials(Vector(0)),Coefficients(Vector(0))];
[Support(Vector(1)),Monomials(Vector(1)),Coefficients(Vector(1))];
Sum(Monomials(V))=V;
Support(0);
";
Test.ExpectedOutput := "[Vector(0, 0, 0, t, 0, 0), Vector(0, x, 0, 0, 0, 0), Vector(0, 0, y, 0, 0, 0), Vector(0, 0, 0, z, 0, 0), Vector(0, 0, 0, 0, 1, 0)]
-------------------------------
[Vector(0, 0, 0, 4t, 0, 0), Vector(0, x, 0, 0, 0, 0), Vector(0, 0, 2y, 0, 0, 0), Vector(0, 0, 0, 3z, 0, 0), Vector(0, 0, 0, 0, 1, 0)]
-------------------------------
[4, 1, 2, 3, 1]
-------------------------------
[[1], [1], [1]]
-------------------------------
[[ ], [ ], [ ]]
-------------------------------
[[ ], [ ], [ ]]
-------------------------------
[[Vector(1)], [Vector(1)], [1]]
-------------------------------
True
-------------------------------
ERROR: Poly/Vector expected
CONTEXT: Support(0)
-------------------------------
";
TSL.RegisterTest(Test);


-------------------------------
-- TEST 05 :
Test := Record[Id := "max_05", Descr := "IsPosTo/IsToPos/ Vector Ordering"];
Test.Input :="W := NewId();
Use Var(W) ::= QQ[t,x,y,z],ToPos;
[IsToPos(Var(W)),IsPosTo(Var(W))];
W := NewId();
Use Var(W) ::= QQ[t,x,y,z],PosTo;
[IsToPos(Var(W)),IsPosTo(Var(W))];
Use Var(NewId()) ::= QQ[x,y,z,t];
Vector(3*x)>Vector(x);
Vector(x+y)>Vector(1);
Vector(1)>Vector(0);
Vector(x)>Vector(x);
Vector(x,0)>Vector(0,x);
Vector(0,x)>Vector(x^2,0);
Vector(x)>Vector(x^2);
Vector(x,0)>Vector(0,x);
Vector(0,x)>Vector(x^2,0);
Vector(x)>Vector(x^2);
Vector(x+y,0)>Vector(y,0);
";

Test.ExpectedOutput := "[True, False]
-------------------------------
[False, True]
-------------------------------
ERROR: Cannot compare (expected terms)
CONTEXT: Vector(3 * x) > Vector(x)
-------------------------------
ERROR: Cannot compare (expected terms)
CONTEXT: Vector(x + y) > Vector(1)
-------------------------------
ERROR: Cannot compare (expected terms)
CONTEXT: Vector(1) > Vector(0)
-------------------------------
False
-------------------------------
True
-------------------------------
False
-------------------------------
False
-------------------------------
True
-------------------------------
False
-------------------------------
False
-------------------------------
ERROR: Cannot compare (expected terms)
CONTEXT: Vector(x + y,0) > Vector(y,0)
-------------------------------
";
TSL.RegisterTest(Test);



------------------------------
-- TEST 06 :
Test := Record[Id := "max_06", Descr := "LPos, LPP, E_ , FirstNonZero, FirstNonZeroPos"];
Test.Input :="Use Var(NewId()) ::= QQ[t,x,y,z],ToPos;
V:=Vector(0,0,x,x^2);
[LPos(V),LPP(V)];
E_(LPos(V),Len(V))*LPP(V)=LT(V);
LPP(x^2+x+1)=x^2;
Use Var(NewId()) ::= QQ[x,y,z,t,a],PosTo;
  V:=Vector(0,0,x,x^2);
  PrintLn [LPos(V),LPP(V)];
  PrintLn E_(LPos(V),Len(V))*LPP(V)=LT(V);
  W:=Vector(0,0,x+y,0,z^2);
  FirstNonZero(W);
  FirstNonZeroPos(W);
  FirstNonZero(Vector(1));
  FirstNonZero(Vector(0));
  FirstNonZeroPos(Vector(1));
  FirstNonZeroPos(Vector(0));
  FirstNonZeroPos(Vector(0,0));
  FirstNonZero(Vector(0,0));
  FirstNonZeroPos(Vector(0,x));
  FirstNonZero(Vector(0,x));
";
Test.ExpectedOutput := "[4, x^2]
-------------------------------
True
-------------------------------
True
-------------------------------
[3, x]

-------------------------------
True

-------------------------------
x + y
-------------------------------
3
-------------------------------
1
-------------------------------
ERROR: FirstNonZero: zero vector
CONTEXT: FirstNonZero(Vector(0))
-------------------------------
1
-------------------------------
ERROR: FirstNonZeroPos: zero vector
CONTEXT: FirstNonZeroPos(Vector(0))
-------------------------------
ERROR: FirstNonZeroPos: zero vector
CONTEXT: FirstNonZeroPos(Vector(0,0))
-------------------------------
ERROR: FirstNonZero: zero vector
CONTEXT: FirstNonZero(Vector(0,0))
-------------------------------
2
-------------------------------
x
-------------------------------
";
TSL.RegisterTest(Test);


------------------------------
-- TEST 07 :
Test := Record[Id := "max_07", Descr := "LM"];
Test.Input :="
Use Var(NewId()) ::= QQ[t,x,y,z],ToPos;
LM(0);
LM(Poly(0));
LM(x);
LM(3*x^2+y);
LM(Vector(1));
LM(Vector(0,0));
LM(Vector(0,x,y^2,5*x*y));
";
Test.ExpectedOutput := "ERROR: LM is not defined on 0
CONTEXT: Return(S)
-------------------------------
ERROR: LM is not defined on 0
CONTEXT: Return(S)
-------------------------------
x
-------------------------------
3x^2
-------------------------------
Vector(1)
-------------------------------
ERROR: LM is not defined on Vector(0, 0)
CONTEXT: Return(S)
-------------------------------
Vector(0, 0, 0, 5xy)
-------------------------------
";

TSL.RegisterTest(Test);

---------------------------------- NR and NF --------------------------
-- TEST 08 :
Test := Record[Id := "max_08", Descr := "NF/NR"];
Test.Input :="Use Var(NewId()) ::= QQ[t,x,y,z],ToPos;
NR(x,[]);
NR(Vector(x),[]);
NR(x,[x]);
NR(Vector(x),[Vector(x)]);
NR(x+y,[x-y,x+y]);
NR(Vector(x+y,x),[Vector(x-y,0),Vector(x+y,0)]);
NF(x+y,[x]);
NF(Vector(x+y),[Vector(x)]);
NF(x+y,Ideal(x-y,x+y));
NF(Vector(x+y,0),Module(Vector(x-y,0),Vector(x+y,0)));
NR(x,[t,x]);
";
Test.ExpectedOutput := "x
-------------------------------
Vector(x)
-------------------------------
0
-------------------------------
Vector(0)
-------------------------------
2y
-------------------------------
Vector(2y, x)
-------------------------------
ERROR: Bad parameters
CONTEXT: Return(ERR.BAD_PARAMS)
-------------------------------
ERROR: Bad parameters
CONTEXT: Return(ERR.BAD_PARAMS)
-------------------------------
0
-------------------------------
Vector(0, 0)
-------------------------------
0
-------------------------------
";
TSL.RegisterTest(Test);
