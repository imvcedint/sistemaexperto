-- $Id: toric.ts, 2000/05/04
-- Example/Test suite for toric ideals

Alias TSL := $ts/ts_lib;
TSL.Initialize();

-------------------------------
-- TEST 01 : PPI8

Test := Record[Id := "toric_01",Descr := "Primitive partition identities 8"];

Test.Input :="
Use QQ[x[1..8],y[1..8]];

PPI8 := [x[1]^I * y[I]  - y[1]^I * x[I] | I In 2..8];
BL := Toric(PPI8, [x[1]]);
Len(Gens(BL));
";

Test.ExpectedOutput :=
"578
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 02 : HPPI8

Test := Record[Id := "toric_02",Descr := "Homogeneous primitive partition identities 8"];

Test.Input :="
Use QQ[x[1..8],y[1..8]];

HPPI8 := [x[1]^I*x[I+2]*y[2]^(I+1) - y[1]^I*y[I+2]*x[2]^(I+1) | I In 1..6];
BL := Toric(HPPI8, [x[1],y[2]]);
Len(Gens(BL));
";

Test.ExpectedOutput :=
"340
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 03 : CG6

Test := Record[Id := "toric_03",Descr := "Complete Graph 6"];

Test.Input :="
Use QQ[x[1..21]];

CG6 :=
Mat([
  [1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0],
  [0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0],
  [0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0],
  [0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1],
  [0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1]
]);

BL := Toric(CG6);

BL = Ideal(-x[4]*x[6] + x[1]*x[11], -x[3]*x[11] + x[2]*x[13], x[7]*x[11] -
x[6]*x[13], -x[4]*x[6] + x[2]*x[8], -x[5]*x[6] + x[2]*x[9], x[4]*x[10] -
x[3]*x[11], -x[5]*x[11] + x[4]*x[12], -x[5]*x[13] + x[4]*x[14], x[5]*x[11] -
x[2]*x[15], -x[3]*x[8] + x[1]*x[13], x[4]*x[7] - x[3]*x[8], -x[3]*x[6] +
x[2]*x[7], x[8]*x[10] - x[6]*x[13], -x[3]*x[12] + x[2]*x[14], x[8]*x[12] -
x[6]*x[15], x[5]*x[7] - x[3]*x[9], x[5]*x[13] - x[3]*x[15], x[9]*x[13] -
x[7]*x[15], x[8]*x[14] - x[7]*x[15], x[7]*x[12] - x[6]*x[14], -x[3]*x[6] +
x[1]*x[10], x[9]*x[10] - x[6]*x[14], x[11]*x[14] - x[10]*x[15], x[9]*x[11] -
x[6]*x[15], x[4]*x[9] - x[1]*x[15], x[5]*x[10] - x[3]*x[12], -x[5]*x[6] +
x[1]*x[12], -x[3]*x[9] + x[1]*x[14], x[5]*x[8] - x[1]*x[15], x[12]*x[13] -
x[10]*x[15], -x[6]*x[13]*x[14] + x[7]*x[10]*x[15], -x[3]*x[11]*x[12] +
x[2]*x[10]*x[15], -x[3]*x[8]*x[9] + x[1]*x[7]*x[15], -x[4]*x[5]*x[6] +
x[1]*x[2]*x[15], x[3]*x[9]*x[12] - x[5]*x[6]*x[14], x[3]*x[8]*x[11] -
x[4]*x[6]*x[13]);
";

Test.ExpectedOutput :=
"True
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 04 : Toric(Ideal(xz-y^2, xt-yz));

Test := Record[Id := "toric_04",Descr := "minors"];

Test.Input :="
Use QQ[x,y,z,t];
Toric(Ideal(x*z-y^2, x*t-y*z));
";

Test.ExpectedOutput :=
"Ideal(-y^2 + xz, -yz + xt, z^2 - yt)
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 05 : Random

Test := Record[Id := "toric_05", Descr := "random"];

Test.Input :="
Use QQ[x,y,z,t];
Ideal(-y^2 + x*z, -x^2*y + t) =
Toric([[1,2,3,4],[1,1,1,3]]);
";

Test.ExpectedOutput :=
"True
-------------------------------
";
TSL.RegisterTest(Test);


--------------------------------------------------------------
-- Example/Test suite for integer programming

    Alias IP := $contrib/intprog; -- proposed alias

-------------------------------
-- TEST 06 : LiGuo_TestSet

Test := Record[Id := "toric_06",Descr := "LiGuo_TestSet"];

Test.Input :="
    A := Mat([
      [3, 1, 11, 2, 3, 5, 3],
      [4, 5,  0, 1, 7, 4, 6],
      [5, 6,  1, 9, 2, 3, 3]]);
    B := [31, 27, 38];
    Cost := [23, 15, 6, 7, 1, 53, 4];

    IP_TS := IP.TestSet(A, Cost); Len(IP_TS.TestSet);
";

Test.ExpectedOutput :=
"31
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 07 : LiGuo_IntProg

Test := Record[Id := "toric_07",Descr := "LiGuo_IntProg"];

Test.Input :="
    A := Mat([
      [3, 1, 11, 2, 3, 5, 3],
      [4, 5,  0, 1, 7, 4, 6],
      [5, 6,  1, 9, 2, 3, 3]]);
    B := [31, 27, 38];
    Cost := [23, 15, 6, 7, 1, 53, 4];

    IP.Sol(A, B, Cost);
";

Test.ExpectedOutput :=
"Record[MinCost := 83, MinSol := [0, 4, 2, 1, 0, 0, 1]]
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 08 : LiGuo_IntProg2

Test := Record[Id := "toric_08", Descr := "LiGuo_IntProg2"];

Test.Input :="
    A := Mat([
      [3, 1, 11, 2, 3, 5, 3],
      [4, 5,  0, 1, 7, 4, 6],
      [5, 6,  1, 9, 2, 3, 3]]);
    B := [31, 27, 38];
    Cost := [23, 15, 6, 7, 1, 53, 4];

    IP_TS := IP.TestSet(A, Cost);
    IP.Sol(IP_TS, B, Cost);
";

Test.ExpectedOutput :=
"Record[MinCost := 83, MinSol := [0, 4, 2, 1, 0, 0, 1]]
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 09 : Rnd412

Test := Record[Id := "toric_09", Descr := "Rnd412"];

Test.Input :="
Rnd412a := Mat([
  [0, 1, 2, 3, 1, 0, 1, 0, 0, 1, 2, 1],
  [1, 3, 3, 0, 0, 1, 0, 3, 1, 3, 0, 0],
  [3, 1, 1, 2, 2, 0, 1, 3, 2, 2, 0, 3],
  [1, 0, 3, 1, 0, 3, 0, 1, 2, 2, 0, 2]]);

A := Rnd412a;

TR := Transposed(A);
B := 5*TR[1] + 4*TR[4] + 10*TR[5] + TR[8] + TR[9];
Cost := [ 1,1,1,1, 1,1,1,1, 1,1,1,1 ];
IP.Sol(A, B, Cost);
Cost := [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37];
IP.Sol(A, B, Cost);
";

Test.ExpectedOutput :=
"Record[MinCost := 21, MinSol := [3, 0, 0, 3, 10, 0, 1, 2, 0, 0, 0, 2]]
-------------------------------
Record[MinCost := 305, MinSol := [9, 0, 0, 1, 8, 0, 0, 0, 0, 0, 5, 1]]
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 10 : Wrong Inputs

Test := Record[Id := "toric_10", Descr := "Wrong Inputs"];

Test.Input :="
E := NewList(6);
Catch BinToric([x-y], [x*y])       In E[1] EndCatch;
Catch BinToric([x-y], [y, 1])      In E[2] EndCatch;
Catch BinToric([True], [])         In E[3] EndCatch;
Catch BinToric([x-y,8], [])        In E[4] EndCatch;
Catch BinToric([x-y,Ideal(0)], []) In E[5] EndCatch;
Catch BinToric([x^2-y], [y, x])    In E[6] EndCatch;
[ Type(X) | X In E ];
";

Test.ExpectedOutput :=
"[ERROR, ERROR, ERROR, ERROR, ERROR, ERROR]
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 11 : Particular Cases
Test := Record[Id := "toric_11", Descr := "Particular Cases"];

Test.Input :="
Use QQ[x,y,z];
Toric([[1,1], [1,2]]);
BinToric([x-x], []);
Toric([x-y, 0, x-z]);
BinToric([x-y], [y, 0]);
";

Test.ExpectedOutput :=
"Ideal(0)
-------------------------------
[ ]
-------------------------------
Ideal(-x + y, x - z)
-------------------------------
[x - y]
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 12 : Lawrence
Test := Record[Id := "toric_12", Descr := "HPPI8 - Lawrence input"];

Test.Input :="
Use QQ[x[1..8],y[1..8]];
M := Mat([[1,1,1,1,1,1,1,1],[1,2,3,4,5,6,7,8]]);
T := Toric(Toric.LawrenceMat(M));
Len(T);
";

Test.ExpectedOutput :=
"340
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 13 : Permutation Matrix
Test := Record[Id := "toric_13", Descr := "4x4 Permutation Matrix"];

Test.Input :="
P := 4;
Use ZZ/(2)[x[1..P,1..P]];
M := Mat([ [ x[I,J] | J In 1..P ] | I In 1..P ] );
M := Mat([ Log(T) | T In Monomials(Det(M)) ]);
Use ZZ/(2)[x[1..P,1..P,1..P,1..P]];
T := Toric(Transposed(M));
T := GBasis(T);  -- hard computation
Len([ X In T | Deg(X)=2 ]);
Len([ X In T | Deg(X)=3 ]);
Len([ X In T | Deg(X)=4 ]);
";

Test.ExpectedOutput :=
"18
-------------------------------
176
-------------------------------
5
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 14 : Veronese Type Examples
Test := Record[Id := "toric_14", Descr := "Veronese Type Examples"];

Test.Input :="
Define VTE(D,R,S)
  MyIndets := First(Indets(),D);
  M := [ First(Log(T),D) | T In Gens(Ideal(MyIndets)^R)
                        And  Max([Deg(GCD(T,X^R))| X In MyIndets])<=S];
  Return Mat(M)
EndDefine;

Use ZZ/(2)[x[1..16]];
T := Toric(Transposed(VTE(4,3,2)));  /*  Sturmfels Prop 14.4  */
T =
Ideal(x[5]^2 + x[4]*x[7], x[7]*x[9] + x[2]*x[16], x[6]^2 + x[3]*x[11],
x[5]*x[6] + x[4]*x[8], x[6]^2 + x[4]*x[9], x[5]*x[10] + x[4]*x[12],
x[6]*x[12] + x[5]*x[13], x[7]*x[11] + x[5]*x[13], x[8]*x[11] + x[6]*x[13],
x[9]*x[11] + x[6]*x[14], x[2]*x[5] + x[1]*x[7], x[5]*x[7] + x[2]*x[12],
x[3]*x[12] + x[2]*x[13], x[8]*x[12] + x[7]*x[13], x[5]*x[13] + x[4]*x[15],
x[7]*x[10] + x[5]*x[12], x[13]^2 + x[12]*x[14], x[8]*x[12] + x[5]*x[15],
x[6]*x[8] + x[5]*x[9], x[6]^2 + x[1]*x[14], x[6]*x[11] + x[4]*x[14],
x[4]*x[8] + x[2]*x[11], x[7]*x[14] + x[6]*x[15], x[6]*x[8] + x[2]*x[14],
x[2]*x[6] + x[1]*x[8], x[13]^2 + x[11]*x[15], x[4]*x[8] + x[3]*x[10],
x[3]*x[5] + x[1]*x[8], x[3]*x[7] + x[2]*x[8], x[3]*x[12] + x[1]*x[15],
x[3]*x[8] + x[2]*x[9], x[5]^2 + x[2]*x[10], x[8]*x[13] + x[7]*x[14],
x[6]*x[10] + x[4]*x[13], x[8]*x[11] + x[4]*x[16], x[2]*x[4] + x[1]*x[5],
x[6]*x[8] + x[3]*x[13], x[8]^2 + x[3]*x[15], x[8]^2 + x[7]*x[9], x[3]*x[4]
+ x[1]*x[6], x[6]*x[10] + x[5]*x[11], x[8]*x[11] + x[5]*x[14], x[8]*x[10] +
x[5]*x[13], x[9]*x[12] + x[8]*x[13], x[8]*x[13] + x[5]*x[16], x[4]*x[6] +
x[1]*x[11], x[4]*x[8] + x[1]*x[13], x[5]^2 + x[1]*x[12], x[6]*x[7] +
x[5]*x[8], x[8]*x[9] + x[3]*x[16], x[9]*x[15] + x[8]*x[16], x[13]*x[15] +
x[12]*x[16], x[14]*x[15] + x[13]*x[16], x[8]*x[15] + x[7]*x[16], x[9]*x[13]
+ x[6]*x[16], x[13]*x[14] + x[11]*x[16], x[9]*x[10] + x[8]*x[11], x[13]^2 +
x[10]*x[16], x[11]*x[13] + x[10]*x[14], x[9]*x[13] + x[8]*x[14], x[6]*x[9] +
x[3]*x[14], x[3]*x[6] + x[1]*x[9], x[11]*x[12] + x[10]*x[13], x[12]*x[13] +
x[10]*x[15], x[4]*x[5] + x[1]*x[10], x[6]*x[8] + x[1]*x[16], x[5]*x[8] +
x[3]*x[12], x[7]*x[8] + x[2]*x[15], x[4]*x[8]*x[10] + x[5]^2*x[11],
x[8]*x[10]^2 + x[5]*x[11]*x[12]);
";

Test.ExpectedOutput :=
"True
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 20 : Rnd612a
Rnd612a := Mat([
  [0, 1, 2, 3, 1, 0, 1, 0, 0, 1, 2, 1],
  [1, 3, 3, 0, 0, 1, 0, 3, 1, 3, 0, 0],
  [3, 1, 1, 2, 2, 0, 1, 3, 2, 2, 0, 3],
  [2, 3, 2, 1, 2, 2, 1, 2, 0, 0, 1, 1],
  [3, 0, 0, 1, 2, 0, 3, 0, 0, 3, 2, 1],
  [1, 0, 3, 1, 0, 3, 0, 1, 2, 2, 0, 2]]);

-------------------------------
-- TEST 21 : Rnd612b
Rnd612b := Mat([
  [3, 1, 1, 1, 0, 2, 1, 3, 2, 2, 2, 1],
  [2, 1, 3, 0, 2, 3, 2, 1, 3, 3, 0, 3],
  [0, 3, 3, 1, 0, 2, 0, 2, 3, 1, 3, 1],
  [3, 2, 0, 0, 3, 0, 0, 1, 2, 3, 0, 2],
  [0, 2, 1, 2, 3, 0, 2, 2, 3, 2, 0, 3],
  [0, 3, 2, 3, 3, 1, 2, 1, 2, 2, 2, 1]]);

-------------------------------
-- TEST 22 : Rnd612c
Rnd612c := Mat([
  [0, 1, 0, 2, 3, 0, 1, 1, 1, 2, 3, 3],
  [1, 1, 2, 1, 3, 1, 3, 3, 3, 0, 0, 1],
  [2, 2, 2, 1, 1, 0, 3, 1, 1, 1, 2, 3],
  [3, 1, 1, 0, 1, 3, 1, 2, 1, 0, 2, 0],
  [1, 0, 3, 1, 1, 3, 2, 1, 1, 2, 3, 1],
  [2, 1, 0, 0, 3, 1, 1, 2, 0, 1, 3, 3]]);

-------------------------------
-- TEST 23 : Rnd510
Rnd510 := Mat([
  [1, 4, 9, 5, 8, 7, 3, 3, 6, 7],
  [1, 6, 2, 4, 4, 1, 2, 4, 0, 4],
  [4, 3, 4, 4, 3, 6, 6, 5, 2, 9],
  [3, 1, 9, 6, 9, 7, 1, 9, 2, 9],
  [9, 7, 0, 6, 3, 9, 2, 0, 8, 4]]);

-------------------------------
-- TEST 24 : Rnd511
Rnd511 := Mat([
  [6, 5, 8, 1, 3, 5, 9, 8, 4, 7, 6],
  [8, 6, 1, 3, 2, 9, 1, 8, 1, 9, 7],
  [4, 3, 4, 4, 4, 8, 2, 4, 6, 5, 9],
  [0, 3, 0, 5, 8, 8, 0, 7, 2, 2, 4],
  [0, 4, 3, 6, 1, 8, 7, 8, 0, 9, 9]]);
