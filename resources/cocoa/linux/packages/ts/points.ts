-- Example/Test suite for the Buchberger-Moeller package in CoCoA.

Alias TSL := $ts/ts_lib;
TSL.Initialize();

Define IdealOfAffinePoints(P)
  Return $points.IdealOfPoints(P);
EndDefine;
Define SeparatorsOfAffinePoints(P)
  Return $points.SeparatorsOfPoints(P);
EndDefine;
Define IdealOfProjectivePoints(P)
  Return $points.IdealOfProjectivePoints(P);
EndDefine;
Define SeparatorsOfProjectivePoints(P)
  Return $points.SeparatorsOfProjectivePoints(P);
EndDefine;

Define BMTest(Id, Points, Affine_or_Projective);
-- PrintLn "Conducting test ", Id;
  If Affine_or_Projective = "Affine" Then
    GB := Gens(IdealOfAffinePoints(Points));
    S := SeparatorsOfAffinePoints(Points);
    EvalGB := [[Eval(F, P) | P In Points] | F In GB];
    EvalGB := MakeSet(EvalGB);
    If Len(EvalGB) <> 1 Or MakeSet(EvalGB[1]) <> [0] Then
      PrintLn "Test ", Id, ": Affine GBasis incorrect.";
    EndIf;
    If Len(S) <> Len(Points) Then
      PrintLn "Test ", Id, ": wrong number of affine separators";
      Return;
    EndIf;
    EvalS := [[Eval(F, P) | P In Points] | F In S];
    For I := 1 To Len(EvalS) Do
      For J := 1 To Len(EvalS[I]) Do
        If (I = J And EvalS[I][J] <> 1) Or (I <> J And EvalS[I][J] <> 0) Then
          PrintLn "Test ", Id, ": affine separator incorrect (",I,",",J,").";
        EndIf;
      EndFor;
    EndFor;
    One := Sum(S);
    If One <> 1 Then
      PrintLn "Test ", Id, ": sum of affine separators not equal to 1.";
    EndIf;
  Else -- Projective case
    I := IdealOfProjectivePoints(Points);
    GB := Gens(I);
    -- don't do test below if there is only 1 point, o/w hit bug in CoCoA.
    If Len(Points) > 1 And Not EqSet(GB, ReducedGBasis(I)) Then
      PrintLn "Test", Id, ": generators are not a GBasis";
    EndIf;
    S := SeparatorsOfProjectivePoints(Points);
    If Characteristic() = 2 Then Scale := 1 Else Scale := 2; EndIf;
    EvalGB := [[Eval(F, Scale*P) | P In Points] | F In GB];
    EvalGB := MakeSet(EvalGB);
    If Len(EvalGB) <> 1 Or MakeSet(EvalGB[1]) <> [0] Then
      PrintLn "Test ", Id, ": projective GBasis incorrect.";
    EndIf;
    If Len(S) <> Len(Points) Then
      PrintLn "Test ", Id, ": wrong number of projective separators";
      Return;
    EndIf;
    EvalS := [[Eval(F, Scale*P) | P In Points] | F In S];
    For I := 1 To Len(EvalS) Do
      For J := 1 To Len(EvalS[I]) Do
        If (I = J And EvalS[I][J] = 0) Or (I <> J And EvalS[I][J] <> 0) Then
          PrintLn "Test ", Id, ": projective separator incorrect (",I,",",J,").";
        EndIf;
      EndFor;
    EndFor;
  EndIf;
EndDefine;

-------------------------------
-- TEST 01 : Buchberger-Moeller

Test := Record[Id := "points_01",Descr := "Buchberger-Moeller HARD tests"];

Test.Input :="


-- Funny structure below to workaround numerous bugs (I mean \"features\" :-)
-- of CoCoA.  Some day this may be unnecessary...
BuchbergerMoellerRing ::= QQ[x[0..9]];
Using BuchbergerMoellerRing Do
  BMTest(1, [[0]], \"Affine\");
  BMTest(2, [[123]], \"Affine\");
  BMTest(3, [[-456]], \"Affine\");
  BMTest(4, [[-314],[-159],[-265]], \"Affine\");
  BMTest(5, [[314],[159],[265]], \"Affine\");
  BMTest(6, [[314],[-159],[265]], \"Affine\");
  BMTest(7, [[314],[159],[-265]], \"Affine\");
  BMTest(8, [[3,1,4],[1,5,9],[2,6,5]], \"Affine\");
  BMTest(9, [[1,1,1],[2,2,2],[3,3,3]], \"Affine\");
  BMTest(10, [[1,1,1],[0,0,0],[-1,-1,-1]], \"Affine\");
  BMTest(11, [[1,1,1],[1,2,4],[1,3,9],[1,4,16]], \"Affine\");
  BMTest(12, [[1,1,16],[1,2,8],[1,4,4],[1,8,2],[1,16,1],[2,1,8],[2,2,4],[2,4,2],[2,8,1],[4,1,4],[4,2,2],[4,4,1],[8,1,2],[8,2,1],[16,1,1]], \"Affine\");
  BMTest(13, [[1/2,1/3,1/4],[1,-2/5,4/7],[-1/11,3/13,9],[1,4,16]], \"Affine\");
EndUsing;
BuchbergerMoellerRing ::= QQ[x[0..9]],Lex;
Using BuchbergerMoellerRing Do
  BMTest(14, [[1,1,1],[1,2,4],[1,3,9],[1,4,16]], \"Affine\");
  BMTest(15, [[1/2,1/3,1/4],[1,-2/5,4/7],[-1/11,3/13,9],[1,4,16]], \"Affine\");
  BMTest(16, [[1,1,16],[1,2,8],[1,4,4],[1,8,2],[1,16,1],[2,1,8],[2,2,4],[2,4,2],[2,8,1],[4,1,4],[4,2,2],[4,4,1],[8,1,2],[8,2,1],[16,1,1]], \"Affine\");
EndUsing;

BuchbergerMoellerRing ::= ZZ/(17)[x[0..9]];
Using BuchbergerMoellerRing Do
  BMTest(100, [[0]], \"Affine\");
  BMTest(101, [[123]], \"Affine\");
  BMTest(102, [[-456]], \"Affine\");
  BMTest(103, [[-314],[-159],[-265]], \"Affine\");
  BMTest(104, [[314],[159],[265]], \"Affine\");
  BMTest(105, [[314],[-159],[265]], \"Affine\");
  BMTest(106, [[314],[159],[-265]], \"Affine\");
  BMTest(107, [[3,1,4],[1,5,9],[2,6,5]], \"Affine\");
  BMTest(108, [[1,1,1],[2,2,2],[3,3,3]], \"Affine\");
  BMTest(109, [[1,1,1],[0,0,0],[-1,-1,-1]], \"Affine\");
  BMTest(110, [[1,1,1],[1,2,4],[1,3,9],[1,4,16]], \"Affine\");
  BMTest(111, [[1,1,16],[1,2,8],[1,4,4],[1,8,2],[1,16,1],[2,1,8],[2,2,4],[2,4,2],[2,8,1],[4,1,4],[4,2,2],[4,4,1],[8,1,2],[8,2,1],[16,1,1]], \"Affine\");
EndUsing;

BuchbergerMoellerRing ::= QQ[x[0..9]];
Using BuchbergerMoellerRing Do
  BMTest(201,[[1]], \"Projective\");
  BMTest(202,[[-123]], \"Projective\");
  BMTest(203,[[1,0,0],[0,1,0],[0,0,1]], \"Projective\");
  BMTest(204,[[1,1,0],[1,0,1],[0,1,1]], \"Projective\");
  BMTest(205,[[1,1,0],[1,2,0],[1,3,0]], \"Projective\");
  BMTest(206,[[1,0],[0,1],[1,1],[2,1],[3,1],[4,1]], \"Projective\");
  BMTest(207,[[0,0,1],[0,1,0],[0,1,1],[0,1,2],[1,0,0],[1,0,1],[1,0,2],
              [1,1,0],[1,1,1],[1,1,2],[1,2,0],[1,2,1],[1,2,2]], \"Projective\");
  BMTest(208, [[0,0,1],[0,1,1],[1,1,1],[1,0,1]], \"Projective\");
  BMTest(209, [[4,6,7],[6,2,9],[0,3,8],[7,6,2]], \"Projective\");
  BMTest(210, [[2,2,1],[4,3,2],[1,2,4],[4,0,3],[0,2,2],[4,2,4],[3,1,3]],
         \"Projective\");
  BMTest(211, [[2^I,3^I,5^I,1]| I In [512, 1024]], \"Projective\");
EndUsing;
BuchbergerMoellerRing ::= QQ[x,y,z],DegLex;
Using BuchbergerMoellerRing Do
  BMTest(212,[[4,1,1],[5,2,-1],[85,4,1],[455,8,-1],[4369,16,1]], \"Projective\");
  BMTest(213,[[4/3,1/7,-1/7],[5,2/99,-1],[85/97,-4/101,1/4],
              [455/311,-8/75,-1/2],[4369/8,16,1]], \"Projective\");
  BMTest(214, [[1, Fact(200), 1],[2,2*Fact(200),1]], \"Projective\");
EndUsing;
BuchbergerMoellerRing ::= ZZ/(5)[x[0..9]];
Using BuchbergerMoellerRing Do
  BMTest(300,[[1]], \"Projective\");
  BMTest(301,[[-123]], \"Projective\");
  BMTest(302,[[1,0,0],[0,1,0],[0,0,1]], \"Projective\");
  BMTest(303,[[1,1,0],[1,0,1],[0,1,1]], \"Projective\");
  BMTest(304,[[1,1,0],[1,2,0],[1,3,0]], \"Projective\");
  BMTest(305,[[1,0],[0,1],[1,1],[2,1],[3,1],[4,1]], \"Projective\");
  BMTest(306,[[0,0,1],[0,1,1],[1,1,1],[1,0,1]], \"Projective\");
EndUsing;
BuchbergerMoellerRing ::= ZZ/(3)[x[0..9]];
Using BuchbergerMoellerRing Do
  BMTest(307,[[0,0,1],[0,1,0],[0,1,1],[0,1,2],[1,0,0],[1,0,1],[1,0,2],
              [1,1,0],[1,1,1],[1,1,2],[1,2,0],[1,2,1],[1,2,2]], \"Projective\");
EndUsing;
";
Test.ExpectedOutput := "";

TSL.RegisterTest(Test);

-------------------------------
-- TEST 02 : Buchberger-Moeller

Test := Record[Id := "points_02",Descr := "Buchberger-Moeller SMALL tests"];

Test.Input :="

-- Funny structure below to workaround numerous bugs (I mean \"features\" :-)
-- of CoCoA.  Some day this may be unnecessary...
BuchbergerMoellerRing ::= QQ[x[0..9]];
Using BuchbergerMoellerRing Do
  BMTest(13, [[1/2,1/3,1/4],[1,-2/5,4/7],[-1/11,3/13,9],[1,4,16]], \"Affine\");
EndUsing;

BuchbergerMoellerRing ::= QQ[x[0..9]],Lex;
Using BuchbergerMoellerRing Do
  BMTest(15, [[1/2,1/3,1/4],[1,-2/5,4/7],[-1/11,3/13,9],[1,4,16]], \"Affine\");
EndUsing;

BuchbergerMoellerRing ::= ZZ/(17)[x[0..9]];
Using BuchbergerMoellerRing Do
  BMTest(110, [[1,1,1],[1,2,4],[1,3,9],[1,4,16]], \"Affine\");
EndUsing;

BuchbergerMoellerRing ::= QQ[x[0..9]];
Using BuchbergerMoellerRing Do
  BMTest(210, [[2,2,1],[4,3,2],[1,2,4],[4,0,3],[0,2,2],[4,2,4],[3,1,3]],
         \"Projective\");
EndUsing;
BuchbergerMoellerRing ::= QQ[x,y,z],DegLex;
Using BuchbergerMoellerRing Do
  BMTest(213,[[4/3,1/7,-1/7],[5,2/99,-1],[85/97,-4/101,1/4],
              [455/311,-8/75,-1/2],[4369/8,16,1]], \"Projective\");
EndUsing;
BuchbergerMoellerRing ::= ZZ/(5)[x[0..9]];
Using BuchbergerMoellerRing Do
  BMTest(306,[[0,0,1],[0,1,1],[1,1,1],[1,0,1]], \"Projective\");
EndUsing;
BuchbergerMoellerRing ::= ZZ/(3)[x[0..9]];
Using BuchbergerMoellerRing Do
  BMTest(307,[[0,0,1],[0,1,0],[0,1,1],[0,1,2],[1,0,0],[1,0,1],[1,0,2],
              [1,1,0],[1,1,1],[1,1,2],[1,2,0],[1,2,1],[1,2,2]], \"Projective\");
EndUsing;
";
Test.ExpectedOutput := "";

TSL.RegisterTest(Test);
