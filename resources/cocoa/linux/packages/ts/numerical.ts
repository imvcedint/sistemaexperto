-- Test Suite: tests the various ABVI functions implemented in CoCoALib
-- 0.98.1 and higher. Test cases initially written by Daniel Heldt, turned
-- into proper tests by Michael Abshoff.
-- Revision: March 27th, 2007

Alias TSL := $ts/ts_lib;
TSL.Initialize();
/*
 -- To be added to all.ts --
Print "numerical5.ts";
$cocoa5.Initialize();
If Not IsServerReady() Then
  PrintLn "CoCoAServer is not reachable: skipping all cocoa5 tests";
Else
  Source CocoaPackagePath()+"/ts/numerical5.ts";
  EnableTime := MEMORY.PKG.CoCoA5.EnableTime;
  MEMORY.PKG.CoCoA5.EnableTime := FALSE;  -- To avoid printing CpuTime
//  TSL.Deselect("....");  -- Not yet implemented
  TSL.RunTests();  // was  TSL.Do();
  MEMORY.PKG.CoCoA5.EnableTime := EnableTime;
EndIf;

-- In case one test fails debug like this:
Source CocoaPackagePath()+"/ts/cocoa5.ts";
$cocoa5.Initialize();
MEMORY.PKG.CoCoA5.PrintOnPath := GetEnv("HOME")+"/tmp";
TSL.Do("cocoa5-01");
-- Then run CoCoAServer -t < ~tmp/CoCoA4Request.cocoa5
-- In gdb:  break CoCoA::error
*/

-------------------------------
-- TEST 01 :
Test := Record[Id := "numerical_01", Descr := "GBasisOfPoints5, without OrderIdeal"];
Test.Input :="MEMORY.PKG.CoCoA5.EnableTime := False;
Use RRR::=QQ[x,y];
Points := [[1.002,0],[0.998,0],[0,1],[-1,0],[0,-1]];
M := Mat(Points);
Result := Numerical.GBasisOfPoints5(M,0.001, False);
If Not(Len(Result) = 3) Then
  Print(\"Failed.\");
Else
  Result := Numerical.GBasisOfPoints5(M,0.01, False);
  If Not(Len(Result) = 3) Then
    Print(\"Failed.\");
  Else
    If Not(Len([P | P In Result And (1 IsIn Support(P)) And (x^2 IsIn Support(P)) And (y^2 IsIn Support(P))] )>0) Then
      Print(\"Failed.\");
    Else
      Print(\"Passed.\");
    EndIf;
  EndIf;
EndIf;";

Test.ExpectedOutput := "Passed.
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 02:
Test := Record[Id := "numerical_02", Descr := "GBasisOfPoints5, with OrderIdeal"];
Test.Input :="MEMORY.PKG.CoCoA5.EnableTime := False;
Use RRR::=QQ[x,y];
Points := [[1.002,0],[0.998,0],[0,1],[-1,0],[0,-1]];
M := Mat(Points);
Result := Numerical.GBasisOfPoints5(M,0.001, True);
If Not(Len(Result[1]) = 3) Then
  Print(\"Failed.\");
Else
  If Not(Len(Result[2]) = 5) Then
    Print(\"Failed.\");
  Else
    Result := Numerical.GBasisOfPoints5(M,0.01,True);
    If Not(Len(Result[1]) = 3) Then
      Print(\"Failed.\");
    Else
      If Not(Len(Result[2]) = 4) Then
        Print(\"Failed.\");
      Else
        If Not(Len([P | P In Result[1] And (1 IsIn Support(P)) And (x^2 IsIn Support(P)) And (y^2 IsIn Support(P))] )>0) Then
          Print(\"Failed.\");
        Else
          Print(\"Passed.\");
        EndIf;
      EndIf;
    EndIf;
  EndIf;
EndIf;";

Test.ExpectedOutput := "Passed.
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 03:
Test := Record[Id := "numerical_03", Descr := "BBasisOfPoints5, without OrderIdeal"];
Test.Input :="MEMORY.PKG.CoCoA5.EnableTime := False;
Use RRR::=QQ[x,y];
Points := [[1.002,0],[0.998,0],[0,1],[-1,0],[0,-1]];
M := Mat(Points);
Result := Numerical.BBasisOfPoints5(M,0.001, False);
If Not(Len(Result) = 5) Then
  Print(\"Failed.\");
Else
  Result := Numerical.BBasisOfPoints5(M,0.01, False);
  If Not(Len(Result) = 4 ) Then
    Print(\"Failed.\");
  Else
    If Not(Len([P | P In Result And (1 IsIn Support(P)) And (x^2 IsIn Support(P)) And (y^2 IsIn Support(P))] )>0) Then
      Print(\"Failed.\");
    Else
      Print(\"Passed.\");
    EndIf;
  EndIf;
EndIf;";

Test.ExpectedOutput := "Passed.
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 04:
Test := Record[Id := "numerical_04", Descr := "BBasisOfPoints5, with OrderIdeal"];
Test.Input :="MEMORY.PKG.CoCoA5.EnableTime := False;
Use RRR::=QQ[x,y];
Points := [[1.002,0],[0.998,0],[0,1],[-1,0],[0,-1]];
M := Mat(Points);
Result := Numerical.BBasisOfPoints5(M,0.001, True);
If Not(Len(Result[1]) = 5) Then
  Print(\"Failed.\");
Else
  If Not(Len(Result[2]) = 5) Then
    Print(\"Failed.\");
  Else
    Result := Numerical.BBasisOfPoints5(M,0.01,True);
    If Not(Len(Result[1]) = 4) Then
      Print(\"Failed.\");
    Else
      If Not(Len(Result[2]) = 4) Then
        Print(\"Failed.\");
      Else
        If Not(Len([P | P In Result[1] And (1 IsIn Support(P)) And (x^2 IsIn Support(P)) And (y^2 IsIn Support(P))] )>0) Then
          Print(\"Failed.\");
        Else
          Print(\"Passed.\");
        EndIf;
      EndIf;
    EndIf;
  EndIf;
EndIf;";

Test.ExpectedOutput := "Passed.
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 05:
Test := Record[Id := "numerical_05", Descr := "FirstVanishingRelations5, without OrderIdeal"];
Test.Input :="MEMORY.PKG.CoCoA5.EnableTime := False;
Use RRR::=QQ[x,y];
Points := [[1.002,0],[0.998,0],[0,1],[-1,0],[0,-1]];
M := Mat(Points);
Result := Numerical.FirstVanishingRelations5(M,0.001, False);
If Not(Len(Result) = 1) Then
  Print(\"Failed.\");
Else
  Result := Numerical.FirstVanishingRelations5(M,0.01, False);
  If Not(Len(Result) = 2) Then
    Print(\"Failed.\");
  Else
    If Not(Len([P | P In Result And (1 IsIn Support(P)) And (x^2 IsIn Support(P)) And (y^2 IsIn Support(P))] )>0) Then
      Print(\"Failed.\");
    Else
      Print(\"Passed.\");
    EndIf;
  EndIf;
EndIf;";

Test.ExpectedOutput := "Passed.
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 06:
Test := Record[Id := "numerical_06", Descr := "FirstVanishingRelations, with OrderIdeal"];
Test.Input :="MEMORY.PKG.CoCoA5.EnableTime := False;
Use RRR::=QQ[x,y];
Points := [[1.002,0],[0.998,0],[0,1],[-1,0],[0,-1]];
M := Mat(Points);
Result := Numerical.FirstVanishingRelations5(M,0.001, True);
If Not(Len(Result[1]) = 1) Then
  Print(\"Failed.\");
Else
  If Not(Len(Result[2]) = 3) Then
    Print(\"Failed.\");
  Else
    Result := Numerical.FirstVanishingRelations5(M,0.01,True);
    If Not(Len(Result[1]) = 2) Then
      Print(\"Failed.\");
    Else
      If Not(Len(Result[2]) = 3) Then
        Print(\"Failed.\");
      Else
        If Not(Len([P | P In Result[1] And (1 IsIn Support(P)) And (x^2 IsIn Support(P)) And (y^2 IsIn Support(P))] )>0) Then
          Print(\"Failed.\");
        Else
          Print(\"Passed.\");
        EndIf;
      EndIf;
    EndIf;
  EndIf;
EndIf;";

Test.ExpectedOutput := "Passed.
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 07:
Test := Record[Id := "numerical_07", Descr := "GBasisOfPointsInIdeal5, without OrderIdeal"];
Test.Input :="MEMORY.PKG.CoCoA5.EnableTime := False;
Use RRR::=QQ[x,y,z];
Points := [[1.002,0,1],[0.998,0,1],[0,1,1],[-1,0,1],[0,-1,1]];
M := Mat(Points);
Result := Numerical.GBasisOfPointsInIdeal5(M,0.001, False,[z]);
If Not(Len(Result) = 4) Then
  Print(\"Failed.\");
Else
  Result := Numerical.GBasisOfPointsInIdeal5(M,0.01, False,[z]);
  If Not(Len(Result) = 4) Then
    Print(\"Failed.\");
  Else
    If Not(Len([P | P In Result And (z IsIn Support(P)) And (zx^2 IsIn Support(P)) And (zy^2 IsIn Support(P))] )>0) Then
      Print(\"Failed.\");
    Else
      Print(\"Passed.\");
    EndIf;
  EndIf;
EndIf;";

Test.ExpectedOutput := "Passed.
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 08:
Test := Record[Id := "numerical_08", Descr := "GBasisOfPointsInIdeal5, with OrderIdeal"];
Test.Input :="MEMORY.PKG.CoCoA5.EnableTime := False;
Use RRR::=QQ[x,y,z];
Points := [[1.002,0,1],[0.998,0,1],[0,1,1],[-1,0,1],[0,-1,1]];
M := Mat(Points);
Result := Numerical.GBasisOfPointsInIdeal5(M,0.001, True,[z]);
If Not(Len(Result[1]) = 4) Then
  Print(\"Failed.\");
Else
  If Not(Len(Result[2]) = 5) Then
    Print(\"Failed.\");
  Else
    Result := Numerical.GBasisOfPointsInIdeal5(M,0.01,True,[z]);
    If Not(Len(Result[1]) = 4) Then
      Print(\"Failed.\");
    Else
      If Not(Len(Result[2]) = 4) Then
        Print(\"Failed.\");
      Else
        If Not(Len([P | P In Result[1] And (z IsIn Support(P)) And (zx^2 IsIn Support(P)) And (zy^2 IsIn Support(P))] )>0) Then
          Print(\"Failed.\");
        Else
          Print(\"Passed.\");
        EndIf;
      EndIf;
    EndIf;
  EndIf;
EndIf;";

Test.ExpectedOutput := "Passed.
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 09:
Test := Record[Id := "numerical_09", Descr := "BBasisOfPointsInIdeal5, without OrderIdeal"];
Test.Input :="MEMORY.PKG.CoCoA5.EnableTime := False;
Use RRR::=QQ[x,y,z];
Points := [[1.002,0,1],[0.998,0,1],[0,1,1],[-1,0,1],[0,-1,1]];
M := Mat(Points);
Result := Numerical.BBasisOfPointsInIdeal5(M,0.001, False, [z]);
If Not(Len(Result) = 10) Then
  Print(\"Failed.\");
Else
  Result := Numerical.BBasisOfPointsInIdeal5(M,0.01, False, [z]);
  If Not(Len(Result) = 8 ) Then
    Print(\"Failed.\");
  Else
    If Not(Len([P | P In Result And (z IsIn Support(P)) And (zx^2 IsIn Support(P)) And (zy^2 IsIn Support(P))] )>0) Then
      Print(\"Failed.\");
    Else
      Print(\"Passed.\");
    EndIf;
  EndIf;
EndIf;";

Test.ExpectedOutput := "Passed.
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 10:
Test := Record[Id := "numerical_10", Descr := "BBasisOfPointsInIdeal5, with OrderIdeal"];
Test.Input :="MEMORY.PKG.CoCoA5.EnableTime := False;
Use RRR::=QQ[x,y,z];
Points := [[1.002,0,1],[0.998,0,1],[0,1,1],[-1,0,1],[0,-1,1]];
M := Mat(Points);
Result := Numerical.BBasisOfPointsInIdeal5(M,0.001, True,[z]);
If Not(Len(Result[1]) = 10) Then
  Print(\"Failed.\");
Else
  If Not(Len(Result[2]) = 5) Then
    Print(\"Failed.\");
  Else
    Result := Numerical.BBasisOfPointsInIdeal5(M,0.01,True,[z]);
    If Not(Len(Result[1]) = 8) Then
      Print(\"Failed.\");
    Else
      If Not(Len(Result[2]) = 4) Then
        Print(\"Failed.\");
      Else
        If Not(Len([P | P In Result[1] And (z IsIn Support(P)) And (zx^2 IsIn Support(P)) And (zy^2 IsIn Support(P))] )>0) Then
          Print(\"Failed.\");
        Else
          Print(\"Passed.\");
        EndIf;
      EndIf;
    EndIf;
  EndIf;
EndIf;";

Test.ExpectedOutput := "Passed.
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 11:
Test := Record[Id := "numerical_11", Descr := "FirstVanishingRelationsInIdeal5, without OrderIdeal"];
Test.Input :="MEMORY.PKG.CoCoA5.EnableTime := False;
Use RRR::=QQ[x,y,z];
Points := [[1.002,0,1],[0.998,0,1],[0,1,1],[-1,0,1],[0,-1,1]];
M := Mat(Points);
Result := Numerical.FirstVanishingRelationsInIdeal5(M,0.001, False,[z]);
If Not(Len(Result) = 1) Then
  Print(\"Failed.\");
Else
  Result := Numerical.FirstVanishingRelationsInIdeal5(M,0.01, False,[z]);
  If Not(Len(Result) = 1) Then
    Print(\"Failed.\");
  Else
    Print(\"Passed.\");
  EndIf;
EndIf;";

Test.ExpectedOutput := "Passed.
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 12:
Test := Record[Id := "numerical_12", Descr := "FirstVanishingRelationsInIdeal5, with OrderIdeal"];
Test.Input :="MEMORY.PKG.CoCoA5.EnableTime := False;
Use RRR::=QQ[x,y,z];
Points := [[1.002,0,1],[0.998,0,1],[0,1,1],[-1,0,1],[0,-1,1]];
M := Mat(Points);
Result := Numerical.FirstVanishingRelationsInIdeal5(M,0.001, True,[z]);
If Not(Len(Result[1]) = 1) Then
  Print(\"Failed.\");
Else
  If Not(Len(Result[2]) = 1) Then
    Print(\"Failed.\");
  Else
    Result := Numerical.FirstVanishingRelationsInIdeal5(M,0.01,True,[z]);
    If Not(Len(Result[1]) = 1) Then
      Print(\"Failed.\");
    Else
      If Not(Len(Result[2]) = 1) Then
        Print(\"Failed.\");
      Else
        Print(\"Passed.\");
      EndIf;
    EndIf;
  EndIf;
EndIf;";

Test.ExpectedOutput := "Passed.
-------------------------------
";
TSL.RegisterTest(Test);
