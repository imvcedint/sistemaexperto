-- 11 May 2006, test suite for CoCoA 4

-- CHEAP HACK - no longer needed? 2007-08-24
-- Some tests assume the initial current ring is as follows
-- Use R ::= QQ[x,y,z];

Print "exbugs.ts";
Source CocoaPackagePath()+"/ts/exbugs.ts";
TSL.RunTests();  // was  TSL.Do();

Print "examples.ts";
Source CocoaPackagePath()+"/ts/examples.ts";
TSL.RunTests();  // was  TSL.Do();

Print "demo.ts";
Source CocoaPackagePath()+"/ts/demo.ts"; TSL.RunTests();  // was  TSL.Do();

Print "random.ts";
Source CocoaPackagePath()+"/ts/random.ts"; TSL.RunTests();  // was  TSL.Do();

Print "factor.ts";
Source CocoaPackagePath()+"/ts/factor.ts"; TSL.RunTests();  // was  TSL.Do();

Print "toric.ts";
Source CocoaPackagePath()+"/ts/toric.ts";
TSL.Deselect("toric_09");  -- requires large order matrix entries
TSL.RunTests();  // was  TSL.Do();

Print "points.ts";
Source CocoaPackagePath()+"/ts/points.ts";
TSL.RunTests();  // was  TSL.Do();

Print "radical.ts";
Source CocoaPackagePath()+"/ts/radical.ts";
TSL.RunTests();  // was  TSL.Do();

Print "alghom.ts";
Source CocoaPackagePath()+"/ts/alghom.ts";
TSL.RunTests();  // was  TSL.Do();



Print "primary.ts";
Source CocoaPackagePath()+"/contrib/primary.ts";
TSL.RunTests();  // was  TSL.Do();

Print "matrixnormalform.ts";
Source CocoaPackagePath()+"/contrib/matrixnormalform.ts";
TSL.RunTests();  // was  TSL.Do();

Print "max.ts";
Source CocoaPackagePath()+"/ts/max.ts";
TSL.RunTests();  // was  TSL.Do();

Print "stat.ts";
Source CocoaPackagePath()+"/contrib/stat.ts";
TSL.RunTests();  // was  TSL.Do();

---=== CoCoAServer tests ===---
-- Not yet implemented functions in CoCoALib
Define DeselectCoCoALib()  Return [15, 16, 17, 23]; EndDefine;

Define DeselectApCoCoALib()
  --  $cocoa5.Initialize();
  Catch TestApCoCoALib := $cocoa5.BBasis5(Ideal(1)); In E EndCatch;
  If Type(E) <> ERROR Then Return []; EndIf;
  If Not IsIn("No operation defined for", GetErrMesg(E)) Then Return E; EndIf;
  S := "ApCoCoA library not linked in: skipping all its tests";
  PrintLn Sum(NewList(Len(S), "="));
  PrintLn S;
  PrintLn Sum(NewList(Len(S), "="));
  Return [31, 32, 33, 34, 35, 36];
EndDefine;

Define DeselectFrobbyLib()
  --  $cocoa5.Initialize();
  Catch TestFrobby := $cocoa5.IrreducibleDecom_Frobby5(Ideal(x)); In E EndCatch;
  If Type(E) <> ERROR Then Return []; EndIf;
  If Not IsIn("No operation defined for", GetErrMesg(E)) Then Return E; EndIf;
  S := "Frobby library not linked in: skipping all its tests";
  PrintLn Sum(NewList(Len(S), "="));
  PrintLn S;
  PrintLn Sum(NewList(Len(S), "="));
  Return [40];
EndDefine;

Print "cocoa5.ts";
Source CocoaPackagePath()+"/ts/cocoa5.ts";
If Not IsServerReady() Then
  PrintLn "CoCoAServer is not reachable: skipping all cocoa5 tests";
Else
  DeselectList := Concat(DeselectCoCoALib(),
			 DeselectApCoCoALib(),
			 DeselectFrobbyLib());
  Foreach N In DeselectList Do TSL.Deselect("cocoa5-" + Sprint(N)); EndForeach;
  PreviousSetting := $cocoa5.ShowServerTime(False);
  TSL.RunTests();  // was  TSL.Do();
  NoPrint := $cocoa5.ShowServerTime(PreviousSetting);
EndIf;
/*
-- In case one test fails debug like this:
Source CocoaPackagePath()+"/ts/cocoa5.ts";
$cocoa5.Initialize();
MEMORY.PKG.CoCoA5.PrintOnPath := GetEnv("HOME")+"/tmp";
TSL.Do("cocoa5-01");
-- Then run CoCoAServer -t < ~tmp/CoCoA4Request.cocoa5
-- In gdb:  break CoCoA::error
*/
