
-- Automatically generated from the file CoCoAHelp.xml
-- Using More Than One Ring --
MEMORY.Doc.Descr := "
In CoCoA, every calculation takes place in a *current* or
*active* ring.  Ring-dependent objects defined by the user such as
polynomials, ideals, and modules are automatically labeled by the
current ring.  Objects that do not depend essentially on the ring,
e.g., lists or matrices of integers, do not get labeled.

CoCoA automatically starts with the ring R = QQ[x,y,z].  The
following example illustrates setting up a ring with the construction
\"::=\" and changing rings with the command \"Use\".  One may temporarily
change rings with the command \"Using\".  The example assumes that you
do not already have a ring with identifier \"S\".

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[x,y];  -- declare and use a ring R
  F := (x+y)^3;
  F;
x^3 + 3x^2y + 3xy^2 + y^3
-------------------------------
  M := [1,\"test\",2];
  S ::= QQ[x,y,z,a,b];   -- declare a ring S with indeterminates x,y,z,a,b
  Use S;            -- switch to the ring S
  F;  -- F is labeled by ring R
R :: x^3 + 3x^2y + 3xy^2 + y^3
-------------------------------
  M;  -- this list is not labeled R since its elements are not
      -- ring dependent (e.g., \"1\" is considered a separate integer, not
      -- part of the ring R)
[1, \"test\", 2]
-------------------------------
  F := Ideal(a^2+b^2);  -- change the definition of F
  Use R;  -- switch back to R
  F;   -- the old F no longer exists
S :: Ideal(a^2 + b^2)
-------------------------------
  GBasis(F);  -- built in functions automatically recognize the ring
[S :: a^2 + b^2]
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";



    