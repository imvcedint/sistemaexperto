
-- Automatically generated from the file CoCoAHelp.xml
-- Flatten --

MEMORY.Doc.Syntax := ""  +NewLine() +
"Flatten(L:LIST):LIST"  +NewLine() +
"Flatten(L:LIST, N:INT):LIST"  +NewLine() +
"";
MEMORY.Doc.Descr := "
Components of lists may be lists themselves, i.e., lists may be
nested.  With one argument this function returns the list obtained
from the list L by removing all nesting, bringing all elements *to
the top level*.  With the optional second argument, N, nesting is
removed down N levels.  Thus, the elements of M := Flatten(L,1) are
formed as follows: go through the elements of L one at a time; if an
elements is not a list, add it to M; if an element is a list, add all
of its elements to M.  Recursively, Flatten(L, N) =
Flatten(Flatten(L, N-1),1).  For N large, depending on L, Flatten(L, N)
gives the same result as Flatten(L).

//==========================  EXAMPLE  ==========================\\\\
  Flatten([1,[\"a\",\"b\",[2,3,4],\"c\",\"d\"],5,6]);
[1, \"a\", \"b\", 2, 3, 4, \"c\", \"d\", 5, 6]
-------------------------------
  L := [1,2, [3,4], [5, [6,7,[8,9]]]];
  Flatten(L,1);
[1, 2, 3, 4, 5, [6, 7, [8, 9]]]
-------------------------------
  Flatten(It,1);
[1, 2, 3, 4, 5, 6, 7, [8, 9]]
-------------------------------
  Flatten(L,2);  -- same as in the previous line
[1, 2, 3, 4, 5, 6, 7, [8, 9]]
-------------------------------
  Flatten(L,3);  -- same as Flatten(L)
[1, 2, 3, 4, 5, 6, 7, 8, 9]
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    