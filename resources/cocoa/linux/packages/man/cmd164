
-- Automatically generated from the file CoCoAHelp.xml
-- IdealAndSeparatorsOfProjectivePoints --

MEMORY.Doc.Syntax := ""  +NewLine() +
"IdealAndSeparatorsOfProjectivePoints(Points:LIST):RECORD"  +NewLine() +
""  +NewLine() +
"where Points is a list of lists of coefficients representing a set of"  +NewLine() +
"distinct points in projective space."  +NewLine() +
"";
MEMORY.Doc.Descr := "
This function computes the results of \"IdealOfProjectivePoints\" and
\"SeparatorsOfProjectivePoints\" together at a cost lower than making
the two separate calls.  The result is a record with three fields:

    Points     -- the points given as argument
    Ideal      -- the result of IdealOfProjectivePoints
    Separators -- the result of SeparatorsOfProjectivePoints

Thus, if the result is stored in a variable with identifier X, then:
X.Ideal will be the ideal of the set of points, with generators
forming a reduced Groebner basis for the ideal; and X.Separators will
be a list of homogeneous polynomials whose i-th element will be
non-zero (actually 1, using the given representatives for the
coordinates of the points) on the i-th point and 0 on the others.

NOTE:

 * the current ring must have at least one more indeterminate than the
   dimension of the projective space in which the points lie, i.e, at
   least as many indeterminates as the length of an element of
   the input, Points;

 * the base field for the space in which the points lie is taken to be
   the coefficient ring, which should be a field;

 * in the polynomials returned, the first coordinate in the space is
   taken to correspond to the first indeterminate, the second to the
   second, and so on;

 * if the number of points is large, say 100 or more, the returned
   value can be very large.  To avoid possible problems when printing
   such values as a single item we recommend printing out the elements
   one at a time as in this example:

     X := IdealAndSeparatorsOfProjectivePoints(Pts);
     Foreach Element In Gens(X.Ideal) Do
       PrintLn Element;
     EndForeach;


For ideals and separators of points in affine space, see
\"IdealAndSeparatorsOfPoints\".

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[x,y,z];
  Points := [[0,0,1],[1/2,1,1],[0,1,0]];
  X := IdealAndSeparatorsOfProjectivePoints(Points);
  X.Points;
[[0, 0, 1], [1, 1, 1], [0, 1, 0]]
-------------------------------
  X.Ideal;
Ideal(xz - 1/2yz, xy - 1/2yz, x^2 - 1/4yz, y^2z - yz^2)
-------------------------------
  X.Separators;
[-2x + z, x, -2x + y]
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    