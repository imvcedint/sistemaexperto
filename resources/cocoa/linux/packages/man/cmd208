
-- Automatically generated from the file CoCoAHelp.xml
-- IsTerm --

MEMORY.Doc.Syntax := ""  +NewLine() +
"IsTerm(X:POLY or VECTOR):BOOL"  +NewLine() +
"";
MEMORY.Doc.Descr := "
The function determines whether X is a term.  For a polynomial, a
*term* is a power-product, i.e., a product of indeterminates.  Thus,
xy^2z is a term, while 4xy^2z and xy+2z^3 are not.  For a vector, a
term is a power-product times a standard basis vector, e.g.,
(0, xy^2z, 0).

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[x,y,z];
  IsTerm(x+y^2);
False
-------------------------------
  IsTerm(x^3yz^2);
True
-------------------------------
  IsTerm(5x^3yz^2);
False
-------------------------------
  IsTerm(Vector(0,0,xyz));
True
-------------------------------
  IsTerm(Vector(x^2,y^2));
False
-------------------------------
  IsTerm(5x^2);
False
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    