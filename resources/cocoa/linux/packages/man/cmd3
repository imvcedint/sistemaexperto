
-- Automatically generated from the file CoCoAHelp.xml
-- AffHilbert --

MEMORY.Doc.Syntax := ""  +NewLine() +
"AffHilbert(R:RING or TAGGED(Quotient)):TAGGED($hp.Hilbert)"  +NewLine() +
"AffHilbert(R:RING or TAGGED(Quotient), N:INT):INT"  +NewLine() +
"";
MEMORY.Doc.Descr := "
The first form of this function computes the affine Hilbert function for R.
The second form computes the N-th value of the affine Hilbert function.  The
weights of the indeterminates of R must all be 1.  For repeated
evaluations of the Hilbert function, use \"EvalHilbertFn\"
instead of \"Hilbert(R, N)\" in order to speed up execution.

This function is the same as \"AffHilbertFn\".

The coefficient ring must be a field.

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[x,y,z];
  AffHilbert(R/Ideal(z^4-1, xz^4-y-3));
H(0) = 1
H(1) = 3
H(t) = 4t - 2   for t >= 2
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    