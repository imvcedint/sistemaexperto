
-- Automatically generated from the file CoCoAHelp.xml
-- Ideal --

MEMORY.Doc.Syntax := ""  +NewLine() +
"Ideal(P_1:POLY,...,P_n:POLY):IDEAL"  +NewLine() +
"Ideal(L:LIST):IDEAL"  +NewLine() +
"Ideal(M:MODULE):IDEAL"  +NewLine() +
""  +NewLine() +
"where L is a list of polynomials and M is contained in a free module"  +NewLine() +
"of rank 1."  +NewLine() +
"";
MEMORY.Doc.Descr := "
The first form returns the ideal generated by \"P_1,...P_n\".  The second
form returns the ideal generated by the polynomials in L.  The third
form returns the ideal generated by the polynomials in M; it is the
same as \"Cast(M, IDEAL)\", and requires that the module be a submodule of
the free module of rank 1.

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[x,y,z];
  I := Ideal(x-y^2, xy-z);
  I;
Ideal(-y^2 + x, xy - z)
-------------------------------
  L := [xy-z, x-y^2];
  J := Ideal(L);
  I = J;
True
-------------------------------
  M := Module([y^3-z],[x-y^2]);
  Ideal(M) = I;
True
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    