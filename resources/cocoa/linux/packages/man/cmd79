
-- Automatically generated from the file CoCoAHelp.xml
-- Elim --

MEMORY.Doc.Syntax := ""  +NewLine() +
"Elim(X:INDETS, M:IDEAL):IDEAL"  +NewLine() +
"Elim(X:INDETS, M:MODULE):MODULE"  +NewLine() +
""  +NewLine() +
"where X is an indeterminate or a list of indeterminates."  +NewLine() +
"";
MEMORY.Doc.Descr := "
This function returns the ideal or module obtained by eliminating the
indeterminates X from M.  The coefficient ring needs to be a field.

As opposed to this function, there is also the *modifier*, \"Elim\",
used when constructing a ring (see \"Orderings\" and
\"Predefined Term-Orderings\").

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[t,x,y,z];
  Set Indentation;
  Elim(t, Ideal(t^15+t^6+t-x, t^5-y, t^3-z));
Ideal(
  -z^5 + y^3,
  -y^4 - yz^2 + xy - z^2,
  -xy^3z - y^2z^3 - xz^3 + x^2z - y^2 - y,
  -y^2z^4 - x^2y^3 - xy^2z^2 - yz^4 - x^2z^2 + x^3 - y^2z - 2yz - z,
  -y^3z^3 + xz^3 - y^3 - y^2)
-------------------------------
  Use R ::= QQ[t,s,x,y,z,w];
  t..x;
[t, s, x]
-------------------------------
  Elim(t..x, Ideal(t-x^2zw, x^2-t, y^2t-w)); -- Note the use of t..x.
Ideal(-zw^2 + w)
-------------------------------
  Use R ::= QQ[t[1..2], x[1..4]];
  I := Ideal(x[1]-t[1]^4, x[2]-t[1]^2t[2], x[3]-t[1]t[2]^3, x[4]-t[2]^4);
  t;
[t[1], t[2]]
-------------------------------
  Elim(t, I);                         -- Note the use t.
Ideal(x[3]^4 - x[1]x[4]^3, x[2]^4 - x[1]^2x[4])
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    