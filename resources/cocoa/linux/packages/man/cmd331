
-- Automatically generated from the file CoCoAHelp.xml
-- Res --

MEMORY.Doc.Syntax := ""  +NewLine() +
"Res(M):TAGGED($gb.Res)"  +NewLine() +
""  +NewLine() +
"where M is of type IDEAL or MODULE or TAGGED(Quotient)."  +NewLine() +
"";
MEMORY.Doc.Descr := "
This function returns the minimal free resolution of M.  If M is a
quotient of a ring by an ideal I or a quotient of a free
module by a submodule N, then the resolution of M is defined
to be that of I or N, respectively.
\"Res\" only works in the homogeneous context, and the coefficient ring
must be a field.

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[x,y,z];
  I := Ideal(x, y, z^2);
  Res(R/I);
0 --> R(-4) --> R(-2)(+)R(-3)^2 --> R(-1)^2(+)R(-2) --> R
-------------------------------
  Describe It;

Mat([
  [y, x, z^2]
])
Mat([
  [x, z^2, 0],
  [-y, 0, z^2],
  [0, -y, -x]
])
Mat([
  [z^2],
  [-x],
  [y]
])
-------------------------------
\\\\==========================  o=o=o=o  ===========================//

For fine control and monitoring of Groebner basis calculations,
including various types of truncations, see \"The Interactive Groebner
Framework\" and \"Introduction to Panels\".
    ";




    