
-- Automatically generated from the file CoCoAHelp.xml
-- NF --

MEMORY.Doc.Syntax := ""  +NewLine() +
"NF(F:POLY, I:IDEAL):POLY"  +NewLine() +
"NF(V:VECTOR, M:MODULE):VECTOR"  +NewLine() +
"";
MEMORY.Doc.Descr := "
The first function returns the normal form of F with respect to I.
It also computes a Groebner basis of I if that basis has not been
computed previously.

The second function returns the normal form of V with respect to M. It
also computes a Groebner basis of M if that basis has not been
computed previously.

The coefficient ring is assumed to be a field.  Note that the
definition of normal form depends on the current value of the option
FullRed of the panel GROEBNER.  If FullRed is False it means that a
polynomial is in normal form when its leading term with respect to the
the current term ordering cannot be reduced. If FullRed is True it
means that a polynomial is in NF if and only if each monomial cannot
be reduced.

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[x,y,z];
  Set FullRed;
  I := Ideal(z);
  NF(x^2+xy+xz+y^2+yz+z^2, I);
x^2 + xy + y^2
-------------------------------
  UnSet FullRed;
  NF(x^2+xy+xz+y^2+yz+z^2, I);
x^2 + xy + y^2 + xz + yz + z^2
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    