
-- Automatically generated from the file CoCoAHelp.xml
-- Subst --

MEMORY.Doc.Syntax := ""  +NewLine() +
"Subst(E:OBJECT, X, F):OBJECT"  +NewLine() +
"Subst(E:OBJECT,[[X_1, F_1],...,[X_r, F_r]]):OBJECT"  +NewLine() +
""  +NewLine() +
"where each X or X_i is an indeterminate and each F or F_i is a number,"  +NewLine() +
"polynomial, or rational function."  +NewLine() +
"";
MEMORY.Doc.Descr := "
The first form of this function substitutes \"F_i\" for \"X_i\" in the
expression E.  The second form is a shorthand for the first in the
case of a single indeterminate.  When substituting for the
indeterminates in order, it is easier to use \"Eval\".

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[x,y,z,t];
  F := x +y +z +t^2;
  Subst(F, x, -2);
t^2 + y + z - 2
-------------------------------
  Subst(F, x, z/y);
(yt^2 + y^2 + yz + z)/y
-------------------------------
  Subst(F, [[x,x^2], [y,y^3], [z,t^5]]);
t^5 + y^3 + x^2 + t^2
-------------------------------
  Eval(F, [x^2,y^3,t^5]); -- the same thing as above
t^5 + y^3 + x^2 + t^2
-------------------------------
  MySubst := [[y,1], [t,3z-x]];
  Subst(xyzt, MySubst);  -- substitute into the function xyzt
-x^2z + 3xz^2
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    