
-- Automatically generated from the file CoCoAHelp.xml
-- Eigenvectors --

MEMORY.Doc.Syntax := ""  +NewLine() +
"Eigenvectors(M:MAT, X:POLY):LIST"  +NewLine() +
"";
MEMORY.Doc.Descr := "
M must be a matrix of numbers, and X an indeterminate.

This function determines the eigenvalues of M, and for each eigenvalue
gives a basis of the corresponding eigenspace -- note that the basis is
probably not orthogonal.  For irrational eigenvalues, the minimal
polynomial of the eigenvalue is given (as a polynomial in X), along with
the eigenvectors expressed in terms of a root of the minimal polynomial
(represented as X).

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[x];
  M := Mat([[1,2,3],[4,5,6],[7,8,9]]);
  Eigenvectors(M, x);
[Record[Eigenspace := [[-1, 2, -1]], MinPoly := x],
Record[Eigenspace := [[1, 1/8x + 1/4, 1/4x - 1/2]], MinPoly := x^2 - 15x - 18]]
-------------------------------
  M := Mat([[0,2,0,0],[1,0,0,0],[0,0,0,2],[0,0,1,0]]);
  Eigenvectors(M, x); -- two irrational eigenvalues, each with eigenspace of dimension 2
[Record[Eigenspace := [[1, 1/2x, 0, 0], [0, 0, 1, 1/2x]], MinPoly := x^2 - 2]]
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    