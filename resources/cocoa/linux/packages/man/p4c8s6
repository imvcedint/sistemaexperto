
-- Automatically generated from the file CoCoAHelp.xml
-- Orderings --
MEMORY.Doc.Descr := "
Polynomials are always sorted with respect to the ordering of their
base ring.  All the operations involving polynomials utilize and
preserve this ordering. The user can define custom orderings or choose
a predefined term-ordering.  Some commands temporarily change the
term-ordering in order to make a computation.  The various
possibilities are discussed more fully, below.

The command \"Ord\" can be used both as a modifier to set the ordering
of the ring and as a way to view the matrix defining the current
term-ordering.

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[x,y,z], Lex;  -- lexicographic term-ordering (predefined)
  Use S ::= QQ[u,v], Ord([[1,1],[2,-3]]);  -- custom term-ordering
  Ord(R);  -- the matrix defining the term-ordering for R
Mat([
  [1, 0, 0],
  [0, 1, 0],
  [0, 0, 1]
])
-------------------------------
  Ord(S);  -- the matrix defining the term-ordering for S
Mat([
  [1, 1],
  [2, -3]
])
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";



    