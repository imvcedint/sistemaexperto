
-- Automatically generated from the file CoCoAHelp.xml
-- Catch --

MEMORY.Doc.Syntax := ""  +NewLine() +
"Catch C EndCatch;"  +NewLine() +
"Catch C In E EndCatch;"  +NewLine() +
""  +NewLine() +
"where C is a sequence of commands and E is a variable identifier."  +NewLine() +
"";
MEMORY.Doc.Descr := "
Usually, when an error occurs during the execution of a command, the
error is automatically propagated out of the nesting of the
evaluation.  This can be prevented with the use of \"Catch\".

If an error occurs during the execution of C, then it is captured by
the command \"Catch\" and (in the second form) assigned to the variable
E.  If no error occurs, then E will contain the value \"Null\".  Note
the use of the function \"GetErrMesg\" in the example below.

//==========================  EXAMPLE  ==========================\\\\
  Deg(0);
ERROR: Deg of zero is not defined
CONTEXT: Error(\"Deg of zero is not defined\")
-------------------------------
  Define MyDeg(F)
    Catch  D := Deg(F);  In E EndCatch;
    If Type(E) = ERROR Then
      If IsIn(\"Deg of zero\", GetErrMesg(E)) Then
        Return -1234;
      Else
        Return E;
      EndIf; 
    EndIf;
    Return D;
  EndDefine;

  MyDeg(x);
1
-------------------------------
  MyDeg(0);
-1234
-------------------------------
  MyDeg(1/x);
ERROR: Deg: first argument must be POLY or VECTOR
CONTEXT: Return(E)
-------------------------------
\\\\==========================  o=o=o=o  ===========================//

IMPORTANT NOTE: There is a bug in \"Catch\".  Any \"Return\" command used
inside \"Catch\" must return some value.  If not, the \"Return\" command
will just return from the Catch-EndCatch statement; it will not return from
the function within which the statement is embedded.  There is an
example below.
//==========================  EXAMPLE  ==========================\\\\
  Define Test2()
    Catch Print \"Hello \"; Return; EndCatch;
    PrintLn \"world.\";
  EndDefine;
  Test2();
Hello world.

-------------------------------
  Define Test3()
    Catch Print \"Hello \"; Return 3; EndCatch;
    PrintLn \"world.\";
  EndDefine;
  Test3();
Hello 3
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    