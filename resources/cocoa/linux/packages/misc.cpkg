Package $misc

Alias 	
  Misc 	:= $misc;

---------------------------------------------------------------

Define About()
  PrintLn "    Version : CoCoA 4.2";
  PrintLn "    Date    : 6 November 2002";
  PrintLn "    Changes : fixed Sum (was too slow before)"
EndDefine; -- About


-----[ Strings ]-----------------------------------------------

-----[ Math ]--------------------------------------------------
/*MAX*/
Define E_(I,A)
  --If Not Shape([I,A]) IsIn [[INT,INT],[INT,MODULE]] Then PrintLn("Bad Parameters") EndIf;
  If Type(A)=MODULE Then Rank:=Len(Head(Gens(A)))
  Else Rank:=A -- Type(A)=INT
  EndIf;
  V:=NewList(Rank,Poly(0));
  V[I]:=Poly(1);
  Return Vector(V)
EndDefine; -- E_
  /*MAX*/


Define ZeroVector(N)  -- N is a MODULE or INT
  If Type(N)=MODULE Then NC := NumComps(N)
  Else NC := Cast(N, INT)
  EndIf;
  Return Vector(NewList(NC,0));
EndDefine; -- ZeroVector


Define PrintVector(V)
  V := Cast(V, VECTOR); -- this will give an error if the cast fails
  I := 1; While I<=Len(V) And V[I]=0 Do I := I+1; EndWhile;
  If I>Len(V) Then
    Print "ZeroVector(",Len(V),")";
  Else
    Print "(",V[I],")*E_(",I,",",Len(V),")";
  EndIf;
  For J := I+1 To Len(V) Do
    If V[J]<>0 Then
      Print " + (",V[J],")*E_(",J, ",",Len(V),")";
    EndIf;
  EndFor;
EndDefine; -- PrintVector


Define Rank(...)
  N:=ARGV[1];
  If Type(N) = MAT Then
    If IsSubset(Flatten(List(Shape(N))), [INT,RAT])
      And Characteristic()<>0 Then
      Using QQt Do Return Rank(N); EndUsing;
    EndIf;
    N := Module(N);
  EndIf;
  If Len(ARGV)=2 Then NumSteps:=ARGV[2] Else NumSteps:=10 EndIf;
  Return Misc.RRank(N,NumSteps)
EndDefine; -- Rank


Define RRank(Var M,NumSteps)
  --Print ".";
  If M=Module([[0]]) Then Return 0 EndIf;
  $gb.Start_Syz(M);
  Repeat
    $gb.Steps(M,NumSteps);
  Until Not ComputationInProgress(M) Or M.Syz<>Module([[0]]);
  If M.Syz<>Module([[0]]) Then
    GenToKill:=FirstNonZeroPos(Head(Gens(M.Syz)));
    NewGens := Gens(M); Remove(NewGens, GenToKill);
    M:=Module(NewGens);
    Return Misc.RRank(M,NumSteps)
  Else
    If M.Syz=Module([[0]]) Then
      Return Len(Gens(M))
    Else
      Return Len(Gens(M))-1
    EndIf;
  EndIf;
EndDefine; -- RRank


Define Concat(...)
  If Len(ARGV) = 0 Then Return [] EndIf;
  Types := MakeSet([Type(X) | X In ARGV]);
  If Not Types IsIn [[LIST], [VECTOR]] Then
    Error("Concat: args must be lists or vectors");
  EndIf;
  If Types = [VECTOR] Then
    Return $.ConcatVectors(ARGV);
  EndIf;
  L := ARGV[1];
  For I := 2 To Len(ARGV) Do
    L := $builtin.Concat(L,ARGV[I])
  EndFor;
  Return L
EndDefine; -- Concat


Define ConcatVectors(VL)
  N := Sum([Len(V) | V In VL]);
  Ans := NewList(N);
  I := 1;
  Foreach V In VL Do
    Foreach Entry In V Do
      Ans[I] := Entry;
      I := I+1;
    EndForeach;
  EndForeach;
  Return Vector(Ans);
EndDefine; -- ConcatVectors


Define IsHomog_Module(M)
  If Misc.IsHomog(Gens(M)) Then Return TRUE; EndIf;
  OldFlag := Option(FullRed);
  Set FullRed;
  Result := Misc.IsHomog(ReducedGBasis(M)); -- hoping the PP order is good!!
  If OldFlag = FALSE Then Unset FullRed; EndIf;
  Return Result;
EndDefine; -- IsHomog_Module


Define IsHomog(...)
  If Len(ARGV)=1 Then
    X := ARGV[1];
    If X = [] Then Return TRUE; EndIf;
    If Type(X) IsIn [IDEAL, MODULE] Then Return Misc.IsHomog_Module(X); EndIf;
    If Type(X) = LIST Then
      Return MakeSet([$builtin.IsHomog(Y)|Y In X]) = [TRUE]
    Else Return $builtin.IsHomog(X)
    EndIf;
  Elif Len(ARGV)<>2 Then Error("IsHomog: 1 or 2 arguments");
  EndIf;
  -- Len(ARGV)=2
  Sh := ARGV[1]; X := ARGV[2];
  If Type(Sh)<>TAGGED("shifts") Then
    Error("IsHomog: first argument must be Shifts([...])");
  EndIf;
  Sh := Untagged(Sh);
  If POLY IsIn Shape(Sh) Then
    For I := 1 To Len(Sh) Do  If Sh[I]=0 Then Sh[I]:=1 EndIf  EndFor;
  Else
    Idt := Indet(1);
    For I := 1 To Len(Sh) Do  Sh[I]:=Idt^(-Sh[I])  EndFor;
  EndIf;
  If Type(X)=LIST Then Return MakeSet([Misc.IsVectHomog(Sh,Y)|Y In X]) = [TRUE]
  Elif Type(X)=VECTOR Then Return Misc.IsVectHomog(Sh,X)
  Else Return Error("IsHomog: second argument must be VECTOR or VECTOR-LIST")
  EndIf;
EndDefine; -- IsHomog

Define IsVectHomog(Sh, V)
  Return $builtin.IsHomog(Vector([ Sh[I]*V[I] | I In 1..Len(Sh)]))
EndDefine; -- IsVectHomog

Define Shifts(S)
  If Type(S) = MODULE Or Type(S) = VECTOR Then
    A := S.Shifts;
    If A = Null Then A := NewList(NumComps(S),Poly(1)) EndIf;
  Else
    A := S;
    If A = Null Then A := NewList(NumComps(S),Poly(1)) EndIf;
  EndIf;
  X := [Type(Y) = POLY Or Y <= 0 | Y In A];
  If MakeSet(X) <> [TRUE] Then
    Error(ERR.BAD_PARAMS,"(integer shifts MUST be <= 0)");
  Else Return Tagged(A,"shifts");
  EndIf;
EndDefine; -- Shifts


Define Module(...)
  If Len(ARGV) = 0 Then Return $builtin.Module([])
  Else
    S := ARGV[1];
    If Type(S) = TAGGED("shifts") Then
      L := Tail(ARGV);
      If Len(L) = 1 Then G := L[1] Else G := L EndIf;
      Return $builtin.Module(S,G)
    Else
      L := ARGV;
      S := MakeSet([Type(X) | X In L]);
      If POLY IsIn S Then
	Error("Module: polynomial gens are ambiguous: please use [] or Vector()");
      EndIf;
      If INT IsIn S Then
	Error("Module: integer gens are ambiguous: please use [] or Vector() in this generator list ", Sprint(L));
      EndIf;
      If Len(L) = 1 Then G := L[1] Else G := L EndIf;
      Return $builtin.Module(G)
    EndIf;
  EndIf;
EndDefine; -- Module


Define Ideal(...)
  If Len(ARGV) = 0 Then Return Cast(Module([[0]]),IDEAL) EndIf;
  If Len(ARGV) > 1 Or Type(ARGV[1]) <= POLY Then Return Ideal(ARGV); EndIf;
  X := ARGV[1];
  T := Type(X);
--  If T = ERROR Then Return ARGV[1]; EndIf;
  If T = IDEAL Then Return X; EndIf;
  If T = MODULE Then Return Cast(X, IDEAL); EndIf;
  If T = LIST Then
    If X <> [] And
      MakeSet([Type(F) <= POLY | F In X]) <> [True] And
      MakeSet([Type(F) | F In X]) <> [VECTOR] Then
      Error("Ideal: bad parameters");
    EndIf;
    RE := Diff(RingEnvSet(X), [""]);
    If RE = [] Then Return Cast(Module([[F] | F In X]), IDEAL); EndIf;
    Return Var(RE[1])::Cast(Module([[F] | F In X]), IDEAL);
  EndIf;
  Return Error("Ideal: bad parameter");
EndDefine; -- Ideal

Define PolyBin(A,B)
  Return Product([A-J | J In 0..(B-1)])/Fact(B);
EndDefine; -- PolyBin

Define Bin(N,K)
  If Type(N) = RAT And Den(N) = 1 Then N := Num(N); EndIf;
  If Type(K) = RAT And Den(K) = 1 Then K := Num(K); EndIf;
  If Type(K) <> INT Then
    Error("Bin: Second argument must be an integer")
  EndIf;
  If K < 0 Then Return 0; EndIf;
  If Type(N)=POLY Then Return Misc.PolyBin(N,K)
  Elif Type(N)=INT Then
    If N >= K Then Return Fact(N)/(Fact(K)*Fact(N-K))
    Else
      If N < 0 Then Return (-1)^K*Bin(K-N-1,K)
      Else Return 0
      EndIf;
    EndIf;
  Elif IsNumber(N) Then Return Misc.PolyBin(N,K)
  Else ERR.BAD_PARAMS
  EndIf;
EndDefine; -- Bin


-------------------------------

-- Rand() returns a random integer
-- Rand(X,Y) returns a random integer between X and Y
Define Rand(...)
  N := Randomized(1);
  If Len(ARGV)=0 Then Return N; EndIf;
  If Len(ARGV) <> 2 Or Type(ARGV[1]) <> INT Or Type(ARGV[2]) <> INT Then
    Return ERR.BAD_PARAMS;
  EndIf;
  X := ARGV[1];
  Y := ARGV[2];
  If X < Y Then Return $builtin.Mod(N,Y-X+1)+X; EndIf;
  Return $builtin.Mod(N,X-Y+1)+Y;
EndDefine; -- Rand


-------------------------------

Define NumComps(X)
  T := Type(X);
  If T = VECTOR Then Return Len(X)
  Elif T = MODULE Then Return X.NumComps
  Elif T = IDEAL Then Return 1
  Elif T = TAGGED("Quotient") Then Return Misc.NumComps(Untagged(X))
  EndIf;
  Return ERR.BAD_PARAMS
EndDefine; -- NumComps


-------------------------------

Define Comps(V)
  If Type(V) <> VECTOR Then ERR.BAD_PARAMS
  Else Return [ V[I] | I In 1..Misc.NumComps(V) ]
  EndIf;
EndDefine; -- Comps

-------------------------------

-- The "old" Sum routine; good for short lists, and lists of integers.
Define SequentialSum(L)
  If Type(L) <> LIST Then ERR.BAD_PARAMS EndIf;
  If Len(L)=0 Then Return 0 EndIf;
  S := Head(L);
  Foreach X In Tail(L) Do
    S := S + X;
  EndForeach;
  Return S;
EndDefine; -- SequentialSum


-- This version is faster for summing monomials (but a bit slower for lists of integers)
Define Sum(L)
  If Type(L) <> LIST Then ERR.BAD_PARAMS EndIf;
  While Len(L) > 10 Do  -- the limit of 10 is not wholly arbitrary, it seems fastest.
    LenL := Len(L);
    L2 := [];
    For I := 3 To LenL Step 3 Do
      Append(L2, L[I-2]+L[I-1]+L[I]);
    EndFor;
    If Mod(LenL, 3) = 1 Then Append(L2, Last(L)); EndIf;
    If Mod(LenL, 3) = 2 Then Append(L2, L[LenL-1]+Last(L)); EndIf;
    L := L2;
  EndWhile;
  Return Misc.SequentialSum(L);
EndDefine; -- Sum

-------------------------------

Define Product(L)
  If Type(L) <> LIST Then ERR.BAD_PARAMS EndIf;
  If Len(L)=0 Then Return 1 EndIf;
  S := 1;
  Foreach X In L Do
    S := S * X;
  EndForeach;
  Return S;
EndDefine; -- Product


-------------------------------

Define Max(...)
  If Len(ARGV) = 0 Then Return ERR.BAD_PARAMS EndIf;
  If Len(ARGV) = 1 And Type(ARGV[1])=LIST Then
    L := ARGV[1]
  Else
    L := ARGV
  EndIf;
  M := Head(L);
  Foreach X In Tail(L) Do
    If X > M Then M := X EndIf;
  EndForeach;
  Return M
EndDefine; -- Max


-------------------------------

Define Min(...)
  If Len(ARGV) = 0 Then Return ERR.BAD_PARAMS EndIf;
  If Len(ARGV) = 1 And Type(ARGV[1])=LIST Then
    L := ARGV[1]
  Else
    L := ARGV
  EndIf;
  M := Head(L);
  Foreach X In Tail(L) Do
    If X < M Then M := X EndIf;
  EndForeach;
  Return M
EndDefine; -- Min


-------------------------------
--- THIS IS ALL WRITTEN IN C NOW (FEB 1999, JAA & MAX)
--- -- Monomials(P:POLY) returns the list of the monomials of P
--- Define Monomials(P)
---   If Not(Type(P) <= POLY) Then Return ERR.EXPECTED_POLY EndIf;
---   If P=0 Then Return [0] EndIf;
---   L := NewList(Len(P));
---   For I := 1 To Len(P) Do
---     M := LC(P)$builtin.LT(P);
---     L[I] := M;
---     P := P-M;
---   EndFor;
---   Return L;
--- EndDefine;
--- Support(P) := [LT(M)|M In Monomials(P)];

-------------------------------
-- Auxiliary (see Coefficients)

Define CoeffList(F,X)
  DegFx := Deg(F,X);
  CL := NewList(DegFx + 1);
  For I := 1 To Len(CL) Do
    C := NR(F,[X^I]);
    CL[DegFx-I+2] := C/X^(I-1);
    F := F - C;
  EndFor;
  Return CL;
EndDefine; -- CoeffList


-------------------------------

-- Coefficients(P:POLY) returns the list of the coefficients of P
-- Coefficients(P:POLY,X:INDET) returns the list of the coefficients
-- of P w.r.t. the indeterminate X
-- Coefficients(P:POLY,L:LIST) returns the list of the coefficients
-- w.r.t a given list of power products
Define Coefficients(...)
  If Len(ARGV)=1 Then Return $builtin.Coefficients(ARGV[1]); EndIf;
  If Len(ARGV) <> 2 Then
    Error("Usage: Coefficients(P:POLY) or Coefficients(P:POLY,X:INDET) or Coefficients(P:POLY,L:LIST)");
  EndIf;
  If Type(ARGV[2]) = LIST Then
    -- the check below excludes (vector, list of vector terms) but the code
    -- would allow it (as far as I can tell).  Would it be useful?
    If Len([X In ARGV[2] | Not(X = 1 Or IsTerm(X))]) > 0 Then
      Error("Coefficients: second argument must be a list of terms or an indeterminate");
    EndIf;
    Return [CoeffOfTerm(Poly(PP), ARGV[1]) | PP In ARGV[2]];
  EndIf;
  Return Misc.CoeffList(ARGV[1],ARGV[2]) -- CoeffList checks that ARGV[2] is OK
EndDefine; -- Coefficients


-------------------------------

Define RelDeg(F,X)
  I := IndetIndex(X);
  Return Max([Comp(Log(LPP(T)),I) | T In Support(F)]);
EndDefine; -- RelDeg


-- Deg(P:POLY) returns the degree of P
-- Deg(P:POLY,X:INDET) returns the degree of P w.r.t. the indeterminate X
Define Deg(...)
  X := ARGV[1];
  If Not ( Type(X)<=POLY Or Type(X)<=VECTOR ) Then
    Error("Deg: first argument must be POLY or VECTOR");
  EndIf;
  If X = X*0 Then Error("Deg of zero is not defined") EndIf;
  If Type(X) <= POLY Then X := Poly(X); EndIf;
  If Len(ARGV)=1 Then Return $builtin.Deg(X)
  Elif Len(ARGV)=2 Then Return Misc.RelDeg(X,ARGV[2])
  Else ERR.BAD_PARAMS_NUM;
  EndIf;
EndDefine; -- Deg


Define MDeg(X)
  If Not ( Type(X)<=POLY Or Type(X)=VECTOR ) Then
    Error("MDeg: argument must be POLY or VECTOR");
  EndIf;
  If X = X*0 Then Error("MDeg of zero is not defined") EndIf;
  If Type(X)<POLY Then Return 0 EndIf;
  Return $builtin.MDeg(X)
EndDefine; -- MDeg


-------------------------------

Define CoeffOfTerm(T,P)
  If Type(T) < POLY Then T := Poly(T); EndIf;
  If Type(P) < POLY Then P := Poly(P); EndIf;
  If Shape([T,P]) IsIn [[POLY,POLY],[VECTOR,VECTOR]] Then
    If Type(T)=VECTOR And Len(T)<>Len(P) Then
      Error("CoeffOfTerm: vector ranks are different");
    EndIf;
    If IsTerm(T) Then
      Return $builtin.CoeffOfTerm(P,T)
    Else
      Error("CoeffOfTerm: first argument must be a term");
    EndIf;
  EndIf;
  Error("CoeffOfTerm: (Term,Poly) or (Term,Vector) expected");
EndDefine; -- CoeffOfTerm


-----[ Other ]-------------------------------------------------------

Define ResetPanelGENERAL()
  Set Echo := FALSE;
  Set Timer := FALSE;
  If DebugVersion() Then Set Memory := FALSE EndIf;
  Set Trace := FALSE;
  Set Indentation := FALSE;
  Set TraceSources := FALSE;
  Set SuppressWarnings := FALSE;
  Set ComputationStack := FALSE;
EndDefine; -- ResetPanelGENERAL


Define ResetPanelGROEBNER()
  Set Sugar := TRUE;
  Set FullRed := TRUE;
  Set SingleStepRed := FALSE;
  Set Verbose := FALSE;
EndDefine; -- ResetPanelGROEBNER


Define ResetPanels()
  Misc.ResetPanelGENERAL();
  Misc.ResetPanelGROEBNER();
EndDefine; -- ResetPanels


Define Reset()
  Misc.ResetPanels();
  Seed(0)
EndDefine; -- Reset


Define IsElementOf(X,L)
  If Type(L) <= MODULE Then
    Return IsZero(NF(X,L));
  Else
    Return $builtin.IsIn(X,L);
  EndIf;
EndDefine; -- IsIn


Define Subst3(E,X,F) Return Misc.Subst2_top(E,[[X,F]]); EndDefine;

Define Subst2_top(E,S)
  S := [P In S|P[1]<>P[2]];
  If Len(S)=0 Then Return E EndIf;
  Return Misc.Subst2(E,S);
EndDefine; -- Subst2_top


Define Subst2(E,S)
  If RATFUN IsIn Flatten(Shape(S)) Then
    SNum := [ [X[1], Num(X[2])] | X In S];
    SDen := Concat([ [X[1], Den(X[2])] | X In S],
		   [ [X,1] | X In Diff(Indets(), [Y[1]|Y In S])]);
    Return Misc.Subst2RatFun(E,SNum,SDen);
  Else
    Return Misc.Subst2Poly(E,S)
  EndIf;
EndDefine; -- Subst2


Define Subst2RatFun(E,SNum,SDen)
  T := Type(E);
  If T<=RAT Or T=ZMOD Then Return E EndIf;
  If T=POLY Then Return Misc.SubstPolyRF(E,SNum,SDen) EndIf;
  If T=RATFUN Then Return
    Misc.SubstPolyRF(E.Num,SNum,SDen)/Misc.SubstPolyRF(E.Den,SNum,SDen)
  EndIf;
  If T=LIST Then Return [Misc.Subst2RatFun(X,SNum,SDen)|X In E] EndIf;
  If T=MAT Then Return Mat([Misc.Subst2RatFun(X,SNum,SDen)|X In E]) EndIf;
  If T=VECTOR Then Return Vector(Misc.Subst2RatFun(Comps(E),SNum,SDen)) EndIf;
  If T=IDEAL Then Return Ideal(Misc.Subst2RatFun(Gens(E),SNum,SDen)) EndIf;
  If T=MODULE Then Return Module(Misc.Subst2RatFun(Gens(E),SNum,SDen)) EndIf;
  Return Error("Illegal call to Subst");
EndDefine; -- Subst2RatFun


Define SubstPolyRF(P,SNum,SDen)
  Return Sum([ LC(M)*SubstPoly(M,SNum)/SubstPoly(M,SDen) | M In Monomials(P)])
EndDefine; -- SubstPolyRF


Define Subst2Poly(E,S)
  T := Type(E);
  If T<=RAT Or T=ZMOD Then Return E EndIf;
  If T=POLY Then Return SubstPoly(E,S) EndIf;
  If T=RATFUN Then Return SubstPoly(E.Num,S)/SubstPoly(E.Den,S) EndIf;
  If T=LIST Then Return [Misc.Subst2Poly(X,S)|X In E] EndIf;
  If T=MAT Then Return Mat([Misc.Subst2Poly(X,S)|X In E]) EndIf;
  If T=VECTOR Then Return Vector(Misc.Subst2Poly(Comps(E),S)) EndIf;
  If T=IDEAL Then Return Ideal(Misc.Subst2Poly(Gens(E),S)) EndIf;
  If T=MODULE Then Return Module(Misc.Subst2Poly(Gens(E),S)) EndIf;
  Return Error("Illegal call to Subst")
EndDefine; -- Subst2Poly


Define Subst(...)
--  Help "Subst({POLY,LIST,MAT,VECTOR,IDEAL,MODULE},SUBST) or Subst(EXPR,X,POLY)";
  If Not NoMoreThanOneRing(ARGV) Then
    Return Error("Illegal call to Subst");
  EndIf;
  N:=Len(ARGV);
  If N=2 Then Return Misc.Subst2_top(ARGV[1],ARGV[2]) EndIf;
  If N=3 Then Return Misc.Subst3(ARGV[1],ARGV[2],ARGV[3]) EndIf;
  Return ERR.BAD_PARAMS_NUM;
EndDefine; -- Subst


-------------------------------

Define Image(X, Phi)
  If Type(Phi) <> TAGGED("RMap") Then
    Error("Image: second argument must be RMap(..)");
  EndIf;
  If Type(X) IsIn [IDEAL, POLY, MODULE, VECTOR, RATFUN] Then
    Using Var(RingEnv(Phi[1])) Do
      If TypeOfCoeffs() <> (Var(RingEnv(X))::TypeOfCoeffs()) Then
	Error("Image: Rings must have same coefficient ring");
      EndIf;
    EndUsing;
    If TypeOfCoeffs() = ZMOD Then
      If Characteristic(Var(RingEnv(Phi[1]))) <>
	Characteristic(Var(RingEnv(X))) Then
	Error("Image: Rings must have same coefficient ring");
      EndIf;
    EndIf;
  EndIf;
  If Len(Tag(X))>0 Then Return Tagged(Image(Untagged(X),Phi),Tag(X)) EndIf;
  T := Type(X);
  If T = IDEAL  Then Return Ideal(Misc.ImagePolyList(Gens(X), Phi)) EndIf;
  If T = POLY   Then Return Comp(Misc.ImagePolyList([X], Phi), 1) EndIf;
  If T = MODULE Then Return Module(Misc.ImageVectorList(Gens(X), Phi)) EndIf;
  If T = VECTOR Then Return Comp(Misc.ImageVectorList([X], Phi), 1) EndIf;
  If T = RATFUN Then
    F:=Misc.ImagePolyList([X.Num,X.Den],Phi);    Return F[1]/F[2]
  EndIf;
  If T = MAT    Then Return Mat([Misc.Image(Row,Phi)| Row In X]) EndIf;
  If T = LIST   Then Return [Misc.Image(Y,Phi)| Y In X] EndIf;
  If T = RECORD Then
    Return Misc.BuildRecord(Fields(X), Misc.Image(FieldValues(X),Phi))
  EndIf;
  Return X;  /* anything left which is ring-dependent? */
EndDefine; -- Image


Define MapPolyList(L, FromRing, ML)
  Return [$builtin.Map(X, FromRing, RingEnv(), ML) | X In L];
EndDefine; -- MapPolyList

Define MapVectorList(L, FromRing, ML)
  Return [Vector([$builtin.Map(Y, FromRing, RingEnv(), ML) | Y In X]) | X In L];
EndDefine; -- MapVectorList

Define ImagePolyList(L, Phi)
  FromRing := RingEnv(L[1]);
  NFrom := NumIndets(Var(FromRing));
  If NFrom <> Len(Untagged(Phi)) Then
    Error("Image: RMap has " + Sprint(Len(Untagged(Phi)))
	  + " image(s), domain has " + Sprint(NFrom)
	  + " indeterminate(s)")
  EndIf;
  RL := $hilop.ListRingEnv(Untagged(Phi));
  If RL <> "" Then  Phi := [Poly(F) | F In Untagged(Phi)];  EndIf;
  Using Var(RingEnv(Phi[1])) Do
    NTo := NumIndets();
    D := Diff(Untagged(Phi), [1]);
    If IsSubset(D, Indets()) And MakeSet(D)=D Then
      MapList := NewList(NTo, 0);
      For I := 1 To NFrom Do
	If Phi[I]<>1 Then MapList[IndetIndex(Phi[I])]:=I EndIf;
      EndFor;
      Return Misc.MapPolyList(L, FromRing, MapList)
    EndIf;
    If NFrom <= NTo Then
      ML := Concat(1..NFrom, NewList(NTo-NFrom, 0));
      Y := Misc.MapPolyList(L, FromRing, ML);
      Y := Subst(Y, [[Indet(I), Phi[I]] | I In 1..NFrom]);
    Else
      RE := RingEnv();
      Using Var(FromRing) Do
	ML := Concat(1..NTo, NewList(NFrom-NTo, 0));
	Phi := Misc.MapPolyList(Var(RE)::[Poly(X)|X In Phi], RE, ML);
	L := Subst(L, [[Indet(I), Phi[I]] | I In 1..(Len(Phi))]);
      EndUsing;
      Y := Misc.MapPolyList(L, FromRing, 1..NTo);
    EndIf;
    Return Y;
  EndUsing;
EndDefine; -- ImagePolyList


Define ImageVectorList(L, Phi)
  FromRing := RingEnv(L[1]);
  NFrom := NumIndets(Var(FromRing));
  If NFrom <> Len(Untagged(Phi)) Then
    Error("Image: RMap has " + Sprint(Len(Untagged(Phi)))
	  + " image(s), domain has " + Sprint(NFrom)
	  + " indeterminate(s)")
  EndIf;
  RL := $hilop.ListRingEnv(Untagged(Phi));
  If RL <> "" And RL <> RingEnv() Then
    Error("Image: RMap must be defined in the codomain " + RingEnv())
  EndIf;
  NTo := NumIndets();
  D := Diff(Untagged(Phi), [1]);
  If IsSubset(D, Indets()) And MakeSet(D)=D Then
    MapList := NewList(NTo, 0);
    For I := 1 To NFrom Do
      If Phi[I]<>1 Then MapList[IndetIndex(Phi[I])]:=I EndIf;
    EndFor;
    Return Misc.MapVectorList(L, FromRing, MapList)
  EndIf;
  If NFrom <= NTo Then
    ML := Concat(1..NFrom, NewList(NTo-NFrom,0));
    Y := Misc.MapVectorList(L, FromRing, ML);
    Y := Subst(Y, [[Indet(I), Phi[I]] | I In 1..NFrom]);
  Else
    RE := RingEnv();
    Using Var(FromRing) Do
      ML := Concat(1..NTo, NewList(NFrom-NTo, 0));
      Phi := Misc.MapPolyList(Var(RE)::[Poly(X)|X In Phi], RE, ML);
      L := Subst(L, [[Indet(I), Phi[I]] | I In 1..(Len(Phi))]);
    EndUsing;
    Y := Misc.MapVectorList(L, FromRing, 1..NTo);
  EndIf;
  Return Y;
EndDefine; -- ImageVectorList


Define Map(E,R1,R2,L)
  -- E, an object
  -- R1, R2 strings, the names of the domain and codomain
  -- L = [N1,...,Nn] list of integers ...
  If Len(L) <> NumIndets(Var(R2)) Then ERR.BAD_PARAMS EndIf;
  T := Type(E);
  If T=POLY Then Return $builtin.Map(E,R1,R2,L) EndIf;
  If T=LIST Then Return [Misc.Map(X,R1,R2,L)|X In E] EndIf;
  If T=MAT Then Return Mat([Misc.Map(X,R1,R2,L)|X In E]) EndIf;
  If T=VECTOR Then
    E1 := Misc.Map(Comps(E),R1,R2,L);
    Return Var(R2) :: Vector(E1)
  EndIf;
  If T=IDEAL Then
    E1 := Misc.Map(E.Gens,R1,R2,L);
    Return Var(R2) :: Ideal(E1)
  EndIf;
  If T=MODULE Then
    E1 := Misc.Map(E.Gens,R1,R2,L);
    Return Var(R2) :: Module(E1)
  EndIf;
  If Len(Tag(E))>0 Then Return Tagged(Misc.Map(Untagged(E),R1,R2,L),Tag(E)) EndIf;
  Return E;
EndDefine; -- Map


-------------------------------

Define BuildRecord(Ns, Os)
  R := Record[];
  For I := 1 To Min(Len(Ns),Len(Os)) Do
    R.Var(Ns[I]) := Os[I]
  EndFor;
  Return R;
EndDefine; -- BuildRecord



-----[ Types ]----------------------------------------------------

Define Shape(V)
  If Type(V) = LIST Then Return [ Shape(X) | X In V ] EndIf;
  If Type(V) = MAT Then Return Mat([ Shape(X) | X In V ]) EndIf;
  If Type(V) = RECORD Then
    R := Record[];
    Foreach F In Fields(V) Do
      R.Var(F) := Shape(V.Var(F))
    EndForeach;
    Return R
  EndIf;
  Return Type(V)
EndDefine; -- Shape


Define IsSubShapeOfSome(S,Shapes)
  Foreach Shape In Shapes Do
    If Misc.IsSubShape(S,Shape) Then Return TRUE EndIf;
  EndForeach;
  Return FALSE
EndDefine; -- IsSubShapeOfSome


Define IsSubShape(S1,S2)
  T1 := Type(S1);
  If S2 = "ANY" Then Return TRUE
  Elif T1 = LIST And S2 = LIST Then Return TRUE
  Elif T1 = RECORD And S2 = RECORD Then Return TRUE
  Elif T1 <> Type(S2) Then Return FALSE
  Elif T1 = LIST Then
    If Len(S1) <> Len(S2) Then
      Return FALSE;
    Else
      For I := 1 To Len(S1) Do
	If Not IsSubShape(S1[I],S2[I]) Then Return FALSE; EndIf;
	Return TRUE;
      EndFor;
    EndIf;
  Elif T1 = RECORD Then
    If Not IsSubset(Fields(S1),Fields(S2)) Then Return FALSE;
    Else Return MakeSet([IsSubShape(S1.Var(F),S2.Var(F))|F In Fields(S1)])=[TRUE]
    EndIf;
  Elif T1 <> TYPE Then ERR.BAD_PARAMS
  Else Return S1 <= S2
  EndIf;
EndDefine; -- IsSubShape

EndPackage;
