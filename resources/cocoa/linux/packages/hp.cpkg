Package $hp -- Hilbert-Poincare Series

Alias HP := $hp,
      GB := $gb,
      SP := $sppoly;

Define About()
  PrintLn "    Author:  A.M.Bigatti";
  PrintLn "    Date:      17 Nov 2005";
  PrintLn "    Added:   non-homogeneous cases";
  PrintLn "    Date:      26 May 2003";
  PrintLn "    Fixed:   PoincareShifts(...)";
  PrintLn "    Date:      13 May 2003";
  PrintLn "    Added:   EvalHilbertFn for univariate non-standard PS";
  PrintLn "    Date:      2 Aug 2002";
  PrintLn "    Added:   Poincare for modules with shifts";
  PrintLn "    Date:      7 Feb 2000";
  PrintLn "    BugFix:  ZMOD Hilbert Function values";
  PrintLn "    BugFix:  TruncBin for BinExp (fixed 0-diml HF accordingly)";
  PrintLn "    BugFix:  DenIsStandard used SP.PPExp";
  PrintLn "    BugFix:  Zero dimensional EvalHilbertFn";
  PrintLn "    Date:      10 Apr 2001";
  PrintLn "    BugFix:  New Tags handling";
EndDefine; -- About

--------------------------------------------------------------

-- TAG "PSeries"; --  [P]oincare [Series]
--     "PSDen";   --  [P]oincare [S]eries [Den]ominator
--     "Hilbert"; --  [Hilbert] Function
-- TYPE PSeries := [SpPoly,PSDen];      --> TAGGED "PSeries"
--      Hilbert := [INT,..,INT,SpPoly]  --> TAGGED "Hilbert"
--      PSDen   := LIST(SpPP);          --> TAGGED "PSDen"

Define MPoincare(Var M, Ws)
  Wa := Option(SuppressWarnings);
  Set SuppressWarnings;
  MVP ::= ZZ[x[1..Len(Ws)]];
  Set SuppressWarnings := Wa;
  If Type(M) = TAGGED("Quotient") Then
    GB := GB.GBasis(M);
    S  := MVPoincare(M, Ws);
  Else
    Error(ERR.BAD_PARAMS, "(Multivariate Poincare)");
  EndIf;
//obsolescent  Destroy MVP;
  Return HP.Tagged(S,"PSeries");
EndDefine;

Define UPoincare(Var M, ShiftsList)
  If Type(M) = TAGGED("Quotient") Then
    GB := GB.GBasis(M);
    If ShiftsList<>[] Then
      Return HP.QuotUPoincareShifts(Untagged(M), ShiftsList);
    EndIf;
    Return HP.Tagged(UVPoincareQuotient(M),"PSeries");
--   Elif Type(M) = RING Then
--     I := Ideal(0);
--     GB := GB.GBasis(I);
--     Return HP.Tagged(UVPoincareQuotient(I),"PSeries");
  Elif Type(M) = MODULE Or Type(M) = IDEAL Then
    Return HP.UPoincareShifts(Module(M), ShiftsList);
  Else
    Return ERR.BAD_PARAMS;
  EndIf;
EndDefine; -- UPoincare

Define UPoincareShifts(Var M, ShiftsList)
  NC := NumComps(M);
  If Len(ShiftsList)<>NC Then
    If ShiftsList = [] Then
      ShiftsList := NewList(NC,0);
    Else
      Error("Poincare: Shifts List has wrong length");
    EndIf;
  EndIf;
  PS := HP.QuotUPoincareShifts(LT(M), ShiftsList);
  Den := Untagged(HP.Den(PS));
  PS0 := HP.QuotUPoincareShifts(Module(NewList(NC,0)),ShiftsList);
  Using QQt Do
    PN := SP.ToPoly(HP.Num(PS0))-SP.ToPoly(HP.Num(PS));
    PSM := HP.PSeries(PN, Den);
  EndUsing;
  Return PSM;
EndDefine; -- UPoincareShifts


Define QuotUPoincareShifts(Var M, ShiftsList)
  NC := NumComps(M);
  If Len(ShiftsList)<>NC Then
    Return Error("Poincare: Shifts List has wrong length");
  EndIf;
  If [ N In ShiftsList | N<0 ] <> [] Then
    Return Error("Poincare: Shifts must be non-negative");
  EndIf;
  Gens_LT_M := Gens(LT(M));
  LT_Ideals := [ Ideal([V[I] | V In Gens_LT_M]) | I In 1..NC ];
  PSs  := [ HP.Poincare(CurrentRing()/LT_Ideals[I]) | I In 1..NC ];
  Den := Untagged(HP.Den(First(PSs)));
  Using QQt Do
    PN  := Sum([ t^(ShiftsList[I])*SP.ToPoly(HP.Num(PSs[I])) | I In 1..NC]);
    PSM := HP.PSeries(PN, Den);
  EndUsing;
  Return PSM;
EndDefine; -- QuotUPoincareShifts

Define ExperimentalUPoincare(Var M)
  If Type(M) = TAGGED("Quotient") Then
    GB := GB.GBasis(M);
    PS := UVPoincareQuotient(M);
--   Elif Type(M) = RING Then
--     I := Ideal(0);
--     GB := GB.GBasis(I);
--     PS := UVPoincareQuotient(I);
  Else
    Error(ERR.BAD_PARAMS, "(Univariate Poincare)");
  EndIf;
  Return QQt :: HP.Tagged([SP.ToPoly(PS[1]), PS[2]],"PSeries");
EndDefine;

Define Poincare(Var M)
  Using Var(RingEnv(M)) Do
    GB := GB.GBasis(M);
    If (False IsIn MakeSet([IsHomog(X) | X In GB])) Then
      PrintLn "WARNING! HilbertPoincare input not homogeneous: computing LT...";
    EndIf;
    W := WeightsMatrix();
    If Len(W)>1 Then Return HP.MPoincare(M,W);
    Else Return HP.UPoincare(M,[]);
    EndIf;
  EndUsing;
EndDefine; -- Poincare

Define AffPoincare(Var M)
  Using Var(RingEnv(M)) Do
    W := WeightsMatrix();
    If Len(W)>1 Then
      Error("AffPoincare: grading must be a positive ZZ-grading: "+Sprint(W));
    EndIf;
    If MakeSet(Comp(List(Ord()),1)) <> [1] Then
      Error("AffPoincare: first row of ordering matrix must have only 1");
    EndIf;
    GB := GBasis(M);
    If Type(M) = TAGGED("Quotient") Then
      PS := Poincare(Tagged(LT(Untagged(M)), "Quotient"));
    Else
      PS := Poincare(LT(M));
    EndIf;
    Return HP.Make(HP.UntaggedNum(PS), Concat(HP.UntaggedDen(PS), [[1]]));
  EndUsing;
EndDefine; -- Poincare

Define PoincareShifts(Var M, ShiftsList)
  W := WeightsMatrix();
  If Len(W)>1 Then
    Error("PoincareShifts does not compute multivariate Poincare Series")
  EndIf;
  Return HP.UPoincare(M, ShiftsList);
EndDefine; -- PoincareShifts

Define Hilbert(...)
  If Len(ARGV)=1 Then
    Return HP.PSerToHilbert(Poincare(ARGV[1]));
  Else
    Return HP.PSerToHilbert(Poincare(ARGV[1]),ARGV[2]);
  EndIf;
EndDefine; -- Hilbert

Define AffHilbert(...)
  If Len(ARGV)=1 Then
    Return HP.PSerToHilbert(AffPoincare(ARGV[1]));
  Else
    Return HP.PSerToHilbert(AffPoincare(ARGV[1]),ARGV[2]);
  EndIf;
EndDefine; -- Hilbert

	
Define HilbertPoly(Q)  Return HP.PSerToHilbertPoly(Poincare(Q));EndDefine;
Define HVector(Q)      Return HP.PSerHVector(Poincare(Q)); EndDefine;

Define Multiplicity(M)
  Using Var(RingEnv(M)) Do
    GB := GB.GBasis(M);
    If (False IsIn MakeSet([IsHomog(X) | X In GB])) Then
      Return HP.PSerMultiplicity(AffPoincare(M));
    Else
      Return HP.PSerMultiplicity(Poincare(M));
    EndIf;
  EndUsing;
EndDefine;

Define Dim(M)
  Using Var(RingEnv(M)) Do
    GB := GB.GBasis(M);
    If (False IsIn MakeSet([IsHomog(X) | X In GB])) Then
      Return HP.PSerDim(AffPoincare(M))-1;
    Else
      Return HP.PSerDim(Poincare(M));
    EndIf;
  EndUsing;
EndDefine;

Define RegularityIndex(X)
  If Type(X) = TAGGED($.Tag("PSeries")) Then
    Return $.PSerRegularityIndex(X);
  EndIf;
  If Type(X) = TAGGED($.Tag("Hilbert")) Then
    Return $.HFRegularityIndex(X);
  EndIf;
  Error("RegularityIndex: TAGGED("
	+ $.Tag("PSeries") + ") or TAGGED("
	+ $.Tag("Hilbert") + ") expected");
EndDefine; -- RegularityIndex

-------------------------------

Define PSerHVector(PSer)
  Return SP.ToHVec(HP.Num(HP.Simplified(PSer)));
EndDefine;

Define PSerDim(PSer)
  Return Len(HP.Den(HP.Simplified(PSer)));
EndDefine;

Define PSerMultiplicity(PSer)
  Return Sum(HP.PSerHVector(PSer));
EndDefine;

-------------------------------

Define UntaggedNum(PSer)
  Return Untagged(PSer[1]);
EndDefine; -- UntaggedNum

Define UntaggedDen(PSer)
  Return Untagged(PSer[2]);
EndDefine; -- UntaggedDen

Define Num(PSer)
  Return SP.Tagged($.UntaggedNum(PSer),"SpPoly");
EndDefine; -- Num

Define Den(PSer)
  Return HP.Tagged($.UntaggedDen(PSer),"PSDen");
EndDefine; -- Den

Define Make(SP,PSDen)
  Return HP.Tagged([SP,PSDen],"PSeries");
EndDefine; -- Make

Define IsStandard(PSer)
  Return HP.DenIsStandard(HP.Den(PSer));
EndDefine; -- IsStandard

Define DenMakeStandard(N)
--  Return HP.Tagged(NewList(N,[1]),"PSDen");
  Return NewList(N,[1]);
EndDefine; -- DenMakeStandard

Define DenToPoly(PSer)
  Return Product([1-LogToTerm(PP) | PP In Untagged(HP.Den(PSer))]);
EndDefine; -- DenToPoly

Define NumToPoly(PSer)
  Return SP.ToPoly(HP.Num(PSer));
EndDefine; -- NumToPoly

Define ToRatFun(PSer)
  Return HP.NumToPoly(PSer)/HP.DenToPoly(PSer);
EndDefine; -- ToRatFun

-------------------------------

Define PolyToPNum(P)
  PSNum := SP.PolyTo(P);
  For I := 1 To Len(PSNum) Do
    PSNumCoeff := Cast(PSNum[I,1],INT);
    If Characteristic()<>0 And PSNumCoeff>Characteristic()/2  Then
      PSNumCoeff := PSNumCoeff-Characteristic()
    EndIf;
    PSNum[I,1] := PSNumCoeff;
  EndFor;
  Return SP.Tagged(Reversed(PSNum),"SpPoly")
EndDefine; -- PolyToPNum


Define PSeries(P, DenExp_Or_Den)
  P := Poly(P);
  Idx := UnivariateIndetIndex(P);
  If Idx=0 Then Error("PSeries: Only univariate POLY") EndIf;
  If Type(DenExp_Or_Den)=INT Then
    DenExp := DenExp_Or_Den;
    If DenExp<0 Then Error("PSeries: Expected non-negative INT") EndIf;
    Den := HP.DenMakeStandard(DenExp);
  Else
    Den := DenExp_Or_Den;
  EndIf;
  PSNum := SP.PolyTo(P);
  LenPSNum := Len(PSNum);
  NewPSNum := NewList(LenPSNum, [0,0]);
  For I := 1 To LenPSNum Do
    PSNumCoeff := PSNum[I,1];
    If Type(PSNumCoeff) = RAT And PSNumCoeff[2]<>1 Then
      Error("PSeries: numerator must have integer coefficients")
    EndIf;
    If Type(PSNumCoeff)<>INT Then  PSNumCoeff := Cast(PSNumCoeff[1],INT) EndIf;
    NewPSNum[LenPSNum-I+1,1] := PSNumCoeff;
    NewPSNum[LenPSNum-I+1,2] := [PSNum[I,2,Idx]];
  EndFor;
  Return HP.Make(NewPSNum, Den)
EndDefine;

-------------------------------

Define Simplified(PSer)
  If Not HP.IsStandard(PSer) Then
    Error("Simplified: Operator not available for non-standard HPSeries")
  EndIf;
  If Untagged(HP.Num(PSer))=[] Then Return
    HP.Make([],HP.DenMakeStandard(0))
  EndIf;
  Using QQt Do
    HPNum    := SP.ToPoly(HP.Num(PSer));
    HPDenExp := Len(Untagged(HP.Den(PSer)));
    While $builtin.Mod(HPNum, 1-t) = 0 Do
      HPNum    := $builtin.Div(HPNum, 1-t);
      HPDenExp := HPDenExp - 1
    EndWhile;
    HPNum := Untagged(SP.PolyTo(HPNum));
  EndUsing;
  Return HP.Make(Reversed(HPNum), HP.DenMakeStandard(HPDenExp))
EndDefine;

-------------------------------

Define DenIsStandard(PSDen)
  Foreach PP In Untagged(PSDen) Do
    If PP<>[1] Then Return FALSE EndIf;
  EndForeach;
  Return TRUE;
EndDefine; -- DenIsStandard

--------------------------------
--<<   Hilbert Functions    >>--
--------------------------------

Define PSerToHilbert(...)
  If Len(ARGV) = 2 Then
    Return HP.EvalHilbertFn(ARGV[1],ARGV[2]);
  Elif
    Len(ARGV) = 1 Then
    Return HP.PSerToHilbertFn(ARGV[1])
  Else Error(ERR.BAD_PARAMS_NUM, ": 1 or 2 expected (Hilbert)")
  EndIf;
EndDefine;

-------------------------------

Define AuxHilbertPoly(SimplifiedPS)
  If Untagged(HP.Num(SimplifiedPS))=[] Then Return 0 EndIf;
  HV  := SP.ToHVec(HP.Num(SimplifiedPS));
  DIM := Len(HP.Den(SimplifiedPS));
  Return Sum([HV[I]*Bin(Indet(1)+DIM-I,DIM-1) | I In 1..Len(HV)])
EndDefine; -- AuxHilbertPoly


Define PSerToHilbertPoly(PS)
  SPS := HP.Simplified(PS);
  If Untagged(HP.Den(SPS)) = [] Then Return 0 EndIf;
  Return QQt :: HP.AuxHilbertPoly(SPS);
EndDefine; -- PSerToHilbertPoly


Define PSerToHilbertFn(PS)
  If Untagged(HP.Num(PS))=[] Then Return HP.Tagged([ [], 0],"Hilbert") EndIf;
  SPS := HP.Simplified(PS);
  DIM := Len(HP.Den(SPS));  HV := SP.ToHVec(HP.Num(SPS));
  If DIM=0 Then Return HP.Tagged([HV,0],"Hilbert") EndIf;
  REG := Len(HV)-DIM;
  If REG > 0 Then
    TrBins := Concat( NewList(REG-1,0),
                   [HP.TruncBin(I,DIM-1) | I In (DIM-1)..(DIM+REG)]);
    HF1 :=[Sum([HV[I]*TrBins[N-I+REG+1] | I In 1..(N+1)]) | N In 0..(REG-1)];
  Else
    HF1 := [];
  EndIf;
  Return HP.Tagged([HF1, HP.PSerToHilbertPoly(SPS)],"Hilbert")
EndDefine; -- PSerToHilbertFn


Define PSerRegularityIndex(PS)
  If Untagged(HP.Num(PS))=[] Then Return 0 EndIf;
  SPS := HP.Simplified(PS);
  DIM := Len(HP.Den(SPS));  HV := SP.ToHVec(HP.Num(SPS));
  Return Len(HV)-DIM;
EndDefine; -- PSerRegularityIndex


Define ExperimentalHF(PS) -- not good
  SPS := HP.Simplified(PS);
  DIM := Len(HP.Den(SPS));  HV := SP.ToHVec(HP.Num(SPS));
  If DIM=0 Then Return HP.Tagged([HV,0],"Hilbert") EndIf;
  REG := Len(HV)-DIM;
  P := HP.NumToPoly(SPS);
  If REG > 0 Then
Time    F := Sum([t^I | I In 0..(REG-1)])^DIM;
Time    F := Mod(F, t^REG);
Time    F := Mod(P*F, t^REG);
    HF1 := NewList(REG,0);
    For I := 1 To Len(F) Do
      T := LT(F); C := LC(F);
      HF1[$builtin.Deg(T)+1] := C;
      F := F - C*T;
    EndFor;
  Else
    REG := 0; HF1 := [];
  EndIf;
  Return HP.Tagged([HF1, HP.PSerToHilbertPoly(SPS)],"Hilbert")
EndDefine;


Define EvalHilbertFn(X, N)
  If Type(X)=Type(HP.Tagged([],"Hilbert")) Then
    If N<Len(X[1]) Then Return X[1,N+1]
    Else  Return QQt :: LC(Subst(X[2],[[t, N]]))
    EndIf;
  EndIf;
  If Type(X)=Type(HP.Tagged([],"PSeries")) Then
    If Untagged(HP.Num(X))=[] Then Return 0 EndIf;
    If HP.IsStandard(X) Then
      SPS := HP.Simplified(X);
      HV  := SP.ToHVec(HP.Num(SPS));
      DIM := Len(HP.Den(SPS));
      If DIM = 0 Then
	If 0<=N And N<Len(HV) Then Return HV[N+1] Else Return 0 EndIf;
      EndIf;
      Return Sum([HV[I]*HP.TruncBin(N+DIM-I,DIM-1) | I In 1..Len(HV)])
    EndIf;
    // univariate and not standard
    HPDen := Untagged($hp.Den(X));
    If Len(HPDen[1]) = 1 Then
      Using QQt Do
	D := 1;
	Foreach W In HPDen Do
	  D := D * Sum([t^(I*W[1]) | I In 0..Div(N, W[1])]);
	  D := NR(D, [t^(N+1)]);
	EndForeach;
	F := NR(D * $hp.NumToPoly(X), [t^(N+1)]);
	RatResult := CoeffOfTerm(t^N, F);
	Return RatResult.Num;
      EndUsing;
    EndIf;
  EndIf;
  Error(ERR.BAD_PARAMS, "(EvalHilbertFn)");
EndDefine; -- EvalHilbertFn


Define HFRegularityIndex(HF)
  HFSmall := HF[1];
  HP := HF[2];
  For D := Len(HFSmall)-1 To 0 Step -1 Do
    If HFSmall[D+1] <> Eval(HP, [D]) Then Return D+1 EndIf;
  EndFor;
  Return 0;
EndDefine; -- HFRegularityIndex


------[   pretty printing   ]--------

Define Tagged(X,T)
  Return Tagged(X, HP.PkgName()+"."+T);
EndDefine;

Define Tag(T)
  Return HP.PkgName() + "." + T;
EndDefine;

Define Print_PSDen(PSDen)
  L := Len(Untagged(PSDen));
  If HP.DenIsStandard(PSDen) Then
    Print "(1-",Indet(1),")";
    If L<>1 Then  Print "^", L  EndIf
  Else
    If L<>1 Then  Print "( " EndIf;
    Foreach PP In PSDen Do  Print "(1-", LogToTerm(PP), ") "  EndForeach;
    If L<>1 Then  Print ")" EndIf;
  EndIf;
EndDefine;


Define Print_PSeries(PSer)
  If HP.IsStandard(PSer) Then
    PSer := HP.Simplified(PSer)
  Else
    PrintLn "---  Non-simplified HilbertPoincare' Series  ---"
  EndIf;
  Print "(", HP.Num(PSer), ")";
  HP_Den := HP.Den(PSer);
  If Len(HP_Den)>0 Then
    Print " / ", HP_Den;
  EndIf;
EndDefine; -- Print_PSeries

Define Print_Hilbert(HF)
  For I:=1 To Len(HF[1]) Do PrintLn "H(",I-1,") = ",HF[1,I] EndFor;
  Using QQt Do
    Print "H(t) = ", HF[2], "   for t >= ", Len(HF[1])
  EndUsing;
EndDefine;


Define TruncBin(A,B)
  If A<B Or B<0 Then Return 0 EndIf;
  Return Bin(A,B);
EndDefine; -- TruncBin

------[ Hilbert multideg ]-----------------------------------------------

Define HilbertSeriesMultiDeg(QR, WM)
  Return $hp.PoincareMultiDeg(QR, WM);
EndDefine;

Define PoincareMultiDeg(QR, WM)
  If Type(QR) <> TAGGED("Quotient") Then
    Error("PoincareMultiDeg: first argument must be a quotient ring", QR);
  EndIf;
  If Type(WM) <> MAT Then
    Error("PoincareMultiDeg: second argument must be a matrix", WM);
  EndIf;
  If NumCols(WM) <> NumIndets() Then
    Error("PoincareMultiDeg: wrong number of weights:" + Sprint(NumCols(WM)) + " " + Sprint(NumIndets()), WM);
  EndIf;
  ZeroesInFirstRow := [ W In WM[1] | W<=0 ] <> [];
  If ZeroesInFirstRow Then WM := PositiveGrading4(WM); EndIf;
  AuxRing := NewId();
  Var(AuxRing) ::= CoeffRing[x[1..Len(WM[1])]], Weights(WM);
  Using Var(AuxRing) Do
//    PS := Poincare(CurrentRing()/$contrib/specvar.CanonicalImage(I));
    PS := Poincare(CurrentRing()/$contrib/specvar.CanonicalImage(Untagged(QR)));
  EndUsing;
//obsolescent  Destroy Var(AuxRing);
  If Not ZeroesInFirstRow Then
    Return PS;
  EndIf;
  PSNum := [ [ T[1], Tail(T[2]) ] | T In HP.Num(PS)];
  PSDen := [ Tail(T) | T In HP.Den(PS)];
  Return HP.Make(PSNum, PSDen);
EndDefine; -- PoincareOfSeparated



/* EXAMPLES
Use R ::= QQ[x,y,z];

//PoincareMultideg(Ideal(Indets())^2, LexMat(NumIndets()));

WM := Mat([[1,0,0],[1,-1,0]]);
PoincareMultiDeg(R/Ideal(Indets())^2, WM);

WM := Mat([[1,0,0],[-1,1,1]]);
PoincareMultiDeg(R/Ideal(0), WM);

WM := Mat([[1,7,0],[0,-5,1]]);
PositiveGrading(WM);
*/
------[ end of Hilbert multideg ]-------------------------------------------


EndPackage; -- Package $hp

