@echo off
rem ############################################################################
rem # INSTRUCTIONS:
rem # Change the definition of COCOA_PREFIX to point to the directory where
rem # you have placed the CoCoA packages: that is the directory in which
rem # "packages/" resides (but not including "packages"). The path must not
rem # be terminated with a final slash!
rem #
rem # Now change the definition of COCOA_EXECUTABLE to be the path to the
rem # binary executable of CoCoA; this will normally end with .../cocoa_text
rem # Normally this is just COCOA_PREFIX with "cocoa_text" tacked on the end.
rem ############################################################################
rem # Make sure these four variables have the correct values for your system.
rem # Below are default values which should work for "standard" installations.

set COCOA_PREFIX=C:\cocoa-4.7
set COCOA_EXECUTABLE=%COCOA_PREFIX%\cocoa_text.exe
set COCOA_PACKAGES=%COCOA_PREFIX%\packages
set COCOARC=.\\userinit.coc

rem # You should not need to alter anything below this line.
rem ############################################################################
rem ############################################################################

:step2
rem # Parse command line arguments...

set QUITATEND=
set COCOABATCHFILES=

:parse
if "%1"=="" goto finished
if "%1"=="-q" goto quitfound
if exist %1 goto appendfile

:fileerror
echo WARNING, ignoring unreadable batch file %1
shift
goto parse

:quitfound
set QUITATEND=Quit;
shift
goto parse

:appendfile
set COCOABATCHFILES=%COCOABATCHFILES%Source("%1");
shift
goto parse

:finished
set COCOABATCHFILES=%COCOABATCHFILES%%QUITATEND%
set QUITATEND=
goto step3

rem ############################################################################

:step3
rem # execute
cd %COCOA_PREFIX%
%COCOA_EXECUTABLE%
