Package $approx -- find simple approximations to rationals

Alias Self := $approx;


Define About()
  PrintLn "    Author: J Abbott";
  PrintLn "    Date: 24 May 2003";
  PrintLn "    Comment: first version, efficiency not a strong point.";
EndDefine; -- About
-----------------------------------------------------------------------------
-- The exported functions of this package are:
--  FloatApprox    finds an approximation of the form M*10^E within a
--                 maximum specified relative error.
--  CFApprox       finds the "simplest" rational approximation within a
--                 maximum specified relative error.
--  CFApproximants returns a list of all continued fraction approximants
--                 to a specified rational
--  ContFrac       returns a list of the continued fraction "denominators"
--                 for a given rational number.

Define FloatApprox(N, RelativeError)
  If Type(N) <> INT And Type(N) <> RAT Then Error("FloatApprox: first argument must be rational"); EndIf;
  If Type(RelativeError) <> INT And Type(RelativeError) <> RAT Then
    Error("FloatApprox: second argument must be rational");
  EndIf;
  If RelativeError <= 0 Or RelativeError > 0.1 Then RelativeError := 0.1; EndIf;
  If N = 0 Then Return 0; EndIf;
  Ndigits := 1;
  Power := 1/5;
  While Power > RelativeError Do
    Power := Power/10;
    Ndigits := Ndigits+1;
  EndWhile;
  Tmp := MantissaAndExponent(N, Ndigits);
  Return Tmp.Mantissa*10^(Tmp.Exponent - Ndigits + 1);
EndDefine; -- FloatApprox


Define ContFrac(N)
  If Type(N) = INT Then Return [N]; EndIf;
  If Type(N) <> RAT Then Error("ContFrac: argument must be rational"); EndIf;
  If N = 0 Then Return [0]; EndIf; -- We define this result for zero.
  D := Den(N);
  N := Num(N);
  Ans := [];
  While D <> 0 Do
    Quot := Div(N, D);
    Append(Ans, Quot);
    N := N - Quot*D;
    Swap := N; N := D; D := Swap;
  EndWhile;
  Return Ans;
EndDefine; -- ContFrac


Define ContFracToRat(L)
  If Type(L) <> LIST Or MakeSet(Shape(L)) <> [INT] Then
    Error("ContFracToRat: argument should be a list of (positive) integer \"quotients\"");
  EndIf;
  N1 := 0; N2 := 1;
  D1 := 1; D2 := 0;
  Foreach Q In L Do
    If D2 <> 0 And Q < 1 Then Error("ContFracToRat: negative continued fraction quotient"); EndIf;
    N1 := N1 + Q*N2;
    Swap := N1; N1 := N2; N2 := Swap;
    D1 := D1 + Q*D2;
    Swap := D1; D1 := D2; D2 := Swap;
  EndForeach;
  Return N2/D2;
EndDefine; -- ContFracToRat
  

Define CFApprox(N, RelativeError)
  If Type(N) <> INT And Type(N) <> RAT Then Error("CFApprox: first argument must be rational"); EndIf;
  If Type(RelativeError) <> INT And Type(RelativeError) <> RAT Then
    Error("CFApprox: second argument must be rational");
  EndIf;
  If RelativeError <= 0 Or RelativeError > 0.1 Then RelativeError := 0.1; EndIf;
  If N = 0 Then Return N; EndIf;
  AbsError := Abs(N)*RelativeError;
  RecipError := 1+Div(Den(AbsError), Num(AbsError));
  CF := Self.ContFrac(N);
  N1 := 0; N2 := 1;
  D1 := 1; D2 := 0;
  Foreach Denom In CF Do
    If D2*Denom > RecipError Then Break; EndIf;
    N1 := N1 + Denom*N2;
    Swap := N1; N1 := N2; N2 := Swap;
    D1 := D1 + Denom*D2;
    Swap := D1; D1 := D2; D2 := Swap;
  EndForeach;
  Return N2/D2;
EndDefine; -- CFApprox


Define CFApproximants(N)
  If Type(N) = INT Then Return [N]; EndIf;
  If N = 0 Then Return [0]; EndIf;
  If Type(N) <> RAT Then Error("CFApproximants: argument must be rational"); EndIf;
  CF := Self.ContFrac(N);
  N1 := 0; N2 := 1;
  D1 := 1; D2 := 0;
  Ans := [];
  Foreach Denom In CF Do
    N1 := N1 + Denom*N2;
    Swap := N1; N1 := N2; N2 := Swap;
    D1 := D1 + Denom*D2;
    Swap := D1; D1 := D2; D2 := Swap;
    Append(Ans, N2/D2);
  EndForeach;
  Return Ans;
EndDefine; -- CFApproximants

EndPackage; -- end of package $approx
