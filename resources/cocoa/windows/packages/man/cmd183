
-- Automatically generated from the file CoCoAHelp.xml
-- IntersectionList --

MEMORY.Doc.Syntax := ""  +NewLine() +
"IntersectionList(L:LIST of LIST):LIST"  +NewLine() +
"IntersectionList(L:LIST of IDEAL):IDEAL"  +NewLine() +
"IntersectionList(L:LIST of MODULE):MODULE"  +NewLine() +
"";
MEMORY.Doc.Descr := "
The function \"IntersectionList\" applies the function \"Intersection\" to
the elements of a list, i.e., \"IntersectionList([X_1,...,X_n])\" is the
same as \"Intersection(X_1,...,X_n)\".

The coefficient ring must be a field.

NOTE: In order to compute the intersection of inhomogeneous ideals, it
may be faster to use the function \"HIntersectionList\".

To compute the intersection of ideals corresponding to
zero-dimensional schemes, see the commands \"GBM\" and \"HGBM\".

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[x,y,z];
  Points := [[0,0],[1,0],[0,1],[1,1]]; -- a list of points in the plane
  IntersectionList([ Ideal(x-P[1]z, y-P[2]z)  |  P In Points]);
Ideal(y^2 - yz, x^2 - xz)
-------------------------------
  Intersection([\"a\",\"b\",\"c\"],[\"b\",\"c\",\"d\"]);
[\"b\", \"c\"]
-------------------------------
  IntersectionList([Ideal(x,y), Ideal(y^2,z)]);
Ideal(yz, xz, y^2)
-------------------------------
  It = Intersection(Ideal(x,y), Ideal(y^2,z));
True
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    