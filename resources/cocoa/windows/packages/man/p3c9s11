
-- Automatically generated from the file CoCoAHelp.xml
-- Supported Packages --
MEMORY.Doc.Descr := "
Several packages are supported by the CoCoA team.  These packages
contain functions that are not built into CoCoA because they are of a
more specialized or experimental nature.

The supported packages are:

    CantStop         -- the first game in CoCoA  ;-)
    conductor.cpkg   -- conductor sequence of points
    control.cpkg     -- tools for geometric Control Theory
    galois.cpkg      -- computing in a cyclic extension
    intprog.cpkg     -- integer programming
    invariants.cpkg  -- generators of an algebra of invariants
    matrixnormalform.cpkg  -- Smith normal form of a matrix
    primary.cpkg     -- primary ideals
    specvar.cpkg     -- special varieties
    stat.cpkg        -- statistics, design of experiment
    thmproving.cpkg  -- geometrical theorem proving
    typevectors.cpkg -- typevectors for ideals of points

All of these packages are included in /packages/contrib of the distribution
of CoCoA.  The packages are likely to be updated more often than
CoCoA, itself, and new packages may appear; so it may be worth
checking at the CoCoA distribution sites, e.g.,
\"http://cocoa.dima.unige.it/\".

HOW TO USE A SUPPORTED PACKAGE
(1) save the package in /packages/contrib/, if necessary;
(2) to get the syntax, description, and examples of the main functions
   and a suggested alias for the package, type
       \"$contrib/\"package_name\".Man();\"
(3) to know the author and version number, type
       \"$contrib/\"package_name\".About();\"
or just XX.Man(); XX.About()
where XX is a defined alias (type \"Aliases();\" to get the list)

NOTE: The packages will load automatically when one of their functions
is called (see \"Package Sourcing and Autoloading\") for more
information.

See below for more details about specific supported packages.
    ";



    