
-- Automatically generated from the file CoCoAHelp.xml
-- QZP --

MEMORY.Doc.Syntax := ""  +NewLine() +
"QZP(F:POLY):POLY"  +NewLine() +
"QZP(F:LIST of POLY):LIST of POLY"  +NewLine() +
"QZP(I:IDEAL):IDEAL"  +NewLine() +
"";
MEMORY.Doc.Descr := "
The functions \"QZP\" and \"ZPQ\" map polynomials and
ideals of other rings into ones of the current ring.
When mapping from one ring to another, one of the rings must have
coefficients in the rational numbers and the other must have
coefficients in a finite field.  The indeterminates in both
rings must be identical.

The function \"QZP\" maps polynomials with rational coefficients to
polynomials with coefficients in a finite field; the function \"ZPQ\"
does the reverse, mapping a polynomial with finite field coefficients
into one with rational (actually, integer) coefficients.  The function
\"ZPQ\" is not uniquely defined mathematically, and currently for each
coefficient the least non-negative equivalent integer is chosen.
Users should not rely on this choice, though any change will be
documented.

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[x,y,z];
  F := 1/2*x^3 + 34/567*x*y*z - 890; -- a poly with rational coefficients
  Use S ::= ZZ/(101)[x,y,z];
  QZP(F);                            -- compute its image with coeffs in ZZ/(101)
-50x^3 - 19xyz + 19
-------------------------------
  G := It;
  Use R;
  ZPQ(G);                      -- now map that result back to QQ[x,y,z]
                               -- it is NOT the same as F...
51x^3 + 82xyz + 19
-------------------------------
  H := It;
  F - H;                       -- ... but the difference is divisible by 101
-101/2x^3 - 46460/567xyz - 909
-------------------------------
  Use S;
  QZP(H) - G;                  -- F and H have the same image in ZZ/(101)[x,y,z]
0
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    