
-- Automatically generated from the file CoCoAHelp.xml
-- GCDFreeBasis --

MEMORY.Doc.Syntax := ""  +NewLine() +
"GCDFreeBasis(L:LIST of INT):LIST of INT"  +NewLine() +
""  +NewLine() +
"";
MEMORY.Doc.Descr := "
This function returns a GCD free basis of a set of integers; you can
think of this as the set of all numbers (except 1) obtainable by performing GCD
and exact division operations.

Given a set N = [ N_1,...N_k ] we seek a basis G = [ G_1,...,G_s ] such that
each N_i is a product of powers of the G_j, and the G_j are pairwise
coprime; the set G is called a GCD free basis for N.  In general the set
G is not uniquely defined.

//==========================  EXAMPLE  ==========================\\\\
  GCDFreeBasis([Fact(20), Fact(10)]);
[46189, 4, 14175]
-------------------------------

\\\\==========================  o=o=o=o  ===========================//";




    