
-- Automatically generated from the file CoCoAHelp.xml
-- Interpolate --

MEMORY.Doc.Syntax := ""  +NewLine() +
"Interpolate(Points:LIST, Values:LIST):POLY"  +NewLine() +
""  +NewLine() +
"where Points is a list of lists of coefficients representing a set of"  +NewLine() +
"distinct points and Values is a list of the same size containing"  +NewLine() +
"numbers from the coefficient ring."  +NewLine() +
"";
MEMORY.Doc.Descr := "
This function returns a multivariate polynomial which takes given
values at a given set of points.

NOTE:

 * the current ring must have at least as many indeterminates as the
   dimension of the space in which the points lie;

 * the base field for the space in which the points lie is taken to be
   the coefficient ring, which should be a field;

 * in the polynomials returned, the first coordinate in the space is
   taken to correspond to the first indeterminate, the second to the
   second, and so on;

 * if the number of points is large, say 100 or more, the returned
   value can be very large.  To avoid possible problems when printing
   such values as a single item we recommend printing out the elements
   one at a time as in this example:

//==========================  EXAMPLE  ==========================\\\\
  X := Interpolate(Pts, Vals);
  Foreach Element In X Do
    PrintLn Element;
  EndForeach;

  Use QQ[x,y];
  Points := [[1/2, 2], [3/4, 4], [5, 6/11], [-1/2, -2]];
  Values := [1/2,1/3,1/5,-1/2];
  F := Interpolate(Points, Values);
  F;
-46849/834000y^2 - 1547/52125x + 13418/52125y + 46849/208500
-------------------------------
  [Eval(F, P) | P In Points] = Values;  -- check
True
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    