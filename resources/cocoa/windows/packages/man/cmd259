
-- Automatically generated from the file CoCoAHelp.xml
-- Monic --

MEMORY.Doc.Syntax := ""  +NewLine() +
"Monic(F:POLY):POLY"  +NewLine() +
"Monic(L:LIST of POLY):LIST of POLY"  +NewLine() +
"";
MEMORY.Doc.Descr := "
In the first form, this function returns F divided by its leading
coefficient (see \"LC\") or, if F is zero, it returns zero.

In its second form, it returns the list obtained by applying the first
form of Monic to each of the components of L.

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[x,y];
  L := [4x^5-y^2,3x-2y^4];
  Monic(L);
[x^5 - 1/4y^2, y^4 - 3/2x]
-------------------------------
  Use R ::= ZZ[x,y];
-- WARNING: Coeffs are not in a field
-- GBasis-related computations could fail to terminate or be wrong
-------------------------------

-------------------------------
  L := [4x^5-y^2,3x-2y^4];
Monic(L);  -- can't invert coefficients over ZZ

-------------------------------
ERROR: Cannot divide
CONTEXT: Cond(Type(X) = LIST, [Monic(A)|A IN X], IsZero(X), X, X / LC(X))
-------------------------------
  Use R ::= ZZ/(5)[x,y];
  F := 2x^2+4y^3;
  Monic(F);
y^3 - 2x^2
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    