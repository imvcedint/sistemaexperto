
-- Automatically generated from the file CoCoAHelp.xml
-- Tagging an Object --
MEMORY.Doc.Descr := "
If E is any CoCoA object and S a string, then the function
\"Tagged(E, S)\" returns the object E tagged with the string S.  The type
of the returned object is \"TAGGED(S)\" (but the type of E is
unchanged).  The function \"Tag\" returns the tag string of an object,
and the function \"Untagged\" (or \"@\") returns the object, stripped of
its tag.

//==========================  EXAMPLE  ==========================\\\\
  L := [\"Dave\",\"March 14, 1959\",372];
  M := Tagged(L, \"MiscData\");  -- L has been tagged with the string \"MiscData\"
  Type(L);  -- L is a list
LIST
-------------------------------
  Type(M);  -- M is a tagged object
TAGGED(\"MiscData\")
-------------------------------
  Tag(M); -- the tag string of M (it would be the empty string if M
          -- where not a tagged object).
MiscData
-------------------------------
  M;  -- Until a special print function is defined, the printing of L
      -- and M is identical.
[\"Dave\", \"March 14, 1959\", 372]
-------------------------------
  Untagged(M) = L; -- \"Untagged\" removes the tag from M, recovering L.
True
-------------------------------
\\\\==========================  o=o=o=o  ===========================//
The next section explains how to define functions for pretty printing
of tagged objects.
    ";



    