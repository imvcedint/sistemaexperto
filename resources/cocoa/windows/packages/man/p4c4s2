
-- Automatically generated from the file CoCoAHelp.xml
-- Commands and Functions for Lists --
MEMORY.Doc.Descr := "
CoCoA provides a variety of commands for manipulating lists.  Note in
particular the command \"In\" which is useful for building lists.

The following are commands and functions for lists:

  * Append -- append an object to an existing list
  * BBasis5 -- Border Basis of zero dimensional ideal
  * BlockMatrix -- create a block matrix
  * BringIn -- bring in objects from another ring
  * CartesianProduct, CartesianProductList -- Cartesian product of lists
  * Comp -- the N-th component of a list
  * Concat -- concatenate lists
  * ConcatLists -- concatenate a list of lists
  * Count -- count the objects in a list
  * Diff -- returns the difference between two lists
  * Distrib -- the distribution of objects in a list
  * EqSet -- checks if the set of elements in two lists are equal
  * FGLM5 -- perform a FGLM Groebner Basis conversion
  * First -- the first N elements of a list
  * Flatten -- flatten a list
  * GBM -- intersection of ideals for zero-dimensional schemes
  * GenericPoints -- random projective points
  * Head -- the first element of a list
  * HGBM -- intersection of ideals for zero-dimensional schemes
  * HIntersection -- intersection of ideals
  * HIntersectionList -- intersection of ideals
  * IdealAndSeparatorsOfPoints -- ideal and separators for affine points
  * IdealAndSeparatorsOfProjectivePoints -- ideal and separators for points
  * IdealOfPoints -- ideal of a set of affine points
  * IdealOfProjectivePoints -- ideal of a set of projective points
  * ImplicitPlot -- outputs the zero locus of a bivariate polynomial to a file
  * ImplicitPlotOn -- outputs the zero locus of a bivariate polynomial to a file
  * In -- create a list satisfying given conditions
  * Insert -- insert an object in a list
  * Interpolate -- interpolating polynomial
  * Interreduce, Interreduced -- interreduce a list of polynomials or vectors
  * Intersection -- intersect lists, ideals, or modules
  * IntersectionList -- intersect lists, ideals, or modules
  * IsIn -- check if one object is contained in another
  * IsInSubalgebra -- check if one polynomial is in a subalgebra
  * IsSubset -- checks if the elements of one list are a subset of another
  * Ker -- Kernel of a K-algebra homomorphism
  * Last -- the last N elements of a list
  * Len -- the length of an object
  * LexSegmentIdeal -- lex-segment ideal containing L, or with the
    same Hilbert fn as I
  * List -- convert an expression into a list
  * MakeMatByRows, MakeMatByCols -- convert a list into a matrix
  * MakeSet -- remove duplicates from a list
  * Mat -- convert an expression into a matrix
  * MatConcatAntiDiag -- create a simple block matrix
  * MatConcatDiag -- create a simple block matrix
  * MatConcatHor -- create a simple block matrix
  * MatConcatVer -- create a simple block matrix
  * Max -- a maximum element of a sequence or list
  * Min -- a minimum element of a sequence or list
  * Monic -- divide polynomials by their leading coefficients
  * NewList -- create a new list
  * NonZero -- remove zeroes from a list
  * Permutations -- returns all permutations of the entries of a list
  * PlotPoints -- outputs the coordinates of the points to a file
  * PlotPointsOn -- outputs the coordinates of the points to a file
  * PreprocessPts5 -- Reduce redundancy in a set of approximate points
  * Product -- the product of the elements of a list
  * Remove -- remove an object in a list
  * Reverse, Reversed -- reverse a list
  * ScalarProduct -- scalar product
  * SeparatorsOfPoints -- separators for affine points
  * SeparatorsOfProjectivePoints -- separators for projective points
  * Size -- the amount of memory used by an object
  * SmoothFactor -- find small prime factors of an integer
  * Sort -- sort a list
  * SortBy -- sort a list
  * Sorted -- sort a list
  * SortedBy -- sort a list
  * StableBBasis5 -- Stable Border Basis of ideal of points
  * StableBBasisNBM5 -- Numerical Border Basis of ideal of points
  * StableIdeal -- stable ideal containing L
  * StronglyStableIdeal -- strongly stable ideal containing L
  * SubalgebraMap -- algebra homomorphism representing a subalgebra
  * SubalgebraRepr -- representation of a polynomial as a subalgebra element
  * Submat -- submatrix
  * Subsets -- returns all sublists of a list
  * Sum -- the sum of the elements of a list
  * Syz -- syzygy modules
  * Tail -- remove the first element of a list
  * Toric -- saturate toric ideals
  * Tuples -- N-tuples
  * WithoutNth -- removes the N-th component from a list
For details look up each item by name.  Online, try
\"?ItemName\" or \"H.Syntax(\"ItemName\")\".
    ";



    