
-- Automatically generated from the file CoCoAHelp.xml
-- GBasis --

MEMORY.Doc.Syntax := ""  +NewLine() +
"GBasis(M:IDEAL, MODULE, or TAGGED(Quotient)):LIST"  +NewLine() +
"";
MEMORY.Doc.Descr := "
If M is an ideal or module, this function returns a list whose
components form a Groebner basis for M with respect to the
term-ordering of the polynomial ring of M.  If M is a quotient ring by
an ideal I or of a free module by a submodule N, then the
Groebner basis for M is defined to be that of I or N, respectively.

If M is a variable, then the result is stored in M for later use.
It can be retrieved as M.GBasis and can also be seen using
the command \"Describe\".

For a reduced Groebner basis, use the command \"ReducedGBasis\".

The coefficient ring must be a field.

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[t,x,y];
  I := Ideal(t^3-x, t^4-y);
  Describe I;
Record[Type := IDEAL, Value := Record[Gens := [t^3 - x, t^4 - y]]]
-------------------------------
  GBasis(I);
[t^3 - x, -tx + y, t^2y - x^2, x^3 - ty^2]
-------------------------------
  Describe(I);  -- the Groebner basis has been stored in I
Record[Type := IDEAL, Value := Record[Gens := [t^3 - x, t^4 - y],
GBasis := [t^3 - x, -tx + y, t^2y - x^2, x^3 - ty^2]]]
-------------------------------
  I.GBasis;
[t^3 - x, -tx + y, t^2y - x^2, x^3 - ty^2]
-------------------------------
\\\\==========================  o=o=o=o  ===========================//

For fine control and monitoring of Groebner basis calculations, see
\"The Interactive Groebner Framework\" and \"Introduction to Panels\".
    ";




    