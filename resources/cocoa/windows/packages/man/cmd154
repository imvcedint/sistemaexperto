
-- Automatically generated from the file CoCoAHelp.xml
-- HilbertSeries --

MEMORY.Doc.Syntax := ""  +NewLine() +
"HilbertSeries(M:MODULE or IDEAL or TAGGED(Quotient)):TAGGED($hp.PSeries)"  +NewLine() +
"";
MEMORY.Doc.Descr := "
This function computes the Hilbert-Poincare series of M.
The input, M, must be homogeneous (with respect to the first row of
the weights matrix).  In the standard case, i.e. the weights of all
indeterminates are 1, the result is simplified so that the power
appearing in the denominator is the dimension of M.

The function \"Poincare\" is exacly the same as \"HilbertSeries\".

NOTES:

(i) the coefficient ring must be a field.

(ii) these functions produce tagged objects: they cannot safely be
     (non-)equality to other values.

Starting from release 4.7.5 the input may also be an ideal.

For more information, see the article: A.M. Bigatti, \"Computations of
Hilbert-Poincare Series\" J. Pure Appl. Algebra, 119/3 (1997),
237--253.

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[t,x,y,z];
  HilbertSeries(R/Ideal(0));
(1) / (1-t)^4
-------------------------------
  Q := R/Ideal(t^2, x, y^3);  Poincare(Q);
(1 + 2t + 2t^2 + t^3) / (1-t)
-------------------------------
  Poincare(R^2/Module([x^2,y^2],[z,y]));
(2 + t) / (1-t)^3
-------------------------------
  Poincare(Module([x^2,y^2],[z,y]));
(t + t^2) / (1-t)^4
-------------------------------
  Use R ::= QQ[t,x,y,z], Weights([1,2,3,4]);
  Poincare(R/Ideal(t^2, x, y^3));
---  Non Simplified Pseries  ---
(1 - 2t^2 + t^4 - t^9 + 2t^11 - t^13) / ( (1-t) (1-t^2) (1-t^3) (1-t^4) )
-------------------------------
  Poincare(Ideal(t^2, x, y^3));
---  Non-simplified HilbertPoincare' Series  ---
(2t^2 - t^4 + t^9 - 2t^11 + t^13) / ( (1-t) (1-t^2) (1-t^3) (1-t^4) )
-------------------------------
  Use R ::= QQ[t,x,y,z], Weights(Mat([[1,2,3,4],[0,0,5,8]]));
  Poincare(R/Ideal(t^2, x, y^3));
---  Non Simplified Pseries  ---
( - t^13x^15 + 2t^11x^15 - t^9x^15 + t^4-2t^2 + 1) / ( (1-t) (1-t^2) (1-t^3x^5) (1-t^4x^8) )
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    