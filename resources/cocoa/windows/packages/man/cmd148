
-- Automatically generated from the file CoCoAHelp.xml
-- Help --

MEMORY.Doc.Syntax := ""  +NewLine() +
"Help(S:STRING):NULL"  +NewLine() +
"";
MEMORY.Doc.Descr := "
This command is used for extending the online help to include
information about user-defined functions.  It is *not* the main
command for getting information about CoCoA online.  For information
about online help in general, enter \"?\" or \"?online help\".

There are two ways to let \"Help\" know about a help string associated
with a user-defined function.  First, one may use the line \"Help S\"
where S is the help string, as the first line of the function definition.

//==========================  EXAMPLE  ==========================\\\\
  Define AddThree(X)
    Help \"adds 3 to its argument\";
    Return X+3;
  EndDefine;
  Help(\"AddThree\");
adds 3 to its argument
-------------------------------
  F(0);
3
-------------------------------
\\\\==========================  o=o=o=o  ===========================//

The second way to provide a help string for \"Help\" is to define a
function \"Help_F\" where F is the function identifier.

//==========================  EXAMPLE  ==========================\\\\
  Define AddFive(X)
    Return X+5;
  EndDefine;
  Define Help_AddFive()
    Return \"adds 5 to its argument\";
  EndDefine;
  Help(\"AddFive\");
adds 5 to its argument
-------------------------------
  AddFive(0);
5
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    