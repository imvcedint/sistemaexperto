
-- Automatically generated from the file CoCoAHelp.xml
-- Setting Up a Ring --
MEMORY.Doc.Descr := "
A CoCoA session automatically starts with the default ring, R =
QQ[x,y,z].  The command \"Use\" is used to change rings.  The following
example shows how to create the ring ZZ/(5)[a,b,c] (the coefficient
ring is the integers mod 5).  Once the ring has been declared, one may
start to play with polynomials, ideals, modules, and other
constructions in that ring.  In the ring declaration, the
indeterminates are optionally separated by commas, and note the use of
two colons.

Some details on handling several rings is provided below in the
section of the Tutorial entitled \"Using More Than One Ring\"
and \"Ring Mapping Example\".

//==========================  EXAMPLE  ==========================\\\\
  Use S ::= ZZ/(5)[a,b,c];
  F := a-b;
  I := Ideal(F^2,c);
  I;
Ideal(a^2 - 2ab + b^2, c)
-------------------------------
  J := Ideal(a-b);
  I + J;
Ideal(a^2 - 2ab + b^2, c, a - b)
-------------------------------
  Minimalized(It);  -- find a minimal set of generators for I+J
Ideal(a - b, c)
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";



    