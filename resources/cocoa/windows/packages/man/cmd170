
-- Automatically generated from the file CoCoAHelp.xml
-- Image --

MEMORY.Doc.Syntax := ""  +NewLine() +
"Image(R::E:OBJECT, F:TAGGED(RMap)):OBJECT"  +NewLine() +
"Image(V:OBJECT, F:TAGGED(RMap)):OBJECT"  +NewLine() +
""  +NewLine() +
"where R is the identifier for a ring and F has the form"  +NewLine() +
"RMap(F_1:POLY,...,F_n:POLY) or the form RMap([F_1:POLY,...,F_n:POLY])."  +NewLine() +
"The number n is the number of indeterminates of the ring R. In the"  +NewLine() +
"second form, V is a variable containing a CoCoA object dependent on R"  +NewLine() +
"or not dependent on any ring."  +NewLine() +
"";
MEMORY.Doc.Descr := "
This function maps the object E from one ring into the current ring as
determined by F.  Suppose the current ring is S, and E is an object
dependent on a ring R; then

               Image(R::E, F)

returns the object in S obtained by substituting \"F_i\" for the i-th
indeterminate of R in E.  Effectively, we get the image of E under
the ring homomorphism,

               F: R   --->  S
                  x_i |--> F_i,

where \"x_i\" denotes the i-th indeterminate of R.

Notes:

1. The coefficient rings for the domain and codomain must be the same.

2. If R = S, one may use \"Image(E, F)\" but in this case it may be
   easier to use \"Eval\" or \"Subst\".

3. The exact domain is never specified by the mapping F.  It is only
   necessary that the domain have the same number of indeterminates as F
   has components. Thus, we are abusing terminology somewhat in
   calling F a map.

4. The second form of the function does not require the prefix \"R::\"
   since the prefix is associated automatically.

5. If the object E in R is a polynomial or rational function (or list,
   matrix, or vector of these) which involves only indeterminates that are
   already in S, the object E can be mapped over to S without change
   using the command \"BringIn\".

//==========================  EXAMPLE  ==========================\\\\
  Use C ::= QQ[u,v];   -- domain
  Use B ::= QQ[x,y];   -- another possible domain
  I := Ideal(x^2-y);   -- an ideal in B
  Use A ::= QQ[a,b,c]; -- codomain
  F := RMap(a,c^2-ab);
  Image(B::xy, F);     -- the image of xy under F:B --> A
-a^2b + ac^2
-------------------------------
  Image(C::uv, F);     -- the image of uv under F:C --> A
-a^2b + ac^2
-------------------------------
  Image(I, F);         -- the image of the ideal I under F: B --> A
Ideal(a^2 + ab - c^2)
-------------------------------
  I; -- the prefix \"B::\" was not needed in the previous example since
     -- I is already labeled by B
B :: Ideal(x^2 - y)
-------------------------------
  Image(B::Module([x+y,xy^2], [x,y]), F); -- the image of a module
Module([-ab + c^2 + a, a^3b^2 - 2a^2bc^2 + ac^4], [a, -ab + c^2])
-------------------------------
  X := C:: u+v;  -- X is a variable in the current ring (the codomain), A,
  X;             -- whose value is an expression in the ring C.
C :: u + v
-------------------------------
  Image(X, F);   -- map X to get a value in C
-ab + c^2 + a
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    