
-- Automatically generated from the file CoCoAHelp.xml
-- Package Sourcing and Autoloading --
MEMORY.Doc.Descr := "
As mentioned above, packages are usually saved in files and then read
into a CoCoA session using the command \"Source\".

(I) Full path name, ordinary file sourcing.

package name: $mypackage
   file name: this/is/my/stuff.cpkg

Suppose the name of your package is \"$mypackage\" and is kept in the
file with full pathname \"this/is/my/stuff.cpkg\".  Then the package can
be loaded into the session as usual with the command:

  Source \"this/is/my/stuff.cpkg\";

Functions can then be called from the package using the package name,
\"$mypackage\", as a prefix (or using aliases).

(II) The standard package path, \"$\"-shortcut.

package name: $mypackage
   file name: packages/anna/stuff.cpkg (relative to cocoa directory)

  Source CocoaPackagePath() + \"/anna/stuff.cpkg\";
*The following syntax is obsolescent!*
  Source \"$anna/stuff\";

(III) Autoloading.

package name: $anna/stuff
   file name: packages/anna/stuff.cpkg (relative to cocoa directory)

Now suppose that the package is in the standard package path, as
above, in the file with pathname (relative to the cocoa directory)
\"packages/anna/stuff.cpkg\".  However, now assume that the name of the
package is \"$anna/stuff\", i.e., that it matches the name of its
file (without \"packages/\" and \".cpkg\").  Then, if any function from the
package is called, say \"$anna/stuff.MyFunction\", the package
will automatically be loaded.  Of course, one may also source the
package using either method I or II, from above.

* Initialize *
NOTE: As explained in the section entitled \"Package Initialization\",
below, no matter which method is used to source a package, any
function in the package named \"Initialize\" will automatically be
executed when the package is loaded.
    ";



    