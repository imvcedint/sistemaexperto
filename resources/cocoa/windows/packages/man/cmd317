
-- Automatically generated from the file CoCoAHelp.xml
-- Randomize --

MEMORY.Doc.Syntax := ""  +NewLine() +
"Randomize(V:POLY):POLY"  +NewLine() +
""  +NewLine() +
"where V is a variable containing a polynomial."  +NewLine() +
"";
MEMORY.Doc.Descr := "
This function replaces the coefficients of terms of the polynomial
contained in V with randomly generated coefficients.  The result is
stored in V, overwriting the original polynomial.

Note: It is possible that some coefficients will be replaced by
zeroes, i.e., some terms from the original polynomial may disappear in
the result.

The similar function \"Randomized\" performs the same operation,
but returns the randomized polynomial without modifying the argument.

NB: every time you restart CoCoA the sequence of random numbers will
be the same (as in other programming languages).  If you want total
randomness read \"Seed\".

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[x];
  F := 1+x+x^2;
  Randomized(F);
-2917104644x^2 + 3623608766x - 2302822308
-------------------------------
  F;
x^2 + x + 1
-------------------------------
  Randomize(F);
  F;
-1010266662x^2 + 1923761602x - 4065654277
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    