
-- Automatically generated from the file CoCoAHelp.xml
-- IdealOfPoints --

MEMORY.Doc.Syntax := ""  +NewLine() +
"IdealOfPoints(Points:LIST):IDEAL"  +NewLine() +
""  +NewLine() +
"where Points is a list of lists of coefficients representing a set of"  +NewLine() +
"distinct points in affine space."  +NewLine() +
"";
MEMORY.Doc.Descr := "
This function computes the reduced Groebner basis for the ideal of all
polynomials which vanish at the given set of points.  It returns the
ideal generated by that Groebner basis.

NOTE:

 * the current ring must have at least as many indeterminates as the
   dimension of the space in which the points lie;

 * the base field for the space in which the points lie is taken to be
   the coefficient ring, which should be a field;

 * in the polynomials returned, the first coordinate in the space is
   taken to correspond to the first indeterminate, the second to the
   second, and so on;

 * if the number of points is large, say 100 or more, the returned
   value can be very large.  To avoid possible problems when printing
   such values as a single item we recommend printing out the elements
   one at a time as in this example:

     I := IdealOfPoints(Pts);
     Foreach Element In Gens(I) Do
       PrintLn Element;
     EndForeach;

For ideals of points in projective space, see
\"IdealOfProjectivePoints\".

//==========================  EXAMPLE  ==========================\\\\
  Use R ::= QQ[x,y];
  Points := [[1, 2], [3, 4], [5, 6]];
  I := IdealOfPoints(Points);
  I;
Ideal(x - y + 1, y^3 - 12y^2 + 44y - 48)
-------------------------------
  I.Gens;  -- the reduced Groebner basis
[x - y + 1, y^3 - 12y^2 + 44y - 48]
-------------------------------
\\\\==========================  o=o=o=o  ===========================//";




    