Package $contrib/conductor  -- conductor sequence of points

/*-----[    suggestions for use:        ]-----*\

     Alias CP := $contrib/conductor;
     CP.About();
     CP.Man();

\*--------------------------------------------*/

Alias  CP   := $contrib/conductor;

Define About()
  PrintLn "    Topic   : Conductor Of Points";
  PrintLn "    Keywords: Conductor Of Points, Socle-Set, Complete Intersection";
  PrintLn "    Authors : L. Bazzotti";
  PrintLn "    Version : CoCoA 4.2";
  PrintLn "    Date    : 20 October 2003";
EndDefine; -- About

------[   help & examples   ]--------

Define Man()
  PrintLn "Suggested alias for this package:";
  PrintLn "    Alias CP := $contrib/conductor;";
  PrintLn "";
  PrintLn "SYNTAX";
  PrintLn "";
  PrintLn "   CP.ConductorSequence(List of Projective Points Or Projective";
  PrintLn "   Zerodimensional Reduced Ideal): List.";
  PrintLn "";
  PrintLn "   CP.Conductor(List of Projective Points Or Projective Zerodimensional";
  PrintLn "   Reduced Ideal): List of Records.";
  PrintLn "";
  PrintLn "   CP.SocleSet(I:Projective Zerodimensional Reduced Ideal): List.";
  PrintLn "";
  PrintLn "   CP.CompleteIntersection(I: Projective Zerodimensional Reduced Ideal):";
  PrintLn "   Ideal.";
  PrintLn "";
  PrintLn "   CP.ConductorOfPoints(P: List Of Projective Points): List Of Records.";
  PrintLn "";
  PrintLn "   CP.ConductorOfIdeal(I:Projective Zerodimensional Reduced Ideal):";
  PrintLn "   List Of Records.";
  PrintLn "";
  PrintLn "";
  PrintLn "DESCRIPTION";
  PrintLn "";
  PrintLn "Given a projective zerodimensional reduced ideal or a list of projective";
  PrintLn "points, we analyse the conductor sequence of the associated points.";
  PrintLn "";
  PrintLn "For more information, see";
  PrintLn "Ph.D. Thesis \"Conduttore di schemi zerodimensionali\", Laura Bazzotti.";
  PrintLn "";
  PrintLn "";
  PrintLn "CP.ConductorSequence(...)";
  PrintLn "returns the sequence of conductor of a zerodimensional reduced projective";
  PrintLn "ideal or of a list of points.(I);";
  PrintLn "";
  PrintLn "CP.Conductor(...)";
  PrintLn "returns a list of records where the first field is a conductor degree And";
  PrintLn "the second field is the number of points having that conductor degree.";
  PrintLn "";
  PrintLn "CP.SocleSet(I)";
  PrintLn "returns the socle set of a zerodimensional reduced projective ideal.";
  PrintLn "";
  PrintLn "CP.CompleteIntersection(I)";
  PrintLn "returns a zerodimensional projective ideal contained in I, which is a";
  PrintLn "complete intersection. In order to find this ideal, we choose the maximum";
  PrintLn "number of generators of I which form a regular sequence And we consider";
  PrintLn "this ideal. If the dimension of the coordinate ring of the Ideal is not 1,";
  PrintLn "Then we complete this regular sequence choosing generic elements from I.";
EndDefine; -- Man




------[   Main functions   ]--------

Define Conductor(...)
  If Len(ARGV)<>1 Then
    Error("Conductor: input must be a projective zerodimensional Ideal or a list of projective points");
  EndIf;
  If Type(ARGV[1])=IDEAL Then
    Return CP.ConductorOfIdeal(ARGV[1])
  Else
    Return CP.ConductorOfPoints(ARGV[1])
  EndIf;
EndDefine; -- Conductor



Define ConductorSequence(...)
  Return CP.Sequence(CP.Conductor(ARGV[1]));
EndDefine; -- ConductorSequence





Define SocleSet(I);
  Ris:= Res(I);
  If Len(Ris[2]) <> NumIndets()-1
    Then Error("SocleSet: input must be zerodimensional Ideal");
  EndIf;
  L:=Ris[2,NumIndets()-1];
  Socle:= [-L[J,2]-NumIndets()+1 | J In 2..Len(L)];
  Return(Socle);
EndDefine; --SocleSet


Define CompleteIntersection(I);
  If Dim(CurrentRing()/I) <> 1 Then
    Error("CompleteIntersection: input must be zerodimensional homogeneous ideal");
  EndIf;
  L:=Gens(I);
  SortBy(L,Function("$contrib/conductor.ByDeg"));
  CurrentDim:= NumIndets();
  LW:=[];
  For J:=1 To Len(L) Do
    LW2:=Concat(LW,[L[J]]);
    D:=Dim(CurrentRing()/Ideal(LW2));
    If D = CurrentDim -1 Then
      LW:=LW2;
      CurrentDim:=D;
      If D=1 Then Return Ideal(LW);
      EndIf;
    EndIf;
  EndFor;
  DEG:=Deg(Last(L));
  IW:=Ideal(LW);
  IW2:=IW;
  While Dim(CurrentRing()/IW2) <>1 Do
    Gen:=[Sum([F*CP.MyRandomized(DensePoly(DEG-Deg(F))) | F In L])|J In 2..D];
    IW2:=IW+Ideal(Gen);
  EndWhile;
  Return(IW2);
EndDefine; -- CompleteIntersection





-------------------------------------------------------------------------------

Define ConductorOfPoints(P);
  Separators:=SeparatorsOfProjectivePoints(P);
  Degs:= [Deg(F) | F In Separators ];
  DegreeSet:=MakeSet(Degs);
  Return [Record[Csi:=D,M:=Len([X In Degs | X=D])] | D In DegreeSet]
EndDefine; -- Conductor



Define ConductorOfIdeal(I);
  MultX:= Multiplicity(CurrentRing()/I);
  Socle:= CP.SocleSet(I);
  IW:= CP.CompleteIntersection(I);
  GW:=Gens(IW);
  SigmaW:= Sum([Deg(F) | F In GW])+2-NumIndets();
  IY:= Colon(IW,I);
  GY:=Gens(IY);
  CS:=[];
  SumMult:=0;
  For J:=Len(Socle) To 1 Step -1 Do
    IXJ:= Colon(IW,Ideal([F In  GY | Deg(F)<=SigmaW-1-Socle[J]]));
    Mult:= Multiplicity(CurrentRing()/IXJ);
    M:=Mult-SumMult;
    If M<>0 Then
      Append(CS,Record[Csi:=Socle[J], M:=M]);
    EndIf;
    If Mult=MultX Then
      Return CS;
    EndIf;
    SumMult:=Mult;
  EndFor;
EndDefine; -- ConductorOfIdeal





-------[   Auxiliary functions   ]--------
	

Define ByDeg(S,T)
  Return Deg(S) < Deg(T);
EndDefine; -- ByDeg


Define MyRandomized(F);
  Return Sum([X*Rand(-5,+5) | X In Support(F)]);
EndDefine; -- MyRandomized


Define Sequence(L);
  Return ConcatLists([NewList(X.M,X.Csi) | X In L]);
EndDefine; -- Sequence



EndPackage; $contrib/conductor  -- conductor sequence of points
