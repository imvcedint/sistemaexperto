-- $Id: matrixnormalform.ts,v 1.6 2009/06/04 15:55:23 bigatti Exp $
-- Example/Test suite for matrixnormalform.pkg

/*

Source CocoaPackagePath() + "/contrib/matrixnormalform.ts";
TSL.RunTests();  // was  TSL.Do();

*/

Alias TSL := $ts/ts_lib;
TSL.Initialize();

-------------------------------
-- TEST 01 :

Test := Record[Id := "mnf_01", Descr := "ex 1"];

Test.Input := "
Use QQ[x,y];
M  := Mat([
        [-x + 2, 0, 0, 0],
        [-1, -x + 1, 0, 0],
        [0, -1, -x, -1],
        [1, 1, 1, -x + 2]
        ]);
$contrib/matrixnormalform.Verify(M, $contrib/matrixnormalform.SmithFactor(M));
";

Test.ExpectedOutput :=
"OK : U*M*V = SmithNF
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 02 :

Test := Record[Id := "mnf_02", Descr := "ex"];

Test.Input := "
Use QQ[x];
M := Mat([  [x, 0, 1, 0],  [0, x, 0, -1] ]);
$contrib/matrixnormalform.Verify(M, $contrib/matrixnormalform.SmithFactor(M));
M := Transposed(M);
$contrib/matrixnormalform.Verify(M, $contrib/matrixnormalform.SmithFactor(M));

M := Mat([  [0, x, 0, -1],  [x, 0, 1, 0] ]);
$contrib/matrixnormalform.Verify(M, $contrib/matrixnormalform.SmithFactor(M));
M := Transposed(M);
$contrib/matrixnormalform.Verify(M, $contrib/matrixnormalform.SmithFactor(M));

M := Mat([  [0, 2, 0, -1],  [2, 0, 1, 0] ]);
$contrib/matrixnormalform.Verify(M, $contrib/matrixnormalform.SmithFactor(M));
M := Transposed(M);
$contrib/matrixnormalform.Verify(M, $contrib/matrixnormalform.SmithFactor(M));
";

Test.ExpectedOutput :=
"OK : U*M*V = SmithNF
-------------------------------
OK : U*M*V = SmithNF
-------------------------------
OK : U*M*V = SmithNF
-------------------------------
OK : U*M*V = SmithNF
-------------------------------
OK : U*M*V = SmithNF
-------------------------------
OK : U*M*V = SmithNF
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 03 :

Test := Record[Id := "mnf_03", Descr := "ex"];

Test.Input := "
M := Mat([
         [2, 0, 0, 0, 0, 0, 0],
         [0, 2, 0, 0, 0, 0, 0],
         [0, 0, 3, 0, 0, 0, 0],
         [0, 0, 0, 3, 0, 0, 0],
         [0, 0, 0, 0, 5, 0, 0],
         [0, 0, 0, 0, 0, 5, 0],
         [0, 0, 0, 0, 0, 0, 5]
         ]);
$contrib/matrixnormalform.Verify(M, $contrib/matrixnormalform.SmithFactor(M));
";

Test.ExpectedOutput :=
"OK : U*M*V = SmithNF
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 04 :

Test := Record[Id := "mnf_04", Descr := "ex"];

Test.Input := "
Use ZZ/(5)[x];
M := Mat([
  [x^2, -2x^2 + x, -2x^2 + 2, 2x - 1],
  [2x + 1, 2x^2 + 2, -x^2 + x, -2x^2 - x],
  [-2x^2 + x + 1, -2x^2 + 2x + 2, 2x + 1, 2x^2 + 2],
  [0, x^2 - 2x, 2x^2, 0]
         ]);
$contrib/matrixnormalform.Verify(M, $contrib/matrixnormalform.SmithFactor(M));
";

Test.ExpectedOutput :=
"OK : U*M*V = SmithNF
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 05 :

Test := Record[Id := "mnf_05", Descr := "ex"];

Test.Input := "
M := Mat([
    [4, 0, 0],
    [0, 2, 0],
    [0, 0, 1]
         ]);
$contrib/matrixnormalform.Verify(M, $contrib/matrixnormalform.SmithFactor(M));
";

Test.ExpectedOutput :=
"OK : U*M*V = SmithNF
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 06 :

Test := Record[Id := "mnf_06", Descr := "ex"];

Test.Input := "
M := Mat([
    [4, 0, 0],
    [0, 8, 0],
    [0, 0, 9]
         ]);
$contrib/matrixnormalform.Verify(M, $contrib/matrixnormalform.SmithFactor(M));
";

Test.ExpectedOutput :=
"OK : U*M*V = SmithNF
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 07 :

Test := Record[Id := "mnf_07", Descr := "ex-bug from E.Sbarra"];

Test.Input := "
M := Mat([
	 [144, -28, -14],
	 [24 ,  8,  80],
	 [-24,   8,  24],
	 [-12,   4,  12]
	 ]);

$contrib/matrixnormalform.Verify(M, $contrib/matrixnormalform.SmithFactor(M));
";

Test.ExpectedOutput :=
"OK : U*M*V = SmithNF
-------------------------------
";
TSL.RegisterTest(Test);

