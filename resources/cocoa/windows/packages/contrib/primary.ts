-- $Id: primary.ts,v 1.7 2009/06/04 15:55:23 bigatti Exp $
-- Example/Test suite for primary.pkg

/*

Source CocoaPackagePath() + "/contrib/primary.ts";
TSL.RunTests();  // was  TSL.Do();

*/

     Alias P := $contrib/primary;

Alias TSL := $ts/ts_lib;
TSL.Initialize();

-------------------------------
-- TEST 01 : 

Test := Record[Id := "primary_01", Descr := "ex 1"];

Test.Input :="
    Use QQ[x,y,z];
    I := Ideal(x^3-y*z, y^2-x*z, z^2-x^2*y);
    P.TgCone(I);
    P.InitialIdeal(I, [y,z]);
    Q := Ideal(y, z);
    PS := P.PrimaryPoincare(I, Q); PS;
    HP.PSerToHilbert(PS); -- the Hilbert Function associated to PS
";

Test.ExpectedOutput :=
"Ideal(-yz, z^2, y^2 - xz)
-------------------------------
Ideal(-xz, -x^2y, x^3, -xy^3 + z^3, z^4)
-------------------------------
(3 + x^2) / (1-x)
-------------------------------
H(0) = 3
H(1) = 3
H(t) = 4   for t >= 2
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 02 :

Test := Record[Id := "primary_02", Descr := "ex 2"];

Test.Input :="
    Use ZZ/(32003)[x,y,z,w];
    I := Ideal(x^5 - y*z, y^4 - x*z^2, x*y^3 - z*w, x^2*z - y*w,
    y^2*z^2 - w^3, y^3*z - x^2*w^2, x^3*w - z^2, x*y*w^2 - z^3,
    x^3*y^2 - w^2, x*z^4 - y^2*w^3, y*z^5 - x*w^5, y^3*w^5 - z^7,
    x^2*w^7 - z^8, z^9 - y*w^8);
    P.TgCone(I);
    Q  := Ideal(x, y, z);
    PS := P.PrimaryPoincare(I, Q); PS;
    HP.PSerToHilbert(PS);
";

Test.ExpectedOutput :=
"Ideal(-yz, -w^2, -zw, -z^2, -yw, y^4)
-------------------------------
(2 + 2x + x^2 + x^3) / (1-x)
-------------------------------
H(0) = 2
H(1) = 4
H(2) = 5
H(t) = 6   for t >= 3
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 03 :

Test := Record[Id := "primary_03", Descr := "ex 3"];

Test.Input :="
    Use ZZ/(32003)[x,y,z];
    I  := Ideal(0);
    Q  := Ideal(x, y, z^2);
    PS := P.PrimaryPoincare(I, Q); PS;
    HP.PSerToHilbert(PS);
    HV := HP.PSerHVector(PS); -- the H-vector associated to PS
";

Test.ExpectedOutput :=
"(2) / (1-x)^3
-------------------------------
H(t) = t^2 + 3t + 2   for t >= 0
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 04 :

Test := Record[Id := "primary_04", Descr := "ex 4"];

Test.Input :="
    Use ZZ/(32003)[x,y,z,w];
    I  := Ideal(-y*z + x*w, z^3 - y*w^2, -x*z^2 + y^2*w, -y^3 + x^2*z);
    Q  := Ideal(x, y, z^2, w^3);
    PS := P.PrimaryPoincare(I, Q); PS;
    HP.PSerToHilbert(PS);
    HV := HP.PSerHVector(PS);
    P.E(0, HV);
    [ P.E(J,HV) | J In 0..(HP.PSerDim(PS)-2) ];
    [ P.E(J,HV) | J In 0..(Len(HV)-1) ];
";

Test.ExpectedOutput :=
"(6 + 4x) / (1-x)^2
-------------------------------
H(t) = 10t + 6   for t >= 0
-------------------------------
10
-------------------------------
[10]
-------------------------------
[10, 4]
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 05 :

Test := Record[Id := "primary_05", Descr := "ex 5"];

Test.Input :="
    Use ZZ/(32003)[x,y,z,w];
    I  := Ideal(x^3-y^7, x^2*y - x*w^3-z^6);
    Q  := Ideal(x, y, z, w);
    PS := P.PrimaryPoincare(I, Q);
    HP.PSerToHilbert(PS);
    HV := HP.PSerHVector(PS);
    [ P.E(J,HV) | J In 0..(HP.PSerDim(PS)-2) ];
    [ P.E(J,HV) | J In 0..(Len(HV)-1) ];
";

Test.ExpectedOutput :=
"H(0) = 1
H(1) = 4
H(2) = 10
H(3) = 18
H(4) = 28
H(5) = 39
H(6) = 52
H(7) = 66
H(8) = 80
H(9) = 95
H(10) = 110
H(11) = 126
H(12) = 141
H(13) = 156
H(t) = 16t - 53   for t >= 14
-------------------------------
[16]
-------------------------------
[16, 69, 212, 579, 1398, 2833, 4636, 6010, 6114, 4841, 2948, 1354, 454, 105, 15, 1]
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 06 :

Test := Record[Id := "primary_06", Descr := "ex 6"];

Test.Input :="
    Use ZZ/(32003)[x,y,z];
    I  := Ideal(z^3);
    Q  := Ideal(x^2, y^2, x*z, y*z);
    PS := P.PrimaryPoincare(I, Q); PS;
    HP.PSerToHilbert(PS);
    HV := HP.PSerHVector(PS);
    [ P.E(J,HV) | J In 0..(HP.PSerDim(PS)-2) ];
    [ P.E(J,HV) | J In 0..(Len(HV)-1) ];
";

Test.ExpectedOutput :=
"(6 + 3x + 4x^2 - x^3) / (1-x)^2
-------------------------------
H(0) = 6
H(1) = 15
H(t) = 12t + 4   for t >= 2
-------------------------------
[12]
-------------------------------
[12, 8, 1, -1]
-------------------------------
";
TSL.RegisterTest(Test);
