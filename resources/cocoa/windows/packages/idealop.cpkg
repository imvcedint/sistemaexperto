Package $idealop

/*---------------------------------------------------------------------------
    16 Jan 01: Assigns to GBasis only when computed GBasis is valid
               with the current PP ordering.
    17 Jul 00: Upgraded to 4.0 standard.
               Added the module/ideal operations.
    Bugs     : Some bugs fixed for extremal cases
    13 Oct 99: Upgraded to 3.7 standard almost fully
    Bugs     : Sometimes repeated use may cause impredictable lo-level errors
-----------------------------------------------------------------------------*/

Alias Op := $idealop;

/*------------------------------------------------------------------*/
Define About()
  PrintLn "    Version  : CoCoA 4.4";
  PrintLn "    Date     : 13 December 2004"
EndDefine;
/*------------------------------------------------------------------*/

Define Man()
PrintLn "  Optimized module operations. The operations (Intersection, IntersectionList,";
PrintLn "  Colon, Saturation) introduce new module indeterminates, plus HDriven if the";
PrintLn "  operands are homogeneous. The H-operations (HIntersection, HIntersectionList,";
PrintLn "  HColon, HSaturation) force the use of the HDriven algorithms by homogenizing";
PrintLn "  the operands,  using an optimized HDriven algorithm, and then de-homogenizing";
PrintLn "  the result. For a description of the algorithms involved, see the paper";
PrintLn "  M. Caboara, C. Traverso Efficient algorithms for ideal operations (extended";
PrintLn "  abstract)} ISSAC 98: Proceedings of the 1998 International Symposium on";
PrintLn "  Symbolic and Algebraic Computation, August 13--15, 1998, University of";
PrintLn "  Rostock, Germany, pp. 147-152, ACM Press, 1998.";
EndDefine;


Define Initialize()

  MEMORY.OP_PKG_IntModN:=1;
  MEMORY.OP_PKG_IntHModN:=2;


  MEMORY.OP_PKG_LogListIntN:=1;
  MEMORY.OP_PKG_ListInt1N:=2;
  MEMORY.OP_PKG_ListInt2N:=3;
  MEMORY.OP_PKG_ListInt3N:=4;

  MEMORY.OP_PKG_TrModN:=1;
  MEMORY.OP_PKG_TrHModN:=2;

  MEMORY.OP_PKG_ColIntN:=1;

  MEMORY.OP_PKG_SatTryN:=3;

  MEMORY.OP_PKG_SSatTryN:=1;
EndDefine;

---------------------------- [ Interface ] ---------------------------------

Define Intersection(I,J) Return Op.Inter(I,J,FALSE); EndDefine;
Define HIntersection(I,J) Return Op.Inter(I,J,TRUE); EndDefine;

Define Inter(I,J,ReqHomog)
  If TypeOfCoeffs()=INT Then
    Error("Operation not implemented for non-field coefficients")
  EndIf;
  If Shape([I,J])<>[IDEAL, IDEAL] Then Error("(IDEAL,IDEAL) Expected") EndIf;
  Homog:=IsHomog(Gens(I)) And IsHomog(Gens(J));
  If IsPosTo(Var(RingEnv())) Then
    If Homog Then Return Op.IntHMod(I,J) EndIf;
    If Not ReqHomog Then Return Op.IntMod(I,J) EndIf;
    NewRing:=NewId();
    Var(NewRing)::=CoeffRing[x[1..(Len(Indets())+1)]],PosTo;
    --Op.MakeThisRing(NewRing,Characteristic(),"x[1.."+Sprint(Len(Indets())+1)+"]");
    Using Var(NewRing) Do
      H:=Last(Indets());
      I:=Op.CanonicalImage(I);
      J:=Op.CanonicalImage(J);
      I:=Homogenized(H,I);
      J:=Homogenized(H,J);
      E:=Op.IntHMod(I,J);
      E:=Ideal([NR(P,[H-1])|P In ReducedGBasis(E)]);
    EndUsing;
    E:=Op.CanonicalImage(E);
//obsolescent    Clear Var(NewRing);
//obsolescent    Destroy Var(NewRing);
    If Op.StandardCond() Then E.GBasis:=Gens(E) EndIf;
    Return E;
  Else -- PosTo=False
    NewRing:=NewId();
    Var(NewRing)::=CoeffRing[x[1..(Len(Indets())+1)]],PosTo;
    Using Var(NewRing) Do
      I1:=Op.CanonicalImage(I);
      J1:=Op.CanonicalImage(J);
      If Homog Then E:=Op.IntMod(I1,J1)
      Elif Not ReqHomog Then E:=Op.IntMod(I1,J1)
      Else
        H:=Last(Indets());
        I1:=Homogenized(H,I1);
        J1:=Homogenized(H,J1);
        E:=Op.IntMod(I1,J1);
        E:=Ideal([NR(P,[H-1])|P In ReducedGBasis(E)]);
      EndIf;
    EndUsing;
    E:=Op.CanonicalImage(E);
//obsolescent    Clear Var(NewRing);
//obsolescent    Destroy Var(NewRing);
    If Op.StandardCond() Then E.GBasis:=Gens(E) EndIf;
    Return E;
  EndIf;
EndDefine;


Define IntersectionList(L) Return Op.InterList(L,FALSE); EndDefine;
Define HIntersectionList(L) Return Op.InterList(L,TRUE); EndDefine;

Define InterList(L,ReqHomog)
  If TypeOfCoeffs()=INT Then
    Error("Operation not implemented for non-field coefficients")
  EndIf;
  If Shape(L)<>NewList(Len(L),IDEAL) Then Error("Ideal List Expected") EndIf;
  Homog:=IsHomog(Flatten([Gens(I)|I In L]));
  If IsPosTo(Var(RingEnv())) Then
    If Homog Then Return Op.ListInt1(L,[1,2,1,1,1,2]) EndIf;
    If Not ReqHomog Then Return Op.ListInt1(L,[1,2,1,1,1,1]) EndIf;
    NewRing:=NewId();
    Var(NewRing)::=CoeffRing[x[1..(Len(Indets())+1)]],PosTo;
    Using Var(NewRing) Do
      H:=Last(Indets());
      L:=[Op.CanonicalImage(I)| I In L];
      L:=[Homogenized(H,I)| I In L];
      E:=Op.ListInt1(L,[1,2,1,1,1,2]);
      E:=Ideal([NR(P,[H-1])|P In ReducedGBasis(E)]);
    EndUsing;
    E:=Op.CanonicalImage(E);
//obsolescent    Clear Var(NewRing);
//obsolescent    Destroy Var(NewRing);
    If Op.StandardCond() Then E.GBasis:=Gens(E) EndIf;
    Return E;
  Else -- PosTo=False
    NewRing:=NewId();
    Var(NewRing)::=CoeffRing[x[1..(Len(Indets())+1)]],PosTo;
    Using Var(NewRing) Do
      H:=Last(Indets());
      L:=[Op.CanonicalImage(I)| I In L];
      If Homog Then E:=Op.ListInt(L,[1,2,1,1,1,2])
      Elif Not ReqHomog Then E:=Op.ListInt1(L,[1,2,1,1,1,1])
      Else
        L:=[Homogenized(H,I)| I In L];
        E:=Op.ListInt1(L,[1,2,1,1,1,2]);
      EndIf;
      E:=Ideal([NR(P,[H-1])|P In ReducedGBasis(E)]);
    EndUsing;
    E:=Op.CanonicalImage(E);
//obsolescent    Clear Var(NewRing);
//obsolescent    Destroy Var(NewRing);
    If Op.StandardCond() Then E.GBasis:=Gens(E) EndIf;
    Return E;
  EndIf;
EndDefine;


Define Colon(I,J)
  If Type(I)=IDEAL Then
    Return Op.CColon(I,J,FALSE)
  Else
    Return Op.ColMInt(I,J)
  EndIf;
EndDefine;

Define HColon(I,J) Return Op.CColon(I,J,TRUE); EndDefine;


-- Splits the computation between the two case : J principal or not.
Define Quotient(I,J,L)
  If Len(Gens(J))=1 Then
    Return Op.Transporter(I,Head(Gens(J)),L)
  Else
    Return Op.Col(I,J,L);
  EndIf;
EndDefine;


Define CColon(I,J,ReqHomog)
  If TypeOfCoeffs()=INT Then
    Error("Operation not implemented for non-field coefficients")
  EndIf;
  If Shape([I,J])<>[IDEAL, IDEAL] Then Error("(IDEAL,IDEAL) Expected") EndIf;
  Homog:=IsHomog(Gens(I)) And IsHomog(Gens(J));
  If IsPosTo(Var(RingEnv())) Then
    If Homog Then Return Op.Quotient(I,J,[1,1,1,1,2,2]) EndIf;
    If Not ReqHomog Then Return Op.Quotient(I,J,[1,1,1,1,1,1]) EndIf;
    NewRing:=NewId();
    // ANNA 2008 04 04
    Var(NewRing) ::= CoeffRing[x[1..(Len(Indets())+1)]], Weights(Concat(WeightsList(), [1])), PosTo;
    //   Var(NewRing)::=CoeffRing[x[1..(Len(Indets())+1)]],PosTo;
    Using Var(NewRing) Do
      H:=Last(Indets());
      I:=Op.CanonicalImage(I);
      J:=Op.CanonicalImage(J);
      I:=Homogenized(H,I);
      J:=Homogenized(H,J);
      E:=Op.Quotient(I,J,[1,1,1,1,2,2]);
      E:=Ideal([NR(P,[H-1])|P In ReducedGBasis(E)]);
    EndUsing;
    E:=Op.CanonicalImage(E);
//obsolescent    Clear Var(NewRing);
//obsolescent    Destroy Var(NewRing);
    If Op.StandardCond() Then E.GBasis:=Gens(E) EndIf;
    Return E;
  Else -- PosTo=False
    NewRing:=NewId();
    // ANNA 2008 04 04
    Var(NewRing) ::= CoeffRing[x[1..(Len(Indets())+1)]], Weights(Concat(WeightsList(), [1])), PosTo;
    //   Var(NewRing)::=CoeffRing[x[1..(Len(Indets())+1)]],PosTo;
    --Op.MakeThisRing(NewRing,Characteristic(),"x[1.."+Sprint(Len(Indets())+1)+"]");
    Using Var(NewRing) Do
      H:=Last(Indets());
      I:=Op.CanonicalImage(I);
      J:=Op.CanonicalImage(J);
      If Homog Then E:=Op.Quotient(I,J,[1,1,1,1,2,2])
      Elif Not ReqHomog Then E:=Op.Quotient(I,J,[1,1,1,1,1,1])
      Else
        I:=Homogenized(H,I);
        J:=Homogenized(H,J);
        E:=Op.Quotient(I,J,[1,1,1,1,2,2]);
      EndIf;
      E:=Ideal([NR(P,[H-1])|P In ReducedGBasis(E)]);
    EndUsing;
    E:=Op.CanonicalImage(E);
//obsolescent    Clear Var(NewRing);
//obsolescent    Destroy Var(NewRing);
    If Op.StandardCond() Then E.GBasis:=Gens(E) EndIf;
    Return E;
  EndIf;
EndDefine;



Define Saturation(I,J)
  If Type(I)=IDEAL Then
   Return Op.SSaturation(I,J,FALSE)
  Else
    Return Op.SSatMCol(I,J)
  EndIf;
EndDefine;

Define HSaturation(I,J) Return Op.SSaturation(I,J,TRUE); EndDefine;


-- Splits the computation between the two case : J principal or not.
Define VDiff(I,J,L)
  If Len(Gens(J))=1 Then
    Return Op.Sat(I,Head(Gens(J)),L)
  Else
    Return Op.SSat(I,J,L);
  EndIf;
EndDefine; -- VDiff


Define SSaturation(I,J,ReqHomog)
  If TypeOfCoeffs()=INT Then
    Error("Operation not implemented for non-field coefficients")
  EndIf;
  If Shape([I,J])<>[IDEAL, IDEAL] Then Error("(IDEAL,IDEAL) Expected") EndIf;
  Homog:=IsHomog(Gens(I)) And IsHomog(Gens(J));
  If IsPosTo(Var(RingEnv())) Then
    If Homog Then Return Op.VDiff(I,J,[1,1,1,1,2,2]) EndIf;
    If Not ReqHomog Then Return Op.VDiff(I,J,[1,1,1,1,1,1]) EndIf;
    NewRing:=NewId();
    Var(NewRing)::=CoeffRing[x[1..(Len(Indets())+1)]],PosTo;
    --Op.MakeThisRing(NewRing,Characteristic(),"x[1.."+Sprint(Len(Indets())+1)+"]");
    Using Var(NewRing) Do
      H:=Last(Indets());
      I:=Op.CanonicalImage(I);
      J:=Op.CanonicalImage(J);
      I:=Homogenized(H,I);
      J:=Homogenized(H,J);
      E:=Op.VDiff(I,J,[1,1,1,1,2,2]);
      E:=Ideal([NR(P,[H-1])|P In ReducedGBasis(E)]);
    EndUsing;
    E:=Op.CanonicalImage(E);
//obsolescent    Clear Var(NewRing);
//obsolescent    Destroy Var(NewRing);
    If Op.StandardCond() Then E.GBasis:=Gens(E) EndIf;
    Return E;
  Else -- PosTo=False
    NewRing:=NewId();
    Var(NewRing)::=CoeffRing[x[1..(Len(Indets())+1)]],PosTo;
    --Op.MakeThisRing(NewRing,Characteristic(),"x[1.."+Sprint(Len(Indets())+1)+"]");
    Using Var(NewRing) Do
      H:=Last(Indets());
      I:=Op.CanonicalImage(I);
      J:=Op.CanonicalImage(J);
      If Homog Then E:=Op.VDiff(I,J,[1,1,1,1,2,2])
      Elif Not ReqHomog Then E:=Op.VDiff(I,J,[1,1,1,1,1,1])
      Else
        I:=Homogenized(H,I);
        J:=Homogenized(H,J);
        E:=Op.VDiff(I,J,[1,1,1,1,2,2]);
      EndIf;
      E:=Ideal([NR(P,[H-1])|P In ReducedGBasis(E)]);
    EndUsing;
    E:=Op.CanonicalImage(E);
//obsolescent    Clear Var(NewRing);
//obsolescent    Destroy Var(NewRing);
    If Op.StandardCond() Then E.GBasis:=Gens(E) EndIf;
    Return E;
  EndIf;
EndDefine;


-------------------------- [ Utilities ] ------------------------------------

Define StandardCond()
  Return IsPosTo(Var(RingEnv())) And Ord() = DegRevLexMat(NumIndets());
EndDefine;


Define AssignGB(I,GB)
  If GB=[0] Then I.GBasis:=[] EndIf;
  I.GBasis:=GB;
  Return I;
EndDefine;

Define WhichVars(P)
  V:=[];
  Foreach X In Indets() Do
    If Der(P,X)<>0 Then Append(V,X) EndIf;
  EndForeach;
  Return V;
EndDefine;

Define WhichVarsL(L)
  S:=[];
  Foreach P In L Do
    S:=MakeSet(Concat(S,Op.WhichVars(P)));
  EndForeach;
  Return S;
EndDefine;


Define PutPolyLInSubring(L,Inds)
  Return [P In L | Diff(Inds,Op.WhichVars(P))=Inds];
EndDefine; -- PutPolyLInSubring

Define IsReducedGBasis(I)
  Return I.GBasis<>Null And I<>Ideal(0);
EndDefine; -- IsReducedGBasis


/*Input  The ReducedGBasis of a Module wrt a PosTo ordering.
  Output The last component of vectors whose other components are 0,
         as a ReducedGBasiszed ideal*/
Define FCMod(E)
  If E=[] Then Return Ideal(0) EndIf;
  E:=Module(E);
  H:=[];
  Rank:=Len(Head(Gens(E)));
  Foreach V In Gens(E) Do
    If FirstNonZeroPos(V)=Rank Then Append(H,V[Rank]) EndIf
  EndForeach;
  I:=Ideal(H);
  I.GBasis:=Gens(I);
  Return I;
EndDefine;


Define CanonicalImage(X)
  T := Type(X);
  If    T = IDEAL  Then L := Gens(X)
  Elif T = LIST   Then If X=[] Then Return [] Else L := X EndIf;
  Elif T = POLY   Then L := [X]
  EndIf;
  RE := RingEnv(L[1]);
  NFrom := NumIndets(Var(RE));
  NTo   := NumIndets();
  If NFrom >= NTo Then MapList := 1..NTo
  Else MapList := $builtin.Concat(1..NFrom, NewList(NTo-NFrom,0))
  EndIf;
  L := [$builtin.Map(X, RE, RingEnv(), MapList) | X In L];
  If    T = IDEAL  Then Return Ideal(L)
  Elif T = LIST   Then Return L
  Elif T = POLY   Then Return L[1]
  EndIf;
EndDefine;



-----------------------------------------------------------------------------
------------------------ [ Algorithms ] -------------------------------------
-----------------------------------------------------------------------------


----------------------- [ Intersection ] -----------------------------------

Define PoincSum(I, J)
    Ps1:=Poincare(CurrentRing()/I);
    Ps2:=Poincare(CurrentRing()/J);
   Using QQt Do
    P:=$hp.NumToPoly(Ps1)+$hp.NumToPoly(Ps2);
    N := Len($hp.Den(Ps1));
    E:=$hp.PSeries(P,N); --WARN, P=0 is NOT OK
   EndUsing;
   Return E;
EndDefine;


Define IntMod(I,J)
  LI:=[[P,0]|P In Gens(I)];
  LJ:=[[P,P]|P In Gens(J)];
  M:=Module($builtin.Concat(LI,LJ));
  E:=ReducedGBasis(M);
  Return Op.FCMod(E);
EndDefine;


Define IntHMod(I, J)
  --If I=Ideal(0) Or J=Ideal(0)  Then Return Ideal(0) EndIf;
  If I=Ideal(1) Or J=Ideal(1)  Then Return Cond(I<>Ideal(1),I,J) EndIf;
  LI:=[[P,0]|P In Gens(I)];
  LJ:=[[P,P]|P In Gens(J)];
  M:=Module($builtin.Concat(LI,LJ));
  M.PSeries:=Op.PoincSum(I,J);
  E:=ReducedGBasis(M);
  Return Op.FCMod(E);
EndDefine;


Define Int(I,J,L)
  N:=Last(L);
  If N=MEMORY.OP_PKG_IntModN Then Return Op.IntMod(I,J)
  Elif N=MEMORY.OP_PKG_IntHModN Then Return Op.IntHMod(I,J)
  EndIf;
EndDefine;

-------------------------- [ List Intersection ] ---------------------------

Define BySecondComponent(H,H1) Return H[2]>H1[2]; EndDefine;


-- One after the other.
Define ListInt1(L,L1)
  NInt:=L1[Len(L1)];
  If L=[] Then Return Ideal(1) EndIf;
  If Len(L)=1 Then Return Head(L)
  Else
    J:=Op.ListInt1(Tail(L),L1);
    H:=Op.Int(J,Head(L),L1);
    Return H
  EndIf;
EndDefine;


Define ListInt(L,L1)
  Return Op.ListInt1(L,L1)
EndDefine;



--------------------------- [ Principal quotient ] --------------------------


Define PoincTrHMod(I,G)
  X:=Head(Indets());
  Ps1:=Poincare(CurrentRing()/(Ideal(X^Deg(G))));
  Ps2:=Poincare(CurrentRing()/(I));
  Using QQt Do
    P:=$hp.NumToPoly(Ps1)+$hp.NumToPoly(Ps2);
    N := Len($hp.Den(Ps1));
    E:=$hp.PSeries(P,N);
  EndUsing;
  Return E;
EndDefine;

-- Modular algorithm.
Define TrMod(I,G)
  LI:=[[P,0]|P In Gens(I)];
  M:=Module(Concat(LI,[[G,1]]));
  E:=ReducedGBasis(M);
  Return Op.FCMod(E);
EndDefine;

-- Modular algorithm, HDriven.
Define TrHMod(I,G)
  If G=0 Then Return Ideal(1) EndIf;
  If I=Ideal(0) Then Return Ideal(0) EndIf;
  If G=1 Then Return I EndIf;
  NewRing:=NewId();
  Var(NewRing)::=CoeffRing[x[1..(Len(Indets())+1)]],PosTo;
  Using Var(NewRing) Do
    If I.GBasis=[] Then
      I1:=Ideal(ReducedGBasis(Op.CanonicalImage(I)));
    Else
      I1:=Ideal(Op.CanonicalImage(ReducedGBasis(I)));
    EndIf;
    I1:=Op.AssignGB(I1,Gens(I1));
    G1:=Op.CanonicalImage(G);
    X:=Last(Indets());
    LI:=[[P,0]|P In Gens(I1)];
    M:=Module(Concat(LI,[[G1,X^Deg(G1)]]));
    M.PSeries:=Op.PoincTrHMod(I1,G1);
    E:=ReducedGBasis(M);
    E:=Op.FCMod(E);
    E:=Ideal([NR(P,[X-1])|P In Gens(E)]);
  EndUsing;
  E:=Op.CanonicalImage(E);
//obsolescent  Clear Var(NewRing);
//obsolescent  Destroy Var(NewRing);
  Return E;
EndDefine;

-- Bayer trick
Define TrTrueBayer(I,X)
  E:=ReducedGBasis(I);
  E:=[Cond(NR(LT(P),[X])=0,P/X^Deg(G1),P) |P In E ];
  Return Ideal(E);
EndDefine;


Define Transporter(I,G,L)
  N:=L[Len(L)-1];
  If N=MEMORY.OP_PKG_TrModN Then Return Op.TrMod(I,G)
  Elif N=MEMORY.OP_PKG_TrHModN Then Return Op.TrHMod(I,G)
  EndIf;
EndDefine;



----------------------[ Quotient ]------------------------------------


-- Repeated principal quotients plus repeated intersection.
Define ColInt(I,J,L)
  If I = Ideal(0) Then Return I; EndIf;
  NInt:=L[Len(L)];
  TrInt:=L[Len(L)-1];
  L_J:=Gens(J);
  H:=Ideal(1);
  I:=Ideal(ReducedGBasis(I));
  For N:=1 To Len(L_J) Do
    Significant:=Not Ideal(L_J[N])*H<I;
    If Significant Then
      J1:=Op.Transporter(I,L_J[N],L);
      H:=Op.Int(H,J1,L);
    EndIf;
  EndFor;
  Return H;
EndDefine;

Define Col(I,J,L)
  Return Op.ColInt(I,J,L)
EndDefine;

----------------------- [ Principal saturations ] ---------------------------

-- Repeated principal quotient algorithms.
Define SatTry(I,G,L1)
  NTr:=L1[Len(L1)-1];
  L:=Op.Transporter(I,G,L1);
  OL:=I;
  OK:=L<OL;
  While Not OK Do
    OL:=Ideal(Gens(L));
    L:=Op.Transporter(L,G,L1);
    OK:=L<OL;
  EndWhile;
  Return L;
EndDefine;


Define Sat(I,G,L)
  Return Op.SatTry(I,G,L)
EndDefine;

-------------------------- [ Saturations ] ---------------------------


-- Repeated quotient algorithms.
Define SSatTry(I,J,L1)
  NCol:=L1[Len(L1)-2];
  L:=Op.Col(I,J,L1);
  OL:=I;
  OK:=L<OL;
  While Not OK Do
    OL:=Ideal(Gens(L));
    L:=Op.Col(L,J,L1);
    OK:=L<OL;
  EndWhile;
  Return L;
EndDefine;

Define SSat(I,J,L)
  Return Op.SSatTry(I,J,L)
EndDefine;

----------------------------- [ New ] ------------------------------

Define FC(E,ToLive)
  If E=[] Then Return Module([[0]]) EndIf;
  E:=Module(E);
  Cps:=NumComps(E);
  ToBeZero:=Cps-ToLive;
  H:=[];
  Foreach V In Gens(E) Do
    If FirstNonZeroPos(V)>ToBeZero Then
      Append(H,Last(V,ToLive))
    EndIf
  EndForeach;
  M:=Module(H);
  M.GBasis:=Gens(M);
  Return M;
EndDefine;


Define TrMMod(N,G)
  Cps:=NumComps(N);
  V0:=Vector(NewList(Cps,0));
  LN:=[Vector(Concat(V,V0))|V In Gens(N)];
  LG:=BlockMatrix([[G*Identity(Cps),Identity(Cps)]]);
  M1:=Module(Concat(LN,LG));
  E:=ReducedGBasis(M1);
  E:=Op.FC(E,Cps);
  Return E
EndDefine;




Define ColMInt(N,I)
  Cps:=NumComps(N);
  IGens:=Gens(I);
  N1:=Op.TrMMod(N,IGens[1]);
  If Len(IGens)=1 Then Return N1 Else H:=N1 EndIf;
  For Index_I:=2 To Len(IGens) Do
    Significant:=Not H*Ideal(IGens[Index_I])<N;
    If Significant Then
      N1:=Op.TrMMod(N,IGens[Index_I]);
      H:=Op.IntMod(H,N1);
    EndIf;
  EndFor;
  Return H;
EndDefine;


Define SatMTry(N,G)
  L:=Op.TrMMod(N,G);
  OL:=N;
  --Ti:=Time
  OK:=L<Module(OL);
  While Not OK Do
    OL:=Module(Gens(L));
    L:=Op.TrMMod(L,G);
    OK:=L<Module(OL);
  EndWhile;
  Return L;
EndDefine;


Define SSatMCol(N,I)
  L:=Op.ColMInt(N,I);
  OL:=I;
  OK:=L<Module(OL);
  While Not OK Do
    OL:=Module(Gens(L));
    L:=Op.ColMInt(L,I);
    OK:=L<Module(OL);
  EndWhile;
  Return L;
EndDefine;

EndPackage;  -- package $idealop
