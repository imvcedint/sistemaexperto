Package $monomial_ideals

Define About()
  PrintLn "    KeyWords : Gin, Borel, Stable, StronglyStable, LexSegment";
  PrintLn "    Author   : A.M.Bigatti";
  PrintLn "    Version  : CoCoA 4.7";
  PrintLn "    Date     : 23 March 2007"
EndDefine; -- About

-----------------------------------------------------------------------
-- Exported functions are:

 -- Gin(I)                 -- a very likely gin for the ideal I
 -- Gin(I, N)              -- coefficients in [-N, N]  (default is 100)

 -- LexSegmentIdeal(PPList)     -- the smallest LS ideal containing PPList
 -- StronglyStableIdeal(PPList) -- the smallest StSt ideal containing PPList
 -- StableIdeal(PPList)         -- the smallest St ideal containing PPList

 -- IsLexSegment(I)
 -- IsStronglyStable(I)
 -- IsStable(I)

 -- StronglyStableGens(I) -- the minimal StronglyStable generating set
-----------------------------------------------------------------------

Define Gin(...)
  I := ARGV[1];
  If Len(ARGV)=1 Then Range := 100; Else Range:=ARGV[2] EndIf;
  If Range<NumIndets() Then Range := NumIndets(); EndIf; --Det(Mat([[] | ]))
  While True Do
    L := Gens(LT($.RandIdeal(I, Range)));
    If $.TryGin(I, L, 3, Range) Then Return Ideal(L) EndIf;
--    If L = [] Then PrintLn "---- Not Equal! Trying again..."; EndIf;
  EndWhile;
EndDefine; -- Gin


Define Gin5(...)
  If Not IsServerReady() Then
    Error("Start CoCoAServer to use Gin5(..) or call Gin(..)");
  EndIf;
  I := ARGV[1];
  If Len(ARGV)=1 Then Range := 10000; Else Range:=ARGV[2] EndIf;
  If Range<NumIndets() Then Range := NumIndets(); EndIf; --Det(Mat([[] | ]))
  While True Do
    L := $.TryGin5(I, 2, Range);
    If MEMORY.PKG.CoCoA5.EnableTime Then -- Not running the tests
      If L = [] Then PrintLn "---- Not Equal! Trying again..."; EndIf;
    EndIf;
    If L<>[] Then Return Ideal(L) EndIf;
  EndWhile;
EndDefine; -- Gin

----------------------------------------------------------------------

Define AlexanderDual(X)
  If Type(X) = POLY Then
    AlexanderDualErr:="Expected squarefree monomial or monomial ideal";
    If Len(X)<>1 Then Error(AlexanderDualErr); EndIf;
    If Type(Product(Indets())/X)<>POLY Then Error(AlexanderDualErr); EndIf;
    L := Log(X);
    Return Ideal([ Indet(I) | I In 1..NumIndets() And L[I]<>0 ]);
  EndIf;
  Return IntersectionList([$.AlexanderDual(G) | G In Gens(X)]);
EndDefine;


Define SqFrPrimaryDecomposition(I)
  AD := $.AlexanderDual(I);
  Return [$.AlexanderDual(G) | G In Gens(AD)];
EndDefine;

----------------------------------------------------------------------

Define LexSegmentIdealPPList(PPList)
  Borel_Aux_Ring ::= CoeffRing[x[1..NumIndets()]], DegLex;
  Using Borel_Aux_Ring Do
    PPList := Image(PPList, RMap(Indets()));
    L := Interreduced(Support(Sum([$.LexSegmentPP_Lex(PP) | PP In PPList ])));
    L := Support(Sum(L));
  EndUsing;
  L := Image(L, RMap(Indets()));
  Return Ideal(L);
EndDefine; -- LexSegmentIdeal


Define LexSegmentDegLexRing(HF, MaxDegGens)
  I := Ideal(0);
  N := NumIndets();
  D := 0;
  HF_D := 1;
  While True Do
    HF_NextD := Num(LC($hp.EvalHilbertFn(HF, D+1)));
    If D > 0 Then
      NumComplMultiples := BinExp(HF_D, D, 1, 1);
    Else
      NumComplMultiples := NumIndets();
    EndIf;
    NumLexGensInNextD := NumComplMultiples - HF_NextD;
    D := D+1;
    If D > MaxDegGens And NumLexGensInNextD = 0 Then Return I; EndIf;
    L := First(Support(DensePoly(D)), Bin(N+D-1, D)-HF_NextD);
    L := Last(L, NumLexGensInNextD);
    I := I + Ideal(L);
    HF_D := HF_NextD;
  EndWhile;
EndDefine; -- LexSegmentDegLexRing


Define LexSegmentIdealHF(I)
  Using Var(RingEnv(I)) Do
    If I = Ideal(1) Then Return I;  EndIf;
    HF := Hilbert(CurrentRing()/I);
    MaxDegGens := Max([Deg(F) | F In Gens(I)]);
    LexSRing ::= CoeffRing[x[1..NumIndets()]], DegLex;
    Using LexSRing Do
      I := $.LexSegmentDegLexRing(HF, MaxDegGens);
      Minimalize(I);  // just to remove 0's
    EndUsing; // LexSRing
    Return Image(I, RMap(Indets()));
  EndUsing; // Var(RingEnv(I))
EndDefine; -- LexSegment


Define LexSegmentIdeal(X)
  If Type(X) = IDEAL Then
    Return $.LexSegmentIdealHF(X)
  Elif $.IsMonomial(X) Then
    Return $.LexSegmentIdealPPList(X)
  EndIf;
  Error("LexSegmentIdeal: expected ideal or list of power-products");
EndDefine; -- LexSegmentIdeal


Define StronglyStableIdeal(PPList)
  If Not $.IsMonomial(PPList) Then
    Error("StronglyStableIdeal: expected list of power-products");
  EndIf;
  If Characteristic()=0 Then
    L := Interreduced(Support(Sum([ $.BorelPP0(PP) | PP In PPList ])));
  Else
    Borel_Aux_Ring ::= QQ[x[1..NumIndets()]];
    Using Borel_Aux_Ring Do
      PPList := ZPQ(PPList);
      L := Interreduced(Support(Sum([ $.BorelPP0(PP) | PP In PPList ])));
    EndUsing;
    L := QZP(L);
  EndIf;
  Return Ideal(Support(Sum(L))); // To sort the list
EndDefine; -- StronglyStableIdeal


Define StableIdeal(PPList)
  If Not $.IsMonomial(PPList)
    Then Error("Stable: expected list of power-products");
  EndIf;
  If Characteristic()=0 Then
    L := Interreduced(Support(Sum([ $.StablePP0(PP) | PP In PPList ])));
  Else
    Borel_Aux_Ring ::= QQ[x[1..NumIndets()]];
    Using Borel_Aux_Ring Do
      PPList := ZPQ(PPList);
      L := Interreduced(Support(Sum([ $.StablePP0(PP) | PP In PPList ])));
    EndUsing;
    L := QZP(L);
  EndIf;
  Return Ideal(Support(Sum(L))); // To sort the list
EndDefine; -- StableIdeal


Define IsLexSegment(I)
  IsLexSegmentErr := "IsLexSegment: monomial ideal expected";
  If Not $.IsMonomial(I) Then Error(IsLexSegmentErr); EndIf;
  Borel_Aux_Ring ::= CoeffRing[x[1..NumIndets()]], Lex;
  Using Borel_Aux_Ring Do
    Return $.IsLexSegment_Lex(Image(I, RMap(Indets())));
  EndUsing;
EndDefine; -- IsLexSegment


Define IsStronglyStable(I)
  IsStronglyStableErr := "IsStronglyStable: monomial ideal expected";
  If Not $.IsMonomial(I) Then Error(IsStronglyStableErr); EndIf;
  B := Gens(I);
  If Characteristic()=0 Then
    Return $.IsBorel0(I);
  Else
    Borel_Aux_Ring ::= QQ[x[1..NumIndets()]];
    Using Borel_Aux_Ring Do
      Return $.IsBorel0(Ideal(ZPQ(B)));
    EndUsing;
  EndIf;
EndDefine; -- IsBorel


Define IsStable(I)
  IsStableErr := "IsStable: monomial ideal expected";
  If Not $.IsMonomial(I) Then Error(IsStableErr); EndIf;
  S := Gens(I);
  If Characteristic()=0 Then
    Return $.IsStable0(I);
  Else
    Borel_Aux_Ring ::= QQ[x[1..NumIndets()]];
    Using Borel_Aux_Ring Do
      Return $.IsStable0(Ideal(ZPQ(S)));
    EndUsing;
  EndIf;
EndDefine; -- IsStable


Define StronglyStableGens(I)
  B := Gens(I);
  If Characteristic()=0 Then
    Return $.BorelGens0(I);
  Else
    Borel_Aux_Ring ::= QQ[x[1..NumIndets()]];
    Using Borel_Aux_Ring Do
      B := $.BorelGens0(Ideal(ZPQ(B)));
    EndUsing;
    Return QZP(B);
  EndIf;
EndDefine; -- StronglyStableGens


//--------------------[ Auxiliary Gin ]--------------------

-- Define RandIdeal(...)
--   If Len(ARGV) = 1 Then Range := 100; Else Range:=ARGV[2] EndIf;
--   S := [ Sum([ Rand(-Range,Range)*Y | Y In Indets()]) | X In Indets()];
--   Return Ideal([ Eval(G, S) | G In Gens(ARGV[1])]);
-- EndDefine; -- RandIdeal


Define RandIdeal(...)
  If Len(ARGV) = 1 Then Range := 100; Else Range:=ARGV[2] EndIf;
  S := [ Sum([ Rand(-Range,Range)*Y | Y In First(Indets(), I)]) | I In 1..NumIndets()];
  Return Ideal([ Eval(G, S) | G In Gens(ARGV[1])]);
EndDefine; -- RandIdeal


Define TryGin(I, L, N, M)
  For J := 1 To N Do
    If Not EqSet(Gens(LT($.RandIdeal(I,M))), L) Then Return False EndIf;
  EndFor;
  Return True
EndDefine; -- TryGin


Define TryPrecisions(MyFun, Arg)
  P := Record[FloatPrecision := 64];
  While True Do
    If MEMORY.PKG.CoCoA5.EnableTime Then -- Not running the tests
      PrintLn "-- trying with FloatPrecision ", P.FloatPrecision;
    EndIf;
    Catch X := Call(Function(MyFun), Arg, P) In E EndCatch;
    If Type(E) <> ERROR Then Return X;
    Elif "insufficient precision" IsIn GetErrMesg(E) Then
      P.FloatPrecision := P.FloatPrecision * 2;
    Else
      Return E;
    EndIf;
  EndWhile;
EndDefine; -- TryPrecisions


Define TryGin5(I, N, Range)
  L := Gens($.TryPrecisions("LT5x", $.RandIdeal(I,Range)));
  For K := 2 To N Do
    If Not EqSet(Gens($.TryPrecisions("LT5x", $.RandIdeal(I,Range))), L) Then
      Return [];
    EndIf;
  EndFor;
  Return L
EndDefine; -- TryGin5


//--------------------[ auxiliary ]--------------------//

Define IsMonomial(X)
  If Type(X)=POLY Then Return Len(X)=1 EndIf;
  If Type(X)=IDEAL Then
    Gs := [ G In Gens(X) | G<>0*G ];
    Return Sum([Len(G)|G In Gs])=Len(Gs);
  EndIf;
  If Type(X)<POLY Then Return TRUE EndIf;
  If Type(X)=LIST Then
    Foreach El In X Do
      If Not $.IsMonomial(El) Then Return False EndIf;
    EndForeach;
    Return True;
  EndIf;
  Error("IsMonomial: POLY, LIST or IDEAL expected");
EndDefine;

-- it assumes ordering is Lex or DegLex
Define LexSegmentPP_Lex(PP)
  F := DensePoly(Deg(PP));
  Return Sum([ T In Support(F) | T >= PP ]);
EndDefine; -- LexSegmentPP_Lex


-- it assumes ordering is Lex or DegLex
Define IsLexSegment_Lex(I)
  LS := Gens(I);
  While LS<>[] Do
    PP := Min(LS);
    LSPP := $.LexSegmentPP_Lex(PP);
    If Not LSPP IsIn I Then  Return FALSE;  EndIf;
    LS := Diff(LS, Support(LSPP));
  EndWhile;
  Return TRUE;
EndDefine; -- IsLexSegment_Lex


-- it assumes Characteristic = 0
Define BorelPP0(PP)
  L := Log(PP);
  Return Product([ Sum(First(Indets(),I))^L[I] | I In 1..NumIndets()]);
EndDefine; -- BorelPP0


-- it assumes Characteristic = 0
Define StablePP0(PP)
  If PP = Indet(1)^Deg(PP) Then Return PP; EndIf;
  L := Log(PP);
  MaxI := Max([ I In 1..NumIndets() | L[I]<>0 ]);
  OneStep := PP*(Sum(First(Indets(), MaxI))/Indet(MaxI))^(L[MaxI]-1);
  Return OneStep + $.StablePP0(PP*(Indet(MaxI-1)/Indet(MaxI))^L[MaxI]);
EndDefine;


-- it assumes Characteristic = 0
Define IsBorel0(I)
  B := Gens(I);
  While B<>[] Do
    PP := Min(B);
    BPP := $.BorelPP0(PP);
    If Not BPP IsIn I Then Return FALSE; EndIf;
    B := Diff(B, Support(BPP));
  EndWhile;
  Return TRUE;
EndDefine; -- IsBorel0


-- it assumes Characteristic = 0
Define IsStable0(I)
  S := Gens(I);
  While S<>[] Do
    PP := Min(S);
    SPP := $.StablePP0(PP);
    If Not SPP IsIn I Then Return FALSE; EndIf;
    S := Diff(S, Support(SPP));
  EndWhile;
  Return TRUE;
EndDefine; -- IsStable0


-- it assumes Characteristic = 0
Define BorelGens0(I)
  B := Gens(I);
  MinGens := [];
  While B<>[] Do
    PP := Min(B);
    Append(MinGens, PP);
    BPP := $.BorelPP0(PP);
    If Not BPP IsIn I Then Error("BorelGens: input is not Borel"); EndIf;
    B := Diff(B, Support(BPP));
  EndWhile;
  Return MinGens;
EndDefine; -- BorelGens


EndPackage;


