-- $Id: demo.ts,v 1.17 2009/05/22 15:09:12 bigatti Exp $
-- Test Suite: demo of some functions

Alias TSL := $ts/ts_lib;
TSL.Initialize();

-------------------------------
-- DEMO 01 : GB - Interreduction

Test := Record[Id := "demo_01",Descr := "GB - Interreduction"];

Test.Input :="
Use S ::= QQ[t,x,y,z];
F := x^3+x*y^2+y^3;
G := x^2+z^2;
H := (x+y)*(x^2-z^2);
I := Ideal(F,G,H);
GB.Interreduced(I);


Define Trace_Interreduced(Var I)
  $gb.Start_Interreduced(I);
  C := 0;
  Repeat
    $gb.Steps(I,1);
    If $gb.StepsDone() <> 0 Then
      PrintLn I.Rules;
      C := C+1;
    EndIf
  Until $gb.StepsDone() = 0;
  PrintLn Dashes();
  Print \"Interreduction performed in \",C,\" steps\";
EndDefine;

Set SingleStepRed;
Use S ::= QQ[t,x,y,z];
F := x^3+x*y^2+y^3;
G := x^2+z^2;
H := (x+y)*(x^2-z^2);
I := Ideal(F,G,H);
Trace_Interreduced(I);
";
Test.ExpectedOutput :=
"[x^2 + z^2, -2xz^2 - 2yz^2, xy^2 + y^3 + yz^2]
-------------------------------
[x^2 + z^2, x^3 + x^2y - xz^2 - yz^2]
[x^2 + z^2, x^3 + x^2y - xz^2 - yz^2, xy^2 + y^3 - xz^2]
[xy^2 + y^3 - xz^2, x^2 + z^2]
[xy^2 + y^3 - xz^2, x^2 + z^2]
[xy^2 + y^3 - xz^2, x^2 + z^2, -2xz^2 - 2yz^2]
[xy^2 + y^3 - xz^2, x^2 + z^2, -2xz^2 - 2yz^2]
[x^2 + z^2, -2xz^2 - 2yz^2]
[x^2 + z^2, -2xz^2 - 2yz^2, xy^2 + y^3 + yz^2]
-------------------------------
Interreduction performed in 8 steps
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- DEMO 02 : GB - Normalization

Test := Record[Id := "demo_02",Descr := "GB - Normalization"];

Test.Input :="
Use S ::= QQ[t,x,y,z];
I := Ideal(t^31-t^6-x, t^8-y, t^10-z);
F := y^5-z^4;
NF(F,I);

Define Trace_NF(F,I)
  $gb.Start_NF(F,I); -- automatically compute I.GBasis if necessary
  PrintLn \"    \",I.CV;
  C := 0;
  Repeat
    $gb.Steps(I,1);
    If $gb.StepsDone() <> 0 Then
      PrintLn \"--> \",I.CV;
      C := C+1;
    EndIf
  Until $gb.StepsDone() = 0;
  PrintLn Dashes();
  Print F,\" --[\",C,\"]--> \",I.CV;
EndDefine;

Set SingleStepRed;
Use S ::= QQ[t,x,y,z];
I := Ideal(t^31-t^6-x, t^8-y, t^10-z);
F := t^33;
Trace_NF(F,I);

Use S ::= QQ[t,x,y,z];
I := Ideal(t^31-t^6-x, t^8-y, t^10-z);
F := y^5-z^4;
G := (t^8-y)*(3*F+t^10-z);
NF(F,I);
NF(G,I);
";
Test.ExpectedOutput :=
"0
-------------------------------
    t^33
--> t^25y
--> t^17y^2
--> t^9y^3
--> ty^4
--> t^2x + y
-------------------------------
t^33 --[5]--> t^2x + y
-------------------------------
0
-------------------------------
0
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- DEMO 03 : Ratfun

Test := Record[Id := "demo_03",Descr := "Ratfun"];

Test.Input :="
Use S ::= QQ[x,y,z];
x/y;
x/(y*z);
x/(2*y);
x*y/z;
(x+y)/z;
z/(z+y);
(x^2-y^2)/(x+y);
(x+y)/(x^2-y^2);
";
Test.ExpectedOutput :=
"x/y
-------------------------------
x/(yz)
-------------------------------
1/2x/y
-------------------------------
xy/z
-------------------------------
(x + y)/z
-------------------------------
z/(y + z)
-------------------------------
x - y
-------------------------------
1/(x - y)
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- DEMO 04 : NFsAreZero

Test := Record[Id := "demo_04",Descr := "NFsAreZero"];

Test.Input :="
Use S ::= QQ[t,x,y,z];
I := Ideal(t^31-t^6-x, t^8-y, t^10-z);
F := y^5-z^4;
G := (t^8-y)*(3*F+t^10-z);
NFsAreZero([F,G],I);
NFsAreZero([F,x,G],I);
";
Test.ExpectedOutput :=
"True
-------------------------------
False
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- DEMO 05 : Colon

Test := Record[Id := "demo_05",Descr := "Colon"];

Test.Input :="
Use S ::= QQ[x,y];
Colon(Module([x,y]),Vector(x,y));
Colon(Module([x,y]),Module(Vector(x,y)));
Colon(Module([x^2,x*y]),Module(Vector(x,y)));
Colon(Module([x^2,x*y]),Module([x,y]));
Colon(Module([x,y]),Module([x,y]));
Colon(Module([x,y]),Module([x^2,y]));
";
Test.ExpectedOutput :=
"ERROR: Colon: arguments must be [MODULE,MODULE], [MODULE,IDEAL], or [IDEAL,IDEAL]
CONTEXT: Error(\"Colon: arguments must be [MODULE,MODULE], [MODULE,IDEAL], or [IDEAL,IDEAL]\")
-------------------------------
Ideal(1)
-------------------------------
Ideal(x)
-------------------------------
Ideal(x)
-------------------------------
Ideal(1)
-------------------------------
Ideal(0)
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- DEMO 06 : Flatten

Test := Record[Id := "demo_06",Descr := "Flatten"];

Test.Input :="
Flatten([1,2]);
Flatten([1,[3,4],2,5]);
Flatten(3);
";
Test.ExpectedOutput :=
"[1, 2]
-------------------------------
[1, 3, 4, 2, 5]
-------------------------------
3
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- DEMO 07 : Distribution Driven GBasis

Test := Record[Id := "demo_07",Descr := "Distribution Driven GBasis"];

Test.Input :="

Define Cyclic()
  D := NumIndets()-1;
  P := NewList(D);
  For K := 0 To D-2 Do
    P[K+1] := Sum([ Product([Indet(Mod(I+J,D)+1) | J In 0..K]) | I In 0..(D-1)]);
  EndFor;
  P[D] := Product([Indet(I)|I In 1..D])-Indet(D)^D;
  Return Ideal(P);
EndDefine;

Define Test_DDriven_GBasis(M)
  L := Gens(Cast(M,MODULE));
  -- first compute G = GBasis(M)
  T1 := Time G1 := GBasis(Module(L));
  -- Print T1;
  -- then let D = distribution of degrees of GBasis
  D := Distrib([Deg(V)|V In G1]);
  -- finally recompute H = DDriven_GBasis(M)
  M := Module(L);
  T2 := Time G2 := DDriven_GBasis(M,D);
  -- Print T2;
  -- compare results and return statistics
  Return [G1,D,T1,T2,MakeSet(G1) = MakeSet(G2),$gb.ResStats(M)];
EndDefine;

Define DDriven_GBasis(Var M,Distrib)
  -- Distribution Driven GBasis
  -- M module
  -- Distrib distribution of degrees of GBasis(M)
  ---- Distrib = List of pairs [Deg,Num]
  ---- where Num is the number of gbasis elements of degree Deg
  $gb.Start_GBasis(M);
  Foreach P In Distrib Do
    M.DegTrunc := P[1];
    M.Discrepancy := P[2];
    $gb.Complete(M);
  EndForeach;
  Return M.GBasis
EndDefine;


-- Set Verbose;
Use S ::= ZZ/(32003)[a,b,c,d,e,f];
I := Cyclic();
Result := Test_DDriven_GBasis(I);
Block
  PrintLn Result[1];
  PrintLn \"          Distribution = \", Result[2];
  --PrintLn \"        Time of GBasis = \", Result[3];
  --PrintLn \"Time of DDriven_GBasis = \", Result[4];
  PrintLn \"           Test passed = \", Result[5];
  PrintLn \"            Statistics = \", Result[6];
EndBlock;

";
Test.ExpectedOutput :=
"[Vector(a + b + c + d + e), Vector(-b^2 - bd + cd - 2be - ce - e^2), Vector(-bc^2 + bcd - c^2d + c^2e - bde - cde - d^2e + be^2 + 2ce^2 - de^2 + e^3), Vector(-c^3d - bcd^2 + c^2d^2 + c^3e - 4c^2de - cd^2e + 2bce^2 + 4c^2e^2 - 3bde^2 - 2cde^2 - 3d^2e^2 + 2be^3 + 5ce^3 - 2de^3 + 2e^4), Vector(-bcd^2 - bcde - c^2de + bd^2e - cd^2e + d^3e + bce^2 + bde^2 + 2d^2e^2 - be^3 - ce^3 + de^3 - e^4), Vector(c^2d^3 - c^2d^2e + 3cd^3e - c^3e^2 - bcde^2 + bd^2e^2 + 2d^3e^2 - bce^3 - 4c^2e^3 + 4bde^3 + cde^3 + 5d^2e^3 - 3be^4 - 6ce^4 + 3de^4 - 3e^5), Vector(-c^2d^2e + bd^3e + 3cd^3e + d^4e - c^3e^2 + bcde^2 + c^2de^2 + 2bd^2e^2 + 2cd^2e^2 + 5d^3e^2 - 4bce^3 - 5c^2e^3 + 2bde^3 - 3cde^3 + 6d^2e^3 - 2be^4 - 6ce^4 - 2e^5), Vector(2bcde^2 + c^2de^2 + 2cd^2e^2 - bce^3 - 2bde^3 - cde^3 - d^2e^3 + be^4 + ce^4 - 2de^4), Vector(16001bd^3e^2 + 15999cd^3e^2 + 16001d^4e^2 - 16001c^3e^3 - bd^2e^3 + 16001cd^2e^3 - 3d^3e^3 - 16000bce^4 - 15999c^2e^4 + 16000bde^4 - 16000cde^4 + 15998d^2e^4 - 16000be^5 - 15998ce^5 + 2e^6), Vector(cd^3e^2 + 16001c^3e^3 + 8001c^2de^3 - 16001bd^2e^3 - 16001cd^2e^3 + d^3e^3 + 8000bce^4 - 2c^2e^4 - 16000bde^4 + 8001cde^4 + 8003d^2e^4 - 8002be^5 - 8003ce^5 - 16001de^5 - e^6), Vector(-16001c^3e^3 - 8001c^2de^3 - bd^2e^3 - cd^2e^3 + 16001d^3e^3 + 8001bce^4 - 16000c^2e^4 + 16001bde^4 + 8000cde^4 - 8002d^2e^4 + 8002be^5 + 8002ce^5 + 16001de^5 + e^6), Vector(bd^4e + 3cd^4e + d^5e + 3d^4e^2 + 16001bd^2e^3 + 16001cd^2e^3 - 16001d^3e^3 - 16001bce^4 - 16001c^2e^4 - 3bde^4 + 15999cde^4 - 2d^2e^4 + 2be^5 + ce^5 - 3de^5 - e^6), Vector(cd^4e - c^4e^2 + 16001c^2de^3 - 14bd^2e^3 - 8cd^2e^3 - 4d^3e^3 + 15995bce^4 + 8bde^4 + 15994cde^4 - 16000d^2e^4 - 15989be^5 + 16000ce^5 - de^5 + 21e^6), Vector(c^4e^2 - d^4e^2 + 2c^2de^3 - 15999bd^2e^3 - 15999cd^2e^3 + 15998d^3e^3 - 15995bce^4 + 16000c^2e^4 - 15990cde^4 - 2d^2e^4 - 9be^5 + 2ce^5 - 11e^6), Vector(-d^4e^3 + c^2de^4 + 15998bd^2e^4 + 16001cd^2e^4 + 15998d^3e^4 - 16001bce^5 + 16001c^2e^5 + 2bde^5 - 15999cde^5 + d^2e^5 + be^6 - ce^6 + 2e^7), Vector(16001bd^2e^4 - 15999cd^2e^4 - 16000d^3e^4 + 15998bce^5 + 15998c^2e^5 + bde^5 + 15997cde^5 + 5d^2e^5 + 3be^6 - 5ce^6 + 4e^7), Vector(-15cd^2e^4 - 10d^3e^4 + 20bce^5 + 20c^2e^5 - 5bde^5 + 25cde^5 - 30d^2e^5 - 15be^6 + 30ce^6 - 20e^7), Vector(15999d^3e^4 + 5bce^5 + 5c^2e^5 - 5bde^5 - 15999cde^5 + 15994d^2e^5 - 15994ce^6 - 5e^7), Vector(d^5e^2 + 2c^2de^4 - 9bce^5 - 11c^2e^5 + 11bde^5 + 2cde^5 + 20d^2e^5 - 2be^6 - 20ce^6 + 6e^7)]
          Distribution = [[1, 1], [2, 1], [3, 1], [4, 2], [5, 3], [6, 6], [7, 5]]
           Test passed = True
            Statistics = Record[CREATED_PAIRS := 171, CREATED_VECTORS := 5, GROEBNER_PAIRS := 14, KILLED_BACK := 0, KILLED_C_PAIRS := 112, KILLED_H_PAIRS := 4, KILLED_H_VECTORS := 0, MINIMAL_GENERATORS := 5, ZERO_PAIRS := 6, ZERO_VECTORS := 8]

-------------------------------
";
TSL.RegisterTest(Test);


-------------------------------
-- DEMO 08 : Distribution Driven Elim

Test := Record[Id := "demo_08", Descr := "Distribution Driven Elim"];

Test.Input :="

Define ChordalIdealGens(L)
  -- the ring is ZZ/(32003)[y[0..N],z[0..N],a,b,x[0..M]]
  -- L is a list of homogeneous polynomials of the same degree in y[0..N]
  -- x[0] = a*L[1] + b*Lp[1]
  -- ...
  -- x[M] = a*L[M+1] + b*Lp[M+1]

  -- First we build Lp, using the following substitution
  N := Len(AllIndetsCalled(\"y\"))-1;
  M := Len(AllIndetsCalled(\"x\"))-1;
  S := [ [y[I], z[I]] | I In 0..N ];
  Lp := Subst(L,S);
  CIG := [ x[I-1]-a*L[I]-b*Lp[I] | I In 1..(M+1) ];
  Return CIG;
EndDefine;

Define Test_DDriven_Elim(M,ElimIndets)
  L := Gens(Cast(M,MODULE));
  -- first compute G = GBasis(M)
  T1 := Time G1 := GBasis(Module(L));
  -- then let DE = distribution of degrees of Elim
  D := Distrib([Deg(V)|V In G1]);
  -- then let DE = distribution of degrees of Elim
  S := [[Indet(I),0]|I In 1..ElimIndets];
  E := [V In G1 | Not IsZero(Subst(LT(V),S))];
  DE := Distrib([Deg(V)|V In E]);
  -- finally recompute H = DDriven_GBasis(M)
  M := Module(L);
  T2 := Time G2 := DDriven_Elim(M,ElimIndets,D,DE);
  -- compare results and return statistics
  Return [G1,D,DE,T1,T2,MakeSet(G1) = MakeSet(G2),$gb.ResStats(M)];
EndDefine;

Define DDriven_Elim(Var M,ElimIndets,Distrib,DistribElim)
  -- Distribution Driven Elim
  -- M module
  -- ElimIndets: number of indets to eliminate (first ElimIndets indets)
  -- Distrib distribution of degrees of GBasis(M)
  ---- Distrib = List of pairs [Deg,Num]
  ---- where Num is the number of gbasis elements of degree Deg
  -- NOTE: for the correct behaviour of this function the current ordering
  -- must be the elimination ordering of the first ElimIndets indeterminates
  $gb.Start_GBasis(M);
  L := Last(DistribElim);
  Foreach P In Distrib Do
    M.ElimIndets := 0;
    M.DegTrunc := P[1];
    M.Discrepancy := P[2];
    If P[1] = L[1] Then
      M.ElimIndets := ElimIndets;
      M.Discrepancy := L[2];
      $gb.Complete(M);
      Return M.GBasis;
    EndIf;
    $gb.Complete(M);
  EndForeach;
  Return M.GBasis
EndDefine;

-- Set Verbose;


N := 2; M := 5; D := 3;
W := Concat(NewList(2*(N+1)+2,1),NewList(M+1,D));

--2-- N := 2; M := 9; D := 4;
--2-- W := Concat(NewList(2*(N+1)+2,1),NewList(M+1,D));

Use S ::= ZZ/(32003)[y[0..N],z[0..N],a,b,x[0..M]],Weights(W),Elim(y[0]..b);
PL := Support(Sum(AllIndetsCalled(\"y\"))^2);
--2-- PL := Support(Sum(AllIndetsCalled(\"y\"))^3);
I := Ideal(ChordalIdealGens(PL));

N := 2;
Result := Test_DDriven_Elim(I,2*(N+1)+2);
Block
  PrintLn Result[1];
  PrintLn \"          Distribution = \", Result[2];
  PrintLn \"  Distribution of Elim = \", Result[3];
  --PrintLn \"          Time of Elim = \", Result[4];
  --PrintLn \"  Time of DDriven_Elim = \", Result[5];
  PrintLn \"           Test passed = \", Result[6];
  PrintLn \"            Statistics = \", Result[7];
EndBlock;

";
Test.ExpectedOutput :=
"[Vector(-y[2]^2a - z[2]^2b + x[5]), Vector(-y[1]y[2]a - z[1]z[2]b + x[4]), Vector(-y[0]y[2]a - z[0]z[2]b + x[3]), Vector(-y[1]^2a - z[1]^2b + x[2]), Vector(-y[0]y[1]a - z[0]z[1]b + x[1]), Vector(-y[0]^2a - z[0]^2b + x[0]), Vector(-y[2]z[1]z[2]b + y[1]z[2]^2b + y[2]x[4] - y[1]x[5]), Vector(-y[2]z[0]z[2]b + y[0]z[2]^2b + y[2]x[3] - y[0]x[5]), Vector(-y[2]z[1]^2b + y[1]z[1]z[2]b + y[2]x[2] - y[1]x[4]), Vector(-y[1]z[0]z[2]b + y[0]z[1]z[2]b + y[1]x[3] - y[0]x[4]), Vector(-y[2]z[0]z[1]b + y[0]z[1]z[2]b + y[2]x[1] - y[0]x[4]), Vector(-y[2]z[0]^2b + y[0]z[0]z[2]b + y[2]x[0] - y[0]x[3]), Vector(-y[1]z[0]z[1]b + y[0]z[1]^2b + y[1]x[1] - y[0]x[2]), Vector(-y[1]z[0]^2b + y[0]z[0]z[1]b + y[1]x[0] - y[0]x[1]), Vector(y[2]z[2]x[2] - y[2]z[1]x[4] - y[1]z[2]x[4] + y[1]z[1]x[5]), Vector(y[2]z[1]x[3] - y[1]z[2]x[3] - y[2]z[0]x[4] + y[0]z[2]x[4] + y[1]z[0]x[5] - y[0]z[1]x[5]), Vector(y[2]z[2]x[1] - y[1]z[2]x[3] - y[2]z[0]x[4] + y[1]z[0]x[5]), Vector(y[1]z[2]x[1] - y[0]z[2]x[2] - y[1]z[1]x[3] + y[0]z[1]x[4]), Vector(y[2]z[2]x[0] - y[2]z[0]x[3] - y[0]z[2]x[3] + y[0]z[0]x[5]), Vector(y[1]z[2]x[0] - y[0]z[2]x[1] - y[1]z[0]x[3] + y[0]z[0]x[4]), Vector(y[2]z[1]x[1] - y[2]z[0]x[2] - y[1]z[1]x[3] + y[1]z[0]x[4]), Vector(y[2]z[1]x[0] - y[2]z[0]x[1] - y[0]z[1]x[3] + y[0]z[0]x[4]), Vector(y[1]z[1]x[0] - y[1]z[0]x[1] - y[0]z[1]x[1] + y[0]z[0]x[2]), Vector(z[2]^2bx[2] - 2z[1]z[2]bx[4] + z[1]^2bx[5] + x[4]^2 - x[2]x[5]), Vector(z[2]^2bx[1] - z[1]z[2]bx[3] - z[0]z[2]bx[4] + z[0]z[1]bx[5] + x[3]x[4] - x[1]x[5]), Vector(z[2]^2bx[0] - 2z[0]z[2]bx[3] + z[0]^2bx[5] + x[3]^2 - x[0]x[5]), Vector(z[1]z[2]bx[1] - z[0]z[2]bx[2] - z[1]^2bx[3] + z[0]z[1]bx[4] + x[2]x[3] - x[1]x[4]), Vector(z[1]z[2]bx[0] - z[0]z[2]bx[1] - z[0]z[1]bx[3] + z[0]^2bx[4] + x[1]x[3] - x[0]x[4]), Vector(z[1]^2bx[0] - 2z[0]z[1]bx[1] + z[0]^2bx[2] + x[1]^2 - x[0]x[2]), Vector(-y[2]x[2]x[3] + y[2]x[1]x[4] + y[1]x[3]x[4] - y[0]x[4]^2 - y[1]x[1]x[5] + y[0]x[2]x[5]), Vector(-y[2]x[1]x[3] + y[1]x[3]^2 + y[2]x[0]x[4] - y[0]x[3]x[4] - y[1]x[0]x[5] + y[0]x[1]x[5]), Vector(z[2]x[2]x[3] - z[2]x[1]x[4] - z[1]x[3]x[4] + z[0]x[4]^2 + z[1]x[1]x[5] - z[0]x[2]x[5]), Vector(z[2]x[1]x[3] - z[1]x[3]^2 - z[2]x[0]x[4] + z[0]x[3]x[4] + z[1]x[0]x[5] - z[0]x[1]x[5]), Vector(-y[2]x[1]^2 + y[2]x[0]x[2] + y[1]x[1]x[3] - y[0]x[2]x[3] - y[1]x[0]x[4] + y[0]x[1]x[4]), Vector(-z[2]x[1]^2 + z[2]x[0]x[2] + z[1]x[1]x[3] - z[0]x[2]x[3] - z[1]x[0]x[4] + z[0]x[1]x[4]), Vector(-x[2]x[3]^2 + 2x[1]x[3]x[4] - x[0]x[4]^2 - x[1]^2x[5] + x[0]x[2]x[5])]
          Distribution = [[3, 6], [4, 8], [5, 9], [6, 6], [7, 6], [9, 1]]
  Distribution of Elim = [[9, 1]]
           Test passed = True
            Statistics = Record[CREATED_PAIRS := 630, CREATED_VECTORS := 6, GROEBNER_PAIRS := 30, KILLED_BACK := 6, KILLED_C_PAIRS := 459, KILLED_H_PAIRS := 53, KILLED_H_VECTORS := 0, MINIMAL_GENERATORS := 6, ZERO_PAIRS := 41, ZERO_VECTORS := 0]

-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- DEMO 09 : Image (and ring maps)

Test := Record[Id := "demo_09", Descr := "Image (and ring maps)"];

Test.Input :="
Use B ::= QQ[x,y];
Use A ::= QQ[a,b,c];
M := RMap(a^2*b+1,c^3-a*b);
Image(B ::(x*y), M);

Use A ::= QQ[a,b,c];
Use B ::= QQ[x,y];
M := RMap(x^2*y,x,y^3); -- list of polys tagged with \"RMap\"
Image(A::(a*b*c),M); -- returns a value in the current ring
Image(A::Ideal(a+b,c^2*a,b),M); -- returns a value in the current ring
A := NewMat(3,3,0);
Image(A,M);
A[2,3] := A :: c;
Image(A,M);
R := Record[A := 3, B := A :: Sum(Indets())];
Image(R,M);
Image(A::Module([[a+b,c^2*a],[a,b]]),M); -- returns a value in the current ring
Image(A::Vector([a+b,c^2*a]),M); -- returns a value in the current ring
Image(A::(a/c),M); -- returns a value in the current ring

//obsolescent Delete A;
Use A ::= QQ[x,y];
I := Ideal(x^2,y^3);
// cocoa-4 bug Poincare(A/I);
Poincare(CurrentRing()/I);

Use B ::= QQ[x,y,z];
M := RMap(x,y); -- list of polys tagged with \"RMap\"
I := Image(I,M);
Poincare(B/I);

Use C ::= QQ[x,y,z]; -- here we have an endomorphism
M := RMap(x,x+y,x+y+z);
Image((x+y+z)^2,M);
Image(x/y+x/z,M);
";

Test.ExpectedOutput :=
"a^2bc^3 - a^3b^2 + c^3 - ab
-------------------------------
x^3y^4
-------------------------------
Ideal(x^2y + x, x^2y^7, x)
-------------------------------
Mat([
  [0, 0, 0],
  [0, 0, 0],
  [0, 0, 0]
])
-------------------------------
Mat([
  [0, 0, 0],
  [0, 0, y^3],
  [0, 0, 0]
])
-------------------------------
Record[A := 3, B := x^2y + y^3 + x]
-------------------------------
Module([[x^2y + x, x^2y^7], [x^2y, x]])
-------------------------------
Vector(x^2y + x, x^2y^7)
-------------------------------
x^2/y^2
-------------------------------
(1 + 2x + 2x^2 + x^3)
-------------------------------
(1 + 2x + 2x^2 + x^3) / (1-x)
-------------------------------
9x^2 + 12xy + 4y^2 + 6xz + 4yz + z^2
-------------------------------
(2x^2 + 2xy + xz)/(x^2 + 2xy + y^2 + xz + yz)
-------------------------------
";

TSL.RegisterTest(Test);

-------------------------------
-- DEMO 10 : Break and Return commands

Test := Record[Id := "demo_10", Descr := "Break and Return commands"];

Test.Input :="

For J := 1 To 100 Do
  Print J,\" \";
  If J = 10 Then PrintLn; Break EndIf;
EndFor;

For I := 5 To 1 Step -1 Do
  For J := 1 To 100 Do
    Print J,\" \";
    If J = I Then PrintLn; Break EndIf;
  EndFor;
EndFor;

Use S ::= QQ[a,b,c,x,d,e,f];

Foreach X In Indets() Do
  Print X;
  If IndetName(X) = \"x\" Then Break; EndIf;
EndForeach;

X := 1;
While True Do
  Print X;
  X := X+1;
  If X > 10 Then Break; EndIf;
EndWhile;

X := 1;
While True Do
  Print X;
  X := X+1;
  If X > 10 Then Break; EndIf;
EndWhile;

For I := 5 To 1 Step -1 Do
  For J := 1 To 100 Do
    Print J,\" \";
    If J = I Then Return \"[exit from both loops]\" EndIf;
  EndFor;
EndFor;

Define P(X)
  Print \"X=\",X;
  Break;
  PrintLn \".\";
EndDefine;

For I := 1 To 10 Do
  P(I);
EndFor;

";

Test.ExpectedOutput :=
"1 2 3 4 5 6 7 8 9 10 

-------------------------------
1 2 3 4 5 
1 2 3 4 
1 2 3 
1 2 
1 

-------------------------------
abcx
-------------------------------
12345678910
-------------------------------
12345678910
-------------------------------
1 2 3 4 5 
-------------------------------
X=1ERROR: Break must be used inside a loop
CONTEXT:  BREAK
-------------------------------
";

TSL.RegisterTest(Test);

-------------------------------
-- DEMO 11 : Associative Operators

Test := Record[Id := "demo_11", Descr := "Associative Operators"];

Test.Input :="
Use QQ[x,y,z];
GCD([x*y,x*z]);
LCM([x*y,x*z]);
Sum(1..5);
Product(1..5);
IntersectionList([Ideal(x,y),Ideal(x,z)]) = Ideal(-x, y*z);
";

Test.ExpectedOutput :=
"x
-------------------------------
xyz
-------------------------------
15
-------------------------------
120
-------------------------------
True
-------------------------------
";

TSL.RegisterTest(Test);

-------------------------------
-- DEMO 12 : Function Function

Test := Record[Id := "demo_12",Descr := "Function Function"];

Test.Input := "
Define PrintXY(X,Y)
  PrintLn X;
  PrintLn Y
EndDefine;
F := Function(\"PrintXY\");
Type(F);
F;
Describe F;
Define Pair(A,B) Return [A,B]; EndDefine;
Describe Function(\"Pair\");
Function(\"Len\");
Describe Function(\"$builtin.Len\");
Describe Function(\"Aliases\");
Describe Function(\"$list.Flatten\");
Function(\"$builtin.UnexistentFunc\");
Function(\"$unexistent/package.Func\");
Function(\"bad/pkg/name.Func\");
Function(\"UnexistentFunc\");
";

Test.ExpectedOutput :=
"FUNCTION
-------------------------------
PrintXY(X,Y)
-------------------------------
Define PrintXY(X,Y)
  PrintLn(X); 
  PrintLn(Y)
EndDefine;
-------------------------------
Define Pair(A,B)
  Return([A, B]); 
EndDefine;
-------------------------------
Len(X1)
-------------------------------
Define Len(X1) /* built in */ EndDefine;
-------------------------------
Define Aliases(...) /* built in */ EndDefine;
-------------------------------
Define Flatten(...)
  If Len(ARGV) = 1 Then 
    Return($list.Flatten2(ARGV[1], -1))
  EndIf; 
  If Len(ARGV) = 2 AND Type(ARGV[2]) = INT AND ARGV[2] >= 0 Then 
    Return($list.Flatten2(ARGV[1],ARGV[2]))
  EndIf; 
  If NOT(Len(ARGV) = 2) Then 
    Error(\"Flatten: 1 or 2 arguments expected\")
  EndIf; 
  Error(\"Flatten: second argument must be a non-negative integer\")
EndDefine;
-------------------------------
ERROR: Unknown operator $builtin.UnexistentFunc
CONTEXT: Function(\"$builtin.UnexistentFunc\")
-------------------------------
ERROR: Unknown package $unexistent/package
CONTEXT: Function(\"$unexistent/package.Func\")
-------------------------------
ERROR: Bad package name (required $) bad/pkg/name
CONTEXT: Function(\"bad/pkg/name.Func\")
-------------------------------
ERROR: Unknown operator UnexistentFunc
CONTEXT: Function(\"UnexistentFunc\")
-------------------------------
";
TSL.RegisterTest(Test);
