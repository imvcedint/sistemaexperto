-- $ 2001/04/10 $
-- Test Suite originally built from examples.coc from CoCoA manual 302b

Alias TSL := $ts/ts_lib;
TSL.Initialize();

-------------------------------
-- TEST 01 : Quick Tour - Expressions

Test := Record[Id := "examples_01",Descr := "Quick Tour - Expressions"];

Test.Input :="
Use S ::= QQ[t,x,y,z];
(x+3/2*y - t)^4;

(x^553432+y)^7;

I := Ideal( x,y,z,t)^3;
J := Ideal( x^2-y*z, x*y-z*t) + I;
J;

A ::= QQ[x,y];
Use A;
(x+1/2*y)^3;

2 + 2;

It;

It + 3;

It;

X := 5;
It;

Use S ::= QQ[x,y,z];
(x*y+y*z)^3;

F := Der(It, y);
F;

For N := 3 To 9 Step 2 Do
  T ::= ZZ/(N)[x,y]; --> define the ring T
  PrintLn Ring(T); --> display the ring T
  T :: (x+y)^7; --> evaluate the power in T
  PrintLn; --> start a new line
  PrintLn
EndFor;

";
Test.ExpectedOutput :=
"t^4 - 4t^3x + 6t^2x^2 - 4tx^3 + x^4 - 6t^3y + 18t^2xy - 18tx^2y + 6x^3y + 27/2t^2y^2 - 27txy^2 + 27/2x^2y^2 - 27/2ty^3 + 27/2xy^3 + 81/16y^4
-------------------------------
x^3874024 + 7x^3320592y + 21x^2767160y^2 + 35x^2213728y^3 + 35x^1660296y^4 + 21x^1106864y^5 + 7x^553432y^6 + y^7
-------------------------------
Ideal(x^2 - yz, xy - tz, x^3, x^2y, x^2z, tx^2, xy^2, xyz, txy, xz^2, txz, t^2x, y^3, y^2z, ty^2, yz^2, tyz, t^2y, z^3, tz^2, t^2z, t^3)
-------------------------------
x^3 + 3/2x^2y + 3/4xy^2 + 1/8y^3
-------------------------------
4
-------------------------------
4
-------------------------------
7
-------------------------------
7
-------------------------------
7
-------------------------------
x^3y^3 + 3x^2y^3z + 3xy^3z^2 + y^3z^3
-------------------------------
3x^3y^2 + 9x^2y^2z + 9xy^2z^2 + 3y^2z^3
-------------------------------
ZZ/(3)[x,y]
T :: x^7 + x^6y - x^4y^3 - x^3y^4 + xy^6 + y^7

ZZ/(5)[x,y]
T :: x^7 + 2x^6y + x^5y^2 + x^2y^5 + 2xy^6 + y^7

ZZ/(7)[x,y]
T :: x^7 + y^7

-- WARNING: Coeffs are not in a field
-- GBasis-related computations could fail to terminate or be wrong
ZZ/(9)[x,y]
T :: x^7 - 2x^6y + 3x^5y^2 - x^4y^3 - x^3y^4 + 3x^2y^5 - 2xy^6 + y^7


-------------------------------
";
TSL.RegisterTest(Test);

--------------------------
-- TEST 02 : Quick Tour - GBasis

Test := Record[Id := "examples_02",Descr :="Quick Tour - GBasis"];
Test.Input :="
Use S ::= QQ[x,y],Lex;
Set Indentation;
I := Ideal(x^7-x-1, x^3*y-4*x+1);
GBasis(I);
GBasis(Module(I));

Use S ::= QQ[x,y,z,t];
Set Indentation;
I := Ideal(t^31+t^6-x, t^8-y, t^10-z );
Elim(t, I);

Use S ::= QQ[x,y,z,t],Elim(t);
Set Indentation;
I := Ideal(t^31+t^6-x, t^8-y, t^10-z );
GBasis(I);

Unset Indentation;
Ord();

T ::= ZZ/(5)[t,a,b,c],Lex;
Use S ::= QQ[t,x,y,z];
J := Ideal(t^31-t^6-x, t^8-y, t^10-z);
F := y^5-z^4;
If NF(F, Elim(t,J)) = 0 Then
  For I := 3 To 9 Step 2 Do
    PrintLn;
    T :: (a+t)^(I+1);
  EndFor
EndIf;

";
Test.ExpectedOutput :=
"[
  1602818757152090759440/34524608236181199361x - 4457540/5875764481y^7 - 47746460716124220/34524608236181199361y^6 + 890175715271333840/34524608236181199361y^5 - 3541992534667352220/34524608236181199361y^4 - 55943894513139464160/34524608236181199361y^3 - 56473654361333280980/34524608236181199361y^2 - 27971979712025453040/34524608236181199361y - 400704689288022689860/34524608236181199361,
  1/16384y^7 - 5/16384y^6 + 147/16384y^4 + 5/128y^3 - 31/16384y^2 + 17/128y - 20479/16384]
-------------------------------
[
  Vector(1602818757152090759440/34524608236181199361x - 4457540/5875764481y^7 - 47746460716124220/34524608236181199361y^6 + 890175715271333840/34524608236181199361y^5 - 3541992534667352220/34524608236181199361y^4 - 55943894513139464160/34524608236181199361y^3 - 56473654361333280980/34524608236181199361y^2 - 27971979712025453040/34524608236181199361y - 400704689288022689860/34524608236181199361),
  Vector(1/16384y^7 - 5/16384y^6 + 147/16384y^4 + 5/128y^3 - 31/16384y^2 + 17/128y - 20479/16384)]
-------------------------------
Ideal(
  y^5 - z^4,
  -y^4z^5 + y^4 - 2xy^2z + x^2z^2,
  -z^8 - 2xy^3 + x^2yz + z^3,
  2xy^4z^4 + yz^7 + 3x^2y^2 - 2x^3z - yz^2,
  -y^2z^6 - 1/2xz^7 + 1/2x^3y + y^2z - 3/2xz^2,
  -1/3x^2y^4z^3 - y^3z^5 - 2/3xyz^6 + 1/3x^4 + y^3 - 4/3xyz)
-------------------------------
[
  -yt^2 + z,
  y^5 - z^4,
  -t^6 - z^3t + x,
  -z^4t - y^2 + xz,
  -y^2t + xzt - y^4z,
  y^4z^5 - y^4 + 2xy^2z - x^2z^2,
  z^8 + 2xy^3 - x^2yz - z^3,
  2xy^4z^4 + yz^7 + 3x^2y^2 - 2x^3z - yz^2,
  xt^2 - x^2z^2t + xy^4z^2 + yz^5 - y,
  zt^2 + x^3yzt + xz^3t - y^4z^3 - xy^2z^4 - x^2z^5 - x^2,
  2xyz^3t - z^7 - x^2y + z^2,
  1/2x^2yt - 1/2z^2t - 1/2y^2z^3 - 1/2xz^4,
  -y^2z^6 - 1/2xz^7 + 1/2x^3y + y^2z - 3/2xz^2,
  3x^2z^3t - 2xy^4z^3 - yz^6 - x^3 + yz,
  1/3x^3t - 1/3yzt - 1/3x^2y^4 - 1/3y^3z^2 - 1/3xyz^3,
  -1/3x^2y^4z^3 - y^3z^5 - 2/3xyz^6 + 1/3x^4 + y^3 - 4/3xyz]
-------------------------------
Mat([
  [0, 0, 0, 1],
  [1, 1, 1, 0],
  [0, 0, -1, 0],
  [0, -1, 0, 0]
])
-------------------------------

T :: t^4 - t^3a + t^2a^2 - ta^3 + a^4
T :: t^6 + t^5a + ta^5 + a^6
T :: t^8 - 2t^7a - 2t^6a^2 + t^5a^3 + t^3a^5 - 2t^2a^6 - 2ta^7 + a^8
T :: t^10 + 2t^5a^5 + a^10
-------------------------------
";

TSL.RegisterTest(Test);

-------------------------------
-- TEST 03 : Quick Tour - Programming

Test := Record[Id := "examples_03", Descr := "Quick Tour - Programming"];
Test.Input := "

Define Prime(X)
  If Type(X) <> INT Then Return Error(\"Expected INT\") EndIf;
  I := 2;
  While I*I <= X  Do
    If Div(X,I)*I = X Then Return False EndIf;
    I := I + 1
  EndWhile;
  Return True
EndDefine;

For I := 3 To 10 Do
  If Prime(I) Then
    P ::= ZZ/(I)[x,y,z,w];
    PrintLn \"I = \", I;
    PrintLn P :: (x+y+z+w)^I;
  EndIf
EndFor;

-- A recursive implementation of the factorial.
Define RecFact(X)
  If X <= 2 Then Return X Else Return X*RecFact(X-1) EndIf
EndDefine;

RecFact(29);
RecFact(RecFact(4));
";
Test.ExpectedOutput :=
"I = 3
P :: x^3 + y^3 + z^3 + w^3
I = 5
P :: x^5 + y^5 + z^5 + w^5
I = 7
P :: x^7 + y^7 + z^7 + w^7

-------------------------------
8841761993739701954543616000000
-------------------------------
620448401733239439360000
-------------------------------
";

TSL.RegisterTest(Test);

-------------------------------
-- TEST 04 : Quick Tour - Rational_Normal_Curve_Ideal

Test := Record[Id := "examples_04",Descr :="Quick Tour - Rational_Normal_Curve_Ideal"];

Test.Input := "
Use S ::= QQ[x[1..5]];
Indets();

Print x[3+Deg(x[1]^2)];

Define Rational_Normal_Curve_Ideal(N)
  -- first define the 2xN matrix whose 2x2 minors generate the ideal
  M := NewMat(2,N);
  For C := 0 To N-1 Do
    M[1,C+1] := x[C];
    M[2,C+1] := x[C+1]
  EndFor;
  -- then construct the generators of the ideal
  L := [];
  For C1 := 1 To N-1 Do
    For C2 := C1+1 To N Do
      P := M[1,C1]*M[2,C2] - M[2,C1]*M[1,C2];
      -- determinant for columns C1,C2
      Append(L,P)
    EndFor
  EndFor;
  Return Ideal(L)
EndDefine;

For N := 3 To 5 Do
  T ::= QQ[x[0..N]],Lex;
  PrintLn;
  PrintLn \"degree \", N;
  -- assign Rational_Normal_Curve_Ideal(N) to the variable I in the ring S
  Using T Do
    I := Rational_Normal_Curve_Ideal(N);
    -- print out the variable I of the ring T
    I;
    PrintLn;
    Print \"Poincare series: \";
    M := T/I;
    Poincare(M);
    PrintLn
  EndUsing
EndFor;

";
Test.ExpectedOutput :=
"[x[1], x[2], x[3], x[4], x[5]]
-------------------------------
x[5]
-------------------------------

degree 3
Ideal(x[0]x[2] - x[1]^2, x[0]x[3] - x[1]x[2], x[1]x[3] - x[2]^2)
Poincare series: (1 + 2x[0]) / (1-x[0])^2

degree 4
Ideal(x[0]x[2] - x[1]^2, x[0]x[3] - x[1]x[2], x[0]x[4] - x[1]x[3], x[1]x[3] - x[2]^2, x[1]x[4] - x[2]x[3], x[2]x[4] - x[3]^2)
Poincare series: (1 + 3x[0]) / (1-x[0])^2

degree 5
Ideal(x[0]x[2] - x[1]^2, x[0]x[3] - x[1]x[2], x[0]x[4] - x[1]x[3], x[0]x[5] - x[1]x[4], x[1]x[3] - x[2]^2, x[1]x[4] - x[2]x[3], x[1]x[5] - x[2]x[4], x[2]x[4] - x[3]^2, x[2]x[5] - x[3]x[4], x[3]x[5] - x[4]^2)
Poincare series: (1 + 4x[0]) / (1-x[0])^2

-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 05 : Quick Tour - Det_SubAlgebra

Test := Record[Id := "examples_05",Descr :="Quick Tour - Det_SubAlgebra"];

Test.Input := "
Define Det_SubAlgebra(N)
  L := [];
  For C1 := 1 To N-1 Do
    For C2 := C1+1 To N Do
      P := y[C1,C2]-(x[1,C1]*x[2,C2] - x[2,C1]*x[1,C2]);
      Append(L,P)
    EndFor
  EndFor;
  Return Ideal(L)
EndDefine;

Define Det_SubAlgebra_Print(N)
  J := Det_SubAlgebra(N);
  PrintLn;
  PrintLn \"N = \",N;
  PrintLn J;
  PrintLn \"Sub-algebra equations:\";
  I := Elim(AllIndetsCalled(\"x\"),J);
  If I = Ideal(0) Then PrintLn [0];
  Else PrintLn Monic(Gens(Elim(AllIndetsCalled(\"x\"),J)));
  EndIf;
EndDefine;

Set Indentation;
For N := 3 To 5 Do
  S ::= ZZ/(32003)[y[1..(N-1),2..N],x[1..2,1..N]];
  S :: Det_SubAlgebra_Print(N);
EndFor;
";
Test.ExpectedOutput :=
"
N = 3
Ideal(
  x[1,2]x[2,1] - x[1,1]x[2,2] + y[1,2],
  x[1,3]x[2,1] - x[1,1]x[2,3] + y[1,3],
  x[1,3]x[2,2] - x[1,2]x[2,3] + y[2,3])
Sub-algebra equations:
[
  0]

N = 4
Ideal(
  x[1,2]x[2,1] - x[1,1]x[2,2] + y[1,2],
  x[1,3]x[2,1] - x[1,1]x[2,3] + y[1,3],
  x[1,4]x[2,1] - x[1,1]x[2,4] + y[1,4],
  x[1,3]x[2,2] - x[1,2]x[2,3] + y[2,3],
  x[1,4]x[2,2] - x[1,2]x[2,4] + y[2,4],
  x[1,4]x[2,3] - x[1,3]x[2,4] + y[3,4])
Sub-algebra equations:
[
  y[1,4]y[2,3] - y[1,3]y[2,4] + y[1,2]y[3,4]]

N = 5
Ideal(
  x[1,2]x[2,1] - x[1,1]x[2,2] + y[1,2],
  x[1,3]x[2,1] - x[1,1]x[2,3] + y[1,3],
  x[1,4]x[2,1] - x[1,1]x[2,4] + y[1,4],
  x[1,5]x[2,1] - x[1,1]x[2,5] + y[1,5],
  x[1,3]x[2,2] - x[1,2]x[2,3] + y[2,3],
  x[1,4]x[2,2] - x[1,2]x[2,4] + y[2,4],
  x[1,5]x[2,2] - x[1,2]x[2,5] + y[2,5],
  x[1,4]x[2,3] - x[1,3]x[2,4] + y[3,4],
  x[1,5]x[2,3] - x[1,3]x[2,5] + y[3,5],
  x[1,5]x[2,4] - x[1,4]x[2,5] + y[4,5])
Sub-algebra equations:
[
  y[2,5]y[3,4] - y[2,4]y[3,5] + y[2,3]y[4,5],
  y[1,5]y[3,4] - y[1,4]y[3,5] + y[1,3]y[4,5],
  y[1,5]y[2,4] - y[1,4]y[2,5] + y[1,2]y[4,5],
  y[1,5]y[2,3] - y[1,3]y[2,5] + y[1,2]y[3,5],
  y[1,4]y[2,3] - y[1,3]y[2,4] + y[1,2]y[3,4]]

-------------------------------
";
TSL.RegisterTest(Test);

--------------------------------
-- TEST 06 : Quick Tour - MyIdealOfPoints

Test := Record[Id := "examples_06", Descr :="Quick Tour - MyIdealOfPoints"];
Test.Comment := "DEBUG: estendere tipo di dato ERROR";
Test.Input := "

Define IOP_Error(P) Return Error(\"Not a projective point\",P) EndDefine;

Define MyIdealOfPoint(P)
  -- first check if the number of the coordinates is correct
  If Len(P) <> NumIndets() Then
    Return Error(\"Wrong number of indeterminates\")
  EndIf;
  -- then compute the index of the first non zero component of P
  F := 1;
  While F <= NumIndets() And P[F]=0 Do
    F := F + 1
  EndWhile;
  -- if all components of P are 0 then reject P
  If F>Len(P) Then Return IOP_Error(P) EndIf;
  -- otherwise return the ideal of P
  L := [ Indet(K)*P[F] - Indet(F)*P[K] | K In 1..NumIndets() And K <> F ];
  -- Indet(N) gives the N-th indeterminate of the current ring
  Return Ideal(L)
EndDefine;

Define MyIdealOfPoints(L)
  -- Ideal of N points in the array L
  I := MyIdealOfPoint(Head(L));
  Foreach P In Tail(L) Do
    Catch J := MyIdealOfPoint(P) In E EndCatch;
    If Type(E) = ERROR Then
      PrintLn Dashes();
      PrintLn \"MyIdealOfPoints() terminated with error\";
      Return IOP_Error(P)
    EndIf;
    I := Intersection(I,J)
  EndForeach;
  Return I
EndDefine;

Use S ::= QQ[x,y,z,w];
A := [
  [1,0,1,0],
  [0,1,1,0],
  [1,1,1,1],
  [0,0,0,0],
  [0,1,0,3]
];

Unset Indentation;

Ideals := [
Ideal(y, -x + z, w),
Ideal(w, x + y - z, y^2 - y*z),
Ideal(x + y - z - w, z*w - w^2, y*w - w^2, y^2 - y*z)
];

For I := 1 To Len(A) Do
  MyIdealOfPoints(First(A,I)) = Ideals[I];
  -- First(A,I) gives the list of the first I elements of the list A
  PrintLn;
EndFor;

Use S ::= ZZ/(32003)[x[1..4]],Lex;
Set Indentation;
F := DensePoly(2);
-- DensePoly(n) returns the polynomial which is the sum of all the
-- terms of degree n of the current ring
L := [ Randomized(F) | I In 1..3 ];
-- Randomized(F) randomizes the coefficients of F
LT(Ideal(L));
";
Test.ExpectedOutput :=
"True
True
True
-------------------------------
MyIdealOfPoints() terminated with error
ERROR: Not a projective point [0, 0, 0, 0]
CONTEXT: Return(Error(\"Not a projective point\", P))
-------------------------------
Ideal(
  x[1]^2,
  x[1]x[2],
  x[1]x[3],
  x[2]^3,
  x[1]x[4]^2,
  x[2]^2x[3],
  x[2]^2x[4]^2,
  x[2]x[3]^3,
  x[2]x[3]^2x[4]^2,
  x[2]x[3]x[4]^4,
  x[2]x[4]^6,
  x[3]^8)
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 07 : Ring

Test := Record[Id := "examples_07", Descr :="Ring"];
Test.Input := "
M:=Mat([[1,1,0,0],[0,-1,0,0],[0,0,1,1],[0,0,0,-1]]);
S ::= QQ[x,y,z,t], Ord(M);
Use S ::= QQ[x,y],ToPos;
LT(Vector(x,y^2));

Use S ::= QQ[x,y],PosTo;
LT(Vector(x,y^2));
";
Test.ExpectedOutput :=
"Vector(0, y^2)
-------------------------------
Vector(x, 0)
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 08 : Language Elements

Test := Record[Id := "examples_08", Descr :="Language Elements"];
Test.Input := "
// This is a line coment
Print 1+1; -- a command followed by a comment
/* example of /* nested */ comment */

Use S ::= QQ[x,y,z,t[1..2]];
x^2*y + 3*x*y*z;
t[1]+2*t[2]^3;

Use S ::= QQ[x,y];
Ideal(x,y^2,2+x*y^2);

Use S ::= QQ[x,y];
Vector(x,y^2,2+x*y^2);

Use S ::= QQ[x,y];
Module([[x,y^2,2+x^2*y],[x,0,y]]);

Use S ::= QQ[x,y];
x/(x+y);
Type((x^2-y^2)/(x+y));

Use S ::= QQ[x,y];
[34*x+y^2,\"string\",\"another\",[],[True,False]];
Shape(It);

Use S ::= QQ[x,y];
Record[I := Ideal(x,y), M := Module([[1,0],[0,1]])];
Record[];
Record[Birth := 1965, Name := \"Antonio\" ];

Use S ::= QQ[x,y,z];
Mat([[x,y,x*y^2],[y,z^2,2+x]]);

Use QQ[x,y];
Use ZZ/(32003)[x[1..5],y[2..5,1..4]];
Indets();
";
Test.ExpectedOutput :=
"2
-------------------------------
x^2y + 3xyz
-------------------------------
2t[2]^3 + t[1]
-------------------------------
Ideal(x, y^2, xy^2 + 2)
-------------------------------
Vector(x, y^2, xy^2 + 2)
-------------------------------
Module([[x, y^2, x^2y + 2], [x, 0, y]])
-------------------------------
x/(x + y)
-------------------------------
POLY
-------------------------------
[y^2 + 34x, \"string\", \"another\", [ ], [True, False]]
-------------------------------
[POLY, STRING, STRING, [ ], [BOOL, BOOL]]
-------------------------------
Record[I := Ideal(x, y), M := Module([[1, 0], [0, 1]])]
-------------------------------
Record[]
-------------------------------
Record[Birth := 1965, Name := \"Antonio\"]
-------------------------------
Mat([
  [x, y, xy^2],
  [y, z^2, x + 2]
])
-------------------------------
[x[1], x[2], x[3], x[4], x[5], y[2,1], y[2,2], y[2,3], y[2,4], y[3,1], y[3,2], y[3,3], y[3,4], y[4,1], y[4,2], y[4,3], y[4,4], y[5,1], y[5,2], y[5,3], y[5,4]]
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 09 : Expressions

Test := Record[Id := "examples_09", Descr :="Expressions"];
Test.Input := "
[ X^2 | X In [1,2,3] And X > 1 ];
[ X In [1,2,3] | X > 1 ];
[ X^2 | X In [1,2,3] ];
[ X In [1,2] >< [2,3,4] | X[1]+X[2]=4];
2 .. 10;
Use S ::= QQ[a[1..5]];
a[2]..a[5];
";
Test.ExpectedOutput :=
"[4, 9]
-------------------------------
[2, 3]
-------------------------------
[1, 4, 9]
-------------------------------
[[1, 3], [2, 2]]
-------------------------------
[2, 3, 4, 5, 6, 7, 8, 9, 10]
-------------------------------
[a[2], a[3], a[4], a[5]]
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 10 : Operators

Test := Record[Id := "examples_10", Descr :="Operators"];
Test.Input := "
[1,2,3] >< [4,5,6];
[1,2,3] >< [4,5,6] >< [7,8];
";
Test.ExpectedOutput :=
"[[1, 4], [1, 5], [1, 6], [2, 4], [2, 5], [2, 6], [3, 4], [3, 5], [3, 6]]
-------------------------------
[[1, 4, 7], [1, 4, 8], [1, 5, 7], [1, 5, 8], [1, 6, 7], [1, 6, 8], [2, 4, 7], [2, 4, 8], [2, 5, 7], [2, 5, 8], [2, 6, 7], [2, 6, 8], [3, 4, 7], [3, 4, 8], [3, 5, 7], [3, 5, 8], [3, 6, 7], [3, 6, 8]]
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 11 : Simple Commands

Test := Record[Id := "examples_11", Descr :="Simple Commands"];
Test.Input := "
S ::= QQ[x,y,z,t], Weights( 1, 1, 2, 2 );

Use QQ[x,y,z];
Use ZZ/(3)[x,y];

--Clear;
Use S ::= QQ[t,x,y,z];
X := 5;
Y := 8;
A := X+Y;
A := A + 1;
--Describe Memory();

L := [1,2,3];
L[2] := L[3];
L;

P := Record[Pol := x*z];
P.Degree := Deg(P.Pol);
P;

Use S ::= ZZ/(32003)[t,x,y,z];
Print Ring(S);

I := Ideal(t^3-x,t^4-y);
G := SyzOfGens(I);
Print I;

Set Indentation;
Describe I;

//obsolescent Clear;
P := 5*y;
I := Ideal(x+P,y+P);
--Describe Memory();
//obsolescent Delete P;
--Describe Memory();

Panels();

Panel(GENERAL);

Help(\"Help\");

Help(\"He\"+\"lp\");
";
If DebugVersion() Then

Test.ExpectedOutput :=
 "[1, 3, 3]
-------------------------------
Record[Degree := 2, Pol := xz]
-------------------------------
ZZ/(32003)[t,x,y,z]
-------------------------------
Ideal(t^3 - x, t^4 - y)
-------------------------------
Record[
  Type := IDEAL,
  Ring := ZZ/(32003)[t,x,y,z],
  RC := 2,
  Value := Record[
    Gens := [
      t^3 - x,
      t^4 - y],
    SyzOfGens := Module([
      [-t^4 + y, t^3 - x],
      [t^4x - xy, -t^3x + x^2]])]]
-------------------------------
[
  \"DEBUG\",
  \"GENERAL\",
  \"GROEBNER\",
  \"MONITOR (ONLY DEBUG MODE, OBSOLETE)\"]
-------------------------------

Echo............... : False
Timer.............. : False
Memory............. : False
Trace.............. : False
Indentation........ : True
TraceSources....... : False
SuppressWarnings... : False
ComputationStack... : False
NoErrorContext..... : False
-------------------------------
Type Help <Op> to get help on operator <Op>
-------------------------------
Type Help <Op> to get help on operator <Op>
-------------------------------
"

Else

Test.ExpectedOutput :=
"[1, 3, 3]
-------------------------------
Record[Degree := 2, Pol := xz]
-------------------------------
ZZ/(32003)[t,x,y,z]
-------------------------------
Ideal(t^3 - x, t^4 - y)
-------------------------------
Record[
  Type := IDEAL,
  Value := Record[
    Gens := [
      t^3 - x,
      t^4 - y],
    SyzOfGens := Module([
      [-t^4 + y, t^3 - x],
      [t^4x - xy, -t^3x + x^2]])]]
-------------------------------
[
  \"GENERAL\",
  \"GROEBNER\"]
-------------------------------

Echo............... : False
Timer.............. : False
Trace.............. : False
Indentation........ : True
TraceSources....... : False
SuppressWarnings... : False
ComputationStack... : False
NoErrorContext..... : False
-------------------------------
Type Help <Op> to get help on operator <Op>
-------------------------------
Type Help <Op> to get help on operator <Op>
-------------------------------
";
/* obsolescent Clear & Delete
 "------------[Memory]-----------
A = 14
X = 5
Y = 8
-------------------------------
[1, 3, 3]
-------------------------------
Record[Degree := 2, Pol := xz]
-------------------------------
ZZ/(32003)[t,x,y,z]
-------------------------------
Ideal(t^3 - x, t^4 - y)
-------------------------------
Record[
  Type := IDEAL,
  Ring := ZZ/(32003)[t,x,y,z],
  RC := 2,
  Value := Record[
    Gens := [
      t^3 - x,
      t^4 - y],
    SyzOfGens := Module([
      [-t^4 + y, t^3 - x],
      [t^4x - xy, -t^3x + x^2]])]]
-------------------------------
------------[Memory]-----------
I = Ideal(
  x + 5y,
  6y)
P = 5y
-------------------------------
------------[Memory]-----------
I = Ideal(
  x + 5y,
  6y)
-------------------------------
[
  \"DEBUG\",
  \"GENERAL\",
  \"GROEBNER\",
  \"MONITOR (ONLY DEBUG MODE, OBSOLETE)\"]
-------------------------------

Echo............... : False
Timer.............. : False
Memory............. : False
Trace.............. : False
Indentation........ : True
TraceSources....... : False
SuppressWarnings... : False
ComputationStack... : False
NoErrorContext..... : False
-------------------------------
Type Help <Op> to get help on operator <Op>
-------------------------------
Type Help <Op> to get help on operator <Op>
-------------------------------
"

Else

Test.ExpectedOutput :=
"------------[Memory]-----------
A = 14
X = 5
Y = 8
-------------------------------
[1, 3, 3]
-------------------------------
Record[Degree := 2, Pol := xz]
-------------------------------
ZZ/(32003)[t,x,y,z]
-------------------------------
Ideal(t^3 - x, t^4 - y)
-------------------------------
Record[
  Type := IDEAL,
  Value := Record[
    Gens := [
      t^3 - x,
      t^4 - y],
    SyzOfGens := Module([
      [-t^4 + y, t^3 - x],
      [t^4x - xy, -t^3x + x^2]])]]
-------------------------------
------------[Memory]-----------
I = Ideal(
  x + 5y,
  6y)
P = 5y
-------------------------------
------------[Memory]-----------
I = Ideal(
  x + 5y,
  6y)
-------------------------------
[
  \"GENERAL\",
  \"GROEBNER\"]
-------------------------------

Echo............... : False
Timer.............. : False
Trace.............. : False
Indentation........ : True
TraceSources....... : False
SuppressWarnings... : False
ComputationStack... : False
NoErrorContext..... : False
-------------------------------
Type Help <Op> to get help on operator <Op>
-------------------------------
Type Help <Op> to get help on operator <Op>
-------------------------------
";
*/
EndIf;
TSL.RegisterTest(Test);

-------------------------------
-- TEST 12 : Structured Commands

Test := Record[Id := "examples_12", Descr :="Structured Commands"];
Test.Input := "

Print \"Hi \"; Print \"World\";

Block Print \"Hi \"; Print \"World\" EndBlock;

A := 5; B := 8;
If A > B Then Print \"A>B\" Else Print \"A<=B\" EndIf;

B := 3;
If A > B Then Print \"A>B\" Else Print \"A<=B\" EndIf;

B := 5;

If A > B Then Print \"A>B\" Elif A=B Then Print \"A=B\" Else Print \"A<B\" EndIf;

For N := 5 To 15 Do Print N^2, \", \" EndFor;

For N := 1 To 11 Step 3 Do Print N, \", \" EndFor;

For N := 11 To 8 Step -1 Do Print N, \", \" EndFor;

For I := 1 To 5 Do
  For J := 1 To 3 Do
    Print I*J, \",\"
  EndFor;
  Print \" \"
EndFor;

For I := 4 To 2 Do Print I EndFor; --> no output for this command

Use S ::= QQ[x,y];

Foreach X In [10,x+y,Ideal(x,y)] Do
  PrintLn X^3
EndForeach;
";
Test.ExpectedOutput :=
"Hi 
-------------------------------
World
-------------------------------
Hi World
-------------------------------
A<=B
-------------------------------
A>B
-------------------------------
A=B
-------------------------------
25, 36, 49, 64, 81, 100, 121, 144, 169, 196, 225, 
-------------------------------
1, 4, 7, 10, 
-------------------------------
11, 10, 9, 8, 
-------------------------------
1,2,3, 2,4,6, 3,6,9, 4,8,12, 5,10,15, 
-------------------------------
1000
x^3 + 3x^2y + 3xy^2 + y^3
Ideal(x^3, x^2y, xy^2, y^3)

-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 13 : Programming

Test := Record[Id := "examples_13", Descr :="Programming"];
Test.Input := "
Define SquareIsIn(X,L)
  Foreach Y In L Do
    If Y=X^2 Then Return True EndIf
  EndForeach;
  Return False;
EndDefine;
SquareIsIn(4,[3,16,35]);

SquareIsIn(3,[3,16,35]);

--- Behaviour of CoCoA 4.1 is now changed, so this
--- example no longer works as it used to...
--Define Assign(Var X, Y)
--  X := Y
--EndDefine;
--
--Assign(B,3);
--B;

-- Reverse(Var L) reverses the list L (changing the value of L)
Define Reverse(Var L)
  N := Len(L);
  For I := 1 To Div(Len(L),2) Do
    C := L[I];
    L[I] := L[N-I+1];
    L[N-I+1] := C
  EndFor
EndDefine;

L := [1,2,3];
Reverse(L);
L;

Define Max(...)
  If Len(ARGV) = 0 Then ERR.BAD_PARAMS EndIf;
  If Len(ARGV) = 1 And Type(ARGV[1])=LIST Then
    L := ARGV[1]
  Else
    L := ARGV
  EndIf;
  M := Head(L);
  Foreach X In Tail(L) Do
    If X > M Then M := X EndIf
  EndForeach;
  Return M
EndDefine;

Max(3);

Max(2,3);

Max([2,3]);

Max();

Define Reversed(L) Return [ L[Len(L)-I+1] | I In 1..Len(L) ]; EndDefine;
Reversed([1,2,3]);

For X := 1 To 10 Do
  If X > 5 Then Return \"Stop\" Else PrintLn X EndIf
EndFor;
";
Test.ExpectedOutput :=
"True
-------------------------------
False
-------------------------------
[3, 2, 1]
-------------------------------
3
-------------------------------
3
-------------------------------
3
-------------------------------
ERROR: Bad parameters
CONTEXT: ERR.BAD_PARAMS
-------------------------------
[3, 2, 1]
-------------------------------
1
2
3
4
5

-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 14 : Advanced

Test := Record[Id := "examples_14", Descr :="Advanced"];
Test.Input := "
Use S ::= QQ[t,x,y,z];
Var(\"2-minors\") := Ideal(Minors(2,Mat([[t,x,y],[x,y,z]])));
Var(\"2-minors\");

Catch 3/0 In E EndCatch;
E;

Catch 3 In E EndCatch;

E = Null;

Define Id(X) Return X; EndDefine;
Define Help_Id() Return \"Id is just the identity!\"; EndDefine;
Help(\"Id\");

Define Help_Advanced()
  PrintLn \"This is an advanced help\";
  PrintLn \"To evaluate the sum of one and one you have to type\";
  PrintLn \"1+1\";
  PrintLn \"You'll get the answer:\",1+1;
EndDefine;
Help(\"Advanced\");

Use S ::= QQ[x,y];

Rs := Res(S/Ideal(x^2,y^3));
Rs;

Describe Rs;

Tag(Rs);

Type(Rs);

Untagged(Rs);

N := Tagged(4,\"Dots\");
N;

Tag(N);

Type(N);

Define Print_Dots(X)
  For I := 1 To X Do
    Print \".\"
  EndFor;
EndDefine;
N;

Tagged(N+N,\"Dots\");

Untagged(It);

Define Describe_Dots(X)
  For I := 1 To X Do
    Print \"*\"
  EndFor;
EndDefine;
Describe N;

-- cleanup (per poter ripetere l'esperimento)
Define Print_Dots(X) Print Untagged(X) EndDefine;
";
Test.ExpectedOutput :=
"Ideal(-x^2 + ty, -xy + tz, -y^2 + xz)
-------------------------------
ERROR: Division by zero
CONTEXT: E
-------------------------------
3
-------------------------------
True
-------------------------------
Id is just the identity!
-------------------------------
This is an advanced help
To evaluate the sum of one and one you have to type
1+1
You'll get the answer:2

-------------------------------
0 --> S(-5) --> S(-2) + S(-3) --> S
-------------------------------
Mat([
  [x^2, y^3]
])
Mat([
  [y^3],
  [-x^2]
])

-------------------------------
$gb.Res
-------------------------------
TAGGED(\"$gb.Res\")
-------------------------------
[\"S\", [[Module([[x^2], [y^3]]), [1, 0]], [Module([[x^2], [y^3]]), [1, -2], [1, -3]], [Module([Shifts([x^2, y^3]), [y^3, -x^2]]), [1, -5]]]]
-------------------------------
Tagged(4, \"Dots\")
-------------------------------
Dots
-------------------------------
TAGGED(\"Dots\")
-------------------------------
....
-------------------------------
........
-------------------------------
8
-------------------------------
****
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 15 : Builtin

Test := Record[Id := "examples_15", Descr :="Builtin"];
Test.Input := "
S::= ZZ/(101)[a,b,c];
Use T::= QQ[t,x,y,z];
CurrentRing();

Ring(S);

RingEnv();

NumIndets();

Indets();

Indet(2);

Characteristic(S);

Characteristic();

Ord();

Ord(S);

--Clear;
Use S ::= QQ[x,y,z];
A := 7;
F := x-y;
--Memory();

Use S ::= QQ[x,y];
Deg(x*y^2);

Use S ::= QQ[x,y],Weights(2,3);
Deg(x*y^2);

Use S ::= QQ[x,y];
DensePoly(3);

Use S ::= QQ[x,y];
Der(x*y^2,x);

Use S ::= QQ[x,y,z];
NR(x^2-x*y-y*z, [x-y]); -- Used to be Mod, but now Mod is only for INTs

x*(x-y)-y*z;

Use S::= QQ[x,y,z];
LT(y^2-x*z);

Use S ::= QQ[x,y,z],Lex;
LT(y^2-x*z);

Use S ::= ZZ/(101)[x,y,z,w];
Deg(Randomized(DensePoly(2)));

Use S ::= QQ[p,q,x];
F := x^3+p*x-q; G := Der(F,x);
Sylvester(F, G,x); Resultant(F,G,x);
";
Test.ExpectedOutput :=
"QQ[t,x,y,z]
-------------------------------
ZZ/(101)[a,b,c]
-------------------------------
T
-------------------------------
4
-------------------------------
[t, x, y, z]
-------------------------------
x
-------------------------------
101
-------------------------------
0
-------------------------------
Mat([
  [1, 1, 1, 1],
  [0, 0, 0, -1],
  [0, 0, -1, 0],
  [0, -1, 0, 0]
])
-------------------------------
Mat([
  [1, 1, 1],
  [0, 0, -1],
  [0, -1, 0]
])
-------------------------------
3
-------------------------------
8
-------------------------------
x^3 + x^2y + xy^2 + y^3
-------------------------------
y^2
-------------------------------
-yz
-------------------------------
x^2 - xy - yz
-------------------------------
y^2
-------------------------------
xz
-------------------------------
2
-------------------------------
Mat([
  [1, 0, p, -q, 0],
  [0, 1, 0, p, -q],
  [3, 0, p, 0, 0],
  [0, 3, 0, p, 0],
  [0, 0, 3, 0, p]
])
-------------------------------
4p^3 + 27q^2
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 16 : Polynomials and Vectors

Test := Record[Id := "examples_16", Descr :="Polynomials and Vectors"];
Test.Input := "
Use S ::= QQ[x,y];
Poly(3);
Type(It);
Monomials(2*x+3*y);
Support(2*x+3*y);
Log(2*x);
LogToTerm([1,0]);
LC(2*x+3*y);
CoeffOfTerm(x,2*x+3*y);

Use S ::= QQ[x,y];
Der(x*y/(x+y),x);

Use S ::= QQ[x,y];
V:= Vector(0,x,y^2);
LT(V);

Use S ::= QQ[x,y], PosTo;
V:= Vector(0,x,y^2);
LT(V);
";
Test.ExpectedOutput :=
"3
-------------------------------
POLY
-------------------------------
[2x, 3y]
-------------------------------
[x, y]
-------------------------------
[1, 0]
-------------------------------
x
-------------------------------
2
-------------------------------
2
-------------------------------
y^2/(x^2 + 2xy + y^2)
-------------------------------
Vector(0, 0, y^2)
-------------------------------
Vector(0, x, 0)
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 17 : Ideals and Lists

Test := Record[Id := "examples_17", Descr :="Ideals and Lists"];
Test.Input := "
Use S ::= QQ[t,x,y,z];
I := Ideal(x,y);
Gens(I);

Gens(I^2);

Use S ::= QQ[t,x,y,z];
I := Ideal(t^3-x, t^4-y, t^5-z);
J := Ideal(Interreduced(I.Gens));
J; I;

Use S ::= QQ[x,y,z];
Jacobian([x^2+y*z,x*y^2-z^4]);

Concat([1,2],[3,4]);

Concat([1,2,3],[4,5],[],[6]);

L:=[x,y,x,x,z]; L;
S:=MakeSet(L); S;

F := x^3+x*y^2+y^3;
G := x^2+z^2;
H := (x+y)*(x^2-z^2);
Interreduced([F,G,H]);

Use S ::= QQ[t,x,y,z];
F := x^2-y^2;
G := (x+y)^3;
GCD(F,G);
";
Test.ExpectedOutput :=
"[x, y]
-------------------------------
[x^2, xy, y^2]
-------------------------------
Ideal(t^3 - x, tx - y, ty - z)
-------------------------------
Ideal(t^3 - x, t^4 - y, t^5 - z)
-------------------------------
Mat([
  [2x, z, y],
  [y^2, 2xy, -4z^3]
])
-------------------------------
[1, 2, 3, 4]
-------------------------------
[1, 2, 3, 4, 5, 6]
-------------------------------
[x, y, x, x, z]
-------------------------------
[x, y, z]
-------------------------------
[x^2 + z^2, -2xz^2 - 2yz^2, xy^2 + y^3 + yz^2]
-------------------------------
x + y
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 18 : Buchberger

Test := Record[Id := "examples_18", Descr :="Buchberger"];
Test.Input := "
Use S ::= QQ[t,x,y,z];
Set Indentation;
Elim(t, Ideal(t^15+t^6+t-x, t^5-y, t^3-z));

Use S ::= QQ[t,s,x,y,z,w];
Elim(t..s, Ideal(x-t^4, y-t^3*s, z-t*s^3, w-s^4));

Use S ::= QQ[t[1..2],x[1..4]];
I := Ideal(x[1]-t[1]^4, x[2]-t[1]^3*t[2], x[3]-t[1]*t[2]^3, x[4]-t[2]^4);
Elim(t[1]..t[2],I);

Use S ::= ZZ/(101)[x[1..5],y[1..6]],Weights(1,1,1,1,1,2,2,2,2,2,2);
I := Ideal(y[1]-(x[2]^2 - x[1]*x[3]), y[2]-(x[2]*x[3] - x[1]*x[4]),
         y[3]-(x[2]*x[4] - x[1]*x[5]), y[4]-(x[3]^2 - x[2]*x[4]),
         y[5]-(x[3]*x[4] - x[2]*x[5]), y[6]-(x[4]^2 - x[3]*x[5]));
I.PSeries := Poincare(S/I);
Elim(x[1]..x[5],I);

Use S ::= QQ[x,y,z];
J := Ideal(x, y, z^2);
Res(S/J);

Unset Indentation;
Describe Res(S/J);
Set Indentation;

Use S ::= QQ[t,x,y];
I := Ideal(t^3-x,t^4-y);
GBasis(I);

Describe I;

Use S ::= ZZ/(101)[x[1..4],y[1..2]];
I := Ideal(y[1]^2 - x[2]^2 - x[1]*x[3], y[2]^2 - x[2]*x[3] - x[1]*x[4]);
I.PSeries := Poincare(S/I);
GBasis(I);

Use S ::= QQ[x,y,z,w];
Homogenized(w,x^3-y);

Homogenized(w,[x^3-y,x^4-z]);

I := Ideal(x^3-y,x^4-z);
Homogenized(w,I);

Use S ::= QQ[x,y,z,w],Weights([2,1,1,1]);
Homogenized(w,[x^3-y,x^4-z]);

Use S ::= QQ[t,x,y,z];

LT(Ideal(x^2-y, x^3-z));

[LT(X) | X In [x,Vector(z+y,x),Ideal(x^2-y, x^3-z)] ];

Use S::= QQ[x,y,z];
I := Ideal(x^3+x*y^2+y^3, x^2*y+z^2, x*y^3+y^4-x*z^2);
J := Minimalized(I);
J;

Use S ::= QQ[t,x,y,z];
Poincare(S/Ideal(x,y));

Use S ::= QQ[t,x,y,z], Weights(1,2,3,4);
Poincare(S/Ideal(x,y));

Use S ::= QQ[t,x,y,z], Weights(Mat([[1,1,1,1],[1,1,0,0]]));
Poincare(S/Ideal(x,y));

Use S ::= QQ[w,x,y,z];
I := Ideal(w*z-x*y, w*y^2-x^2*z, w^2*y-x^3, x*z^2-y^3);
SyzOfGens(I);

Use S::= QQ[w,x,y,z];
I := Ideal(w*z-x*y, w*y^2-x^2*z, w^2*y-x^3, x*z^2-y^3);
M := SyzOfGens(I);
MinGens(SyzOfGens(M));
";
If DebugVersion() Then

  Test.ExpectedOutput :=
"Ideal(
  -z^5 + y^3,
  -y^4 - yz^2 + xy - z^2,
  -xy^3z - y^2z^3 - xz^3 + x^2z - y^2 - y,
  -y^2z^4 - x^2y^3 - xy^2z^2 - yz^4 - x^2z^2 + x^3 - y^2z - 2yz - z,
  -y^3z^3 + xz^3 - y^3 - y^2)
-------------------------------
Ideal(
  -yz + xw,
  z^3 - yw^2,
  -xz^2 + y^2w,
  -y^3 + x^2z)
-------------------------------
Ideal(
  -x[2]x[3] + x[1]x[4],
  x[3]^3 - x[2]x[4]^2,
  -x[1]x[3]^2 + x[2]^2x[4],
  -x[2]^3 + x[1]^2x[3])
-------------------------------
Ideal(
  -y[3]y[4] + y[2]y[5] - y[1]y[6])
-------------------------------
0 --> S(-4) --> S(-2) + S(-3)^2 --> S(-1)^2 + S(-2) --> S
-------------------------------
Mat([
  [y, x, z^2]
])
Mat([
  [x, z^2, 0],
  [-y, 0, z^2],
  [0, -y, -x]
])
Mat([
  [z^2],
  [-x],
  [y]
])

-------------------------------
[
  t^3 - x,
  -tx + y,
  t^2y - x^2,
  x^3 - ty^2]
-------------------------------
Record[
  Type := IDEAL,
  Ring := QQ[t,x,y],
  RC := 2,
  Value := Record[
    Gens := [
      t^3 - x,
      t^4 - y],
    MRC := 1,
    GBasis := [
      t^3 - x,
      -tx + y,
      t^2y - x^2,
      x^3 - ty^2]]]
-------------------------------
[
  -x[2]x[3] - x[1]x[4] + y[2]^2,
  -x[2]^2 - x[1]x[3] + y[1]^2,
  -x[1]x[3]^2 + x[1]x[2]x[4] + x[3]y[1]^2 - x[2]y[2]^2]
-------------------------------
x^3 - yw^2
-------------------------------
[
  x^3 - yw^2,
  x^4 - zw^3]
-------------------------------
Ideal(
  x^3 - yw^2,
  -xy + zw,
  x^2z - y^2w,
  y^3 - xz^2)
-------------------------------
[
  x^3 - yw^5,
  x^4 - zw^7]
-------------------------------
Ideal(
  x^2,
  xy,
  y^2)
-------------------------------
[
  x,
  Vector(0, x),
  Ideal(
    x^2,
    xy,
    y^2)]
-------------------------------
Ideal(
  x^3 + xy^2 + y^3,
  x^2y + z^2)
-------------------------------
(1) / (1-t)^2
-------------------------------
---  Non-simplified HilbertPoincare' Series  ---
(1 - t^2 - t^3 + t^5) / ( (1-t) (1-t^2) (1-t^3) (1-t^4) )
-------------------------------
---  Non-simplified HilbertPoincare' Series  ---
(t^2x - tx - t + 1) / ( (1-tx) (1-tx) (1-t) (1-t) )
-------------------------------
Module([
  [-y^2, z, 0, x],
  [-xz, y, 0, w],
  [-wy, -x, z, 0],
  [-x^2, -w, y, 0]])
-------------------------------
[
  Vector(-w, x, y, -z)]
-------------------------------
";

Else

  Test.ExpectedOutput :=
"Ideal(
  -z^5 + y^3,
  -y^4 - yz^2 + xy - z^2,
  -xy^3z - y^2z^3 - xz^3 + x^2z - y^2 - y,
  -y^2z^4 - x^2y^3 - xy^2z^2 - yz^4 - x^2z^2 + x^3 - y^2z - 2yz - z,
  -y^3z^3 + xz^3 - y^3 - y^2)
-------------------------------
Ideal(
  -yz + xw,
  z^3 - yw^2,
  -xz^2 + y^2w,
  -y^3 + x^2z)
-------------------------------
Ideal(
  -x[2]x[3] + x[1]x[4],
  x[3]^3 - x[2]x[4]^2,
  -x[1]x[3]^2 + x[2]^2x[4],
  -x[2]^3 + x[1]^2x[3])
-------------------------------
Ideal(
  -y[3]y[4] + y[2]y[5] - y[1]y[6])
-------------------------------
0 --> S(-4) --> S(-2) + S(-3)^2 --> S(-1)^2 + S(-2) --> S
-------------------------------
Mat([
  [y, x, z^2]
])
Mat([
  [x, z^2, 0],
  [-y, 0, z^2],
  [0, -y, -x]
])
Mat([
  [z^2],
  [-x],
  [y]
])

-------------------------------
[
  t^3 - x,
  -tx + y,
  t^2y - x^2,
  x^3 - ty^2]
-------------------------------
Record[
  Type := IDEAL,
  Value := Record[
    Gens := [
      t^3 - x,
      t^4 - y],
    GBasis := [
      t^3 - x,
      -tx + y,
      t^2y - x^2,
      x^3 - ty^2]]]
-------------------------------
[
  -x[2]x[3] - x[1]x[4] + y[2]^2,
  -x[2]^2 - x[1]x[3] + y[1]^2,
  -x[1]x[3]^2 + x[1]x[2]x[4] + x[3]y[1]^2 - x[2]y[2]^2]
-------------------------------
x^3 - yw^2
-------------------------------
[
  x^3 - yw^2,
  x^4 - zw^3]
-------------------------------
Ideal(
  x^3 - yw^2,
  -xy + zw,
  x^2z - y^2w,
  y^3 - xz^2)
-------------------------------
[
  x^3 - yw^5,
  x^4 - zw^7]
-------------------------------
Ideal(
  x^2,
  xy,
  y^2)
-------------------------------
[
  x,
  Vector(0, x),
  Ideal(
    x^2,
    xy,
    y^2)]
-------------------------------
Ideal(
  x^3 + xy^2 + y^3,
  x^2y + z^2)
-------------------------------
(1) / (1-t)^2
-------------------------------
---  Non-simplified HilbertPoincare' Series  ---
(1 - t^2 - t^3 + t^5) / ( (1-t) (1-t^2) (1-t^3) (1-t^4) )
-------------------------------
---  Non-simplified HilbertPoincare' Series  ---
(t^2x - tx - t + 1) / ( (1-tx) (1-tx) (1-t) (1-t) )
-------------------------------
Module([
  [-y^2, z, 0, x],
  [-xz, y, 0, w],
  [-wy, -x, z, 0],
  [-x^2, -w, y, 0]])
-------------------------------
[
  Vector(-w, x, y, -z)]
-------------------------------
";

EndIf;
TSL.RegisterTest(Test);

-------------------------------
-- TEST 19 : Misc

Test := Record[Id := "examples_19", Descr :="Misc"];
Test.Input := "
Type(6/2);

Use S ::= QQ[t,x,y,z];
L := [x+y, Ideal(x,y,z), [1,x]];
Shape(L);

Types();

Starting(\"U\");

Use S ::= QQ[x,y];
F := x^3+3*x^2+y+3;
Subst(F, x, -2);

Use S ::= QQ[x,y,z,t];
F := x^5+y^4+z^3+t^2;
Subst(F, [[x,y],[y,x],[t,(z-t)^2]]);

Use S ::= QQ[x,y,z];
MySubst := [[x,y+z],[y,(x-z)^2]];
Subst(x*y, MySubst);
";
Test.ExpectedOutput :=
"INT
-------------------------------
[POLY, IDEAL, [INT, POLY]]
-------------------------------
[NULL, BOOL, STRING, TYPE, ERROR, RECORD, DEVICE, INT, RAT, ZMOD, POLY, RATFUN, VECTOR, IDEAL, MODULE, MAT, LIST, RING, TAGGED(\"\"), FUNCTION]
-------------------------------
[\"Untagged\", \"UVPoincareQuotient\", \"UpdatedRecord\", \"UnivariateIndetIndex\"]
-------------------------------
y + 7
-------------------------------
y^5 + x^4 + z^4 - 4z^3t + 6z^2t^2 - 4zt^3 + t^4 + z^3
-------------------------------
x^2y + x^2z - 2xyz - 2xz^2 + yz^2 + z^3
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 20 : Hilbert

Test := Record[Id := "examples_20", Descr :="Hilbert"];
Test.Input := "
Use S ::= QQ[t,x,y,z];
HVector(S/Ideal(x,y,z)^5);

Dim(S/Ideal(x,y,z)^5);

Multiplicity(S/Ideal(x,y,z)^5);

Use S ::= QQ[t,x,y,z];
P := HP.ToRatFun(Poincare(S/Ideal(x,y,z)^5));
Type(P);

P;

HP.PSeries(t^3+1,2);

Tag(It);

Use S ::= QQ[t,x,y,z];
Hilbert(S/Ideal(x,y,z)^5);

Hilbert(S/Ideal(x,y,z)^5, 16);

Use S ::= QQ[t,x,y,z];
BE := BinExp(13,4);
BE;

BinExp(13,4, 1,1);

EvalBinExp(BE, 1,1);
";
Test.ExpectedOutput :=
"[1, 3, 6, 10, 15]
-------------------------------
1
-------------------------------
35
-------------------------------
RATFUN
-------------------------------
(-15t^4 - 10t^3 - 6t^2 - 3t - 1)/(t - 1)
-------------------------------
(1 + t^3) / (1-t)^2
-------------------------------
$hp.PSeries
-------------------------------
H(0) = 1
H(1) = 4
H(2) = 10
H(3) = 20
H(t) = 35   for t >= 4
-------------------------------
35
-------------------------------
Bin(5,4) + Bin(4,3) + Bin(3,2) + Bin(1,1)
-------------------------------
16
-------------------------------
16
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 21 : Div


Test := Record[Id := "examples_21", Descr :="Div"];
Test.Input := "
Use S ::= QQ[x,y];
I:=Ideal(x^2 + y - 3, x*y^2 + 2*x, y^3);
ScalarProduct(I.Gens,GenRepr(1, I))=1;
";
Test.ExpectedOutput :=
"True
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 22 : Coefficients

Test := Record[Id := "examples_22", Descr :="Coefficients"];
Test.Input := "
Use QQ[x,y,z];
F := 3x^2y + 5y^3 - xy^5;
Coefficients(F) = [-1, 3, 5];
ScalarProduct(Coefficients(F), Support(F)) = F;

V := Vector(3x^2+y, x-5z^3);
Coefficients(V) = [-5, 3, 1, 1];
ScalarProduct(Coefficients(V), Support(V)) = V;

Coefficients(x^3z+xy+xz+y+2z, x) = [z, 0, y + z, y + 2z];

F := (1 + 2x + 3y^4 + 5z^6)^7;
Skeleton := [1, x^3, y^12, z^19, x^2*y^8*z^12];
Coefficients(F, Skeleton) = [1, 280, 945, 0, 567000];
";
Test.ExpectedOutput :=
"True
-------------------------------
True
-------------------------------
True
-------------------------------
True
-------------------------------
True
-------------------------------
True
-------------------------------
";
TSL.RegisterTest(Test);

-------------------------------
-- TEST 23 : Radical membership

Test := Record[Id := "examples_23", Descr :="Radical membership"];
Test.Input := "
  Use QQ[x,y,z];
  I := Ideal(x^6y^4, z);
  IsInRadical(xy, I);
  Not IsInRadical(Ideal(x,y), I);
  MinPowerInIdeal(xy, I) = 6;
";
Test.ExpectedOutput :=
"True
-------------------------------
True
-------------------------------
True
-------------------------------
";
TSL.RegisterTest(Test);
