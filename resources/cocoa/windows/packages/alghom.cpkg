-- $Id: alghom.cpkg,v 1.1 2009/06/04 15:20:11 bigatti Exp $
Package $alghom    -- K-algebra homomorphism

Alias Alg := $alghom;

  /*

  Save in lib/contrib/alghom.pkg
  Alias Alg := $contrib/alghom;
  Alg.Man();

  */

Define About()
  PrintLn "    KeyWords : algebra homomorphism, subalgebra";
  PrintLn "    Author   : A.M.Bigatti";
  PrintLn "    Version  : CoCoA-4.7.5";
  PrintLn "    Date     : 4 Jun 2009";
  PrintLn "    Version  : CoCoA-4.7";
  PrintLn "    Date     : 12 Feb 2009";
  PrintLn "    Version  : CoCoA-4.6";
  PrintLn "    Date     : 20 Dec 2005";
  PrintLn "    Version  : CoCoA3.7";
  PrintLn "    Date     : 15 June 1999";
EndDefine; -- About


------[   Manual   ]--------

Define Man()
  PrintLn "Suggested alias for this package:";
  PrintLn "    Alias Alg := $contrib/alghom;";
  PrintLn "";
  PrintLn "SYNTAX";
  PrintLn "    Alg.Map(RFrom: STRING, RTo: STRING, MapList:  LIST): TAGGED(\"Map\")";
  PrintLn "";
  PrintLn "    Alg.Ker(         Phi: TAGGED(\"Map\")): IDEAL";
  PrintLn "    Alg.IsInjective( Phi: TAGGED(\"Map\")): BOOL";
  PrintLn "    Alg.IsSurjective(Phi: TAGGED(\"Map\")): BOOL";
  PrintLn "";
  PrintLn "    Alg.IsInImage(   F: POLY, Phi: TAGGED(\"Map\")): BOOL";
  PrintLn "    Alg.PreImage(    F: POLY, Phi: TAGGED(\"Map\")): POLY";
  PrintLn "    Alg.Image(       F: POLY, Phi: TAGGED(\"Map\")): POLY";
  PrintLn "";
  PrintLn "    Alg.SubalgebraMap(L: LIST): TAGGED(\"Map\")";
  PrintLn "";
  PrintLn "    Alg.IsInSubalgebra(F: POLY, SA: TAGGED(\"Map\")): BOOL";
  PrintLn "    Alg.SubalgebraRepr(F: POLY, SA: TAGGED(\"Map\")): POLY";
  PrintLn "";
  PrintLn "DESCRIPTION";
  PrintLn "Let R and S be polynomial rings on the same coefficient field K.";
  PrintLn "Let Phi be a K-algebra homomorphism:  Phi: R --> S,  Phi(x_i) := F_i .";
  PrintLn "";
  PrintLn "    The function";
  PrintLn "            Alg.Map(\"R\", \"S\", [F[1],..., F[N]])";
  PrintLn "    returns the representation of Phi expected by the following functions:";
  PrintLn "";
  PrintLn "            Alg.Ker(Phi)           -->  Kernel of Phi (in R)";
  PrintLn "            Alg.IsInjective(Phi)   -->  is Phi injective?";
  PrintLn "            Alg.IsSurjective(Phi)  -->  is Phi surjective?";
  PrintLn "";
  PrintLn "            Alg.IsInImage(F, Phi)  -->  F (in S) is in Phi(R)?";
  PrintLn "            Alg.PreImage(F, Phi)   -->  G (in R) such that Phi(G) = F";
  PrintLn "            Alg.Image(F, Phi)      -->  Phi(F) (in S) [similar to \"Image\",";
  PrintLn "                                                 but only for polynomials]";
  PrintLn "";
  PrintLn "    The function";
  PrintLn "            Alg.SubalgebraMap([F[1],..., F[N]])";
  PrintLn "    returns the representation of a subalgebra expected by the following";
  PrintLn "    functions:";
  PrintLn "";
  PrintLn "        Alg.IsInSubalgebra(F, SA)  -->  F is in K[F[1],...,F[N]]?";
  PrintLn "        Alg.SubalgebraRepr(F, SA)  -->  G (in K[y[1],...,y[N]]) such that";
  PrintLn "                                        G(F[1],...,F[N]) = F";
  PrintLn "";
  PrintLn "";
  PrintLn ">EXAMPLE<";
  PrintLn "";
  PrintLn "    Rxyz ::= QQ[x,y,z];";
  PrintLn "    Rab  ::= QQ[a,b];";
  PrintLn "";
  PrintLn "    Use Rab;";
  PrintLn "    -- like RMap + the ring names";
  PrintLn "    Phi := Alg.Map(\"Rxyz\", \"Rab\", [a+1, ab+3, b^2]);";
  PrintLn "";
  PrintLn "    -- or, in any ring";
  PrintLn "    Use ZZ/(2)[w]; Phi := Alg.Map(\"Rxyz\", \"Rab\", Rab::[a+1, ab+3, b^2]);";
  PrintLn "";
  PrintLn "    Alg.Ker(Phi);";
  PrintLn "    Alg.IsInjective(Phi);";
  PrintLn "    Alg.IsSurjective(Phi);";
  PrintLn "";
  PrintLn "    Use Rab;";
  PrintLn "    Alg.IsInImage(a^2, Phi);";
  PrintLn "    Alg.PreImage(a^2, Phi);";
  PrintLn "    Alg.Image(Rxyz :: (x^2 - 2x + 1), Phi);";
  PrintLn "";
  PrintLn "    -- linear maps";
  PrintLn "    M := Mat([[1,2,3],[2,3,4],[1,0,1]]);";
  PrintLn "    Det(M);  -- invertible";
  PrintLn "";
  PrintLn "    Use R ::= QQ[x[1..3]];";
  PrintLn "    L := [ Sum([ Row[I]*x[I] | I In 1..Len(Row)]) | Row In M ];";
  PrintLn "    Psi := Alg.Map(\"R\", \"R\", L);  -- linear map defined by M";
  PrintLn "    Alg.IsInjective(Psi);";
  PrintLn "    Alg.IsSurjective(Psi);";
  PrintLn "";
  PrintLn "    Inv := [Alg.PreImage(x[I], Psi) | I In 1..3];";
  PrintLn "    MM := Mat([ [ CoeffOfTerm(x[I], Inv[J]) | I In 1..3 ]";
  PrintLn "	      | J In 1..3]);";
  PrintLn "    M * MM;";
  PrintLn "";
  PrintLn "    Use Rxyz;";
  PrintLn "    -- elementary symmetric polynomials";
  PrintLn "    S0 := 1;  S1 := x+y+z;  S2 := xy + xz + yz;  S3 := xyz;";
  PrintLn "    SA := Alg.SubalgebraMap([S0, S1, S2, S3]);";
  PrintLn "    -- x^4 + y^4 + z^4 is a symmetric polynomial";
  PrintLn "    Alg.IsInSubalgebra(x^4 + y^4 + z^4, SA);";
  PrintLn "    Repr := Alg.SubalgebraRepr(x^4 + y^4 + z^4, SA);";
  PrintLn "    Repr;";
  PrintLn "    Image(Repr, RMap([S0, S1, S2, S3]));";
  PrintLn "    -- equivalent to";
  PrintLn "    Alg.Image(Repr, SA);";
EndDefine; -- Man



---------------[   functions   ]---------------

Define Initialize()
  MEMORY.PKG.AlgHom := Record[]; /* TODO: REMOVE */
  MEMORY.PKG.AlgHom.RingNo := 0;
EndDefine; -- Initialize


Define DomainName(Phi)
  DomInds := Phi.DomainIndets;
  Return RingEnv(DomInds[1]);
EndDefine; -- DomainName


Define CodomainName(Phi)
  CodImages := Phi.Images;
  Return RingEnv(CodImages[1]);
EndDefine; -- CodomainName


Define Map(R1, R2, MapList)
  -- Checks
  If Not IsSubSet([R1,R2], RingEnvs()) Then
    Error("Alg.Map: R1, R2 must be names of existing rings");
  EndIf;
  If Var(R1)::NumIndets() <> Len(MapList) Then
    Error("Alg.Map: wrong number of elements in MapList");
  EndIf;
  If Not $hilop.ListRingEnv(MapList) IsIn [R2] Then
    Error("Alg.Map: elements in MapList must be in R2")
  EndIf;
  -- Definition of AlgHomRing
  N1 := NumIndets(Var(R1));
  N2 := NumIndets(Var(R2));
  Using Var(R1) Do -- for CoeffRing
    AlgHomRing ::=
    CoeffRing[x[1..N1], y[1..N2]],
    Weights(Concat(Var(R1)::WeightsList(), Var(R2)::WeightsList())),
    Elim(y[1]..y[N2]);
  EndUsing;
  -- Representation of the homomorphism
  Using AlgHomRing Do
    I := Ideal([ x[I]-Image(MapList[I], RMap(AllIndetsCalled("y")))
	       | I In 1..N1])
  EndUsing;
  Return Alg.Tagged(Record[
		    DomainIndets := Var(R1)::Indets(),
		    Images := Var(R2)::[Poly(MapList[I]) | I In 1..N1],
		    ElimIdeal := I]);
EndDefine; -- Map


Define IsSurjective(Var Phi)
  Phi := Untagged(Phi);  -- tag bug?
  LTPhi := LT(Phi.ElimIdeal);
  Phi := Alg.Tagged(Phi);
  Using Var(RingEnv(Phi.ElimIdeal)) Do
    Foreach Y In AllIndetsCalled("y") Do
      If Not Y IsIn LTPhi Then Return FALSE EndIf;
    EndForeach;
    Return TRUE;
  EndUsing;
EndDefine; -- IsSurjective


Define Ker(Var Phi)
  Phi := Untagged(Phi);  -- TAG bug?
  GBPhi := GBasis(Phi.ElimIdeal);
  Phi := Alg.Tagged(Phi);  -- TAG bug?
  Using Var(RingEnv(Phi.ElimIdeal)) Do
    K := Ideal([F In GBPhi | LT(F) < Min(AllIndetsCalled("y")) ]);
  EndUsing;
  Proj1 := RMap(Concat(
		Phi.DomainIndets,
		NewList(NumIndets(Var(Alg.CodomainName(Phi))), 0)) );
  Return Image(K, Proj1);
EndDefine; -- Ker


Define IsInjective(Var Phi)
  Return IsZero(Alg.Ker(Phi));
EndDefine; -- IsInjective


---------------------[  element in image  ]---------------------

Define IsInImage(F, Var Phi)
  Using Var(RingEnv(Phi.ElimIdeal)) Do
    Phi := Untagged(Phi);  -- TAG bug?
    GBPhi := GBasis(Phi.ElimIdeal);
    Phi := Alg.Tagged(Phi);  -- TAG bug?
    NFF := NR(Image(F, RMap(AllIndetsCalled("y"))), GBPhi);
    Return LT(NFF) < Min(AllIndetsCalled("y"))
  EndUsing;
EndDefine; -- IsInImage


Define PreImage(F, Var Phi)
  Phi := Untagged(Phi);  -- TAG bug?
  GBPhi := GBasis(Phi.ElimIdeal);
  Phi := Alg.Tagged(Phi);  -- TAG bug?
  Using Var(RingEnv(Phi.ElimIdeal)) Do
    Y := AllIndetsCalled("y");
    NFF := NR(Image(F, RMap(Y)), GBPhi);
    If LT(NFF) >= Min(Y) Then Return NULL EndIf;
  EndUsing;
  Using Var(Alg.DomainName(Phi)) Do
    Proj1 := RMap(Concat(Phi.DomainIndets, NewList(Len(Y), 0)));
    Return Image(NFF, Proj1);
  EndUsing;
EndDefine; -- PreImage


Define Image(F, Phi)
//  Return Var(Alg.CodomainName(Phi)) :: Image(F, RMap(Phi.Images))
  Return Image(F, RMap(Phi.Images));
EndDefine; -- Image


---------------------[  Sub-Algebras  ]---------------------

Define SubalgebraMap(L)
  R2 := $hilop.ListRingEnv(L);
  Using Var(R2) Do
    Ws := [Deg(F) | F In L];
    If 0 IsIn Ws Then
      SubalgebraRing ::= CoeffRing[x[1..Len(L)]];
    Else
      SubalgebraRing ::= CoeffRing[x[1..Len(L)]], Weights(Ws);
    EndIf;
    Return Alg.Map("SubalgebraRing", R2, L);
  EndUsing;
EndDefine; -- Subalgebra


Define IsInSubalgebra(F, Var SA)
  If Type(SA) = LIST Then Return Alg.IsInSubalgebra(F, Alg.SubalgebraMap(SA)); EndIf;
  Return Alg.IsInImage(F, SA);
EndDefine; -- IsInSubalgebra


Define SubalgebraRepr(F, Var SA)
  If Type(SA) = LIST Then Return Alg.SubalgebraRepr(F, Alg.SubalgebraMap(SA)); EndIf;
  Return Alg.PreImage(F, SA)
EndDefine; -- SubalgebraRepr


/*
  M := Mat([[1,2,3],[2,3,4],[1,0,1]]);
  Det(M);  -- invertible

  Use R ::= QQ[x[1..3]];
  L := [ Sum([ Row[I]x[I] | I In 1..Len(Row)]) | Row In M ];
  Psi := Alg.Map("R", "R", L);  -- linear map defined by M

  Alg.IsInjective(Psi);
  Alg.IsSurjective(Psi);

    PreImages := [Alg.PreImage(x[I], Psi) | I In 1..3];

    MM := Mat([ [ CoeffOfTerm(x[I], PreImages[J]) | I In 1..3 ]
	      | J In 1..3]);
  M MM;
  */

------[   pretty printing   ]--------

Define Tagged(X)
  Return Tagged(X, $.PkgName()+".Map");
EndDefine;

Define Tag()
  Return $.PkgName() + ".Map";
EndDefine;

Define Print_Map(Map)
  DomName := Alg.DomainName(Map);
  CodName := Alg.CodomainName(Map);
  PrintLn "Record[";
  Using Var(DomName) Do
    PrintLn "  DomainIndets := ", DomName, "::", Map.DomainIndets, ",";
  EndUsing;
  Using Var(CodName) Do
    PrintLn "  Images := ", CodName, "::", Map.Images, ",";
  EndUsing;
  Indent := Option(Indentation);
  Set Indentation;
  PrintLn "  ElimIdeal := ", Map.ElimIdeal;
  Set Indentation := Indent;
  PrintLn "]";
EndDefine; -- Print_Map


EndPackage; -- Package $contrib/alghom


/*

    Rxyz ::= QQ[x,y,z];
    Rab  ::= QQ[a,b];

    Use Rab;
    -- like RMap + the ring names
    Phi := Alg.Map("Rxyz", "Rab", [a+1, ab+3, b^2]);

    -- or, in any ring
    Use ZZ/(2)[w]; Phi := Alg.Map("Rxyz", "Rab", Rab::[a+1, ab+3, b^2]);

    Alg.Ker(Phi);
    Alg.IsInjective(Phi);
    Alg.IsSurjective(Phi);

    Use Rab;
    Alg.IsInImage(Rab::a^2, Phi);
    Alg.PreImage(Rab::a^2, Phi);
    Alg.Image(Rxyz :: (x^2 - 2x + 1), Phi);

    -- linear maps
    M := Mat([[1,2,3],[2,3,4],[1,0,1]]);
    Det(M);  -- invertible

    Use R ::= QQ[x[1..3]];
    L := [ Sum([ Row[I]*x[I] | I In 1..Len(Row)]) | Row In M ];
    Psi := Alg.Map("R", "R", L);  -- linear map defined by M
    Alg.IsInjective(Psi);
    Alg.IsSurjective(Psi);

    Inv := [Alg.PreImage(x[I], Psi) | I In 1..3];
    MM := Mat([ [ CoeffOfTerm(x[I], Inv[J]) | I In 1..3 ]
	      | J In 1..3]);
    M * MM;

    Use Rxyz;
    -- elementary symmetric polynomials
    S0 := 1;  S1 := x+y+z;  S2 := xy + xz + yz;  S3 := xyz;
    SA := Alg.SubalgebraMap([S0, S1, S2, S3]);
    -- x^4 + y^4 + z^4 is a symmetric polynomial
    Alg.IsInSubalgebra(x^4 + y^4 + z^4, SA);
    Repr := Alg.SubalgebraRepr(x^4 + y^4 + z^4, SA);
    Repr;
    Image(Repr, RMap([S0, S1, S2, S3]));
    -- equivalent to
    Alg.Image(Repr, SA);
*/