
            Documentation
-----------------------------------
18/01/2016
Ignacio de Mendizabal Vazquez
ignacio.mendizabal.vazquez@gmail.com
-----------------------------------
This project consists of an Rule Based Expert System for vaginal cytology diagnosis. 
It has two main parts: the calculation core and the graphical user interface

The calculation core was developed by Carlos Gamallo-Chicano, Eugenio Roanes-Lozano and Carlos Gamallo-Amat. They can be contacted by the following email addresses: 
carlos.gamallo@gmail.com, eroanes@mat.ucm.es, cgamallo@iib.uam.es

The detailed description of the development can be foun in the 
next article: 

Title: "A Rule-Based Expert System for Vaginal Cytology Diagnosis"
Book Title: Artificial Intelligence and Symbolic Calculation
Book Subtitle: 12th International Conference, AISC 2014, Seville, Spain, December 11-13, 2014. Proceedings
Pages: pp 34-48
Copyright: 2014
DOI: 10.1007/978-3-319-13770-4_5
Print ISBN: 978-3-319-13769-8
Online ISBN: 978-3-319-13770-4
Series Title: Lecture Notes in Computer Science
Publisher: Springer International Publishing
Series Volume: 8884
Series ISSN: 0302-9743
