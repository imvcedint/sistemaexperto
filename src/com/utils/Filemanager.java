package com.utils;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Class FileManager
 * 
 * @author imendizabal
 * Useful functions to manage files
 * 
 */
public class Filemanager 
{
	
	private static String pathLog 				 = "resources/log/log.txt";
	
	/**
	 * Read a file line by line
	 * @param nameFile The path of the file to be open
	 * @return An ArrayList of Strings, each String corresponds to each line of the file
	 * 
	 */
	public static ArrayList<String> readFromFile( String nameFile )
	{
		String line = null;
		ArrayList<String> listText = new ArrayList<String>();
		try
		{
			FileReader fileReader = new FileReader( nameFile );
			BufferedReader bufferedReader = new BufferedReader( fileReader );
			while( ( line = bufferedReader.readLine() ) != null )
			{
				listText.add( line );
			}
			bufferedReader.close();
		}
		catch( FileNotFoundException ex )
		{
			System.err.println("File " + nameFile + " not found");
			ex.printStackTrace();
		}
		catch( IOException ex )
		{
			System.err.println("Error I/O");
			ex.printStackTrace();
		}
		return listText;
	}
	
	/**
	 * Appends a line at the end of a file
	 * @param nameFile The name of the file
	 * @param content The string to be appended
	 * @return True if the process has been correct, false otherwise
	 */
	public static boolean appendToFile( String nameFile, String content )
	{
		if( nameFile.length() == 0 || content.length() == 0 )
		{
			System.err.println("The parameters cannot be empty");
			System.err.println("File name: " + nameFile + ", Content: " + content );
		}
		try
		{
			FileWriter fileWriter = new FileWriter( nameFile, true );
			BufferedWriter bufferedWriter = new BufferedWriter( fileWriter );
			
			bufferedWriter.write( content );
			bufferedWriter.write("\n");
			bufferedWriter.close();
			fileWriter.close();
			return true;
		}
		
		catch( IOException ex )
		{
			System.err.println("Error writing to file: " + nameFile );
			ex.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 
	 * @return Human-readable timestamp
	 * 
	 */
	
	public static String formatDate()
	{
		 return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	}
	
	public static void appendToLog( String text)
	{
		Filemanager.appendToFile( pathLog, formatDate() + " " + text );
	}
	
}



