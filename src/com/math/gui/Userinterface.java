package com.math.gui;


import java.io.IOException;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.swt.graphics.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

import com.math.core.Cocoa;
import com.math.core.RBES;

/**
 * The library Standard Widget Toolkit (SWT) has been used to generate the user interface
 * To develop with this library follow the steps:
 * 
 * 1) Download the SWT (zip file) from the official repository: https://www.eclipse.org/swt/eclipse.php
 * 2) Within eclipse, import a project and select the archive file SWT
 */


/**
 * An User Interface to interact with an Expert System for vaginal cytology diagnosis
 * The expert system was developed by Eugenio Roanes and Carlos Gamallo
 * Article DOI: 10.1007/978-3-319-13770-4_5
 * Date: December 11-13, 2014
 * 
 */

 /**
 * @author imendizabal
 * @contact ignacio.mendizabal.vazquez@gmail.com
 * @version 1.0
 * @date 18/01/2016
 */



public class Userinterface 
{	
	
	private Button buttonExit;
	private Button buttonPicture;
	private Button questionBox;
	private String rootImages = "resources/img/";
	
	private ArrayList<Button> questionBoxesX;
	private ArrayList<Button> questionBoxesH;
	private ArrayList<Button> questionBoxesY;
	
	private ArrayList<Button> imagesX;
	
	private RBES rbes;
	private Cocoa cocoa;
	

	/** GUI parameters **/
	
	private static int displayWidth  = 600;
	private static int displayHeight = 600;
	private static int numColumns    = 2;
	
	private static Font labelsFont_1;
	private static Font buttonsFont_1;
	private static Font buttonsFont_2;
	
	private static String titleDisplay = "Expert system for vaginal cytology diagnosis";
	
	public void loadGui() throws InterruptedException, IOException
	{
		
		questionBoxesX = new ArrayList<Button>();
		imagesX   = new ArrayList<Button>();
		
		//Main layout
		final Display displayMain = new Display();  
		final Shell shellMain = new Shell( displayMain, SWT.SHELL_TRIM & (~SWT.RESIZE) );
		
		//Main Shell
		shellMain.setText( titleDisplay );
		shellMain.setSize( displayWidth, displayHeight );
		buttonsFont_1 = new Font( displayMain, "Arial", 14, SWT.BOLD );
		buttonsFont_2 = new Font( displayMain, "Arial", 12, SWT.ITALIC );
		labelsFont_1  = new Font( displayMain, "Arial", 11, SWT.NORMAL );
		
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = numColumns;
		gridLayout.makeColumnsEqualWidth = true;
		shellMain.setLayout( gridLayout );		
		
		/** RBES stands for Rule Based Expert System **/
		rbes = new RBES();
		rbes.loadVariables();
		cocoa = new Cocoa( rbes );
		ArrayList<String> logicX = rbes.getLogicX();
		
		for( int i = 0; i < logicX.size(); i++ )
		{
			questionBox = new Button( shellMain, SWT.CHECK );
			questionBox.setText(logicX.get(i));			
			questionBoxesX.add( questionBox );
			
			buttonPicture = new Button( shellMain, SWT.PUSH );
			buttonPicture.setText( "Ver Imagen #" + Integer.toString( i ) );
			buttonPicture.setFont( buttonsFont_2 );

			buttonPicture.setData("buttonID", i + 1);
			imagesX.add( buttonPicture );
			
			final Shell child = new Shell( shellMain,  SWT.SHELL_TRIM | SWT.DOUBLE_BUFFERED );
			child.setSize( 400, 400 );
			child.setLayout( new FillLayout() );
			Image image = new Image( displayMain, rootImages + "muestra/" + buttonPicture.getData("buttonID").toString() + ".jpg" );
			Label label = new Label( child, SWT.BORDER );
			label.setImage( image  );
			
			buttonPicture.addSelectionListener( new SelectionListener()
			{
				@Override
				public void widgetSelected( SelectionEvent e ) {
					if( !shellMain.isDisposed() )
					{
						
						if( !child.isDisposed() )
						{
							child.open();
						}
					}
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					// TODO Auto-generated method stub
					
				}
			});
		}			
		
		buttonExit = new Button( shellMain, SWT.PUSH );
		buttonExit.setText("Cerrar");
		buttonExit.setFont( buttonsFont_1 );
		buttonExit.addSelectionListener( new SelectionAdapter() 
		{
			@Override
			public void widgetSelected( SelectionEvent e )
			{
				shellMain.close();
			}
		});
		
		Button buttonLaunchCocoa = new Button( shellMain, SWT.PUSH );
		buttonLaunchCocoa.setText("Calcular");
		buttonLaunchCocoa.setFont( buttonsFont_1 );
		
		buttonLaunchCocoa.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected( SelectionEvent e )
			{
				try 
				{
					cocoa.stepOne( questionBoxesX );
					cocoa.initCocoa();	
					shellMain.dispose();
			    } catch (InterruptedException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				catch( Exception ex )
				{
					ex.printStackTrace();
				}
			}
		});		
	
		shellMain.open();
		while( !shellMain.isDisposed() )
		{
			if( !displayMain.readAndDispatch() )
			{
				displayMain.sleep();
			}
		}
			
		if( cocoa.getStepTwo() == true )
		{
			System.out.println("###STEP TWO: " + cocoa.getStepTwo() );
			questionBoxesH = new ArrayList<Button>();
			ArrayList<String> logicH = rbes.getLogicH();
			final Shell shellTwo = new Shell( displayMain, SWT.SHELL_TRIM & (~SWT.RESIZE) );
			shellTwo.setSize( displayWidth, displayHeight );
			shellTwo.setText( titleDisplay );
			GridLayout gLayoutShellTwo = new GridLayout();
			gLayoutShellTwo.numColumns = numColumns;
			gLayoutShellTwo.makeColumnsEqualWidth = true;
			shellTwo.setLayout( gLayoutShellTwo );
			
			for( int i = 0; i < logicH.size(); i++ )
			{
				questionBox = new Button( shellTwo, SWT.CHECK );
				questionBox.setText( logicH.get(i) );			
				questionBoxesH.add( questionBox );		    			
				buttonPicture = new Button( shellTwo, SWT.PUSH );
				buttonPicture.setText( "Ver foto" );
				buttonPicture.setFont( buttonsFont_2 );
			}	
			
			buttonExit = new Button( shellTwo, SWT.PUSH );
			buttonExit.setText("Cerrar");
			buttonExit.setFont( buttonsFont_1 );
			buttonExit.addSelectionListener( new SelectionAdapter() 
			{
				@Override
				public void widgetSelected( SelectionEvent e )
				{
					shellMain.close();
				}
			});
			
			buttonLaunchCocoa = new Button( shellTwo, SWT.PUSH );
			buttonLaunchCocoa.setText("Calcular");
			buttonLaunchCocoa.setFont( buttonsFont_1 );
			buttonLaunchCocoa.addSelectionListener(new SelectionAdapter()
			{ 
				@Override
				public void widgetSelected( SelectionEvent e )
				{
					try {
						cocoa.stepTwo( questionBoxesH );			
						shellTwo.dispose();
				   } catch( Exception ex )
					{
						ex.printStackTrace();
					}
				}
			});		
			
		
			
			shellTwo.open();	
			
			while( !shellTwo.isDisposed() )
			{
				if( !displayMain.readAndDispatch() )
				{
					displayMain.sleep();
				}
			}
		}
		
		if( cocoa.getStepThree() == true )
		{
			System.out.println("PASO TRES: " + cocoa.getStepThree() );
			questionBoxesY = new ArrayList<Button>();
			ArrayList<String> logicY = rbes.getLogicY();
			final Shell shellThree = new Shell( displayMain, SWT.SHELL_TRIM & (~SWT.RESIZE) );
			shellThree.setSize( displayWidth, displayHeight );
			shellThree.setText( titleDisplay );
			GridLayout gLayoutShellThree = new GridLayout();
			gLayoutShellThree.numColumns = 2;
			gLayoutShellThree.makeColumnsEqualWidth = true;
			shellThree.setLayout( gLayoutShellThree );
			
			for( int i = 0; i < logicY.size(); i++ )
			{
				questionBox = new Button( shellThree, SWT.CHECK );
				questionBox.setText( logicY.get(i) );			
				questionBoxesY.add( questionBox );		    
				questionBox.setFont( labelsFont_1 );
				buttonPicture = new Button( shellThree, SWT.PUSH );
				buttonPicture.setText( "Ver foto" );
				buttonPicture.setFont( buttonsFont_2 );
				buttonPicture.addSelectionListener( new SelectionListener() {
					
					@Override
					public void widgetSelected(SelectionEvent e) {
						Shell child = new Shell( shellMain );
						child.setSize( 200, 200 );
						child.open();
						
					}
					
					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
					
					}
				});
			}	


			buttonExit = new Button( shellThree, SWT.PUSH );
			buttonExit.setText("Cerrar");
			buttonExit.setFont( buttonsFont_1 );
			buttonExit.addSelectionListener( new SelectionAdapter() 
			{
				@Override
				public void widgetSelected( SelectionEvent e )
				{
					shellMain.close();
				}
			});
			
			buttonLaunchCocoa = new Button( shellThree, SWT.PUSH );
			buttonLaunchCocoa.setText("Calcular");
			buttonLaunchCocoa.setFont( buttonsFont_1 );
			buttonLaunchCocoa.addSelectionListener(new SelectionAdapter()
			{ 
				@Override
				public void widgetSelected( SelectionEvent e )
				{
					try {
						cocoa.stepThree( questionBoxesY );					
				   } catch( Exception ex )
					{
						ex.printStackTrace();
					}
				}
			});		
			
			
		
			
			shellThree.open();	
			
			
		
			while( !shellThree.isDisposed() )
			{
				if( !displayMain.readAndDispatch() )
				{
					displayMain.sleep();
				}
			}
			cocoa.stepFour();
			cocoa.stepFive();
		}
			
			
		}
	
	public static void main( String[] args ) throws InterruptedException, IOException
	{
		Userinterface ui = new Userinterface();
		ui.loadGui();
	}	
}





























