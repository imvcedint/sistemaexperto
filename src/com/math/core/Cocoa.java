package com.math.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

import org.eclipse.swt.widgets.Button;

import com.utils.Filemanager;

/**
 * 
 * @author imendizabal
 * @contact ignacio.mendizabal.vazquez@gmail.com
 * @date 20/01/2016
 * @version 1.0
 * 
 */

public class Cocoa
{
	
	/**
	 * 20/01/2016
	 * Para poder ejecutar Cocoa y otra aplicaciones externas al exportar el JAR no sirve solo con copiar el directorio
	 * 1) Hay que añadir al carpeta al Build Path como source folder
	 * 2) En lugar de utilizar las cadenas literales, hay que utilizar la funcion NombreDeLaClase.class.getResource("nombredelrecurso.ext");
	 */

	/**
	 * Leer/Escribir por la entrada estandar de un proceso en ejecucion: Process
	 * proc = runtime.exec( cmd_cocoa_linux ); FileInputStream fileIn = null;
	 * FileOutputStream fileOut = null; procIn = proc.getOutputStream(); procOut
	 * = proc.getInputStream(); fileIn = new
	 * FileInputStream("resources/calc/1.coc"); fileOut = new
	 * FileOutputStream("resources/calc/test.coc"); pipeStream( fileIn, procIn
	 * ); pipeStream( procOut, fileOut ); fileIn.close(); fileOut.close();
	 * readStandardInput( proc );
	 */

	private String linux_pathCocoa_app   = System.getProperty("user.dir") + "/resources/cocoa/linux/cocoa_text";

	private String windows_pathCocoa_app = System.getProperty("user.dir") + "\\resources\\cocoa\\windows\\cocoa_text";
	
	//private String linux_pathCocoa_app   = "/resources/cocoa/linux/cocoa_text";
	
	private String bash				     = "/bin/sh";
	private String extension_coc 	     = ".coc";
	private String extension_txt 	     = ".txt";
	private String rootPathResourcesCalc = "resources/calc/";

	private String cocoaInput = rootPathResourcesCalc + "cocoaInput" + extension_coc;
	private String tempResults = rootPathResourcesCalc + "cocoaTempResults" + extension_txt;

	private Process proc;	
	private RBES    rbes;
	
	OutputStream procIn  = null;
	InputStream  procOut = null;

	OutputStream stdin   = null;
	InputStream  stderr  = null;
	InputStream  stdout  = null;

	private ProcessBuilder pb;
	private ArrayList<String> arguments;
	
	private File inputFile;
	private File outputFile;
	private File errorFile;
	
	//TODO: Ver como tratar las inconsistencias logicas
	private boolean logicalInconsistency = false;
	
	/** The processing is done through different paths, depending on the user input, so it is done by steps**/
	private boolean stepTwo = false;
	private boolean stepThree = false;

	public Cocoa(RBES rbes)
	{
		this.rbes = rbes;
	}
	
	public boolean getStepTwo()
	{
		return stepTwo;
	}
	
	public boolean getStepThree()
	{
		return stepThree;
	}
	
	public void setStepTwo( boolean stepTwo )
	{
		this.stepTwo = stepTwo;
	}
	
	public void setStepThree( boolean stepThree )
	{
		this.stepThree = stepThree;
	}

    public void stepOne( ArrayList<Button> x )
    {
    	inputFile = new File( cocoaInput );
    	if( inputFile.exists() ) inputFile.delete();
    	initConditions( cocoaInput );		
    	initRules( cocoaInput );
    	stepOne( x, cocoaInput );
    	processPartialResults( tempResults, "E1", 'p');
    	Filemanager.appendToLog( "STEP TWO: " + String.valueOf( stepTwo ) + ", STEP THREE " + String.valueOf( stepThree ) );
    }
    
    public void stepTwo( ArrayList<Button> h ) throws InterruptedException, IOException
    {
    	stepTwo( h, cocoaInput );
    	execCocoa();
     	Filemanager.appendToFile( cocoaInput, processPartialResults( tempResults, "E2", 'd') );
    }
    
    public void stepThree( ArrayList<Button> y ) throws InterruptedException, IOException 
    {
    	stepThree( y, cocoaInput );
    	execCocoa();
    	Filemanager.appendToFile( cocoaInput, processPartialResults( tempResults, "E3", 'd') );
    }
    
    /**
     * Runs Cocoa application without caring the operating system
     * @return The thread wich is running Cocoa
     * @throws InterruptedException 
     * @throws IOException
     */
    
    public Process execCocoa() throws InterruptedException, IOException
    {
    	try
    	{
    		/** Checks the operating system **/
    		/** TODO: Check the functioning in Windows --> CoCoa is not compatible with Windows >= 7 **/
    		
    		String so = System.getProperty("os.name");
    		arguments = new ArrayList<String>();
    		System.out.println("### Building URL from: " + linux_pathCocoa_app );
    	
    		
    		if( so.equals("Linux") )
    		{
    			pb = new ProcessBuilder( linux_pathCocoa_app );
    		}
    		else
    		{
    			System.out.println("### Running CoCoa from: " + linux_pathCocoa_app );
    		//	pb = new ProcessBuilder( "cmd", windows_pathCocoa_app );
    			pb = new ProcessBuilder( windows_pathCocoa_app );
    		}
    		
    		/** Temporal file where partial results are saved during execution. This file is needed to communicate with the Cocoa core **/
    		/** NOTE: it would be better to send and receive data through the standard I/O of the process **/
    		File fTempResults = new File( tempResults );
    		if( fTempResults.exists() ) fTempResults.delete();
    		fTempResults = null;
    		
    		pb.inheritIO();
    		inputFile = new File( cocoaInput );
    		pb.redirectInput( inputFile );
    		
    		proc = pb.start();
    		proc.waitFor();
    		IOThreadHandler outputHandler = new IOThreadHandler( proc.getInputStream() );				
    		outputHandler.start();
    		
    	}
    	catch(Exception ex )
    	{
    		ex.printStackTrace();
    	}
    	finally
    	{
    		proc.destroy();
			proc.getErrorStream().close();
			proc.getInputStream().close();
			proc.getOutputStream().close();	
    	}
    	return proc;
    }
    
    /**
     * THe Cocoa core starts
     * @throws InterruptedException
     * @throws IOException
     */
    
	public void initCocoa() throws InterruptedException, IOException 
	{
		execCocoa();
		Filemanager.appendToFile( cocoaInput, processPartialResults( tempResults, "E1", 'p') );
	}

	/**
	 * Redirects the standard input/output of a running thread. Useful to get what an external application is providing as output
	 * @author imendizabal
	 */
	
	private static class IOThreadHandler extends Thread
	{
		private InputStream inputStream;
		private StringBuilder output = new StringBuilder();
		
		IOThreadHandler(InputStream inputStream)
		{
			this.inputStream = inputStream;
		}

		public void run() 
		{
			Scanner br = null;
			try
			{
				br = new Scanner(new InputStreamReader(inputStream));
				String line = null;
				while (br.hasNextLine()) 
				{
					line = br.nextLine();
					output.append(line + System.getProperty("line.separator"));
				}
			} finally {
				br.close();
			}
		}

		public StringBuilder getOutput() 
		{
			return output;
		}
	}

	/**
	 * Initialize logical variables and the rules of the expert system
	 * @param file
	 * @return
	 */
	
	public ArrayList<String> initConditions( String file ) 
	{
		ArrayList<String> initStrings = new ArrayList<String>();
		String ring = "A::=Z/(2)[";
		ring += "x[" + rbes.init_X + ".." + rbes.end_X + "],";
		ring += "p[" + rbes.init_P + ".." + rbes.end_P + "],";
		ring += "h[" + rbes.init_H + ".." + rbes.end_H + "],";
		ring += "y[" + rbes.init_Y + ".." + rbes.end_Y + "],";
		ring += "d[" + rbes.init_D + ".." + rbes.end_D + "]";
		ring = ring + "];";
		initStrings.add(ring);
		initStrings.add("USE A;");

		String ideal = "MEMORY.I:=Ideal(";
		for (int i = rbes.init_X; i <= rbes.end_X; i++) {
			ideal += "x[" + String.valueOf(i) + "]^2-x[" + String.valueOf(i)
					+ "],";
		}
		for (int i = rbes.init_P; i <= rbes.end_P; i++) {
			ideal += "p[" + String.valueOf(i) + "]^2-p[" + String.valueOf(i)
					+ "],";
		}
		for (int i = rbes.init_H; i <= rbes.end_H; i++) {
			ideal += "h[" + String.valueOf(i) + "]^2-h[" + String.valueOf(i)
					+ "],";
		}
		for (int i = rbes.init_Y; i <= rbes.end_Y; i++) {
			ideal += "y[" + String.valueOf(i) + "]^2-y[" + String.valueOf(i)
					+ "],";
		}
		for (int i = rbes.init_D; i < rbes.end_D; i++) {
			ideal += "d[" + String.valueOf(i) + "]^2-d[" + String.valueOf(i)
					+ "],";
		}
		ideal += "d[" + String.valueOf(rbes.end_D) + "]^2-d["
				+ String.valueOf(rbes.end_D) + "]";

		ideal += ");";

		initStrings.add(ideal);

		String logicRule_neg = "Define NEG(M) Return NF(1-M,MEMORY.I); EndDefine;";
		String logicRule_or =  "Define O(M,N) Return NF(M+N-M*N,MEMORY.I); EndDefine; ";
		String logicRule_and = "Define Y(M,N) Return NF(M*N,MEMORY.I); EndDefine; ";
		String logicRule_imp = "Define IMP(M,N) Return NF(1+M+M*N,MEMORY.I); EndDefine; ";

		initStrings.add(logicRule_neg);
		initStrings.add(logicRule_or);
		initStrings.add(logicRule_and);
		initStrings.add(logicRule_imp);
		initStrings.add("PrintLn\"Operaciones inicializadas correctamente\";");

		for (int i = 0; i < initStrings.size(); i++) 
		{
			Filemanager.appendToFile(file, initStrings.get(i));
		}
		return initStrings;
	}

	/**
	 * Initialize the rules of the expert system 
	 * @param file The path of the file where the rules are going to be stored 
	 * @return An ArrayList with each of the rules. One rule per element
	 */
	public ArrayList<String> initRules( String file ) 
	{
		ArrayList<String> rules = new ArrayList<String>();
		rules.add("R1:=IMP(O(O(x[2],x[3]),x[4]),p[1]);");
		rules.add("R2:=IMP(Y(x[1],x[6]),p[2]);");
		rules.add("R3:=IMP(Y(p[2],x[9]),p[3]);");
		rules.add("R4:=IMP(Y(NEG(Y(x[0],x[6])),x[10]),d[5]);");
		rules.add("R5:=IMP(Y(p[2],x[11]),p[3]);");
		rules.add("R6:=IMP(Y(Y(h[0],h[1]),p[3]),d[5]);");
		rules.add("R7:=IMP(Y(Y(Y(NEG(h[1]),NEG(Y(h[2],h[3]))),h[0]),p[3]),d[4]);");
		rules.add("R8:=IMP(Y(Y(Y(NEG(h[1]),Y(h[2],h[3])),h[0]),p[3]),d[3]);");
		rules.add("R9:=IMP(Y(p[3],NEG(h[0])),d[4]);");
		rules.add("R10:=IMP( Y(Y(Y(p[3],x[7]),x[8]),x[5]) , d[0]);");
		rules.add("R11:=IMP(Y(O(O(O(O(y[4],x[9]),y[3]),y[2]),y[1]),d[0]),d[2]);");
		rules.add("R12:= IMP(Y(O(O(O(O(O(NEG(y[5]),x[9]),y[4]),y[3]),y[2]),y[1]),d[0]),d[1]);");
		rules.add("R13:=IMP( O(O(d[0],d[1]),d[2]) , Y(Y(NEG(d[3]),NEG(d[5])),NEG(d[6])) );");
		rules.add("R14:=IMP( d[3] , Y(Y(Y(Y(NEG(d[0]),NEG(d[1])),NEG(d[2])),NEG(d[5])),NEG(d[6])));");
		rules.add("R15:=IMP( d[5] , Y(Y(Y(Y(NEG(d[0]),NEG(d[1])),NEG(d[2])),NEG(d[3])),NEG(d[6])));");
		rules.add("R16:=IMP( d[6] , Y(Y(Y(Y(NEG(d[0]),NEG(d[1])),NEG(d[2])),NEG(d[3])),NEG(d[5])));");
		rules.add("J1:=Ideal( NEG(R1),NEG(R2),NEG(R3), NEG(R5) );");
		rules.add("J2:=Ideal( NEG(R6),NEG(R7),NEG(R8),NEG(R9),NEG(R10) );");
		rules.add("J3:=Ideal( NEG(R11),NEG(R12) );");
		rules.add("J4:=Ideal( NEG(R4) );");
		rules.add("J5:=Ideal( NEG(R13),NEG(R14),NEG(R15),NEG(R16) );");
		rules.add("PrintLn\"Reglas inicializadas correctamente\";");

		for (int i = 0; i < rules.size(); i++)
		{
			Filemanager.appendToFile(file, rules.get(i));
		}

		return rules;
	}

	/**
	 * Counts the number of logical trues found within an ArrayList
	 * This function is used to check what conditions have been activated by the final user
	 * @param x The ArrayList to check the logical variables
	 * @return The positions where the logical variables are true
	 */
	private ArrayList<Integer> countTrues( ArrayList<Button> x )
	{
		ArrayList<Integer> indexes = new ArrayList<Integer>();
		for( int i = 0; i < x.size(); i++ )
		{
			if( x.get(i).getSelection() ) indexes.add( i );
		}
		return indexes;
	}
	
	/**
	 * Step one of the expert system. The logical variables are translated into Cocoa code to perform the first step of calculations
	 * @param x ArrayList which contains the logical variables 
	 * @param file The path of the file where the results 
	 * @return 
	 */
	public boolean stepOne(ArrayList<Button> x, String file) 
	{
		String ideal = "K1:=Ideal( ";
		ArrayList<Integer> indexes = countTrues( x );
		for (int i = 0; i < indexes.size(); i++)
		{
				ideal = ideal + "NEG(x[ " + String.valueOf(indexes.get(i)) + "])";
				if (i < indexes.size() - 1 ) 
				{
					ideal = ideal + ",";
				}
		}
		
		ideal = ideal + ");";
		Filemanager.appendToFile(file, ideal);
		
		/**
		 * If GBasis(MEMORY.I+J1+K1)=[1] Then PrintLn "Logical inconsistency"
		 * Else PrintLn "Logical consistency" EndIf;
		 * 
		 * NF(NEG(p[1]),MEMORY.I+J1+K1); NF(NEG(p[2]),MEMORY.I+J1+K1);
		 * NF(NEG(p[3]),MEMORY.I+J1+K1);
		 */
		Filemanager.appendToFile(file, "If GBasis(MEMORY.I+J1+K1)=[1]");
		Filemanager.appendToFile(file,
				"   Then PrintLn \"Logical inconsistency\"");
		Filemanager.appendToFile(file,
				"   Else PrintLn \"Logical consistency\"");
		Filemanager.appendToFile(file, " EndIf;");
		Filemanager.appendToFile(file, "NF(NEG(p[1]),MEMORY.I+J1+K1);");
		Filemanager.appendToFile(file, "NF(NEG(p[2]),MEMORY.I+J1+K1);");
		Filemanager.appendToFile(file, "NF(NEG(p[3]),MEMORY.I+J1+K1);");		
		Filemanager.appendToFile(file, "PrintLn\"Ideal K1: INICIALIZADO\";");
		Filemanager.appendToFile(file, "Fichero := OpenOFile(" + "\"" + tempResults + "\"" + ");");
		
		for( int i = 0; i < rbes.getLogicP().size(); i++ )
		{
			Filemanager.appendToFile( file, "PrintLn " + "\"" + "p" + String.valueOf(i+1) + "=" + "\"" + "+" + "Sprint( NF(NEG(p[" + String.valueOf( i + 1 ) + "]),MEMORY.I+J1+K1) ) " + "On Fichero;");
		}
		
		Filemanager.appendToFile(file, "Close(Fichero);");		
		return true;
		
	}
	
	/**
	 * Step two of the expert system. The logical variables are translated into Cocoa code to perform the second step of calculations
	 * @param h
	 * @param file
	 * @return
	 */
	public boolean stepTwo( ArrayList<Button> h, String file )
	{
		String ideal = "K2:=Ideal( ";
		ArrayList<Integer> indexes = countTrues( h );
		
		for( int i = 0; i < indexes.size(); i++ )
		{
			ideal = ideal + "NEG(h[" + String.valueOf(indexes.get(i)) + "])";
			if (i < indexes.size() - 1 ) 
			{
				ideal = ideal + ",";
			}
		}
		
		ideal = ideal + ");";
		
		Filemanager.appendToFile(file, ideal);
		
		Filemanager.appendToFile(file, "If GBasis(MEMORY.I+E1+J2+K1+K2)=[1]");
		Filemanager.appendToFile(file,
				"   Then PrintLn \"Logical inconsistency\"");
		Filemanager.appendToFile(file,
				"   Else PrintLn \"Logical consistency\"");
		Filemanager.appendToFile(file, " EndIf;");
		Filemanager.appendToFile(file, "NF(NEG(d[0]),MEMORY.I+E1+J2+K1+K2);");
		Filemanager.appendToFile(file, "NF(NEG(d[3]),MEMORY.I+E1+J2+K1+K2);");
		Filemanager.appendToFile(file, "NF(NEG(d[4]),MEMORY.I+E1+J2+K1+K2);");
		Filemanager.appendToFile(file, "NF(NEG(d[5]),MEMORY.I+E1+J2+K1+K2);");
		
		Filemanager.appendToFile(file, "PrintLn\"Ideal K2: INICIALIZADO\";");
		Filemanager.appendToFile(file, "Fichero := OpenOFile(" + "\"" + tempResults + "\"" + ");");
		
		Filemanager.appendToFile( file, "PrintLn " + "\"" + "d" + String.valueOf(0) + "=" + "\"" + "+" + "Sprint( NF(NEG(d[" + String.valueOf( 0 ) + "]),MEMORY.I+E1+J2+K1+K2 ) ) " + "On Fichero;");
		Filemanager.appendToFile( file, "PrintLn " + "\"" + "d" + String.valueOf(3) + "=" + "\"" + "+" + "Sprint( NF(NEG(d[" + String.valueOf( 3 ) + "]),MEMORY.I+E1+J2+K1+K2 ) ) " + "On Fichero;");
		Filemanager.appendToFile( file, "PrintLn " + "\"" + "d" + String.valueOf(4) + "=" + "\"" + "+" + "Sprint( NF(NEG(d[" + String.valueOf( 4 ) + "]),MEMORY.I+E1+J2+K1+K2 ) ) " + "On Fichero;");
		Filemanager.appendToFile( file, "PrintLn " + "\"" + "d" + String.valueOf(5) + "=" + "\"" + "+" + "Sprint( NF(NEG(d[" + String.valueOf( 5 ) + "]),MEMORY.I+E1+J2+K1+K2 ) ) " + "On Fichero;");

		Filemanager.appendToFile(file, "Close(Fichero);");		
		
		return true;
	}
	
	
	
	/**
	 * Step three of the expert system. The logical variables are translated into Cocoa code to perform the third step of calculationss
	 * @param y
	 * @param file
	 * @return
	 */
	
	public boolean stepThree( ArrayList<Button> y, String file )
	{
		String ideal = "K3:=Ideal( ";
		ArrayList<Integer> indexes = countTrues( y );
		
		for( int i = 0; i < indexes.size(); i++ )
		{
			ideal = ideal + "NEG(y[" + String.valueOf(indexes.get(i)+1) + "])";
			if (i < indexes.size() - 1 ) 
			{
				ideal = ideal + ",";
			}
		}
		
		ideal = ideal + ");";
		
		Filemanager.appendToFile(file, ideal);
		
		Filemanager.appendToFile(file, "If GBasis(MEMORY.I+E2+J3+K1+K3)=[1]");
		Filemanager.appendToFile(file,
				"   Then PrintLn \"Logical inconsistency\"");
		Filemanager.appendToFile(file,
				"   Else PrintLn \"Logical consistency\"");
		Filemanager.appendToFile(file, " EndIf;");
		Filemanager.appendToFile(file, "Fichero := OpenOFile(" + "\"" + tempResults + "\"" + ");");		
		Filemanager.appendToFile( file, "PrintLn " + "\"" + "d" + String.valueOf(1) + "=" + "\"" + "+" + "Sprint( NF(NEG(d[" + String.valueOf( 1 ) + "]),MEMORY.I+E2+J3+K1+K3 ) )  " + "On Fichero;");
		Filemanager.appendToFile( file, "PrintLn " + "\"" + "d" + String.valueOf(2) + "=" + "\"" + "+" + "Sprint( NF(NEG(d[" + String.valueOf( 2 ) + "]),MEMORY.I+E2+J3+K1+K3 ) )  " + "On Fichero;");
		Filemanager.appendToFile(file, "Close(Fichero);");		
		
		return true;
	}
	
	/**
	 * Step four of the expert system. The logical variables are translated into Cocoa code to perform the fourth step of calculations
	 * @return
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public boolean stepFour() throws InterruptedException, IOException
	{
		Filemanager.appendToFile(cocoaInput, "If GBasis(MEMORY.I+J4+K1)=[1]");
		Filemanager.appendToFile(cocoaInput,
				"   Then PrintLn \"Logical inconsistency\"");
		Filemanager.appendToFile(cocoaInput,
				"   Else PrintLn \"Logical consistency\"");
		Filemanager.appendToFile(cocoaInput, " EndIf;");
		String ideal = "E4:=Ideal( NEG(d[5]));";
		Filemanager.appendToFile(cocoaInput, ideal);	
		execCocoa();
		return true;
	}
	
	/**
	 * Step five of the expert system. The logical variables are translated into Cocoa code to perform the fifth step of calculations
	 * @return
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public boolean stepFive() throws InterruptedException, IOException 
	{
		Filemanager.appendToFile(cocoaInput, "If  GBasis(MEMORY.I+E1+E2+E3+E4)=[1]");
		Filemanager.appendToFile(cocoaInput,
				"   Then PrintLn \"Logical inconsistency\"");
		Filemanager.appendToFile(cocoaInput,
				"   Else PrintLn \"Logical consistency\"");
		Filemanager.appendToFile(cocoaInput, " EndIf;");
		execCocoa();
		return true;
	}
		
	/**
	 * 
	 * TODO: check and improve this function 
	 * @param fileName 
	 * @param ideal
	 * @param name
	 * @return
	 * 
	 */
	
		public String processPartialResults( String fileName, String ideal, char name )
		{
			 try
			  {				
			  FileInputStream in = new FileInputStream(fileName);
			  BufferedReader  br = new BufferedReader(new InputStreamReader(in));
			  String strLine;		
			  ArrayList<ArrayList<Character>> processed = new ArrayList<ArrayList<Character>>();
			  int countTrues = 0;
			  while((strLine = br.readLine())!= null && strLine.length() > 0 )
			  {
				  String[] separated = strLine.split("=");
				  ArrayList<Character> lineCharacter = new ArrayList<Character>();
				  lineCharacter.add( separated[0].charAt(0) );
				  lineCharacter.add( separated[0].charAt(1) );
				 
				  if( separated[0].charAt(0) == name )
				  {
					  if( !( separated[1].length() > 1) )
					  {
						  if( ideal.equals( "E3") )
						  {
							  if( separated[0].charAt(0) == 'd' && ( separated[0].charAt(1) == '1' ||separated[0].charAt(1) == '2' ) )
							  {
								  lineCharacter.add('1');
								  countTrues++;
							  }
						  }
						  
						    if( separated[0].charAt(0) == 'p' && separated[0].charAt(1) == '3' )
						  	{		
							    stepTwo = true;
						  	}
						  	else if( stepTwo == true && ( separated[0].charAt(0) == 'd' && separated[0].charAt(1) == '0') )
						  	{
						     	stepThree = true;
						  	}
						  lineCharacter.add( '1' );
						  countTrues++;
					  	}
					  	else
					  	{
					  		
					  		lineCharacter.add( '0' );
					  	}	
					  processed.add( lineCharacter );
				  }
			  } 
			  
			  String line = "";
			  line = line + ideal + ":=Ideal(";
			  
			  
			  for( int i = 0; i < processed.size(); i++ )
			  {
				  if( processed.get(i).get(2) == '1' )
				  {
				    line = line + "NEG( " ;
				    line = line + processed.get(i).get(0) + "[" + processed.get(i).get(1) + "]";
				    line = line + ")";
				  
				  
					  line = line + ",";
				  
			  }
			  }
			  if( line.charAt( line.length() -1 ) == ',' )
			  {
				  line = line.substring( 0, line.length() - 2 );
			  }
			  line = line + ");";
			  
			  return line;
			  
			  }
			 	  catch(Exception e){
				  System.out.println(e);
				  e.printStackTrace();
				  return null;
				  
			  }
		}
}




































