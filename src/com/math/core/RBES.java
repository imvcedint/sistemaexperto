package com.math.core;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;

/**
 * RBES: Rules Based Expert System 
 * Define the elements that compounds the interface between the data provided by the core of the expert system and the user interface
 * @author Ignacio de Mendizabal Vazquez
 * @contact  ignacio.mendizabal.vazquez@gmail.com
 * @version 1.0
 * @date 23/05/2015
 * 
 */

public class RBES
{
	private String file_rules_text = "resources/rules/rules.properties";
	private String file_rules_definition = "resources/rules/rules_definition.txt";
	
	/** There are five logic variables defined in the published article: X, P, H, Y, D**/
	private ArrayList<String> logicX;
	private ArrayList<String> logicP;
	private ArrayList<String> logicH;
	private ArrayList<String> logicY;
	private ArrayList<String> logicD;
	
	private ArrayList<Boolean> logicXValue;
	private ArrayList<Boolean> logicPValue;
	private ArrayList<Boolean> logicHValue;
	private ArrayList<Boolean> logicYValue;
	private ArrayList<Boolean> logicDValue;
	
	public int init_X = 0;
	public int end_X  = 11;
	public int init_P = 1;
	public int end_P  = 3;
	public int init_H = 0;
	public int end_H  = 3;
	public int init_Y  = 1;
	public int end_Y   = 5;
	public int init_D   = 0;
	public int end_D   = 6;
	
	Properties prop   = new Properties();
	InputStream input = null;
	
	public ArrayList<String> getLogicX()
	{
		return logicX;
	}
	public ArrayList<String> getLogicP()
	{
		return logicP;
	}
	public ArrayList<String> getLogicH()
	{
		return logicH;
	}
	public ArrayList<String> getLogicY()
	{
		return logicY;
	}
	public ArrayList<String> getLogicD()
	{
		return logicD;
	}
	
	public ArrayList<Boolean> getLogicPValue()
	{
		return logicPValue;
	}
	
	public ArrayList<Boolean> getLogicXValue()
	{
		return logicXValue;
	}
	
	public ArrayList<Boolean> getLogicHValue()
	{
		return logicHValue;
	}
	
	public ArrayList<Boolean> getLogicDValue()
	{
		return logicDValue;
	}
	
	public ArrayList<Boolean> getLogicYValue()
	{
		return logicYValue;
	}
	
	/**
	 * Variables are loaded from a properties file
	 */
	public void loadVariables()
	{
		try
		{
			logicX = new ArrayList<String>();
			logicP = new ArrayList<String>();
			logicH = new ArrayList<String>();
			logicY = new ArrayList<String>();
			logicD = new ArrayList<String>();
			
			input = new FileInputStream( file_rules_text );
			if( input != null )
			{
				prop.load( input );
			}
			else
			{
				System.err.println("Rules file does not exist");
			}
			
			//Variables to load: X,P,H,Y,D
			for( int i = init_X; i <= end_X; i++ )
			{
				logicX.add( prop.getProperty( "X" + String.valueOf( i ) ) );
			}
			for( int i = init_P; i <= end_P; i++ )
			{
				logicP.add( prop.getProperty( "P" + String.valueOf( i ) ) );
			}
			for( int i = init_H; i <= end_H; i++ )
			{
				logicH.add( prop.getProperty( "H" + String.valueOf( i ) ) );
			}
			for( int i = init_Y; i <= end_Y; i++ )
			{
				logicY.add( prop.getProperty( "Y" + String.valueOf( i ) ));
			}
			for( int i = init_D; i <= end_D; i++ )
			{
				logicD.add( prop.getProperty( "D" + String.valueOf( i ) ) );
			}
				System.out.println("### Logic Variables loaded correctly ###");
		}
		catch( Exception ex )
		{
			ex.printStackTrace();
		}
	}
	
	

}









